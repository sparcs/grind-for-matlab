function out = vandermeer_handles
out{9}=[];
out{1} = @init;
out{2} = @fun_eval;
out{3} = @jacobian;
out{4} = @jacobianp;
out{5} = @hessian;
out{6} = @hessianp;
out{7} = @der3;
out{8} = @der4;
out{9} = @der5;


% --------------------------------------------------------------------------
function [tspan,y0,options] = init
handles = vandermeer_handles;
y0=zeros(4,1);
options = odeset('Jacobian',handles(3),'JacobianP',handles(4),'Hessians',handles(5),'HessiansP',handles(6));
tspan = [0 10];


% --------------------------------------------------------------------------
%function created by GRIND
function g_X2=fun_eval(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
F1=g_X1(3,:)+beta.*g_X1(4,:);
F2=g_X1(4,:)+beta.*g_X1(3,:);
g_X2(1,:)=a.*F1.*g_X1(1,:)./(1+b.*F1)-m.*g_X1(1,:);
g_X2(2,:)=a.*F2.*g_X1(2,:)./(1+b.*F2)-m.*g_X1(2,:);
g_X2(3,:)=r1.*g_X1(3,:).*(K-g_X1(3,:)-alpha.*g_X1(4,:))./K-a.*g_X1(3,:).*(g_X1(1,:)./(1+b.*F1)+beta.*g_X1(2,:)./(1+b.*F2));
g_X2(4,:)=r2.*g_X1(4,:).*(K-g_X1(4,:)-alpha.*g_X1(3,:))./K-a.*g_X1(4,:).*(g_X1(2,:)./(1+b.*F2)+beta.*g_X1(1,:)./(1+b.*F1));


% --------------------------------------------------------------------------
function g_X2=jacobian(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
g_X2=zeros(4,4);
g_X2(1,1) = (a*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)-m;
g_X2(1,3) = (g_X1(1)*a)./(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(1)*a*b*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(1,4) = (g_X1(1)*a*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(1)*a*b*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(2,2) = (a*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)-m;
g_X2(2,3) = (g_X1(2)*a*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(2)*a*b*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(2,4) = (g_X1(2)*a)./(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(2)*a*b*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(3,1) = -(g_X1(3)*a)./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(3,2) = -(g_X1(3)*a*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(3,3) = g_X1(3)*a*((g_X1(1)*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-a*(g_X1(1)/(b*(g_X1(3)+g_X1(4)*beta)+1)+(g_X1(2)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1))-(g_X1(3)*r1)/K-(r1*(g_X1(3)-K+g_X1(4)*alpha))/K;
g_X2(3,4) = g_X1(3)*a*((g_X1(1)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-(g_X1(3)*alpha*r1)/K;
g_X2(4,1) = -(g_X1(4)*a*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(4,2) = -(g_X1(4)*a)./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(4,3) = g_X1(4)*a*((g_X1(1)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-(g_X1(4)*alpha*r2)/K;
g_X2(4,4) = g_X1(4)*a*((g_X1(2)*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(g_X1(1)*b*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2)-a*(g_X1(2)/(b*(g_X1(4)+g_X1(3)*beta)+1)+(g_X1(1)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1))-(g_X1(4)*r2)/K-(r2*(g_X1(4)-K+g_X1(3)*alpha))/K;


% --------------------------------------------------------------------------
function g_X2=jacobianp(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
g_X2=zeros(4,8);
g_X2(1,1) = (g_X1(1)*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(1,3) = -(g_X1(1)*a*(g_X1(3)+g_X1(4)*beta)^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(1,4) = (g_X1(1)*g_X1(4)*a)./(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(1)*g_X1(4)*a*b*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(1,6) = -g_X1(1);
g_X2(2,1) = (g_X1(2)*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(2,3) = -(g_X1(2)*a*(g_X1(4)+g_X1(3)*beta)^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(2,4) = (g_X1(2)*g_X1(3)*a)./(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(2)*g_X1(3)*a*b*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(2,6) = -g_X1(2);
g_X2(3,1) = -g_X1(3)*(g_X1(1)/(b*(g_X1(3)+g_X1(4)*beta)+1)+(g_X1(2)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1));
g_X2(3,2) = -(g_X1(3)*g_X1(4)*r1)/K;
g_X2(3,3) = g_X1(3)*a*((g_X1(1)*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2);
g_X2(3,4) = g_X1(3)*a*((g_X1(1)*g_X1(4)*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-g_X1(2)/(b*(g_X1(4)+g_X1(3)*beta)+1)+(g_X1(2)*g_X1(3)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2);
g_X2(3,5) = (g_X1(3)*r1)/K+(g_X1(3)*r1*(g_X1(3)-K+g_X1(4)*alpha))/K^2;
g_X2(3,7) = -(g_X1(3)*(g_X1(3)-K+g_X1(4)*alpha))/K;
g_X2(4,1) = -g_X1(4)*(g_X1(2)/(b*(g_X1(4)+g_X1(3)*beta)+1)+(g_X1(1)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1));
g_X2(4,2) = -(g_X1(3)*g_X1(4)*r2)/K;
g_X2(4,3) = g_X1(4)*a*((g_X1(2)*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(g_X1(1)*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2);
g_X2(4,4) = g_X1(4)*a*((g_X1(2)*g_X1(3)*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-g_X1(1)/(b*(g_X1(3)+g_X1(4)*beta)+1)+(g_X1(1)*g_X1(4)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2);
g_X2(4,5) = (g_X1(4)*r2)/K+(g_X1(4)*r2*(g_X1(4)-K+g_X1(3)*alpha))/K^2;
g_X2(4,8) = -(g_X1(4)*(g_X1(4)-K+g_X1(3)*alpha))/K;


% --------------------------------------------------------------------------
function g_X2=hessian(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
g_X2=zeros(4,4,4);
g_X2(41) = (2*g_X1(1)*a*b^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(1)*a*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([57,45]) = (2*g_X1(1)*a*b^2*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(1)*a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(61) = (2*g_X1(1)*a*b^2*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(1)*a*b*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(62) = (2*g_X1(2)*a*b^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(2)*a*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2([58,46]) = (2*g_X1(2)*a*b^2*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(2)*a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(42) = (2*g_X1(2)*a*b^2*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(2)*a*b*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2([35,11]) = (g_X1(3)*a*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-a/(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2([51,15]) = (g_X1(3)*a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([55,31]) = (g_X1(3)*a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2([39,27]) = (g_X1(3)*a*b*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(a*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2([56,32]) = (g_X1(4)*a*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-a/(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2([36,12]) = (g_X1(4)*a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([40,28]) = (g_X1(4)*a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2([52,16]) = (g_X1(4)*a*b*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(a*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2([49,13]) = (a*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)-(a*b*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([38,26]) = (a*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)-(a*b*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(63) = -g_X1(3)*a*((2*g_X1(2)*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3+(2*g_X1(1)*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);
g_X2(44) = -g_X1(4)*a*((2*g_X1(1)*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3+(2*g_X1(2)*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(43) = 2*a*((g_X1(1)*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-(2*r1)/K-g_X1(3)*a*((2*g_X1(1)*b^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3+(2*g_X1(2)*b^2*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(64) = 2*a*((g_X1(2)*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(g_X1(1)*b*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2)-(2*r2)/K-g_X1(4)*a*((2*g_X1(2)*b^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3+(2*g_X1(1)*b^2*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);
g_X2([59,47]) = a*((g_X1(1)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-(alpha*r1)/K-g_X1(3)*a*((2*g_X1(1)*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3+(2*g_X1(2)*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2([60,48]) = a*((g_X1(1)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-(alpha*r2)/K-g_X1(4)*a*((2*g_X1(2)*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3+(2*g_X1(1)*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);
g_X2([33,9]) = a/(b*(g_X1(3)+g_X1(4)*beta)+1)-(a*b*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([54,30]) = a/(b*(g_X1(4)+g_X1(3)*beta)+1)-(a*b*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;


% --------------------------------------------------------------------------
function g_X2=hessianp(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
g_X2=zeros(4,4,8);
g_X2(57) = (2*g_X1(1)*g_X1(4)*a*b^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(1)*g_X1(4)*a*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(41) = (2*g_X1(1)*a*b*(g_X1(3)+g_X1(4)*beta)^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(1)*a*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(45) = (2*g_X1(1)*a*b*beta*(g_X1(3)+g_X1(4)*beta)^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(1)*a*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(62) = (2*g_X1(2)*g_X1(3)*a*b^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(2)*g_X1(3)*a*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(46) = (2*g_X1(2)*a*b*(g_X1(4)+g_X1(3)*beta)^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(2)*a*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(42) = (2*g_X1(2)*a*b*beta*(g_X1(4)+g_X1(3)*beta)^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(2)*a*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(61) = (g_X1(1)*a)./(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(1)*a*b*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(2*g_X1(1)*g_X1(4)*a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(2*g_X1(1)*g_X1(4)*a*b^2*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2(13) = (g_X1(1)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(1)*b*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(58) = (g_X1(2)*a)./(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(2)*a*b*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(2)*g_X1(3)*a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(2*g_X1(2)*g_X1(3)*a*b^2*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2(10) = (g_X1(2)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(2)*b*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(1) = (g_X1(3)+g_X1(4)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(51) = (g_X1(3)*g_X1(4)*a*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(56) = (g_X1(3)*g_X1(4)*a*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(54) = (g_X1(3)*a)./(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(3)*a*b*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(35) = (g_X1(3)*a*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(39) = (g_X1(3)*a*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(79) = (g_X1(3)*alpha*r1)/K^2;
g_X2(55) = (g_X1(3)^2*a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(g_X1(3)*a)./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(6) = (g_X1(4)+g_X1(3)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(49) = (g_X1(4)*a)./(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(4)*a*b*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(40) = (g_X1(4)*a*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(36) = (g_X1(4)*a*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(76) = (g_X1(4)*alpha*r2)/K^2;
g_X2(52) = (g_X1(4)^2*a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(g_X1(4)*a)./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(107) = -g_X1(3)/K-(g_X1(3)-K+g_X1(4)*alpha)/K;
g_X2(128) = -g_X1(4)/K-(g_X1(4)-K+g_X1(3)*alpha)/K;
g_X2(111) = -(g_X1(3)*alpha)/K;
g_X2(7) = -(g_X1(3)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(31) = -(g_X1(3)*r1)/K;
g_X2(32) = -(g_X1(3)*r2)/K;
g_X2(124) = -(g_X1(4)*alpha)/K;
g_X2(4) = -(g_X1(4)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(27) = -(g_X1(4)*r1)/K;
g_X2(28) = -(g_X1(4)*r2)/K;
g_X2(33) = -(a*(g_X1(3)+g_X1(4)*beta)^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(38) = -(a*(g_X1(4)+g_X1(3)*beta)^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2([81,86]) = -1;
g_X2(3) = -g_X1(3)/(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(8) = -g_X1(4)/(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(9) = g_X1(1)/(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(1)*b*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2(14) = g_X1(2)/(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(2)*b*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(11) = g_X1(3)*((g_X1(1)*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-g_X1(1)/(b*(g_X1(3)+g_X1(4)*beta)+1)-(g_X1(2)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1);
g_X2(15) = g_X1(3)*((g_X1(1)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2);
g_X2(63) = g_X1(3)*a*((g_X1(1)*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(1)*g_X1(4)*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(2)*g_X1(3)*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(47) = g_X1(3)*a*((g_X1(1)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(1)*b*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(2)*b*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(12) = g_X1(4)*((g_X1(1)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2);
g_X2(16) = g_X1(4)*((g_X1(2)*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(g_X1(1)*b*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2)-g_X1(2)/(b*(g_X1(4)+g_X1(3)*beta)+1)-(g_X1(1)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1);
g_X2(60) = g_X1(4)*a*((g_X1(1)*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(1)*g_X1(4)*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(2)*g_X1(3)*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(44) = g_X1(4)*a*((g_X1(1)*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(1)*b*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(2)*b*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(43) = a*((g_X1(1)*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)+g_X1(3)*a*(g_X1(1)/(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(g_X1(2)*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(1)*b*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(2)*b*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(59) = a*((g_X1(1)*g_X1(4)*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-g_X1(2)/(b*(g_X1(4)+g_X1(3)*beta)+1)+(g_X1(2)*g_X1(3)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2)-g_X1(3)*a*((2*g_X1(1)*g_X1(4)*b^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*g_X1(2)*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(2*g_X1(2)*g_X1(3)*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(48) = a*((g_X1(2)*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(g_X1(1)*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^2)+g_X1(4)*a*(g_X1(2)/(b*(g_X1(4)+g_X1(3)*beta)+1)^2+(g_X1(1)*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(2*g_X1(2)*b*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(1)*b*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);
g_X2(64) = a*((g_X1(2)*g_X1(3)*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-g_X1(1)/(b*(g_X1(3)+g_X1(4)*beta)+1)+(g_X1(1)*g_X1(4)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2)-g_X1(4)*a*((2*g_X1(2)*g_X1(3)*b^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*g_X1(1)*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2+(2*g_X1(1)*g_X1(4)*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);
g_X2(75) = r1/K+(g_X1(3)*r1)/K^2+(r1*(g_X1(3)-K+g_X1(4)*alpha))/K^2;
g_X2(80) = r2/K+(g_X1(4)*r2)/K^2+(r2*(g_X1(4)-K+g_X1(3)*alpha))/K^2;


% --------------------------------------------------------------------------
function g_X2=der3(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
g_X2=zeros(4,4,4,4);
g_X2([163,139,43]) = (2*a*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(2*g_X1(3)*a*b^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([248,224,128]) = (2*a*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(4)*a*b^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([244,208,64]) = (2*a*b*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(2*g_X1(4)*a*b^2*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([167,155,107]) = (2*a*b*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(3)*a*b^2*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([161,137,41]) = (2*a*b^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*a*b)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([246,222,126]) = (2*a*b^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*a*b)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2([225,177,201,57,141,45]) = (2*a*b^2*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([230,182,218,122,158,110]) = (2*a*b^2*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2([241,205,61]) = (2*a*b^2*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(2*a*b*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2;
g_X2([166,154,106]) = (2*a*b^2*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(2*a*b*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2;
g_X2(169) = (6*g_X1(1)*a*b^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*g_X1(1)*a*b^3*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([233,185,173]) = (6*g_X1(1)*a*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*g_X1(1)*a*b^3*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([249,237,189]) = (6*g_X1(1)*a*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*g_X1(1)*a*b^3*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2(253) = (6*g_X1(1)*a*b^2*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*g_X1(1)*a*b^3*beta^3*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2(254) = (6*g_X1(2)*a*b^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*g_X1(2)*a*b^3*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([250,238,190]) = (6*g_X1(2)*a*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*g_X1(2)*a*b^3*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([234,186,174]) = (6*g_X1(2)*a*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*g_X1(2)*a*b^3*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2(170) = (6*g_X1(2)*a*b^2*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*g_X1(2)*a*b^3*beta^3*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([227,179,203,59,143,47]) = (a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(2*g_X1(3)*a*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([228,180,204,60,144,48]) = (a*b*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^2-(2*g_X1(4)*a*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([231,183,219,123,159,111]) = (a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(3)*a*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([232,184,220,124,160,112]) = (a*b*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^2-(2*g_X1(4)*a*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([247,223,127]) = -(2*g_X1(3)*a*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([243,207,63]) = -(2*g_X1(3)*a*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([164,140,44]) = -(2*g_X1(4)*a*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([168,156,108]) = -(2*g_X1(4)*a*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2(171) = g_X1(3)*a*((6*g_X1(1)*b^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-3*a*((2*g_X1(1)*b^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3+(2*g_X1(2)*b^2*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2([235,187,175]) = g_X1(3)*a*((6*g_X1(1)*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-2*a*((2*g_X1(1)*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3+(2*g_X1(2)*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2([251,239,191]) = g_X1(3)*a*((6*g_X1(1)*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-a*((2*g_X1(2)*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3+(2*g_X1(1)*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);
g_X2(255) = g_X1(3)*a*((6*g_X1(2)*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4+(6*g_X1(1)*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4);
g_X2(172) = g_X1(4)*a*((6*g_X1(1)*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4);
g_X2([236,188,176]) = g_X1(4)*a*((6*g_X1(1)*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-a*((2*g_X1(1)*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3+(2*g_X1(2)*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3);
g_X2(256) = g_X1(4)*a*((6*g_X1(2)*b^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4+(6*g_X1(1)*b^3*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4)-3*a*((2*g_X1(2)*b^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3+(2*g_X1(1)*b^2*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);
g_X2([252,240,192]) = g_X1(4)*a*((6*g_X1(2)*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4+(6*g_X1(1)*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4)-2*a*((2*g_X1(2)*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3+(2*g_X1(1)*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3);


% --------------------------------------------------------------------------
function g_X2=der4(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
g_X2=zeros(4,4,4,4,4);
g_X2(681) = (24*g_X1(1)*a*b^4*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*g_X1(1)*a*b^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([937,745,697,685]) = (24*g_X1(1)*a*b^4*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*g_X1(1)*a*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([1001,953,761,941,749,701]) = (24*g_X1(1)*a*b^4*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*g_X1(1)*a*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([1017,1005,957,765]) = (24*g_X1(1)*a*b^4*beta^3*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*g_X1(1)*a*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2(1021) = (24*g_X1(1)*a*b^4*beta^4*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*g_X1(1)*a*b^3*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2(1022) = (24*g_X1(2)*a*b^4*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*g_X1(2)*a*b^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([1018,1006,958,766]) = (24*g_X1(2)*a*b^4*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*g_X1(2)*a*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([1002,954,762,942,750,702]) = (24*g_X1(2)*a*b^4*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*g_X1(2)*a*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([938,746,698,686]) = (24*g_X1(2)*a*b^4*beta^3*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*g_X1(2)*a*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2(682) = (24*g_X1(2)*a*b^4*beta^4*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*g_X1(2)*a*b^3*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([675,651,555,171]) = (6*g_X1(3)*a*b^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(6*a*b^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([931,739,691,907,715,811,235,571,187,655,559,175]) = (6*g_X1(3)*a*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(4*a*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([1015,991,895,511]) = (6*g_X1(3)*a*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([995,947,755,971,827,251,911,719,815,239,575,191]) = (6*g_X1(3)*a*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(2*a*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([999,951,759,987,891,507,927,735,879,495,639,447]) = (6*g_X1(3)*a*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(2*a*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([1011,975,831,255]) = (6*g_X1(3)*a*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([935,743,695,923,731,875,491,635,443,671,623,431]) = (6*g_X1(3)*a*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(4*a*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([679,667,619,427]) = (6*g_X1(3)*a*b^3*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(6*a*b^2*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([1016,992,896,512]) = (6*g_X1(4)*a*b^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(6*a*b^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([676,652,556,172]) = (6*g_X1(4)*a*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([1000,952,760,988,892,508,928,736,880,496,640,448]) = (6*g_X1(4)*a*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(4*a*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([932,740,692,908,716,812,236,572,188,656,560,176]) = (6*g_X1(4)*a*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(2*a*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([936,744,696,924,732,876,492,636,444,672,624,432]) = (6*g_X1(4)*a*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(2*a*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3;
g_X2([996,948,756,972,828,252,912,720,816,240,576,192]) = (6*g_X1(4)*a*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(4*a*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([680,668,620,428]) = (6*g_X1(4)*a*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([1012,976,832,256]) = (6*g_X1(4)*a*b^3*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(6*a*b^2*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3;
g_X2([673,649,553,169]) = (6*a*b^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*a*b^3*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([1014,990,894,510]) = (6*a*b^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*a*b^3*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([929,737,689,905,713,809,233,569,185,653,557,173]) = (6*a*b^2*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*a*b^3*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([998,950,758,986,890,506,926,734,878,494,638,446]) = (6*a*b^2*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*a*b^3*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([993,945,753,969,825,249,909,717,813,237,573,189]) = (6*a*b^2*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*a*b^3*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([934,742,694,922,730,874,490,634,442,670,622,430]) = (6*a*b^2*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*a*b^3*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([1009,973,829,253]) = (6*a*b^2*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^3-(6*a*b^3*beta^3*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([678,666,618,426]) = (6*a*b^2*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^3-(6*a*b^3*beta^3*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2(1023) = -g_X1(3)*a*((24*g_X1(2)*b^4*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5+(24*g_X1(1)*b^4*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5);
g_X2(684) = -g_X1(4)*a*((24*g_X1(1)*b^4*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([1003,955,763,943,751,703]) = 2*a*((6*g_X1(1)*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-g_X1(3)*a*((24*g_X1(1)*b^4*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([1004,956,764,944,752,704]) = 2*a*((6*g_X1(1)*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-g_X1(4)*a*((24*g_X1(1)*b^4*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([939,747,699,687]) = 3*a*((6*g_X1(1)*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-g_X1(3)*a*((24*g_X1(1)*b^4*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([1020,1008,960,768]) = 3*a*((6*g_X1(2)*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4+(6*g_X1(1)*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4)-g_X1(4)*a*((24*g_X1(2)*b^4*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5+(24*g_X1(1)*b^4*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5);
g_X2(683) = 4*a*((6*g_X1(1)*b^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-g_X1(3)*a*((24*g_X1(1)*b^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^5)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2(1024) = 4*a*((6*g_X1(2)*b^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4+(6*g_X1(1)*b^3*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4)-g_X1(4)*a*((24*g_X1(2)*b^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5+(24*g_X1(1)*b^4*beta^5)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5);
g_X2([940,748,700,688]) = a*((6*g_X1(1)*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4+(6*g_X1(2)*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4)-g_X1(4)*a*((24*g_X1(1)*b^4*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([1019,1007,959,767]) = a*((6*g_X1(2)*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4+(6*g_X1(1)*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4)-g_X1(3)*a*((24*g_X1(1)*b^4*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);


% --------------------------------------------------------------------------
function g_X2=der5(t,g_X1,a,alpha,b,beta,K,m,r1,r2)
g_X2=zeros(4,4,4,4,4,4);
g_X2([4003,3811,3043,3763,2995,2803,3979,3787,3019,3883,3307,1003,3643,2875,3259,955,2299,763,3727,2959,2767,3631,2863,3247,943,2287,751,2623,2239,703]) = (12*a*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(3)*a*b^4*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([4004,3812,3044,3764,2996,2804,3980,3788,3020,3884,3308,1004,3644,2876,3260,956,2300,764,3728,2960,2768,3632,2864,3248,944,2288,752,2624,2240,704]) = (12*a*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(4)*a*b^4*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([4007,3815,3047,3767,2999,2807,3995,3803,3035,3947,3563,2027,3707,2939,3515,1979,2555,1787,3743,2975,2783,3695,2927,3503,1967,2543,1775,2687,2495,1727]) = (12*a*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(3)*a*b^4*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([4008,3816,3048,3768,3000,2808,3996,3804,3036,3948,3564,2028,3708,2940,3516,1980,2556,1788,3744,2976,2784,3696,2928,3504,1968,2544,1776,2688,2496,1728]) = (12*a*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(4)*a*b^4*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2(2729) = (120*g_X1(1)*a*b^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(120*g_X1(1)*a*b^5*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^6;
g_X2([3753,2985,2793,2745,2733]) = (120*g_X1(1)*a*b^4*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(120*g_X1(1)*a*b^5*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^6;
g_X2([4009,3817,3049,3769,3001,2809,3757,2989,2797,2749]) = (120*g_X1(1)*a*b^4*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(120*g_X1(1)*a*b^5*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^6;
g_X2([4073,4025,3833,3065,4013,3821,3053,3773,3005,2813]) = (120*g_X1(1)*a*b^4*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(120*g_X1(1)*a*b^5*beta^3*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^6;
g_X2([4089,4077,4029,3837,3069]) = (120*g_X1(1)*a*b^4*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(120*g_X1(1)*a*b^5*beta^4*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^6;
g_X2(4093) = (120*g_X1(1)*a*b^4*beta^5)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(120*g_X1(1)*a*b^5*beta^5*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^6;
g_X2(4094) = (120*g_X1(2)*a*b^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(120*g_X1(2)*a*b^5*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^6;
g_X2([4090,4078,4030,3838,3070]) = (120*g_X1(2)*a*b^4*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(120*g_X1(2)*a*b^5*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^6;
g_X2([4074,4026,3834,3066,4014,3822,3054,3774,3006,2814]) = (120*g_X1(2)*a*b^4*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(120*g_X1(2)*a*b^5*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^6;
g_X2([4010,3818,3050,3770,3002,2810,3758,2990,2798,2750]) = (120*g_X1(2)*a*b^4*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(120*g_X1(2)*a*b^5*beta^3*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^6;
g_X2([3754,2986,2794,2746,2734]) = (120*g_X1(2)*a*b^4*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(120*g_X1(2)*a*b^5*beta^4*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^6;
g_X2(2730) = (120*g_X1(2)*a*b^4*beta^5)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(120*g_X1(2)*a*b^5*beta^5*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^6;
g_X2([3747,2979,2787,2739,3723,2955,2763,3627,2859,3243,939,2283,747,2619,2235,699,2703,2607,2223,687]) = (18*a*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(3)*a*b^4*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([4072,4024,3832,3064,4060,3964,3580,2044,4000,3808,3040,3952,3568,2032,3712,2944,3520,1984,2560,1792]) = (18*a*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(4)*a*b^4*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([4068,4020,3828,3060,4044,3900,3324,1020,3984,3792,3024,3888,3312,1008,3648,2880,3264,960,2304,768]) = (18*a*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(4)*a*b^4*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([3751,2983,2791,2743,3739,2971,2779,3691,2923,3499,1963,2539,1771,2683,2491,1723,2719,2671,2479,1711]) = (18*a*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(3)*a*b^4*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([2723,2699,2603,2219,683]) = (24*a*b^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(3)*a*b^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([4088,4064,3968,3584,2048]) = (24*a*b^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(4)*a*b^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([4084,4048,3904,3328,1024]) = (24*a*b^3*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(4)*a*b^4*beta^5)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([2727,2715,2667,2475,1707]) = (24*a*b^3*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(3)*a*b^4*beta^5)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([2721,2697,2601,2217,681]) = (24*a*b^4*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*a*b^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([4086,4062,3966,3582,2046]) = (24*a*b^4*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*a*b^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([3745,2977,2785,2737,3721,2953,2761,3625,2857,3241,937,2281,745,2617,2233,697,2701,2605,2221,685]) = (24*a*b^4*beta*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*a*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([4070,4022,3830,3062,4058,3962,3578,2042,3998,3806,3038,3950,3566,2030,3710,2942,3518,1982,2558,1790]) = (24*a*b^4*beta*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*a*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([4001,3809,3041,3761,2993,2801,3977,3785,3017,3881,3305,1001,3641,2873,3257,953,2297,761,3725,2957,2765,3629,2861,3245,941,2285,749,2621,2237,701]) = (24*a*b^4*beta^2*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*a*b^3*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([4006,3814,3046,3766,2998,2806,3994,3802,3034,3946,3562,2026,3706,2938,3514,1978,2554,1786,3742,2974,2782,3694,2926,3502,1966,2542,1774,2686,2494,1726]) = (24*a*b^4*beta^2*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*a*b^3*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([4065,4017,3825,3057,4041,3897,3321,1017,3981,3789,3021,3885,3309,1005,3645,2877,3261,957,2301,765]) = (24*a*b^4*beta^3*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*a*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([3750,2982,2790,2742,3738,2970,2778,3690,2922,3498,1962,2538,1770,2682,2490,1722,2718,2670,2478,1710]) = (24*a*b^4*beta^3*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*a*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([4081,4045,3901,3325,1021]) = (24*a*b^4*beta^4*(g_X1(3)+g_X1(4)*beta))./(b*(g_X1(3)+g_X1(4)*beta)+1)^5-(24*a*b^3*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4;
g_X2([2726,2714,2666,2474,1706]) = (24*a*b^4*beta^4*(g_X1(4)+g_X1(3)*beta))./(b*(g_X1(4)+g_X1(3)*beta)+1)^5-(24*a*b^3*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4;
g_X2([3748,2980,2788,2740,3724,2956,2764,3628,2860,3244,940,2284,748,2620,2236,700,2704,2608,2224,688]) = (6*a*b^3*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(4)*a*b^4*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([4071,4023,3831,3063,4059,3963,3579,2043,3999,3807,3039,3951,3567,2031,3711,2943,3519,1983,2559,1791]) = (6*a*b^3*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(3)*a*b^4*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([4067,4019,3827,3059,4043,3899,3323,1019,3983,3791,3023,3887,3311,1007,3647,2879,3263,959,2303,767]) = (6*a*b^3*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^4-(24*g_X1(3)*a*b^4*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([3752,2984,2792,2744,3740,2972,2780,3692,2924,3500,1964,2540,1772,2684,2492,1724,2720,2672,2480,1712]) = (6*a*b^3*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^4-(24*g_X1(4)*a*b^4*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([4087,4063,3967,3583,2047]) = -(24*g_X1(3)*a*b^4*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2([4083,4047,3903,3327,1023]) = -(24*g_X1(3)*a*b^4*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([2724,2700,2604,2220,684]) = -(24*g_X1(4)*a*b^4*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5;
g_X2([2728,2716,2668,2476,1708]) = -(24*g_X1(4)*a*b^4*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5;
g_X2(2731) = g_X1(3)*a*((120*g_X1(1)*b^5)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6+(120*g_X1(2)*b^5*beta^6)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6)-5*a*((24*g_X1(1)*b^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^5)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([3755,2987,2795,2747,2735]) = g_X1(3)*a*((120*g_X1(1)*b^5*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6+(120*g_X1(2)*b^5*beta^5)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6)-4*a*((24*g_X1(1)*b^4*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([4011,3819,3051,3771,3003,2811,3759,2991,2799,2751]) = g_X1(3)*a*((120*g_X1(1)*b^5*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6+(120*g_X1(2)*b^5*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6)-3*a*((24*g_X1(1)*b^4*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([4075,4027,3835,3067,4015,3823,3055,3775,3007,2815]) = g_X1(3)*a*((120*g_X1(1)*b^5*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6+(120*g_X1(2)*b^5*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6)-2*a*((24*g_X1(1)*b^4*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2(4095) = g_X1(3)*a*((120*g_X1(2)*b^5*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6+(120*g_X1(1)*b^5*beta^5)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6);
g_X2([4091,4079,4031,3839,3071]) = g_X1(3)*a*((120*g_X1(2)*b^5*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6+(120*g_X1(1)*b^5*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6)-a*((24*g_X1(2)*b^4*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5+(24*g_X1(1)*b^4*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5);
g_X2(2732) = g_X1(4)*a*((120*g_X1(1)*b^5*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6+(120*g_X1(2)*b^5*beta^5)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6);
g_X2([3756,2988,2796,2748,2736]) = g_X1(4)*a*((120*g_X1(1)*b^5*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6+(120*g_X1(2)*b^5*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6)-a*((24*g_X1(1)*b^4*beta)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2([4012,3820,3052,3772,3004,2812,3760,2992,2800,2752]) = g_X1(4)*a*((120*g_X1(1)*b^5*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6+(120*g_X1(2)*b^5*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6)-2*a*((24*g_X1(1)*b^4*beta^2)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^3)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
g_X2(4096) = g_X1(4)*a*((120*g_X1(2)*b^5)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6+(120*g_X1(1)*b^5*beta^6)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6)-5*a*((24*g_X1(2)*b^4)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5+(24*g_X1(1)*b^4*beta^5)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5);
g_X2([4092,4080,4032,3840,3072]) = g_X1(4)*a*((120*g_X1(2)*b^5*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6+(120*g_X1(1)*b^5*beta^5)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6)-4*a*((24*g_X1(2)*b^4*beta)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5+(24*g_X1(1)*b^4*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5);
g_X2([4076,4028,3836,3068,4016,3824,3056,3776,3008,2816]) = g_X1(4)*a*((120*g_X1(2)*b^5*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^6+(120*g_X1(1)*b^5*beta^4)./(b*(g_X1(3)+g_X1(4)*beta)+1)^6)-3*a*((24*g_X1(1)*b^4*beta^3)./(b*(g_X1(3)+g_X1(4)*beta)+1)^5+(24*g_X1(2)*b^4*beta^2)./(b*(g_X1(4)+g_X1(3)*beta)+1)^5);
