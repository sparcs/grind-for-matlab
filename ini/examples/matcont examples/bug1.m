 %This script is created by GRIND
%Date: 26-Apr-2020 21:47:40

%open the model
use('C:\d\alg\MAT LAB\grind\ini\examples\matcont examples\vandermeer.ini');

%create object "g_cont" that uses the matcont engine
global g_cont;
g_cont=grind_matcont;

%first add all initial points
g_set{1}=[2;0.8;1.3;0.18;1;0.1;1;1];
g_cont.add_points('id','EP1','msg','unstable spiral (++)','p0',g_set{1},...
  'x0',[0.4162179546;0.4162179546;0.04531858969;0.04531858969]);
g_cont.add_points('id','EP10','msg','saddle (--)','p0',g_set{1},...
  'x0',[0.1557071145;0;-0.1367874784;1.057018968]);
g_cont.add_points('id','EP11','msg','saddle (--)','p0',g_set{1},...
  'x0',[0;0.1557071144;1.057018968;-0.1367874784]);
g_cont.add_points('id','EP2','msg','unstable spiral (+/0)','p0',g_set{1},...
  'x0',[0;2.088269363;0.2970885324;0]);
g_cont.add_points('id','EP3','msg','unstable spiral (+/0)','p0',g_set{1},...
  'x0',[2.088269363;0;0;0.2970885324]);
g_cont.add_points('id','EP4','msg','saddle (+/0)','p0',g_set{1},...
  'x0',[0;0;0.5555555555;0.5555555556]);
g_cont.add_points('id','EP5','msg','saddle (+/0)','p0',g_set{1},...
  'x0',[0;0;0;1]);
g_cont.add_points('id','EP6','msg','saddle (+/0)','p0',g_set{1},...
  'x0',[0;0;1;0]);
g_cont.add_points('id','EP7','msg','unstable spiral (+/0)','p0',g_set{1},...
  'x0',[0;0.5061626012;0;0.05347593583]);
g_cont.add_points('id','EP8','msg','unstable spiral (+/0)','p0',g_set{1},...
  'x0',[0.5061626012;0;0.05347593583;0]);
g_cont.add_points('id','EP9','msg','saddle (0)','p0',g_set{1},...
  'x0',[0;0;0;0]);
g_cont.add_points('id','P1','msg','initial point','p0',g_set{1},...
  'x0',[0.3572787883;0.3169791762;0.003837554074;0.2530784848]);

g_cont.select_point('P1',true); %select the last parameter values in GRIND

%change the matcont/GRIND settings
g_cont.set('MaxNumPoints',200,...
    'par1','beta');

%Continue Equilibrium to a EP - Equilibrium points curve
g_cont.cont('EP1','EP');

%change the matcont/GRIND settings
g_cont.set('par2','a');

%Continue Hopf to a H - Hopf bifurcation curve
g_cont.cont('H1','H');

%change the matcont/GRIND settings
g_cont.set('par2','');

%Continue Hopf to a EP - Equilibrium points curve
g_cont.cont('H1','EP');

%change the matcont/GRIND settings
g_cont.set('par2','a');

%Continue Generalized Hopf to a H - Hopf bifurcation curve

g_cont.cont('GH1','LPC')
g_cont.show;

g_cont.plot;

disp('You can use <a href="matlab:conteq">conteq</a> to continue working with this session')

