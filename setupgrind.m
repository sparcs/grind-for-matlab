%SETUPGRIND   Setup GRIND
%   Setup script for GRIND, adds the grind system directory to
%   the default path. If the grind directories are not found they are downloaded from
%   the download websites: http://content.alterra.wur.nl/webdocs/internet/aew/downloads/ or
%   http://www.sparcs-center.org/uploads/files/. After successful installation, setupgrind needs not to be run 
%   again.
%
%Usage:
%SETUPGRIND - standard setup
%   SETUPGRIND -d - runs setupgrind also if grind was already installed.
%   SETUPGRIND -s - creates a startup.m file that runs setupgrind each time that MATLAB
%      is opened. Used in older MATLAB versions if the path is not saved correctly.
%   SETUPGRIND -u - uninstalls grind (without removing files)
%   SETUPGRIND -up - update grind from the website (this is the same as calling 
%   <a href="matlab:help updategrind">updategrind</a>).
%   SETUPGRIND -restore - if grind is installed in an old version of MATLAB (< 7.12) some syntax needs to be changed.
%      'SETUPGRIND -restore' restores the original files.
%
%
%
%   See also Installing, updategrind
%
%   Reference page in Help browser:
%      <a href="matlab:commands('setupgrind')">commands setupgrind</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:39 $
function res=setupgrind(option)
    %urls for downloading
    if ~isoctave && verLessThan('matlab', '7.6')
        error('grind:setup', 'MATLAB version R2008a or newer is required for GRIND');
    end
    if nargin > 0 && strcmp(option, '-restore')
        checkversions(true);
        return;
    end
    %this url should redirect to grindversion.txt and return the exact urls
    %for grind.zip etc
    urls = read_grindversion('https://sparcs-center.org/grindfiles/grindversion.txt');
    if nargin > 0 && strcmp(option, '-version')
        res=urls;
        return;
    end
%     for i = 1:length(urls)
%         %find first valid url
%         try
%         dummy = webread(sprintf('%s%s', urls{i}, 'grindversion.txt')); %#ok<ASGLU>
%         catch err
%             if strcmp(err.identifier,'MATLAB:webservices:HTTP404StatusCodeError')
%             url = urls{i};
%             end
%         end
%     end
    doanyway = 0;
    uninstall = 0;
    addstartup = 0;
    warning('off', 'backtrace');
    if nargin == 1
        if strncmpi(option, '-d', 2)
            doanyway = 1;
        end
        if strncmpi(option, '-up', 3)
            updategrindfromweb(urls);
            if isempty(which('grind_coco'))
                addpath(fullfile(grindpath, 'sys2'));
            end
            %update coco from the grind website (only if coco is available)
            if ~isempty(grind_coco.findupdate(false, urls))
                grind_coco.findupdate(true, urls);
            end
            if ~isempty(grind_matcont.findupdate(false, urls, true))
                grind_matcont.findupdate(true, urls, true);
            end
            if ~isempty(grind_matcont.findupdate(false, urls, false))
                grind_matcont.findupdate(true, urls, false);
            end
        elseif strncmpi(option, '-u', 2)
            uninstall = 1;
        end
        if strncmpi(option, '-s', 2)
            addstartup = 1;
        end
    end
    k = which('grind.m');
    if ~isempty(k) && ~doanyway
        thegrindpath = fileparts(k);
        if uninstall
            rmpath(thegrindpath);
            savethepath; %adds permanently
            fprintf('Grind uninstalled from %s (no files removed)\n', thegrindpath);
        else
            checkversions(false); %if version < 7.12 [~, ] is not supported (replaced with [DUMM]
            if exist_newversion(urls) < 1
                warning('GRIND:setupgrind:newerversion', 'There is a newer version available, use <a href="matlab: updategrind">updategrind</a> to update.');
            end
            fprintf('Grind was already installed in %s\n', thegrindpath);
            if addstartup
                doaddstartup;
            end
        end
        return;
    elseif isempty(k) && exist(fullfile('.', 'sys', 'sys2'), 'dir') ~= 7
        %install grind using only the setupgrind.m file
        folder = getgrinddir();
        warning off MATLAB:MKDIR:DirectoryExists
        mkdir(folder);
        warning on MATLAB:MKDIR:DirectoryExists
        cd(folder);
        fprintf('Downloading GRIND...\n')
        unzip(urls.grind_url);
        disp('Successfully downloaded');
        cd grind
        setupgrind
        return
    elseif uninstall
        fprintf('Grind was not installed\n');
        return;
    end
    try
        try
            mroot = OCTAVE_PATH;
        catch
            mroot = matlabroot;
        end
    catch
        mroot = '';
    end
    thegrindpath = fullfile(mroot, 'work', 'grind', 'sys');
    k = which(fullfile(thegrindpath, 'grind.m'));
    if isempty(k)
        thegrindpath = fullfile(pwd, 'sys');
    end
    addpath(thegrindpath);
    savethepath;
    checkversions(false); %if version < 7.12 [~, ] is not supported (replaced with [DUMM]
    w = 0;
    if ~canwriteto(grindpath)
        fileattrib(grindpath, '+w');
        if ~canwriteto(grindpath)
            warning('GRIND:setupgrind:writepermission', 'No permission to write to %s %s %s\n\n', grindpath, ...
                'If you have permission in another directory it is possible to run GRIND.', ...
                'Preferrably set write permission to the full GRIND directory.');
            fileattrib(grindpath, '+w');
            w = 1;
        end
    end
    p = fullfile(mroot, 'toolbox', 'local', 'pathdef.m');
    if exist(p, 'file') %if in future MATLAB versions pathdef.m does not exist there
        [~, aa] = fileattrib(p);
        if ~aa.UserWrite
            fileattrib(p, '+w');
            [~, aa] = fileattrib(p);
            if ~aa.UserWrite
                warning('GRIND:setupgrind:writepermission', 'The path cannot be stored as there is no write permission in %s. %s\n\n', ...
                    p, 'Each session GRIND needs to be reinstalled (or the permission should be changed).');
                w = 1;
            end
        end
    end
    if addstartup
        doaddstartup;
    end
    %if exist_newversion(url)<1
    %    warning('GRIND:setupgrind:newerversion','There is a newer version available, use <a href="matlab: updategrind">updategrind</a> to update.');
    %end
    if w||~isadmin
        disp('GRIND installed, but there are some problems (see above)');
    else
        disp('Grind installed successfully');
    end
end

function doaddstartup
    thegrindpath = grindpath;
    u = userpath;
    if length(u) < 4 %R11 gives not the workdirectory
        u = fullfile(matlabroot, 'work');
    end
    f = strfind(u, '; ');
    if ~isempty(f)
        u = u(1:f(1) - 1);
    end
    u = fullfile(u, 'startup.m');
    if exist(u, 'file') == 2
        %   lines = {};
        fid = fopen(u, 'r');
        hassetup = 0;
        while ~feof(fid)
            line = fgetl(fid);
            %      lines = {lines, line};
            if ~isempty(strfind(line, 'setupgrind')) %#ok<STREMP>
                hassetup = 1;
            end
        end
        fclose(fid);
        if ~hassetup
            fid = fopen(u, 'a');
            fprintf(fid, '\ncd ''%s''\nsetupgrind\n', thegrindpath(1:end - 4));
            fclose(fid);
            disp('setupgrind added to startup.m');
        else
            disp('setupgrind was already in startup.m');
        end
    else
        fid = fopen(u, 'w');
        fprintf(fid, '\ncd ''%s''\nsetupgrind\n', thegrindpath(1:end - 4));
        disp('startup.m created');
        fclose(fid);
    end
end

function ok = canwriteto(path)
    ok = 0;
    oldpath = pwd;
    try
        cd(path);
        fid = fopen('t.tmp', 'wt');
        ok = fid >= 0;
        if ok
            fclose(fid);
        end
        delete('t.tmp');
        cd(oldpath);
    catch
        cd(oldpath);
    end
end

function res = isoctave
    res = exist('OCTAVE_VERSION', 'builtin') ~= 0;
end



function updategrindfromweb(urls)
 
    % if ~exist('urlread','file') %exists in R2008
    %    error('GRIND:updategrind:MATLABversion','This function works only for newer versions of MATLAB');
    % end
    [nonewer, web, current] = exist_newversion(urls);
    if isempty(web)
        error('GRIND:updategrind:noconnection', 'Cannot download GRIND, is internet connected?')
    end
    if nonewer
        warning('GRIND:updategrind:nonewversion', 'There is no newer version of GRIND available');
    else
        if exist('i_use', 'file') == 2
            model('-c', '1');
        end
        fprintf('Removing current version (%s)...\n', current.date)
        gp = fileparts(which('grind.m'));
        if ~isempty(gp)
            cd(gp);
        else
            gp = pwd;
        end
        delete('*.m');
        delete('*.mat');
        delete('*.exe');
        delete('*.chm');
        delete('*.chw');
        delete('*.h');
        cd('sys2')
        delete('*.m');
        cd ..
        if exist('csolver', 'dir') == 7
            cd('csolver')
            delete('*.cpp');
            delete('*.h');
            copyfile('compile.bat', 'compile1.bat')
            cd ..
        end
        cd ..
        cd ..
        fprintf('Downloading new version (%s)...\n', web.date)
        unzip(urls.grind_url);
        cd(gp)
        cd('csolver')
        if exist('compile1.bat', 'file') == 2
            copyfile('compile1.bat', 'compile.bat');
            delete('compile1.bat');
        end
        cd ..
        cd ..
        cd ..
        disp('Successfully updated');
    end
    fprintf('GRIND version %s\nRelease date: %s\n', web.version, web.date);
end

function [existnew, web, current] = exist_newversion(urls)
    existnew = 1;
    current = [];
    web = [];
%     try
%         [s, status] = urlread([url 'grindversion.txt']);
%         if status == 1
%             h = str2cell(s);
%         else
%             return;
%         end
%     catch
%         return;
%     end
    web.version = urls.grind_version;
    web.date = urls.grind_build;
    fid = fopen('use.m', 'r');
    while ~feof(fid)
        line = fgetl(fid);
        f = strfind(line, 'Revision:');
        if ~isempty(f)
            f1 = strfind(line, '$');
            current.version = strtrim(line(f + 9:f1(1) - 1));
            f = strfind(line, 'Date:');
            current.date = strtrim(line(f + 5:f1(end) - 1));
        end
    end
    fclose(fid);
    try
        f='dd-mm-yyyy HH:MM:SS';
        existnew = datenum(web.date,f) - datenum(current.date,f) < 1E-4;
    catch
        return
    end
end

function out = isadmin
    %ISADMIN True if this user is in admin role.
    if strncmp(computer,'PCWIN',5)
        wi = System.Security.Principal.WindowsIdentity.GetCurrent();
        wp = System.Security.Principal.WindowsPrincipal(wi);
        out = wp.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator);
    else
        out=true;
    end
end
function savethepath
    if ~isadmin
        warning('grind:savepatherror','To save the search path admin rights are needed: GRIND needs to be reinstalled every MATLAB session\nSolution: run MATLAB with administrative rights or <a href="matlab:savepath">enter admin username and password</a>\n')
        return;
    end
    try
        savepath; %adds permanently
    catch
        path2rc;
    end
end
function checkversions(back)
    if back || verLessThan('MATLAB', '7.12')
        olddir = cd;
        %replace [~] with [DUMM1]
        if isempty(which('fileregexp'))
            addpath(fullfile(grindpath, 'sys2'));
        end
        files = {fullfile(grindpath, '*.m'), fullfile(grindpath, 'sys2', '*.m')};
        oldpath = cd;
        if ~back
            %change to dumm1
            nchanged = fileregexp(files, ...
                struct('subdir', false, 'action', 'replace', 'addcounter', true, 'noquote', true), '[~]+(?=[ ]*[,\]\)])', 'DUMM');
            cocopath = grind_coco.findupdate;
            cd(cocopath)
            nchanged = nchanged + fileregexp({'*.m'}, ...
                struct('subdir', true, 'action', 'replace', 'addcounter', true, 'noquote', true), '[~]+(?=[ ]*[,\]\)])', 'DUMM');
        else
            %change dumm1 back to ~
            nchanged = fileregexp(files, ...
                struct('subdir', false, 'action', 'replace', 'addcounter', false, 'noquote', true), 'DUMM[0-9]*(?![a-zA-Z_0-9])', '~');
            cocopath = grind_coco.findupdate;
            cd(cocopath)
            nchanged = nchanged + fileregexp({'*.m'}, ...
                struct('subdir', true, 'action', 'replace', 'addcounter', false, 'noquote', true), 'DUMM[0-9]*(?![a-zA-Z_0-9])', '~');
        end
        cd(oldpath);
        if nchanged > 0
            fprintf('Made %d changes in the code for this matlab version\n', nchanged)
        end
        cd(olddir);
    end
 
end
function currdir = getgrinddir()
    writeable = false;
    doc_folder=getenv('PERSONAL');
    if ~isempty(doc_folder) %might not work on Mac
        currdir = fullfile(doc_folder, 'MATLAB');
    else
        currdir=cd;
    end
    while ~writeable
        currdir = getgrinddir_dlg(currdir);
        writeable = isempty(currdir) || canwriteto(currdir);
        if ~writeable
            h=errordlg(sprintf('Cannot write to this directory %s, please choose another', currdir),'setupgrind','modal');
            uiwait(h);
        end
    end
  
end

function currdir = getgrinddir_dlg(currdir1)
    hfig = figure(...
        'PaperUnits', 'inches', ...
        'Units', 'characters', ...
        'Position', [135.8 67.5384615384615 109.8 16.8461538461538], ...
        'IntegerHandle', 'off', ...
        'MenuBar', 'none', ...
        'Name', 'Setupgrind', ...
        'NumberTitle', 'off', ...
        'HandleVisibility', 'callback', ...
        'Tag', 'grinddirdlg', ...
        'Resize', 'off', ...
        'UserData', [], ...
        'CreateFcn', @(h, events)movegui(h, 'center'), ...
        'Visible', 'on');


    uicontrol(...
        'Parent', hfig, ...
        'Units', 'characters', ...
        'String', 'Installing GRIND for MATLAB', ...
        'Style', 'text', ...
        'Position', [19.8 12.1538461538462 64.6 2.30769230769231], ...
        'Children', [], ...
        'Tag', 'text2', ...
        'FontSize', 12 );

    uicontrol(...
        'Parent', hfig, ...
        'Units', 'characters', ...
        'FontUnits', get(0, 'defaultuicontrolFontUnits'), ...
        'HorizontalAlignment', 'left', ...
        'String', 'Please enter a base directory where GRIND can be installed', ...
        'Style', 'text', ...
        'Position', [6.2 7.92307692307692 70.2 1.15384615384615], ...
        'Children', [], ...
        'Tag', 'text3');

    uicontrol(...
        'Parent', hfig, ...
        'Units', 'characters', ...
        'FontUnits', get(0, 'defaultuicontrolFontUnits'), ...
        'HorizontalAlignment', 'left', ...
        'String', currdir1, ...
        'Style', 'edit', ...
        'Position', [6.2 5.23076923076923 86.4 1.69230769230769], ...
        'Children', [], ...
        'Tag', 'pathedit');

    uicontrol(...
        'Parent', hfig, ...
        'Units', 'characters', ...
        'FontUnits', get(0, 'defaultuicontrolFontUnits'), ...
        'String', 'Browse', ...
        'Callback', @browsepressed, ...
        'Style', get(0, 'defaultuicontrolStyle'), ...
        'Position', [93.2 5.23076923076923 13.8 1.69230769230769], ...
        'Children', [], ...
        'Tag', 'browsebutton' );

    uicontrol(...
        'Parent', hfig, ...
        'Units', 'characters', ...
        'FontUnits', get(0, 'defaultuicontrolFontUnits'), ...
        'String', 'OK', ...
        'callback', @OKpressed, ...
        'Style', get(0, 'defaultuicontrolStyle'), ...
        'Position', [61.8 1.38461538461538 13.8 1.69230769230769], ...
        'Children', [], ...
        'Tag', 'okbutton');

    uicontrol(...
        'Parent', hfig, ...
        'Units', 'characters', ...
        'String', 'Cancel', ...
        'Callback', @cancelpressed, ...
        'Position', [33.2 1.38461538461538 13.8 1.69230769230769], ...
        'Children', [], ...
        'DeleteFcn', blanks(0), ...
        'Tag', 'cancelbutton');

    drawnow()
    currdir = '';
    uiwait(hfig)
   % try
        ud = get(hfig, 'userdata');
        if isfield(ud, 'path')
            currdir = ud.path;
        end
        close(hfig)
        drawnow();
   % catch
   % end
    function cancelpressed(~, ~)
        uiresume;
    end
    function OKpressed(hObj, ~)
        hfig = get(hObj, 'parent');
        h = findobj(hfig, 'Tag', 'pathedit');
        dirname = get(h, 'string');
        set(hfig, 'userdata', struct('path', dirname));
        uiresume;
    end
    function browsepressed(hObj, ~)
        hfig = get(hObj, 'parent');
        h = findobj(hfig, 'Tag', 'pathedit');
        dirname = get(h, 'string');
        dirname = uigetdir(dirname, 'Select base directory for GRIND');
        set(h, 'string', dirname);
        set(hfig, 'userdata', struct('path', dirname));
        drawnow();
    end
end


function str = read_grindversion(url_or_file)
    %reads the grind version and file locations from an url or filename
    if nargin == 0
        url_or_file = 'http://sparcs-center.org/grindfiles/grindversion.txt';
    end
    if strncmp(url_or_file, 'https:', 6) || strncmp(url_or_file, 'http:', 5)
        s = webread(url_or_file, weboptions('contenttype', 'text'));
        
    else
        s = evalc(['type ' url_or_file]);
    end
    s = regexp(s, '[^=\n]*', 'match');
    str = struct(s{:});
    f = fieldnames(str);
    for i = 1:length(f)
        if isempty(strfind(f{i}, '_url'))
            % [~, name, ext] = fileparts(str.(f{i}));
            % str.([f{i}(1:end - 4) '_file']) = [name ext];
            %for some reason matlab removes \\ while reading url
            str.(f{i}) = regexprep(str.(f{i}), 'http:[\/]([^\/].*)', 'http://$1');
            str.(f{i}) = regexprep(str.(f{i}), 'https:[\/]([^\/].*)', 'https://$1');
        end
    end
    str.current_grind_version = '';
    str.current_grind_build = '';

    if exist('use', 'file')
        fid = fopen('use.m', 'r');
        while ~feof(fid)
            line = fgetl(fid);
            f = strfind(line, 'Revision:');
            if ~isempty(f)
                f1 = strfind(line, '$');
                str.current_grind_version = strtrim(line(f + 9:f1(1) - 1));
                f = strfind(line, 'Date:');
                str.current_grind_build = strtrim(line(f + 5:f1(end) - 1));
            end
        end
        fclose(fid);
    end
end



