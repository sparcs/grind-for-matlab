%PLOTDATA   Plot dataset
%   Create a plot of any dataset (variable, file or paste)
%
%   Reference page in Help browser:
%      <a href="matlab:commands('plotdata')">commands plotdata</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:38 $
function plotdata
    i_plotdatadlg;
end
