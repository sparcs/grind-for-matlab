%DEFEXTERN   Define external variables. 
%   External variables are parameters that are variable in time. With the command <a href="matlab:help setdata">setdata</a> or 
%   <a href="matlab:help loaddata">loaddata</a> the data are entered. This command can be used in the model definition, but can 
%   also be entered as a GRIND command to change a parameter in an external variable. If you want to shift in time, 
%   you can use the function <a href="matlab:help externlag">externlag</a>.
%
%   Usage:
%   DEFEXTERN VAR DEFAULT - Defines the variable VAR. DEFAULT is the default value used for 
%   simulation outside the scope of the data.
%   DEFEXTERN NAME DEFAULT DATA - You can also enter the data matrix directly.
%   DEFEXTERN NAME DEFAULT -cycle - option cycle reuses the data outside the range
%   DEFEXTERN('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'cycle' [logical] - option cycle reuses the data outside the range
%     'data' [number] - You can also enter the data matrix directly.
%     'default' [string or number] - The default value that is used if there is no data.
%     'floor' [logical] - floor, do not interpolate within time steps.
%     'name' [identifier] - Name of the external variable.
%   DEFEXTERN('-opt1','-opt2',...) - Valid command line <a href="matlab:commands func_args">options</a>:
%     '-a' - reactivate external variables.
%     '-c' - cycle: option cycle reuses the data outside the range
%     '-d' - deactivate: external variable are (temporarily) considered to be parameters so all 
%    data is neglected.
%     '-f' - floor, do not interpolate within time steps.
%     '-n' - nocycle: outside the range of the data the default value is used (default behaviour).
%
%   See also model, definepars, defpermanent, setdata, loaddata, externlag
%
%   Reference page in Help browser:
%      <a href="matlab:commands('defextern')">commands defextern</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:36 $
function g_result = defextern(varargin)
    %(name, default, data, opt1, opt2)
    global g_grind g_Y; %#ok<GVMIS>
    fieldnams = {'name', 'U1', 'Name of the external variable.', ''; ...
        'default', 's#n', 'The default value that is used if there is no data.', 0; ...
        'data', 'n', 'You can also enter the data matrix directly.', []; ...
        'cycle', 'l', 'option cycle reuses the data outside the range', true; ...
        'floor', 'l', 'floor, do not interpolate within time steps.', false}';
    args = i_parseargs(fieldnams, 'name,default,data', '-c,-n,-f,-d,-a', varargin, false, {@i_isid});
    if strcmp(g_grind.model{1}, '%external odefile')
        error('grind:defextern:externalode', 'Cannot use this command when an external odefile is used');
    end
    if any(strcmp(args.opts, '-c'))
        args.cycle = true;
    elseif any(strcmp(args.opts, '-n'))
        args.cycle = false;
    elseif ~isfield(args, 'cycle')
        args.cycle = false;
    end
    if any(strcmp(args.opts, '-f'))
        args.floor = true;
    elseif ~isfield(args, 'floor')
        args.floor = false;
    end
 
    if nargin < 1
        if ~isfield(g_grind, 'externvars') || (isempty(g_grind.externvars))
            disp('No external variables defined');
        else
            disp('External variables (default values):');
            maxparlen = par('-maxparlen');
            for i = 1:length(g_grind.externvars)
                if isempty(g_grind.externvars{i}.data)
                    fprintf(['%-' num2str(maxparlen) 's = %s  [No data] %s\n'], ...
                        g_grind.externvars{i}.name, g_grind.externvars{i}.default, getoption(i));
                else
                    fprintf(['%-' num2str(maxparlen) 's = %s  [datasize:%dx%d] %s\n'], ...
                        g_grind.externvars{i}.name, g_grind.externvars{i}.default, ...
                        size(g_grind.externvars{i}.data), getoption(i));
                end
            end
        end
        return;
        % error('Cannot define external variable');
    end
    if any(strcmp(args.opts, '-d'))
        %deactivate
        for i = 1:length(g_grind.externvars)
            g_grind.externvars{i}.options.active = 0;
        end
        g_grind.lastsettings = {};
        disp('All external variables are now fixed');
        return;
    end
    if any(strcmp(args.opts, '-a'))
        %activate
        for i = 1:length(g_grind.externvars)
            g_grind.externvars{i}.options.active = 1;
        end
        g_grind.lastsettings = {};
        disp('All external variables are activated');
        return;
    end
    %     if isempty(strfind(' ',name))&evalin('base',sprintf('exist(''%s'',''var'')',name))
    %        default=num2str(evalin('base',name));
    %     else
    if ~isfield(args, 'default')
        args.default = '0';
    elseif isnumeric(args.default)
        if size(args.default, 1) > 1
            args.default = ['''' mat2str(args.default) ''''];
        else
            args.default = mat2str(args.default);
        end
        %     end
    end
    if isfield(args, 'data') && iscell(args.data)
        args.data = args.data{1};
    end
    
    ivar = 0;
    for i = 1:length(g_grind.externvars)
        if strcmp(args.name, g_grind.externvars{i}.name)
            ivar = i;
        end
    end
    if ivar == 0
        ivar = length(g_grind.externvars) + 1;
    end
    ndx = ~strcmp(args.name, g_grind.pars);
    ppars = g_grind.pars(ndx);
    %if length(g_grind.pars) > length(ppars)
    eval(['global ' args.name]);
    eval(['clear ' args.name]);
    if length(args.default) < 20000
        evalin('base', sprintf('%s=%s;', args.name, args.default));
    end
    g_grind.pars = ppars;
    n = i_checkstr(args.default);
    externv=struct('name', args.name,'default',args.default,'dim1',size(n,1),...
        'dim2',size(n,2),'options',struct('cycle',args.cycle,'tofloor',args.floor,'active',true,'nodatawarning',false));
%     g_grind.externvars{ivar}.default = args.default;
%     n = i_checkstr(args.default);
%     g_grind.externvars{ivar}.dim1 = size(n, 1);
%     g_grind.externvars{ivar}.dim2 = size(n, 2);
%     g_grind.externvars{ivar}.options.cycle = args.cycle;
%     g_grind.externvars{ivar}.options.tofloor = args.floor;
%     g_grind.externvars{ivar}.options.active = 1;
  g_grind.externvars{ivar}=extern_interpol(externv);
    if isfield(args, 'data')
        if ~isfield(g_grind.externvars{ivar}, 'data') || xor(isempty(g_grind.externvars{ivar}.data), isempty(args.data)) ...
                || (min(size(g_grind.externvars{ivar}.data) ~= size(args.data)) == 0) ...
                || (min(min(g_grind.externvars{ivar}.data == args.data)) == 0)
            g_Y = [];
        end
        %externv.data = args.data;
    else
        if isfield(g_grind.externvars{ivar}, 'data')
            args.data = g_grind.externvars{ivar};
        else
            args.data=[];
        end
    end
    g_grind.externvars{ivar}.data=args.data;
    modelchange = 1;
    opts = sprintf(' %s', args.opts{:});
    %remove comments
    g_model = regexp(g_grind.model, '^[^%]*', 'match', 'once');
 
    ndx1 = ~cellfun('isempty', regexp(g_model, '\<defextern ', 'once'));
    ndx2 = ~cellfun('isempty', regexp(g_model, '\<defextern(', 'once'));
    for i = 1:length(g_grind.model)
        %parses defextern name value;%ff
        %or defextern(name,value);%DD
        if ndx1(i)
            d1 = parsed_equation(g_grind.model{i}).fs;
            %d1=regexp(g_grind.model{i},'(?<!%.*)[ ;]','split');
        elseif ndx2(i)
            d1 = parsed_equation(g_grind.model{i}).fs;
            %d1=regexp(g_grind.model{i},'(?<!%.*)[\(\),]','split');
            %not perfect for nested functions
        else
            d1 = {};
        end
        d1 = d1(~strcmp(d1, ' '));
        if length(d1) > 1 && strcmpi(d1{1}, 'defextern')
            if length(d1) == 2
                d1{3} = '0';
            end
            if strcmp(d1{2}, args.name) && ~strcmpi(d1{3}, args.default)
                g_grind.model{i} = sprintf('defextern %s %s%s', args.name, args.default, opts);
                i_makemodel;
                g_Y = [];
                if nargout == 1
                    g_result = sprintf('%s=g_grind.externvars{%d}.getvalue(t);', args.name, ivar);
                end
                return;
            elseif strcmp(d1{2}, args.name)
                modelchange = 0;
            end
        end
        %     if strncmpi(d2, g_grind.model{i}, length(d2))
        %         d3=[d2 ', ''' args.default ''''];
        %         if ~strncmpi(d3, g_grind.model{i}, length(d3))
        %             g_grind.model{i} = [d3 ');'];
        %             i_makemodel;
        %             g_Y = [];
        %             if nargout == 1
        %                 g_result= sprintf('%s=externvar(%d,%s,t);',args.name,ivar,args.default);
        %             end
        %             return;
        %         else
        %             modelchange = 0;
        %         end
        %     end
    end
    if modelchange
        g_grind.model = [sprintf('defextern %s %s%s', args.name, args.default, opts), g_grind.model];
        starting = isfield(g_grind, 'starting');
        i_makemodel;
        if starting
            g_grind.starting = 1;
        end
        g_Y = [];
    end
    if nargout == 1
        g_result = sprintf('%s=g_grind.externvars{%d}.getvalue(t);', args.name, ivar);
    end
end
function s = getoption(ivar)
    global g_grind; %#ok<GVMIS>
    s = '';
    if g_grind.externvars{ivar}.options.cycle
        s = [s '-cycle '];
    end
    if g_grind.externvars{ivar}.options.tofloor
        s = [s '-floor '];
    end
end
