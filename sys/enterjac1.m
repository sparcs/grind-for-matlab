%ENTERJAC   Enter equations of the Jacobian matrix
%   Enter the analytical Jacobian matrixes, consisting of the derivatives
%   of the right-hand sides of all differential/difference equations with respect to each state
%   variable, parameters and sensitivity.
%   This is used to calculate the linearized system in equilibria (stability) and for 
%   continuation of bifurcations. This way you can study the stability of equilibria.
%
%   Usage:
%   ENTERJAC - the user is prompted to give the elements of the Jacobian
%   ENTERJAC({ J11,J12;J21,J22}) - the whole matrix is entered in one message 
%   (J11 is the first string with an equation.
%   ENTERJAC 1 1 J11 - each element is entered separately
%   ENTERJAC('jacobian',@myjac) - use user supplied function (myjac):
%   this function should look like this:
%   jac=function myjac(t,x,par1,par2,par3...) t=time,x=vector of statevars, par1-parn the names of the parameters.
%   Please note that they need to be in the same order as g_grind.pars (will not be checked)!
%
%   ENTERJAC('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'colnr' [integer>0] - column number for input
%     'hessian' [function_handle or cell] - the Hessian 3d matrix as cell of strings or function handle.
%     'implicit' [logical] - the Jacobian for a implicit model, to be used for ODE15i
%     'jacelement' [string] - single element of Jacobian
%     'jacobian' [function_handle or cell] - the whole Jacobian as cell of strings or function handle.
%     'jacobianp' [function_handle or cell] - the Jacobian of parameters (columns) as cell of strings or function handle.
%     'jacobian_y' [function_handle or cell] - the Jacobian of an implicit model.
%     'jacobian_yp' [function_handle or cell] - the Jacobian with respect to the derivatives of an implicit model.
%     'maxorder' [integer>=0 and integer<=5] - limit order of derivatives to maxorder (default=5)
%     'maxtime' [number] - maximum time for deriving the derivatives (default NaN)
%     'rownr' [integer>0] - row number for input
%   ENTERJAC('-opt1','-opt2',...) - Valid command line <a href="matlab:commands func_args">options</a>:
%     '-?' - list all Jacobians.
%     '-?der3' - list the 3rd derivatives
%     '-?der4' - list the 4th derivatives
%     '-?der5' - list the 5th derivatives
%     '-?hess' - list the Hessian matrix
%     '-?Hessianp' - list the Hessian with respect to parameters
%     '-?jac' - list the Jacobian only.
%     '-?jacp' - list the Jacobian with respect to parameters
%     '-c' - clear all symbolic Jacobians
%     '-s' - the Jacobian is calculated using the Symbolic toolbox.
%     '-t' - test if the analytical Jacobian is different from the numerical one.
%     '-update' - update the handles for the implicit Jacobian.
%
%   See also eigen, findeq, lyapspect
%
%   Reference page in Help browser:
%      <a href="matlab:commands('enterjac')">commands enterjac</a>

%   Copyright 2020 WUR
%   Revision: 1.2.1 $ $Date: 12-Feb-2020 23:08:03 $
function [varargout] = enterjac1(varargin)
    global g_grind; %#ok<GVMIS>
    if ~isfield(g_grind,'sym')||isempty(g_grind.sym)
        g_grind.sym=symmodel(g_grind);
    end
    if nargout>0
       [varargout]=g_grind.sym.enterjac(varargin{:});
    else
       g_grind.sym.enterjac(varargin{:});
    end
end
