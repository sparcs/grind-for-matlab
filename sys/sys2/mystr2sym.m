function s = mystr2sym(ch)
    try
        ch=regexprep(ch,'\<i\>','i1');
        %do not use i for complex but instead use 1i
        s = str2sym(ch);
    catch err
        if strcmp(err.identifier,'MATLAB:UndefinedFunction')
        s = str2sym_old(ch);
        else
            rethrow(err);
        end
    end
end
function g_asym = str2sym_old(g_s)
    %not very nice but a way to translate string to sym
    g_s1 = symvar(g_s);
    for g_i = 1:length(g_s1)
        eval(sprintf('%s=sym(''%s'');', g_s1{g_i}, g_s1{g_i}))
    end
    g_asym = eval(g_s);
end