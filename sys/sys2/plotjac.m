function jac = plotjac(name, jac, dx, dy, dhess)
    if nargin < 5
        dhess = '';
    end
    if isempty(jac)
        fprintf('No %s defined\n', name)
    elseif isa(jac,'function_handle')
        fprintf('Symbolic %s defined   @%s\n', name, func2str(jac))
    elseif isstruct(jac)
        s1 = sprintf('%d,', jac.size);
        fprintf('%s = zeros(%s)\n', name, s1(1:end - 1))
        if isa(jac,'function_handle')
            disp(func2str(jac.handle))
        elseif isfield(jac,'unique')            
            for i = 1:numel(jac.unique)
                u = jac.unique(i);
                for j = 1:size(u.indices)
                    c = i_enterjacdlg('jac2str', {u.equation});
                    if numel(jac.size)<=2&&~isempty(dx)
                        [i1,j1]=ind2sub(jac.size, u.indices(j));
                        if isempty(dy{i1})
                           fprintf('%s = %s\n', dx{j1}, c{1});
                        else
                           fprintf('d(%s)/d(%s) = %s\n', dx{i1}, dy{j1}, c{1});
                        end
                    else
                       fprintf('%s(%s) = %s\n', name, index2sub(jac.size, u.indices(j)), c{1})
                    end
                end
            end
        end
    else
        jac = i_enterjacdlg('jac2str', jac);
        if isempty(dhess)
            fprintf('%s:\n', name)
        end
        for i = 1:length(dx)
            for j = 1:length(dy)
                if ~isempty(dhess)
                    fprintf('d2(%s)/d(%s)/d(%s) = %s\n', dx{i}, dy{j}, dhess, jac{i, j});
                elseif isempty(dy{1})
                    fprintf('%s = %s\n', dx{i}, jac{i, j});
                else
                    fprintf('d(%s)/d(%s) = %s\n', dx{i}, dy{j}, jac{i, j});
                end
            end
        end
    end
end
function s = index2sub(siz, index)
    a = cell(size(siz));
    [a{1:end}] = ind2sub(siz, index);
    s = sprintf('%d,', a{:});
    s = s(1:end - 1);
end