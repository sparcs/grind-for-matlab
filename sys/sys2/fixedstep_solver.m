%FIXEDSTEP_SOLVER  Various ODE solvers using their Butcher tableaus.
%   Less efficient than dedicated ODE solvers (factor 1.5-2 times slower)
%   Solve an ordinary differential equation using different methods.
%   
%Usage:
%[tout, yout] = fixedstep_solver(solvername, odefunct, tspan, y0, options)
% First argument should be the solvername, other arguments like other
% MATLAB solvers. options.MaxStep or options.StepSize should provide the
% step size (default 0.1).
%
% Solver names:
%    'euler' : simplest method. Butcher tableau:
%         0   | 0
%         --------
%             | 1
%
%    'midpoint' : 2nd order midpoint method. Butcher tableau:
%         0   | 0    0
%       0.5   | 0.5  0
%       -----------------
%             | 0.5  0.5
%
%     'heun' : 2nd order Heum method. Butcher tableau:
%         0   | 0   0
%         1   | 1   0
%         -------------
%             | 0.5 0.5
%
%      'ralston' : 2nd order Ralston method (effcient). Butcher tableau:
%        0    | 0    0
%       2/3   | 2/3  0
%       ------------------
%             | 1/4  3/4
%
%      'kutta3' : 3rd order Kutta method. Butcher tableau: 
%       0     | 0     0    0
%       0.5   | 0.5   0    0
%       1     | -1    2    0
%      ----------------------
%             | 1/6  2/3 1/6
%
%      'heun3' 3rd order Heun method. Butcher tableau: 
%       0     | 0    0    0
%       1/3   | 1/3  0    0
%       2/3   | 0   2/3   0
%       -----------------------
%             | 1/4  0  3/4
%
%         case 'ralston3'
%             butcher = [0, 0, 0, 0;  ...
%                 1/2, 1/2, 0, 0;  ...
%                 3/4, 0, 3/4, 0;  ....
%                 NaN, 2/9, 1/3, 4/9];
%         case 'rk4'
%             butcher = [0, 0, 0, 0, 0;  ...
%                 0.5, 0.5, 0, 0, 0;  ...
%                 0.5, 0, 0.5, 0, 0;  ....
%                 1, 0, 0, 1, 0;  ...
%                 NaN, 1 / 6, 1 / 3, 1 / 3, 1 / 6];
%
%   Reference page in Help browser:
%      <a href="matlab:commands('heun')">commands heun</a>

%   Copyright 2021 WUR
%   Revision: 1.2.1 $ $Date: 23-Nov-2021 16:54:00 $
function [tout, yout] = fixedstep_solver(solvername, odefunct, tspan, y0, options)
    global g_grind;
    if (nargin < 5)
        %if there are no valid options use default stepsize
        delta = 0.1;
        options.StepSize = 0.1;
        options.OutputFcn = [];
    else
        %use the option StepSize as step size
        if ~isfield(options, 'StepSize') && isfield(options, 'MaxStep')
            options.StepSize = options.MaxStep;
        end
        if ~isfield(options, 'StepSize') || isempty(options.StepSize)
            options.StepSize = 0.1;
        end
        if ~isfield(options, 'OutputFcn')
            options.OutputFcn = [];
        end
        
        delta = options.StepSize;
    end
    if nargin < 5
        nonNegative = [];
    else
        nonNegative = odeget(options, 'NonNegative', []);
    end
    nout = 0;
    anyNonNegative = ~isempty(nonNegative);
    haveOutputFcn = ~isempty(options.OutputFcn);
    outputFcn = options.OutputFcn;
    if haveOutputFcn
        feval(outputFcn, tspan, y0, 'init');
    end
    switch solvername
        case 'euler'
            butcher = [0 0;  ...
                NaN 1];
        case 'midpoint'
            butcher = [0 0 0;  ...
                0.5 0.5 0;  ...
                NaN 0.5 0.5];
        case 'heun'
            butcher = [0 0 0;  ...
                1 1 0;  ...
                NaN 0.5 0.5];
        case 'ralston'
            butcher = [0, 0, 0;  ...
                2 / 3, 2 / 3, 0;  ...
                NaN, 1 / 4, 3 / 4];
        case 'kutta3'
            butcher = [0, 0, 0, 0;  ...
                0.5, 0.5, 0, 0;  ...
                1, -1, 2, 0;  ....
                NaN, 1 / 6, 2 / 3, 1 / 6];
        case 'heun3'
            butcher = [0, 0, 0, 0;  ...
                1 / 3, 1 / 3, 0, 0;  ...
                2 / 3, 0, 2 / 3, 0;  ....
                NaN, 1 / 4, 0, 3 / 4];
        case 'ralston3'
            butcher = [0, 0, 0, 0;  ...
                1 / 2, 1 / 2, 0, 0;  ...
                3 / 4, 0, 3 / 4, 0;  ....
                NaN, 2 / 9, 1 / 3, 4 / 9];
        case 'rk4'
            butcher = [0, 0, 0, 0, 0;  ...
                0.5, 0.5, 0, 0, 0;  ...
                0.5, 0, 0.5, 0, 0;  ....
                1, 0, 0, 1, 0;  ...
                NaN, 1 / 6, 1 / 3, 1 / 3, 1 / 6];
        case 'ralston4'
            butcher = [0, 0, 0, 0, 0; ...
                0.4, 0.4, 0, 0 , 0; ...
                0.45573725, 0.29697761, 0.15875964, 0, 0; ...
                1, 0.21810040, -3.05096516, 3.83286476, 0; ....
                NaN, 0.17476028, -0.55148066, 1.20553560, 0.17118478];
        case 'kutta4'
             butcher = [0, 0, 0, 0, 0;  ...
                1/3, 1/3, 0, 0, 0;  ...
                2/3, -1/3, 1, 0, 0;  ....
                1, 1, -1, 1, 0;  ...
                NaN, 1/8, 3/8, 3/8, 1/8];
        otherwise
            error('unknown solver');
    end
    % Test that tspan is internally consistent.
    tspan = tspan(:);
    ntspan = length(tspan);
    if ntspan == 1
        t0 = 0;
        next = 1;
    else
        t0 = tspan(1);
        next = 2;
    end
    tfinal = tspan(ntspan);
    if t0 == tfinal
        error('GRIND:euler:tpan', 'The last entry in tspan must be different from the first entry.');
    end
    tdir = sign(tfinal - t0);
    if any(tdir * (tspan(2:ntspan) - tspan(1:ntspan - 1)) <= 0)
        error('GRIND:euler:tspan', 'The entries in tspan must strictly increase or decrease.');
    end
    t = t0;
    y = y0;
    %  y = y0(:);
    neq = length(y);
    %adapt delta if there should be given more output
    step_tspan = median(diff(tspan));
    delta = min(delta, step_tspan);
    g_grind.solver.opt.StepSize = delta;
    % Set the output flag.
    
    outflag = ntspan > 2; % output only at tspan points
    
    % Allocate memory if we're generating output.
    delta = delta * tdir;
    
    if nargout > 0
        if outflag % output only at tspan points
            tout = tspan;
            outflag = delta ~= step_tspan;
            yout = nan(ntspan, neq);
        else
            tout = transpose(t0:delta:tfinal);
            if tout(end) < tfinal %if tfinal cannot divided in delta's
                tout(end + 1) = tfinal;
            end
            
            yout = nan(size(tout, 1), neq);
        end
        nout = 1;
        tout(nout) = t;
        yout(nout, :) = transpose(y);
    end
    k = cell(1, size(butcher, 1) - 1);
   
    %
    %MAIN LOOP
    %evaluate the odefunction for the next time steps
    %fold=[];
    %running = 1;
    while nout < length(tout)
        %tried different ways to speed this up, but did not succeed (this
        %seems relatively fast)
        for i = 1:size(butcher, 1) - 1
            sumk = zeros(size(y0));
            for j = 1:i - 1
                sumk = sumk + k{j} .* butcher(i, j + 1);
            end
            k{i} = feval(odefunct, t + butcher(i, 1) * delta, y + delta * sumk);
        end
        ynew = 0;
        for i = 1:size(butcher, 1) - 1
            ynew = ynew + k{i} .* butcher(end, i + 1);
        end
        ynew = y + delta * ynew;

        if anyNonNegative
            ynew(nonNegative) = max(ynew(nonNegative), 0);
        end
        
        if ~outflag % computed points, no refinement only the last value
            nout = nout + 1;
            %             if ~running
            %                 if nout > length(tout)
            %                     nout = length(tout);
            %                 end
            %                 t1 = tout(nout);
            %                 yout(nout, :) = transpose(y + (ynew - y) ./ (tnew - t) .* (t1 - t));
            %             else
            yout(nout, :) = transpose(ynew);
            %             end
            tnew = tout(nout);
            if haveOutputFcn
                if feval(outputFcn, tnew, transpose(ynew), '')
                    ndx = ~isnan(yout(:, 1));
                    yout = yout(ndx, :);
                    tout = tout(ndx);
                    nout = length(tout) + 1;
                    % running = false;
                end
            end
        elseif (tdir * (tnew - tspan(next)) >= 0) % at tspan, tspan assumed to be larger than delta
            nout = nout + 1;
            t1 = tout(nout);
            y1 = transpose((y + (ynew - y) ./ (tnew - t) .* (t1 - t)));
            yout(nout, :) = y1;
            next = next + 1;
            if haveOutputFcn
                if feval(outputFcn, t1, y, '')
                    ndx = ~isnan(yout(:, 1));
                    yout = yout(ndx, :);
                    tout = tout(ndx);
  
                end
            end
        end
        
        y = ynew;
        t = tnew;
    end
    g_grind.solver.opt.StepSize = options.StepSize;
    if haveOutputFcn
        feval(outputFcn, [], [], 'done');
    end
    if nargout == 1
        tout = struct('x', tout, 'y', yout, 'solver', solvername);
    end
    % if nout<length(tout)
    %    tout=tout(1:nout);
    %    yout=yout(1:nout,:);
    % end
end
