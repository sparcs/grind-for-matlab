
function str = read_grindversion(url_or_file)
    %reads the grind version and file locations from an url or filename
    if nargin == 0
        url_or_file = 'http://sparcs-center.org/grindfiles/grindversion.txt';
    end
    if strncmp(url_or_file, 'https:', 6) || strncmp(url_or_file, 'http:', 5)
        s = webread(url_or_file, weboptions('contenttype', 'text'));
        
    else
        s = evalc(['type ' url_or_file]);
    end
    s = regexp(s, '[^=\n]*', 'match');
    str = struct(s{:});
    f = fieldnames(str);
    for i = 1:length(f)
        if strcontains(f{i}, '_url')
            % [~, name, ext] = fileparts(str.(f{i}));
            % str.([f{i}(1:end - 4) '_file']) = [name ext];
            %for some reason matlab removes \\ while reading url
            str.(f{i}) = regexprep(str.(f{i}), 'http:[\/]([^\/].*)', 'http://$1');
            str.(f{i}) = regexprep(str.(f{i}), 'https:[\/]([^\/].*)', 'https://$1');
        end
    end
    str.current_grind_version = '';
    str.current_grind_build = '';
    str.current_matcont = [];
    str.current_matcontm = [];
    str.current_coco = [];
    if exist('grind.cfg', 'file')
        fid = fopen('grind.cfg', 'r');
        lines = textscan(fid, '%s', 'delimiter', '\n', 'whitespace', '');
        lines = lines{1};
        fclose(fid);
        lines = regexp(lines, '[^=\n]*', 'match');
        for i = 1:length(lines)
            if exist(lines{i}{2}, 'dir')
                str.(lines{i}{1}) = lines{i}{2};
            end
        end
    end
    if exist('use', 'file')
        fid = fopen('use.m', 'r');
        while ~feof(fid)
            line = fgetl(fid);
            f = strfind(line, 'Revision:');
            if ~isempty(f)
                f1 = strfind(line, '$');
                str.current_grind_version = strtrim(line(f + 9:f1(1) - 1));
                f = strfind(line, 'Date:');
                str.current_grind_build = strtrim(line(f + 5:f1(end) - 1));
            end
        end
        fclose(fid);
    end
end
