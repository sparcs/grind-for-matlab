classdef analyse_stability
    properties
        sym_model = [];
        J = [];
        statevars = []
        characteristic = [];
        isdiffer = false;
        equilibrium = [];
        title = '';
    end
    methods (Static)
        function res = simplify(res)
            if iscell(res)
                for i2 = 1:length(res)
                    res{i2} = analyse_stability.simplify(res{i2});
                end
                return;
            end
            f = fieldnames(res);
            for i1 = 1:length(f)
                if isa(res.(f{i1}), 'sym')
                    res.(f{i1}) = simplify(res.(f{i1}));
                end
            end
        end
        function c1 = plotresults(res, varargin)
            function tab = maketable(sy, colheader, rowheader, border)
                if nargin < 4
                    border = 1;
                end
                tab = cell(size(sy, 1) + 2, 1);
                tab{1} = sprintf('<table border="%d">', border);
                if ~isempty(colheader)
                    s1 = sprintf('<td><b>%s</b></td>', colheader{:});
                    if isempty(rowheader)
                        rh = '';
                    else
                        rh = '<td> </td>';
                    end
                    tab{1} = sprintf('%s<tr>%s%s</tr>', tab{1}, rh, s1);
                end
                for i1 = 1:size(sy, 1)
                    if isnumeric(sy(i1, :))
                        if any(~isreal(sy(i1, :)))
                            s1 = '';
                            for j1 = 1:size(sy, 2)
                                if ~isreal(sy(i1, j1))
                                    if imag(sy(i1, j1)) < 0
                                        s1 = sprintf('%s<td>%g - %gi</td>', s1, real(sy(i1, j1)), abs(imag(sy(i1, j1))));
                                    else
                                        s1 = sprintf('%s<td>%g + %gi</td>', s1, real(sy(i1, j1)), imag(sy(i1, j1)));
                                    end
                                else
                                    s1 = sprintf('%s<td>%g</td>', s1, sy(i1, j1));
                                end
                            end
                        else
                            s1 = sprintf('<td>%g</td>', sy(i1, :));
                        end
                    else
                        s1 = sprintf('<td><eq>%s</eq></td>', sy(i1, :));
                    end
                    if isempty(rowheader)
                        rh = '';
                    else
                        rh = ['<td>' rowheader{i1} '</td>'];
                    end
                    tab{i1 + 1} = sprintf('<tr>%s%s</tr>', rh, s1);
                end
                tab{end} = '</table>';
            end
            c = {sprintf('<h2>Stability analysis %s</h2>', res.title)};
            if ~isempty(res.equilibrium)
                noeq = strcontains(res.title, 'no equilibrium!');
                if noeq
                    h1 = '';
                else
                    h1 = '_eq';
                end
                s = cell(numel(res.equilibrium), 1);
                for i = 1:length(res.equilibrium)
                    if ~isempty(res.statevars)
                        if iscell(res.statevars)
                            s{i} = sprintf('%s%s = %s', tostr(res.statevars{i}), h1, tostr(res.equilibrium(i)));
                        else
                            s{i} = sprintf('<eq>%s%s = %s</eq>', char(res.statevars(i)), h1, char(res.equilibrium(i)));
                        end
                    else
                        s{i} = tostr(res.equilibrium(i));
                    end
                end
                if noeq
                    c = [c; {'<h3>Initial condition:</h3>'}; s];
                else
                    c = [c; {'<h3>Equilibrium:</h3>'}; s];
                end
            end
            if ~isempty(res.J)
                s = maketable(res.J, {}, {});
                if isa(res.J, 'sym') && isempty(res.equilibrium)
                    c = [c; {'<h3>General Jacobian (J):</h3>'}; s];
                else
                    c = [c; {'<h3>Jacobian (J):</h3>'}; s];
                end
            end
            if ~isempty(res.characteristic)
                rownr = sprintf('&lambda;^%d|', length(res.characteristic) - 1: -1:0);
                tab = maketable(res.characteristic.', {}, regexp(rownr(1:end - 1), '[|]', 'split'));
                c = [c; {'<h3>Characteristic equation:</h3>'}; tab; ];
                if ~res.isdiffer
                    c = [c; {''; '<i>Necessary condition for stability all elements &gt; 0</i>'}];
                end
                if length(res.characteristic) == 3
                    c = [c; {sprintf('<br><b>trace(J)</b> = %s', tostr(trace(res.J)))}];
                    c = [c; {sprintf('<b>det(J)</b> = %s', tostr(det(res.J))); ''; }];
                    if res.isdiffer
                        %  c=[c; {'<i>Stable if trace(J)<0 and det(J)>0</i>'}];
                    else
                        c = [c; {'<i>Stable if trace(J)&lt;0 and det(J)&gt;0</i>'}];
                    end
                end
                %disp('Routh table:');
                %disp(a)
                %[res.hurwitz, res.routh_table, res.special_cases] = obj.hurwitz; %#ok<ASGLU>
                if ~res.isdiffer
                    colnr = sprintf('Col %d|', [1:size(res.routh_table, 2)]);
                    rownr = sprintf('s^%d|', [size(res.routh_table, 1) - 1: -1:0]);
                    tab = maketable(res.routh_table, regexp(colnr(1:end - 1), '[|]', 'split'), regexp(rownr(1:end - 1), '[|]', 'split'));
                    c = [c; {'<h3>Routh Table:</h3>'}; tab; {'<i>Stable if no changes of sign in first column</i>'}];
                    %str2cell(evalc('disp(routh_table)'))
                    if isnumeric(res.routh_table)
                        chan = sum(abs(diff(sign(res.routh_table(:, 1))) / 2));
                        if chan == 0
                            c = [c; {sprintf('<i>Stable: %d positive eigenvalues\n</i>', chan)}];
                        else
                            c = [c; {sprintf('Unstable: %d positive eigenvalues\n', chan)}];
                        end
                    end
                    %         cHurwitz=cell(length(hurwitz1)+1,1);
                    c = [c; '<h3>Hurwitz determinants:</h3>'];
                    rownr = sprintf('D(%d)|', [1:length(res.hurwitz)]);
                    tab = maketable(res.hurwitz.', {}, regexp(rownr(1:end - 1), '[|]', 'split'));
                    %                 for i = 1:length(hurwitz1)
                    %                     cHurwitz{i} = sprintf('D(%2d)  =  %s', i, tostr(hurwitz1(i)));
                    %                 end
                    c = [c; tab; {'<i>All positive for stability</i>'}];
                    %               cHurwitz{end}  = '<i>All positive for stability</i>';
                    if isnumeric(res.hurwitz)
                        if any(res.hurwitz < 0)
                            s = 'Unstable';
                        else
                            s = 'Stable';
                        end
                        c = [c; {s}];
                    end
                end
                if ~isempty(res.J)
                    c = [c; {'<h3>Eigenvalues:</h3>'}];
                    eigs = res.eigen;
                    rownr = sprintf('&lambda;(%d)|', [1:length(eigs)]);
                    tab = maketable(eigs, {}, regexp(rownr(1:end - 1), '[|]', 'split'));
                    %for i = 1:length(eigs)
                    %   c = [c;{sprintf('lambda_%d  =  %s', i,tostr(eigs(i)))}];
                    %end
                    if res.isdiffer
                        c = [c; tab; ''; '<i>Stable if absolute values of all eigenvalues are less than one</i>'];
                    else
                        c = [c; tab; ''; '<i>Stable if real parts of all eigenvalues are negative</i>'];
                    end
                    if isnumeric(res.J)
                        [isstable, issaddle, isspiral] = i_stability(eigs, res.isdiffer, false);
                        if issaddle
                            s1 = 'Saddle';
                        end
                        if isstable
                            s1 = 'Stable ';
                        else
                            s1 = 'Unstable ';
                        end
                        if isspiral
                            s1 = [s1 'spiral'];
                        else
                            s1 = [s1 'node'];
                        end
                        c = [c; {['<i>' s1 '</i>']}];
                    end
                end
            end
            c = regexprep(c, {'[_][_]([0-9])*[_]([0-9]*)', '[_][_]([0-9])*'}, {'($1,$2)', '($1)'});
            if nargout == 1
                c1.html = c;
                c1.txt = regexprep(c, '[<][^>]*[>]', '');
            else
                figopts = struct(varargin{:});
                if ~isfield(figopts, 'visible') || ~strcmpi(figopts.visible, 'off') %figopts: {'visible'  'off'}
                    s = sprintf('%s<br>\n', c{:});
                    web(sprintf('text://<html><title>trdet analysis</title><font face="Courier New" size=3> %s<br></font></form></html>', s));
                else
                    fprintf('%s\n', c{:});
                end
            end
        end
    end
    methods
        function obj = analyse_stability(varargin)
            %analyse_stability(property,value)
            % different ways to evaluate stability of differential
            % equations:
            %  1) all elements of characteristic polynomial >0 (not
            %     sufficient condition)
            %  2) no sign change in first row of Routh table
            %  3) all Hurwitz determinants >0
            %  4) all real eigenvalues <0
            %
            %  extra: loop analysis (Levins) is other interpretation of
            %  Hurwitz determinants
            %  most analyses can also be done with sym
            %
            %properties:
            %  'isdiffer' is difference equation?
            %  'niters' iterations of difference equation
            %  'J' is numeric Jacobian or function/sym with general
            %  Jacobian.
            %  'statevars' = names of state variables of symbolic J (syms
            %       or character cell)
            %  'equilibrium' = the equilibrium (sym or double). Leave empty if a numeric Jacobian is given
            %  'characteristic' = characteristic equation
            %  'symmodel' = analyse symbolic grind model
            if nargin == 0
                options = struct('grindmodel', true);
            elseif nargin == 1 && isstruct(varargin{1})
                options = varargin{1};
            else
                options = struct(varargin{:});
            end
            if isfield(options, 'parent')
                obj = options.parent;
                obj.equilibrium = [];
                %obj.J = options.parent.J;
                %obj.isdiffer = options.parent.isdiffer;
                %obj.statevars = options.parent.statevars;
                %obj.niters = options.parent.niters;
            end
            if isfield(options, 'grindmodel') && options.grindmodel
                options.sym_model = symmodel;
            end
            if isfield(options, 'sym_model')
                %obj.isdiffer = options.symmodel.model.isdiffer;
                %                obj.niters = options.symmodel.model.niters;
                obj.sym_model = options.sym_model;
                if isempty(obj.sym_model.Jacobian)
                    obj.sym_model.update;
                end
                obj.J = mysym(obj.sym_model.Jacobian);
                obj.statevars = obj.sym_model.symstatevars;
                obj.isdiffer = obj.sym_model.model.isdiffer;
            end
            %             if isfield(options, 'niters')
            %                 obj.niters = options.niters;
            %             end
            if isfield(options, 'J')
                obj.J = options.J;
                if isa(obj.J, 'sym')
                    obj.J = simplify(obj.J);
                end
            end
            if isfield(options, 'title')
                obj.title = options.title;
            end
            if isfield(options, 'equilibrium')
                obj.equilibrium = options.equilibrium;
            end
            if isfield(options, 'statevars')
                obj.statevars = options.statevars;
            end
            if isfield(options, 'characteristic')
                obj.characteristic = options.characteristic;
            end
            if isfield(options, 'isdiffer')
                obj.isdiffer = options.isdiffer;
            end

            %substitute the equilibrium in the Jacobian (symbolic)
            if isa(obj.J, 'sym') && ~isempty(obj.equilibrium)
                J1 = obj.J;
                for i = 1:numel(J1)
                    for j = 1:length(obj.statevars)
                        J1(i) = simplify(subs(J1(i), obj.statevars(j), obj.equilibrium(j)));
                    end
                end
                obj.J = J1;
            end
            %calculate the characteristic equation
            %  if false
            if isempty(obj.characteristic) && ~isempty(obj.J)
                if isnumeric(obj.J)
                    if size(obj.J, 1) > 1500
                        obj.characteristic = poly(obj.J); %very fast (uses eig) but less exact?
                    else
                        obj.characteristic = fastcharpoly(obj.J); %slow for size(J)>2000x2000
                    end
                elseif exist('sym/charpoly','file')  %newer versions
                    obj.characteristic = simplify(charpoly(obj.J));
                else
                    obj.characteristic = simplify(poly(obj.J));                   
                end
            end
            % end
        end

        function [res] = summary(obj)
            res.title = obj.title;

            res.isdiffer = obj.isdiffer;
            res.statevars = obj.statevars;
            res.J = obj.J;
            res.equilibrium = obj.equilibrium;
            res.characteristic = obj.characteristic;
            if ~isempty(res.characteristic)
                if ~res.isdiffer
                    [res.hurwitz, res.routh_table, res.special_cases] = obj.hurwitz;
                end
                res.eigen = eig(obj.J);
            end
            if nargout > 0
                res.results = analyse_stability.plotresults(res);
            else
                analyse_stability.plotresults(res);
            end
        end

        function [table, special_cases] = routh_table(obj)
            %first row should be all positive or all negative for stability
            [table, special_cases] = routh(obj.characteristic);
        end

        function [V, D] = eig(obj)
            if nargout < 2
                V = eig(obj.J);
            else
                [V, D] = eig(obj.J);
            end
        end

        function [hurwitz1, routh_table, special_cases] = hurwitz(obj)
            % hurwitz1=hurwitz_determinants(obj.characteristic);
            [routh_table, special_cases] = routh(obj.characteristic);
            hurwitz1 = routh_to_hurwitz(routh_table);
            %all hurwitz determinants should be positive for stability
        end
        function s = strcharpoly(obj)
            dim = length(obj.characteristic) - 1;
            powers = (dim: - 1:0);
            s = sprintf('&lambda;^%d', dim);
            if isa(obj.characteristic, 'sym')
                for i = 2:length(powers)
                    if powers(i) == 1
                        s = sprintf('%s<br> + (%s)*&lambda;', s, obj.characteristic(i));
                    elseif powers(i) == 0
                        s = sprintf('%s<br> + %s', s, obj.characteristic(i));
                    else
                        s = sprintf('%s<br> + (%s)*&lambda;^%d', s, obj.characteristic(i), powers(i));
                    end
                end

            else
                for i = 2:length(powers)
                    if obj.characteristic(i) < 0
                        aplus = '-';
                        h = - 1; %to prevent + - in the equation
                    else
                        aplus = '+';
                        h = + 1;
                    end
                    if powers(i) == 1
                        s = sprintf('%s %s %g*&lambda;', s, aplus, h * obj.characteristic(i));
                    elseif powers(i) == 0
                        s = sprintf('%s %s %g', s, aplus, h * obj.characteristic(i));
                    else
                        s = sprintf('%s %s %g*&lambda;^%d', s, aplus, h * obj.characteristic(i), powers(i));
                    end
                end
            end
            %       disp(s)
        end
    end
end

function hurwitz = routh_to_hurwitz(routh_table)
    %Relationship between Hurwitz and Routh table
    %A=H2/H1
    hurwitz = zeros(1, size(routh_table, 1));
    if isa(routh_table, 'sym')
        hurwitz = sym(hurwitz);
    end
    hurwitz(1) = routh_table(1);
    for i = 2:length(hurwitz)
        hurwitz(i) = hurwitz(i - 1) * routh_table(i);
    end
end

function res = tostr(s)
    if isnumeric(s)
        res = sprintf('%g', s);
    else
        res = ['<eq>' char(s) '</eq>'];
        %res=strrep(res,{'>','<'},{'&gt;','&lt;'});
    end
end

function res = hurwitz_determinants(cs)
    %hurwitz matrices (without routh)
    n = length(cs);
    if isa(cs, 'sym')
        H = sym(zeros(n));
        res = sym(zeros(1, n));
    else
        H = zeros(n);
        res = zeros(1, n);
    end
    cs1 = cs;
    if rem(n, 2) == 1
        cs1 = [cs1 0];
        n_half = round((n + 1) / 2);
    else
        n_half = round((n) / 2);
    end
    cs_arr = flipud(reshape(cs1, [2, n_half]));
    for i = 1:2:n
        H(i:i + 1, round(i / 2):round(i / 2) + n_half - 1) = cs_arr;
    end
    
    for i = 1:length(res)
        res(i) = det(H(1:i - 1, 1:i - 1));
    end
end

function [rhTable, special_cases] = routh(coeffVector)
    %% Routh-Hurwitz stability criterion
    %
    %  The Routh-Hurwitz stability criterion is a necessary (and frequently
    %  sufficient) method to establish the stability of a single-input,
    %  single-output(SISO), linear time invariant (LTI) control system.
    %  More generally, given a polynomial, some calculations using only the
    %  coefficients of that polynomial can lead us to the conclusion that it
    %  is not stable.
    %  Instructions
    %  ------------
    %
    %  in this program you must give your system coefficients and the
    %  Routh-Hurwitz table would be shown
    %
    %   Farzad Sagharchi ,Iran
    %   2007/11/12
    %   E van Nes, lot of bugs in special cases solved
    %% Initialization
    special_cases = struct('singlezero', false, 'allzeros', false);
    ceoffLength = length(coeffVector);
    rhTableColumn = round(ceoffLength / 2);
    %  Initialize Routh-Hurwitz table with empty zero array
 
    rhTable = zeros(ceoffLength, rhTableColumn);
    if isa(coeffVector, 'sym')
        rhTable = sym(rhTable);
    end
    %  Compute first row of the table
    rhTable(1, :) = coeffVector(1, 1:2:ceoffLength);
    %  Check if length of coefficients vector is even or odd
    if (rem(ceoffLength, 2) ~= 0)
        % if odd, second row of table will be
        rhTable(2, 1:rhTableColumn - 1) = coeffVector(1, 2:2:ceoffLength);
    else
        % if even, second row of table will be
        rhTable(2, :) = coeffVector(1, 2:2:ceoffLength);
    end
    %% Calculate Routh-Hurwitz table's rows
    %  Set epss as a small value
    epss = 1E-8;
    %  Calculate other elements of the table
    for i = 3:ceoffLength
        %  special case 1: zero in the first column
        % replace with small number
        if (rhTable(i - 1, 1) == 0) && ~all(rhTable(i - 1, :) == 0)
            special_cases.singlezero = true;
            rhTable(i - 1, 1) = epss;
        end
        %  special case 2: row of all zeros
        %  add auxiliary table (even powers) and derivative of that
        if all(rhTable(i - 1, :) == 0)
            special_cases.allzeros = true;
            order = ceoffLength - (i - 2);
            cnt1 = 0;
            cnt2 = 1;
            RowBefore = rhTable(i - 2, :);
            RowBefore = RowBefore ./ abs(RowBefore(1, 1)); %normalize the first column
            %this is always allowed as it just multiplies the whole
            %equation
            for j = 1:rhTableColumn - 1
                rhTable(i - 1, j) = (order - cnt1) * RowBefore(cnt2);
                cnt2 = cnt2 + 1;
                cnt1 = cnt1 + 2;
            end
        end

        %vectorized per row (speed bottleneck of routh)
        rhTable(i, 1:end - 1) = ((rhTable(i - 1, 1) .* rhTable(i - 2, 2:end)) - ....
            (rhTable(i - 2, 1) .* rhTable(i - 1, 2:end))) ./ rhTable(i - 1, 1);

        %
        %     firstElemUpperRow = rhTable(i-1,1);
        %     secondElemUpperRow = rhTable(i-2,1);
        %
        %     for j = 1:rhTableColumn - 1     
        %         %  compute each element of the table
        %         rhTable(i,j) = ((firstElemUpperRow * rhTable(i-2,j+1)) - ....
        %             (secondElemUpperRow * rhTable(i-1,j+1))) / firstElemUpperRow;
        %     end
 
    end


end

function cs = slowcharpoly(J) %#ok<DEFNU> %use for testing
    %Method for getting the charcteristic equation:
    %    Bernard P. (2006) The coefficients of the characteristic polynomial in terms of the
    %     eigenvalues and the elements of an n × n matrix. Applied Mathematics Letters 19 511–515        %very slow algorithm
    dim = size(J, 1);
    powers = (dim: - 1:0);
    cs = zeros(size(powers));
    for i = 1:dim
        for j = 1:length(powers)
            combs = nchoosek(1:dim, powers(j));
            if isempty(combs)
                cs(j) = det(J);
            else
                cs(j) = 0;
                for k = 1:size(combs, 1)
                    cs(j) = cs(j) + det_m_cols(J, combs(k, :));
                end
            end
        end
    end
    cs = cs * cs(powers == dim);
end
function res = det_m_cols(J, removecols)
    J(removecols, :) = 0;
    J(:, removecols) = 0;
    for i = 1:length(removecols)
        J(removecols(i), removecols(i)) = - 1;
    end
    res = det(J);
end

function p = fastcharpoly(A)
    % Fast algorithm for characteristic polynomial of square matrices.
    % Algortihm is described in
    % La Budde's Method For Computing Characteristic Polynomials
    % by Rizwana Rehman and Ilse C.F. Ipsen
    %
    % Input: square matrix A
    %
    % Output: charactersitic polynomial p of A
    %
    % Author: Sebastian J. Schlecht
    % Date: 11.04.2015

    %fastCharPoly - Fast algorithm for characteristic polynomial of square matrices.
    %Algortihm is described in
    %La Budde's Method For Computing Characteristic Polynomials
    %by Rizwana Rehman and Ilse C.F. Ipsen
    %
    % Syntax:  p = fastCharPoly( A )
    %
    % Inputs:
    %    A - square matrix complex or real
    %
    % Outputs:
    %    p - coefficients of characteristic polynomial
    %
    % Example:
    %    p = fastCharPoly( randn(4) )
    %    p = charpoly(magic(4)) - fastCharPoly(magic(4))
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % Author: Dr.-Ing. Sebastian Jiro Schlecht,
    % International Audio Laboratories, University of Erlangen-Nuremberg
    % email address: sebastian.schlecht@audiolabs-erlangen.de
    % Website: sebastianjiroschlecht.com
    % 10. December 2018; Last revision: 10. December 2018


    N = size(A, 1);
    H = hess(A);
    beta = diag(H(2:end, 1:end - 1));
    alpha = diag(H);

    pH = zeros(N + 1, N + 1);
    pH(0 + 1, 1) = 1;
    pH(1 + 1, 1:2) = [ - alpha(1), 1];

    for it = 2:N
        partB = flipud(cumprod(flipud(beta(1:it - 1))));
        partH = (H(1:it - 1, it));
        partP = pH(1:it - 1, :);
 
        rec = sum(bsxfun(@times, partB .* partH, partP), 1);
        convp = conv(pH(it - 1 + 1, :), [ - alpha(it), 1]);
 
        pH(it + 1, :) = convp(1:end - 1) - rec;
    end

    p = fliplr(pH(end, :));
end
