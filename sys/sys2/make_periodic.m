function [xnew, ynew] = make_periodic(x, y, xlimit, ylimit)
    %hase still a bug around zero
    if nargin < 3
        xlimit = [0 2*pi];
    end
    if nargin < 4
        ylimit = [0 2*pi];
    end
    if length(xlimit) == 1
        xlimit = [0 xlimit];
    end
    if length(ylimit) == 1
        ylimit = [0 ylimit];
    end
    x=x(:);
    y=y(:);
    if ~any(isnan(xlimit))
        xnew = x - floor((x - xlimit(1)) / (xlimit(2) - xlimit(1))) * (xlimit(2) - xlimit(1)) + xlimit(1);
        xrange=(xlimit(2) - xlimit(1));
    else
        xnew = x;
        xrange=0;
    end
    if ~any(isnan(ylimit))
        ynew = y - floor((y - ylimit(1)) / (ylimit(2) - ylimit(1))) * (ylimit(2) - ylimit(1)) + ylimit(1);
        yrange=(xlimit(2) - xlimit(1));
    else
        ynew = y;
        yrange=0;
    end
    change_base_y = diff(y - ynew) ~= 0;
    change_base_x = diff(x - xnew) ~= 0;
    anychange = find(change_base_y | change_base_x);
    for i = length(anychange): -1:1
        i1 = anychange(i);
        isinx = change_base_x(i1);
        isiny = change_base_y(i1);
        if isinx && isiny
            nextx=xnew(i1+1)+xrange;
            nexty=ynew(i1+1)+yrange;
            xinterp2=0;
            yinterp2=0;
            if nextx>xlimit(2)&&nexty>ylimit(2)
                xinterp = xnew(i1) + (x(i1 + 1) - x(i1)) / (y(i1 + 1) - y(i1)) * (ylimit(2)-ynew(i1));
                yinterp = ynew(i1) + (y(i1 + 1) - y(i1)) / (x(i1 + 1) - x(i1)) * (xlimit(2) - xnew(i1));
                if xinterp>xlimit(2)
                    xinterp = xnew(i1) + (x(i1 + 1) - x(i1)) / (y(i1 + 1) - y(i1)) * (yinterp-ynew(i1));
                    xinterp2 = yrange-yinterp;
                    yinterp2 = xrange-xinterp;
                end
                if yinterp>ylimit(2)
                    yinterp = ynew(i1) + (y(i1 + 1) - y(i1)) / (x(i1 + 1) - x(i1)) * (xinterp - xnew(i1));
                    xinterp2 = yrange-yinterp;
                    yinterp2 = xrange-xinterp;
                end
                xnew = [xnew(1:i1); xinterp; nan; xinterp2; xnew(i1 + 1:end)];
                ynew = [ynew(1:i1); yinterp; yinterp;yinterp2; ynew(i1 + 1:end)];
            end
        elseif isinx
            nextx=xnew(i1+1)+xrange;
            if nextx>xlimit(2)
                xinterp = xlimit(2);
                yinterp = ynew(i1) + (y(i1 + 1) - y(i1)) / (x(i1 + 1) - x(i1)) * (xinterp - xnew(i1));
                xnew = [xnew(1:i1); xinterp; nan; 0; xnew(i1 + 1:end)];
                ynew = [ynew(1:i1); yinterp; yinterp; yinterp; ynew(i1 + 1:end)];
            end
        else
            nexty=ynew(i1+1)+yrange;
            if nexty>ylimit(2)
                yinterp = ylimit(2);
                xinterp = xnew(i1) + (x(i1 + 1) - x(i1)) / (y(i1 + 1) - y(i1)) * (yinterp-ynew(i1));
                xnew = [xnew(1:i1); xinterp; nan; xinterp; xnew(i1 + 1:end)];
                ynew = [ynew(1:i1); yinterp; yinterp; 0; ynew(i1 + 1:end)];
            end
        end
    end
%        figure
%        plot(xnew,ynew,'-o')
%        xlim(xlimit)
%        ylim(ylimit)
end
