classdef symmodel < handle
    properties
        model = struct('pars', {{}}, 'parelems', {{}}, 'statevars', {{}}, 'model', {{}}, 'rhs' , {{}}, 'odehandle', {[]},'isimplicit', false, 'isdiffer', false, 'haslags', false, 'hastoolbox', false);
        oldengine = true;
        sympars = {};
        symstatevars = {};
        symrhs = [];
        symequilibria = {};
        Jacobian = sym_jacobian('Jacobian');
        Hessian = sym_jacobian('Hessian');
        der3 = sym_jacobian('der3');
        der4 = sym_jacobian('der4');
        der5 = sym_jacobian('der5');
        Jacobianp = sym_jacobian('Jacobianp');
        Hessianp = sym_jacobian('Hessianp');
        Sensitivp = sym_jacobian('Sensitivp');
        Jacobian_y = sym_jacobian('Jacobian_y');
        Jacobian_yp = sym_jacobian('Jacobian_yp');
        errormsg = struct('message', '', 'report', '');
    end
    properties (Hidden)
        jac_names = {'Jacobian', 'Hessian', 'der3', 'der4', 'der5', 'Jacobianp', 'Hessianp', 'Sensitivp', 'Jacobian_y', 'Jacobian_yp'};
    end
    methods (Static)
 
        function s = mycell2sym(eqn)
            %cell2sym is more recent than R2012, this function makes older versions
            %compatible
            try
                s = cell2sym(eqn);
            catch err
                %for older MATLAB versions we try to do the same thing here
                if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
                    s = mysym(zeros(size(eqn)));
                    for i = 1:numel(s)
                        %check if it is an equation in the cell
                        if ischar(eqn{i}) && isempty(regexp(eqn{i}, '[A-Za-z][A-Za-z0-9_]*$', 'once'))
                            s(i) = mysym(mystr2sym(eqn{i}));
                        else
                            %no equation
                            s(i) = mysym(eqn{i});
                        end
                    end
                else
                    rethrow(err);
                end
            end
        end
        function full = struct2full(jac, makesym)
            if nargin == 1
                makesym = false;
            end
            if isfield(jac, 'unique') || isa(jac, 'sym_jacobian')
                n = length(jac.unique);
            elseif isempty(jac)
                if makesym
                    full = mysym([]);
                else
                    full = {};
                end
                return;
            elseif iscell(jac)
                if makesym
                    full = mysym(zeros(size(jac)));
                    for i = 1:numel(full)
                        full(i) = mysym(mystr2sym(jac{i}));
                    end
                else
                    full = jac;
                end
                return;
            else
                n = 0;
            end
            if makesym
                full = mysym(zeros(jac.size));
                for i = 1:n
                    if ~isfield(jac.unique(i), 'sym')
                        full(jac.unique(i).indices) = mysym(mystr2sym(jac.unique(i).equation));
                    else
                        full(jac.unique(i).indices) = jac.unique(i).sym;
                    end
                end
            elseif isempty(jac.size)
                full={};
            else  
                full = cell(jac.size);
                full(:) = {'0'};
                for i = 1:n
                    full(jac.unique(i).indices) = {jac.unique(i).equation};
                end
            end
        end
    end
 
    methods
 
        function obj = symmodel(gg_grind)
            global g_grind;
            if nargin == 0
                gg_grind = g_grind;
            end
            pars=sprintf(',%s',g_grind.pars{:});
            odehandle=i_getodehandle(1,pars);
            obj.model = struct('pars', {gg_grind.pars}, 'parelems', {gg_grind.pars}, 'statevars', gg_grind.statevars, 'model', {gg_grind.model}, 'rhs', {{}}, 'odehandle', ...
                odehandle, 'isimplicit', gg_grind.solver.isimplicit, 'isdiffer', gg_grind.solver.isdiffer, 'haslags', gg_grind.solver.haslags, 'hastoolbox', i_hastoolbox('symbolic'));
        end
 
        function set.oldengine(obj, value)
            if value ~= obj.oldengine
                obj.oldengine = value;
                obj.reset;
            end
        end
 
        function s1 = save(obj)
            %save all jacobians in a condensed way as string. Running the
            %string brings is back
            function s = write_jac(jac)
                if ~isempty(jac)
                    if isempty(jac.unique)
                        s = sprintf('''%s'',{%s}', jac.name, mat2str(jac.size));
                        return;
                    end
                    jac = condensejac(jac);
                    s = sprintf('''%s'',{%s,...\n', jac.name, mat2str(jac.size));
                    for i = 1:length(jac.unique) - 1
                        s = sprintf('%s   %s,''%s'',...\n', s, mat2str(jac.unique(i).indices), jac.unique(i).equation);
                    end
                    s = sprintf('%s   %s,''%s''}', s, mat2str(jac.unique(end).indices), jac.unique(end).equation);
                else
                    s = '';
                end
            end
            c = {write_jac(obj.Jacobian), ...
                write_jac(obj.Hessian), ...
                write_jac(obj.der3), ...
                write_jac(obj.der4), ...
                write_jac(obj.der5), ...
                write_jac(obj.Jacobianp), ...
                write_jac(obj.Hessianp), ...
                write_jac(obj.Sensitivp)};
            f = cellfun('isempty', c);
            c = c(~f);
            if length(c) == 1
                s = sprintf( 'g_grind.enterjac(%s);\n', c{1});
            else
                s = sprintf('%s,...\n   ', c{1:end - 1});
                s = sprintf( 'g_grind.enterjac(%s%s);\n', s, c{end});
            end
            if nargout == 0
                disp(s);
            else
                s1 = s;
            end
        end
        %         
        %         function res = mysym(obj)
        %             if isempty(obj.Jacobian) %||obj.oldengine
        %                 %                 if obj.oldengine
        %                 %                     disp('setting to new engine');
        %                 %                 end
        %                 %                 obj.oldengine=false;
        %                 obj.update;
        %             end
        %             res = struct('pars', {obj.sympars}, ...
        %                 'statevars', obj.symstatevars, ...
        %                 'rhs', obj.symrhs, ...
        %                 'Jacobian', mysym(obj.Jacobian), ...
        %                 'Hessian', mysym(obj.Hessian), ...
        %                 'der3', mysym(obj.der3), ...
        %                 'der4', mysym(obj.der4), ...
        %                 'der5', mysym(obj.der5));
        %         end
        function [res, maxdif] = enterjac(obj, varargin)
            global g_grind
            fieldnams = {'jacobian', 'f#c#r', 'the whole Jacobian as cell of strings or function handle', ''; ...
                'jacobianp', 'f#c#r', 'the Jacobian of parameters (columns) as cell of strings or function handle.', ''; ...
                'hessian', 'f#c#r', 'the Hessian 3d matrix as cell of strings or function handle.', ''; ...
                'hessianp', 'f#c#r', 'the Hessian 3d matrix of parameters as cell of strings or function handle', ''; ...
                'sensitivp', 'f#c#r', 'the matrix of sensitivities to parameters', ''; ...
                'der3', 'f#c#r', 'the third order derivatives', ''; ...
                'der4', 'f#c#r', 'the fourth order derivatives', ''; ...
                'der5', 'f#c#r', 'the fifth order derivatives', ''; ...
                'jacobian_y', 'f#c#r', 'the Jacobian of an implicit model.', ''; ...
                'jacobian_yp', 'f#c#r', 'the Jacobian with respect to the derivatives of an implicit model.', ''; ...
                'maxtime', 'n', 'maximum time for deriving the derivatives (default NaN)', NaN; ...
                'maxorder', 'i>=0&i<=5', 'limit order of derivatives to maxorder (default=5)', 5; ...
                'rownr', 'i>0', 'row number for input', 1; ...
                'colnr', 'i>0', 'column number for input', 1; ...
                'jacelement', 's', 'single element of Jacobian', ''}';
            args = i_parseargs(fieldnams, ['if(hasoption(''-update'')),deffields=''implicit'';', ...
                'elseif(nargs==1&&iscell(args{1}))', ...
                'deffields=''jacobian'';', ....
                'else,deffields=''rownr,colnr,jacelement'';end;'], '-?jacp,-?Hessianp,-?hess,-?der3,-?der4,-?der5,-?jac,-?,-c,-update,-t,-s', varargin);
            if any(strncmp(args.opts, '-?', 2))
                obj.disp(varargin(:));
            end
            if any(isfield(args, 'jacelement'))
                obj.add_jac_element('Jacobian', [args.rownr, args.colnr], args.jacelement);
            end
            if any(strcmp(args.opts, '-update'))
                %update only the jacobian for implicit model
                g_grind.solver.opt.Jacobian = i_getodehandle('Jacobian');
            end
            if any(strcmp(args.opts, '-t'))
                [res1, maxdif1] = obj.test;
                if nargout > 0
                    res = res1;
                    maxdif = maxdif1;
                end
            end
            if any(strcmp(args.opts, '-c'))
                obj.reset;
            end
            if any(strcmp(args.opts, '-s'))
                obj.update;
                obj.disp;
            end
            obj.add_jac(args)
            if nargin == 0
                i_enterjacdlg; %dialog box where you can enter and view all equations
            end
        end
        
        function update_equilibria(obj)
            if ~obj.model.hastoolbox
                warning('grind:symmodel:notoolbox', 'No Symbolic Math toolbox found')
                return;
            end
            if isempty(obj.Jacobian)
                obj.update;
            end
            if isempty(obj.symequilibria)
                eqs = solve(obj.symrhs, obj.symstatevars, 'IgnoreAnalyticConstraints', true);
                if isempty(eqs)
                    warning('Searching for one equilibrium, as we could not find all solutions');
                    eqs = solve(obj.symrhs, obj.symstatevars, 'IgnoreAnalyticConstraints', true, 'PrincipalValue', true);
                end
                if length(obj.symstatevars) == 1
                    eqs = struct(obj.model.statevars.names{1}, eqs);
                end
                f = fieldnames(eqs);
                if ~isempty(f)
                    eqn = sym(zeros(size(f)));
                    obj.symequilibria = cell(1, length(eqs.(f{1})));
                    for i = 1:length(eqs.(f{1}))
                        for j = 1:length(f)
                            eqn(j) = simplify(eqs.(f{j})(i));
                        end
                        %Jac = analyse_stability('parent', genJac, 'equilibrium', eqn, 'title', sprintf('Equilibrium %d', i));
                        %Jac.summary
                        obj.symequilibria{i} = eqn;
                    end
                end
            end
        end
 
        function [res,htmlres] = analyse_stab(obj, varargin)
            options = struct(varargin{:});
            if ~isfield(options, 'maxlength')
                options.maxlength = 5000;
            end
            if ~isfield(options, 'equil')
                options.equil = [];
            end
            if ~isfield(options, 'simplify')
                options.simplify = false;
            end
            if ~isfield(options, 'general')
                options.general = false;
            end
            if isempty(obj.symequilibria) && ~options.general
                obj.update_equilibria;
            end
            c = {};
            if isempty(obj.symequilibria)
                if options.general
                    titl = 'General J';
                else
                    titl = 'General J (could not find analytic equilibria)';
                end
                anal = analyse_stability('sym_model', obj, 'title', titl);
                summ = anal.summary;
                if options.simplify
                    summ = analyse_stability.simplify(summ);
                end
                c = [c; summ.results.html];
                res1 = {summ};
            else

                res1 = cell(length(obj.symequilibria), 1);
                for i = 1:length(obj.symequilibria)
                    if isempty(options.equil) || options.equil == i
                        equil = mysym2str(obj.symequilibria{i});
                        if iscell(equil)
                            equil = sprintf('%s', equil{:});
                        end

                        if length(equil) < options.maxlength
                            anal = analyse_stability('sym_model', obj, 'equilibrium', obj.symequilibria{i}, 'title', sprintf('Equilibrium %d', i));
                            summ = anal.summary;
                            if options.simplify
                                summ = analyse_stability.simplify(summ);
                            end
                            c = [c; {sprintf('<a id="equil%d"></a>', i)}; summ.results.html];
                            res1{i} = summ;
                        end
                    end
                end
            end
            if length(obj.symequilibria) > 1
                mod=str2cell(evalc('obj.disp(''-?rhs'')'));
                mod=mod(2:end);
                mod=sprintf('<eq>%s</eq><br>',mod{:});
                summ = cell(length(obj.symequilibria) + 1, 1);
                summ{1} = sprintf('<h1>Stability analysis</h1>Model:<br>%s<br>%d equilibria found:', mod,length(obj.symequilibria));
                for i = 1:length(obj.symequilibria)
                    summ{i + 1} = sprintf('<a href="#equil%d">Equilibrium %d:<br></a><eq>%s</eq><br>', i, i, char(obj.symequilibria{i}));
                end
                c = [summ; c];
            end
            s = sprintf('%s<br>\n', c{:});
            s=sprintf('<html><title>Stability analysis</title><font face="Courier New" size=3> %s<br></font></form></html>', s);
            web(['text://', s]);
            if nargout > 0
                res = res1;
                htmlres=s;
            end
        end

        function add_jac_element(obj, field, subs, jacelement)
            %not recommended, inefficient for backward compatibility
            if isempty(obj.(field))
                jac = cell(jacsize(field, obj.model.statevars.dim, numel(obj.model.parelems)));
                jac(:) = {'0'};
            else
                jac = obj.struct2full(obj.(field));
            end
            subs = num2cell(subs);
            jac{sub2ind(size(jac), subs{:})} = jacelement;
            obj.(field).s = obj.full2struct(jac);
        end
 
        function add_jac(obj, args)
            %jacobians = {'Jacobian', 'Hessian', 'der3', 'der4', 'der5', 'Jacobianp', 'Hessianp', 'Sensitivp', 'Jacobian_y', 'Jacobian_yp'};
            fields = fieldnames(args);
            for i = 1:length(fields)
                f = find(strcmpi(fields{i}, obj.jac_names));
                if ~isempty(f)
                    name1 = obj.jac_names{f};
                    jacc = args.(fields{i});
                    if isempty(jacc)
                        obj.(name1).s = [];
                    elseif iscell(jacc) && isnumeric(jacc{1})
                        size1 = jacc{1};
                        jacc = jacc(2:end);
                        jac = struct('size', size1, 'ishandle', false, 'unique', struct('indices', cell(1, length(jacc) / 2), 'equation', ''), 'name', name1);
                        for k = 1:round(length(jacc) / 2)
                            j = k * 2 - 1;
                            jac.unique(k) = struct('indices', jacc{j}, 'equation', jacc{j + 1});
                        end
                        obj.(name1).s = jac;
                    elseif iscell(jacc)
                        %assume that it is a cell array of strings with
                        %equations
                        obj.(name1).s = obj.full2struct(jacc);
                    elseif isa(jacc, 'function_handle')
                        N0 = i_initvar;
                        jac = struct('size', jacsize(name1, obj.model.statevars.dim, numel(obj.model.parelems)), 'ishandle', true, 'handle', jacc, 'hasparameters', false, 'name', name1);
                        iserror = false;
                        try
                            jac1 = jac.handle(1, N0);
                        catch
                            try
                                jac.hasparameters = true;
                                s = sprintf('%s,', obj.model.pars{:});
                                s = ['{' s(1:end - 1) '}'];
                                parvalues = evalin('base', s);
                                jac1 = jac.handle(1, N0, parvalues{:});
                            catch
                                iserror = true;
                            end
                        end
                        if iserror
                            error('grind:enterjac:run', 'Cannot run handle of "%s"', name1);
                        end
                        if numel(size(jac1)) ~= numel(jac.size) || any(size(jac1) ~= jac.size)
                            error('grind:enterjac:size', 'Size of resulting "%s" of function is not correct', name1);
                        end
                        obj.(name1).s = jac;
                    elseif isstruct(jacc) && isfield(jacc, 'unique')
                        obj.(name1).s = jacc;
                    end
                end
            end
        end
 
        function [res, maxdif] = test(obj)
            %test the analytical Jacobian against the numerical one
            if isempty(obj.Jacobian)
                if i_hastoolbox('symbolic')
                    disp('Cannot test analytical Jacobian,  use <a href="matlab:enterjac(''-sym'')">enterjac -sym</a> to generate analytical Jacobian');
                else
                    disp('Cannot test analytical Jacobian, enter the Jacobian first with <a href="matlab:enterjac">enterjac</a> ');
                end
                if nargout == 0
                    res1 = false;
                    maxdif1 = nan;
                end
            elseif obj.model.isimplicit
                N0 = i_initvar;
                [~, ~] = i_implicit_Jac(0, N0, N0);
                res1 = true;
                maxdif1 = 0;
            else
                N0 = i_initvar;
                try
                    i_calcjac(true, 1, N0);
                    Jnum = i_calcjac(true, 1, N0); %run twice to use improved factors
                    jachan = i_getodehandle('Jacobian', [], obj.struct2full(obj.replace_statevars(obj.Jacobian)));
                    Janal = jachan(1, N0);
                    %Janal = i_calcjac(0, g_grind.solver.iters, N0);
                    maxdif1 = max(max(abs(Jnum - Janal)));
                    if nargout > 0
                        res1 = maxdif1 < 1E-4;
                    else
                        if maxdif1 < 1E-4
                            fprintf('OK: max difference numeric and analytical Jacobian = %g\n', maxdif1);
                        else
                            fprintf('Error: max difference numeric and analytical Jacobian = %g\n', maxdif1);
                        end
                    end
                catch err
                    disp('cannot run analytical Jacobian');
                    obj.disp;
                    rethrow(err);
                end
            end
            if nargout > 0
                res = res1;
                maxdif = maxdif1;
            end
          
        end

        function calc_rhs(obj)
            %       try
            if obj.model.hastoolbox
                symt = mysym('t');
                obj.sympars = cell(size(obj.model.pars));
                if ~obj.model.statevars.vector
                    obj.model.parelems = obj.model.pars;
                    for i = 1:length(obj.model.pars)
                        obj.sympars{i} = mysym(obj.model.pars{i});
                    end
                    obj.symstatevars = mysym('g_X1', [obj.model.statevars.dim, 1]);
                    for i = 1:obj.model.statevars.dim
                        %obj.statevars(i) = str2sym(sprintf('%s(%s)',
                        %g_grind.statevars.names{i}, dependentvar)); %needed
                        %for sensitivity analysis
                        obj.symstatevars(i) = mysym(obj.model.statevars.names{i});
                    end
                    if obj.model.isimplicit
                        symyp = mysym('g_X3', [obj.model.statevars.dim, 1]);
                        for i = 1:obj.model.statevars.dim
                            symyp(i) = mysym(sprintf('%s__p', obj.model.statevars.names{i}));
                        end
                    end
                    obj.model.parelems = obj.model.pars;
                else %vector notation
                    n = 0;
                    for i = 1:length(obj.model.pars)
                        siz = evalin('base', sprintf('size(%s)', obj.model.pars{i}));
                        n = n + prod(siz);
                        if prod(siz) == 1
                            obj.sympars{i} = mysym(obj.model.pars{i});
                        else
                            obj.sympars{i} = mysym([obj.model.pars{i} '__'], siz);
                        end
                    end
                    obj.model.parelems = cell(1, n);
                    k = 1;
                    for i = 1:length(obj.sympars)
                        p1 = cellfun(@char, mysym2cell(obj.sympars{i}), 'UniformOutput', false);
                        p1 = p1(:);
                        obj.model.parelems(k:k + numel(p1) - 1) = p1;
                        k = k + numel(p1);
                    end
                    obj.symstatevars = mysym('g_X1', [obj.model.statevars.dim, 1]);
                    for i = 1:length(obj.model.statevars.vectnames)
                        obj.symstatevars(obj.model.statevars.dims{i}.from:obj.model.statevars.dims{i}.to) = ...
                            mysym([obj.model.statevars.vectnames{i} '__'], [obj.model.statevars.dims{i}.dim1 obj.model.statevars.dims{i}.dim2]);
                    end
                end
                if obj.model.isimplicit
                    obj.symrhs = obj.model.odehandle(symt, obj.symstatevars, symyp, obj.sympars{:});
                else
                    obj.symrhs = obj.model.odehandle(symt, obj.symstatevars, obj.sympars{:});
                end
                obj.model.rhs = mysym2cell(obj.symrhs);
                for i = 1:numel(obj.model.rhs)
                    obj.model.rhs{i} = mysym2str(obj.model.rhs{i}, obj.model.statevars.vector);
                end
                %        catch err
                %            obj.errormsg = struct('message', err.message, 'report', err.getReport);
                %        end
            end
        end


        function add_to_g_grind(obj)
            %temporal method for testing
            global g_grind;
            [jac, statevars1, newvars] = obj.replace_statevars(obj.Jacobian);
            g_grind.syms.Jacobian = obj.struct2full(jac);
            g_grind.syms.Jacobianp = obj.struct2full(obj.replace_statevars(obj.Jacobianp, statevars1, newvars));
            g_grind.syms.Hessian = obj.replace_statevars(obj.Hessian, statevars1, newvars);
            g_grind.syms.Hessianp = obj.replace_statevars(obj.Hessianp, statevars1, newvars);
            g_grind.syms.der3 = obj.replace_statevars(obj.der3, statevars1, newvars);
            g_grind.syms.der4 = obj.replace_statevars(obj.der4, statevars1, newvars);
            g_grind.syms.der5 = obj.replace_statevars(obj.der5, statevars1, newvars);
        end

        function [eqn, statevars1, newvars] = replace_statevars(obj, eqn, statevars1, newvars)
            if isempty(eqn)
                return;
            end
            if nargin < 3
                if ~isfield(eqn, 'name') || ~strcmp(eqn.name, 'Sensitivp')
                    statevars1 = i_statevars_names;
                    if strcontains(statevars1{1}, '(')
                        statevars1 = regexprep(statevars1, '([/(/)])', '[/$1]');
                        statevars1 = regexp(sprintf('\\<%s|', statevars1{:}), '[|]', 'split');
                    else
                        statevars1 = regexp(sprintf('\\<%s\\>|', statevars1{:}), '[|]', 'split');
                    end
                    statevars1 = statevars1(1:end - 1);
                    newvars = regexp(sprintf('g_X1(%d,:)|', 1:numel(statevars1)), '[|]', 'split');
                    newvars = newvars(1:end - 1);
                else
                    %Sensitivp
                    statevars1 = i_statevars_names;
                    pars1 = [regexprep(obj.model.parelems, {'__([0-9]+)\>', '__([0-9]+)_([0-9]+)\>'}, {'($1)', '($1,$2)'}) statevars1];
                    %pars1 = [obj.model.parelems, statevars1];
                    tovars = repmat(statevars1', 1, length(pars1));
                    pars1 = repmat(pars1, length(statevars1), 1);
                    s = [tovars(:), pars1(:)]';
                    %all diff(var,par) combinations
                    sensitiv = regexp(sprintf('\\<diff[\\(]%s,[ ]*%s[\\)]|', s{:}), '[\|]', 'split');
                    sensitiv = sensitiv(1:end - 1);
                    if strcontains(statevars1{1}, '(')
                        statevars1 = regexprep(statevars1, '([/(/)])', '[/$1]');
                        statevars1 = regexp(sprintf('\\<%s|', statevars1{:}), '[|]', 'split');
                    else
                        statevars1 = regexp(sprintf('\\<%s\\>|', statevars1{:}), '[|]', 'split');
                    end
                    statevars1 = [statevars1(1:end - 1), sensitiv];
                    newvars = regexp(sprintf('g_X1(%d,:)|', 1:numel(statevars1)), '[|]', 'split');
                    newvars = newvars(1:end - 1);
                    %the sensitivities needs to be replaced first,
                    %therefore we flip the order
                    statevars1 = fliplr(statevars1);
                    newvars = fliplr(newvars);
                end
            end
            if ~isstruct(eqn)
                %replace statevar symbols with g_X1(i)
                for i = 1:length(statevars1)
                    eqn = regexprep(eqn, statevars1{i}, newvars{i});
                end
                return;
            end
            eqn1 = {eqn.unique(:).equation};
            eqn1 = regexprep(eqn1, statevars1, newvars);
            [eqn.unique(:).equation] = eqn1{:};
        end

        function disp(obj, jac)
            if nargin == 1
                jac = '';
            end
            objVar = 'g_grind.sym.enterjac'; %inputname(1);
            if ~isempty(obj.errormsg.message)
                if strcmp(jac, '-?error')
                    disp(obj.errormsg.report);
                else
                    fprintf('Translating the model to symbolic notation failed\n%s <a href="matlab:%s.disp(''-?error'')">details</a>\n', obj.errormsg.message, objVar);
                end
                return;
            end
            if isempty(jac) || strcmp(jac, '-?')
                if ~isempty(obj.model.rhs)
                    fprintf('Symbolic model              <a href="matlab:%s(''-?rhs'')">%s</a>\n', objVar, mat2str(size(obj.model.rhs)));
                    %                 else
                    %                     disp('Model has been reset');
                end
                if ~isempty(obj.Jacobian)
                    if obj.model.isimplicit
                        fprintf('Symbolic Jacobian defined   <a href="matlab:%s(''-?jac'')">3x%s</a>\n', objVar, mat2str(obj.Jacobian.size));
                    else
                        fprintf('Symbolic Jacobian defined   <a href="matlab:%s(''-?jac'')">%s</a>\n', objVar, mat2str(obj.Jacobian.size));
                    end
                else
                    disp('No symbolic Jacobian defined')
                end
                if ~isempty(obj.Hessian)
                    fprintf('Symbolic Hessian defined    <a href="matlab:%s(''-?hess'')">%s</a>\n', objVar, mat2str(obj.Hessian.size));
                else
                    disp('No symbolic Hessian defined')
                end
                if ~isempty(obj.der3)
                    fprintf('Symbolic der3 defined       <a href="matlab:%s(''-?der3'')">%s</a>\n', objVar, mat2str(obj.der3.size));
                else
                    disp('No symbolic der3 defined')
                end
                if ~isempty(obj.der4)
                    fprintf('Symbolic der4 defined       <a href="matlab:%s(''-?der4'')">%s</a>\n', objVar, mat2str(obj.der4.size));
                else
                    disp('No symbolic der4 defined')
                end
                if ~isempty(obj.der5)
                    fprintf('Symbolic der5 defined       <a href="matlab:%s(''-?der5'')">%s</a>\n', objVar, mat2str(obj.der5.size));
                else
                    disp('No symbolic der5 defined')
                end
                if ~isempty(obj.Jacobianp)
                    fprintf('Symbolic Jacobianp defined  <a href="matlab:%s(''-?jacp'')">%s</a>\n', objVar, mat2str(obj.Jacobianp.size));
                else
                    disp('No symbolic Jacobianp defined')
                end
                if ~isempty(obj.Hessianp)
                    fprintf('Symbolic Hessianp defined   <a href="matlab:%s(''-?hessianp'')">%s</a>\n', objVar, mat2str(obj.Hessianp.size));
                else
                    disp('No symbolic Hessianp defined')
                end
                if ~isempty(obj.Sensitivp)
                    fprintf('Symbolic Sensitivp defined  <a href="matlab:%s(''-?sens'')">%s</a>\n', objVar, mat2str(obj.Sensitivp.size));
                end
            elseif strcmpi(jac, '-?rhs')
                if obj.model.isimplicit
                    plotjac('Equations', obj.model.rhs, {'0'} , {''});
                else
                    plotjac('Equations', obj.model.rhs, i_statevars_names , {'t'});
                end
            elseif strcmpi(jac, '-?jac')
                vars = i_statevars_names;
                vars2 = vars;
                for i = 1:length(vars)
                    vars2{i} = ['f_' vars{i}];
                end
                obj.Jacobian.dx = vars2;
                obj.Jacobian.dy = vars;
                %plotjac('Jacobian', obj.Jacobian, vars2 , vars );
                obj.Jacobian.disp;
                if obj.model.isimplicit
                    obj.Jacobian_y.disp;
                    obj.Jacobian_yp.disp;
                    %plotjac('Jacobian_y', obj.Jacobian_y, [] , []);
                    %plotjac('Jacobian_yp', obj.Jacobian_yp, [] , []);
                end
            elseif strcmpi(jac, '-?hess')
                obj.Hessian.disp;
            elseif strcmpi(jac, '-?der3')
                obj.der3.disp;
            elseif strcmpi(jac, '-?der4')
                obj.der4.disp;
            elseif strcmpi(jac, '-?der5')
                obj.der5.disp;
            elseif strcmpi(jac, '-?sens')
 
                jacstr = regexprep({obj.Sensitivp.unique.equation}, '\<diff\(([A-Za-z][A-Za-z0-9_]*([\(][0-9,]*[\)])?)[,][ ]?([A-Za-z][A-Za-z0-9_]*([\(][0-9,]*[\)])?)\)', 'Sens__$1_$2');
                %
                %                jacstr = regexprep({obj.Sensitivp.unique.equation}, '\<diff[\(]([A-Za-z0-9_]*)[, ]*([A-Za-z0-9_]*)[\)]', 'Sens_$1_$2');
                jac = obj.Sensitivp;
                [jac.unique.equation] = jacstr{:};
                vars = i_statevars_names;
                fvars = vars;
                for i = 1:length(vars)
                    fvars{i} = ['f_' vars{i}];
                end
 
                pars = [regexprep(obj.model.parelems, {'__([0-9]+)\>', '__([0-9]+)_([0-9]+)\>'}, {'($1)', '($1,$2)'}) i_statevars_names];
                obj.Sensitivp.dx = fvars;
                obj.Sensitivp.dy = pars;
                obj.Sensitivp.disp;
                %plotjac('Sensitivp', jac, fvars , pars);
            elseif strcmpi(jac, '-?jacp')
                if isempty(obj.Jacobianp.dx)
                    vars = i_statevars_names;
                    for i = 1:length(vars)
                        vars{i} = ['f_' vars{i}];
                    end
                    pars = regexprep(obj.model.parelems, {'__([0-9]+)\>', '__([0-9]+)_([0-9]+)\>'}, {'($1)', '($1,$2)'});
                    obj.Jacobianp.dx = vars;
                    obj.Jacobianp.dy = pars;
                end
                obj.Jacobianp.disp;
                %                plotjac('Jacobianp', obj.Jacobianp, vars , pars);
            elseif strcmpi(jac, '-?hessianp')
                obj.Hessianp.disp;
                %plotjac('Hessianp', obj.Hessianp, [], []);
            end
        end

        function jac = full2struct(obj, symjac1)
            if isempty(symjac1)
                jac = symjac1;
                return;
            end
            symzero = mysym('0');
            jac = struct('size', size(symjac1), 'ishandle', false);
            jac.unique(numel(symjac1)) = struct('equation', {''}, 'sym', {''}, 'indices', {[]});
            selected = true(size(jac.unique));
            indx = cell(size(jac.size));
            k = 1;
            for j = 1:numel(symjac1)
                [indx{:}] = ind2sub(jac.size, j);
                if (isa(symjac1, 'sym') && ~isequal(symjac1(j), symzero)) || ...
                        (isa(symjac1, 'cell') && ~any(strcmp(symjac1{j}, {'0', '0.0'})))
                    rhs_no = indx{1};
                    diffvars = sort([indx{2:end}]);
                    for n = 1:k - 1
                        [jac_rhs_no, jac_diffvars] = getdiffvars(jac.size, jac.unique(n).indices);
                        if all(jac_diffvars == diffvars) && jac_rhs_no == rhs_no
                            selected(j) = false;
                        end
                    end
                    if selected(j)
                        if length(unique(diffvars)) == 1 %shortcut if all diffvars are the same
                            subs = num2cell(diffvars);
                        else
                            subs = num2cell(unique(perms(diffvars), 'rows'), 1);
                        end
                        inds = sub2ind(jac.size, zeros(size(subs{1})) + rhs_no, subs{:});
                        if isa(symjac1, 'sym')
                            jac.unique(k) = struct('equation', mysym2str(symjac1(j), obj.model.statevars.vector), 'sym', symjac1(j), 'indices', inds);
                        else
                            jac.unique(k) = struct('equation', mysym2str(symjac1{j}, obj.model.statevars.vector), 'sym', symjac1{j}, 'indices', inds);
                        end
                        k = k + 1;
                    end
                else
                    selected(j) = false;
                end
            end
            jac.unique = jac.unique(1:k - 1);
        end

        function reset(obj)
            obj.sympars = {};
            obj.symstatevars = {};
            obj.symrhs = [];
            obj.symequilibria = {};
            obj.model.rhs = {};
            obj.model.parelems = {};
            for i = 1:length(obj.jac_names)
                obj.(obj.jac_names{i}) = obj.(obj.jac_names{i}).reset;
            end
            obj.errormsg = struct('message', '', 'report', '');
        end

        function update_sens(obj)
            if ~obj.model.hastoolbox
                warning('grind:symmodel:notoolbox', 'No Symbolic Math toolbox found')
                return;
            end
            if isempty(obj.Sensitivp)
                %   if obj.oldengine || ~exist('str2sym', 'file') && ~obj.model.statevars.vector
                if obj.oldengine && ~obj.model.statevars.vector
                    %new version of sensitivity needs newer MATLAB version
                    i_update_sym(obj, true, 300, 1);
                    %obj.Sensitivp.s = obj.full2struct(obj.Sensitivp);
                    jacstr = regexprep({obj.Sensitivp.unique.equation}, '\<SENS_[\(]', 'diff(');
                    [obj.Sensitivp.unique.equation] = jacstr{:};
                    obj.Sensitivp.name = 'Sensitivp';
                    if isempty(obj.sympars)
                        obj.sympars = obj.model.pars;
                        for k = 1:length(obj.sympars)
                            obj.sympars{k} = cell2sym(obj.sympars{k});
                    
                        end
                    end
                    if isempty(obj.symstatevars)
                        obj.symstatevars = mysym(symmodel.mycell2sym(obj.model.statevars.names));
                    end
                    if isempty(obj.symrhs)
                        obj.symrhs = symmodel.struct2full(obj.model.rhs, true);
                    end
                    return;
                end
                %  if ~exist('str2sym', 'file')
                %      error('symmodel:oldversion', 'For this functionality a newer MATLAB version (F>R2017a) is required')
                %   end
                if isempty(obj.symrhs)
                    calc_rhs(obj);
                end
                rhs1 = obj.symrhs;
                vars = obj.symstatevars;
                vars0 = cellfun(@char, mysym2cell(vars), 'UniformOutput', false)';

                for i = 1:length(vars)
                    vars0{i} = [vars0{i} '__0'];
                end
                charpars = [obj.model.parelems vars0];
                allpars = cellfun(@mysym, charpars);
                f = strcmp(charpars, 'i'); %we need to replace i with sym('i') to prevent interpretation as complex number
                if any(f)
                    charpars(f) = {'sym(''i'')'};
                end
                spars = sprintf('%s,', charpars{:});
                spars = sprintf('(%s)', spars(1:end - 1));
                symstatevars2 = obj.symstatevars;
                for i = 1:length(vars)
                    symstatevars2(i) = mystr2sym([char(vars(i)) spars]);
                end
                rhs1 = subs(rhs1, obj.symstatevars, symstatevars2);
                jac = jacobian(rhs1, allpars);
                jac = obj.full2struct(jac);
                %some preprocessing of the str equations (remove long
                %depencies for instance)
                jacstr = cell(size(jac.unique));
                for i = 1:length(jacstr)
                    jacstr{i} = char(jac.unique(i).sym);
                end
                rstr = mysym2cell(vars);
                vars1 = cell(size(vars));
                for i = 1:length(vars)
                    vars1{i} = char(vars(i));
                    rstr{i} = sprintf('%s(%s,[^\\)]*[\\)]', char(vars(i)), char(allpars(1)));
                end
                jacstr = regexprep(jacstr, rstr, vars1);
                jacstr = regexprep(jacstr, {'__0\>', ', '}, {'', ','});
                if obj.model.statevars.vector
                    jacstr = regexprep(jacstr, {'__([0-9]+)\>', '__([0-9]+)_([0-9]+)\>'}, {'($1)', '($1,$2)'});
                end
                [jac.unique.equation] = jacstr{:};
                %to replace diff(X,P) with Sens_X_P:
                %jacstr = regexprep(jacstr, '\<diff[\(]([A-Za-z0-9_]*)[, ]*([A-Za-z0-9_]*)[\)]', 'Sens__$1_$2');
                obj.Sensitivp.s = jac;
                obj.Sensitivp.name = 'Sensitivp';
            end
        end

        function update(obj, maxtime, maxorder)
            function jac = first_jacobian(name, rhs1, statevars1)
                if isempty(rhs1) || isempty(statevars1) || maxtimereached()
                    jac = [];
                    return;
                end
                jac = obj.full2struct(jacobian(rhs1, statevars1));
                jac.name = name;
            end
            function jac = add_jaclevel(name, prevjac, diffvars, dopermute)
                if nargin < 4
                    dopermute = true;
                    %for Hessianp permutations are not possible as it works
                    %on the normal Jacobian
                end
                if isempty(prevjac) || maxtimereached() || prevjac.ishandle
                    jac = [];
                    return;
                end
                newsize = [prevjac.size numel(diffvars)];
                jac = struct('size', newsize, 'ishandle', false, 'unique', struct('equation', {}, 'sym', {}, 'indices', []), 'name', name);
                for i1 = 1:length(prevjac.unique)
                    [prevrhs_nos, prevdiffvarss] = getdiffvars(prevjac.size, prevjac.unique(i1).indices);
                    for k = 1:length(prevrhs_nos)
                        prevrhs_no = prevrhs_nos(k);
                        prevdiffvars = prevdiffvarss(k, :);
                        for j1 = max(prevdiffvars):numel(diffvars)
                            if isfield(prevjac.unique(i1), 'sym')
                                if ischar(prevjac.unique(i1).sym)
                                    prevjac.unique(i1).sym = mystr2sym(prevjac.unique(i1).sym);
                                end
                                newequation = diff(prevjac.unique(i1).sym, diffvars(j1));
                            else
                                newequation = diff(mystr2sym(prevjac.unique(i1).equation), diffvars(j1));
                            end
                            if maxtimereached()
                                jac = [];
                                return;
                            end
                            %fprintf('jac(%d,%s) = %s\n', prevjac.unique(i1).rhs_no, num2str( [prevjac.unique(i1).diffvars, j1]), char(newequation));
                            if ~isequal(newequation, symzero)
                                newdiffvars = [prevdiffvars, j1];
                                if ~dopermute || length(unique(newdiffvars)) == 1 %shortcut if all diffvars are the same
                                    subs = num2cell(newdiffvars);
                                else
                                    subs = num2cell(unique(perms(newdiffvars), 'rows'), 1);
                                end
                                inds = sub2ind(newsize, zeros(size(subs{1})) + prevrhs_no, subs{:});
                                jac.unique(length(jac.unique) + 1) = struct('equation', ...
                                    mysym2str(newequation, obj.model.statevars.vector), 'sym', newequation, 'indices', inds);
                            end
                        end
                    end
                end
                %
                %jac = condensejac(jac);
                %end
            end
            function res = maxtimereached
                res = ~isnan(maxtime) && toc(tstart) > maxtime;
            end
            if nargin < 2 || isempty(maxtime) || isinf(maxtime)
                maxtime = inf;
            end
            if ~obj.model.hastoolbox
                warning('grind:symmodel:notoolbox', 'No Symbolic Math toolbox found')
                return;
            end
            if nargin < 3 || isempty(maxorder)
                %default values for the max order of the derivatives
                %der3-der5 get slow with increasing dimensiona
                %more than exponential increasing
                if obj.model.statevars.dim <= 5
                    maxorder = 5; %der5
                elseif obj.model.statevars.dim <= 7
                    maxorder = 4; %der4
                elseif obj.model.statevars.dim <= 10
                    maxorder = 3; %der3
                elseif obj.model.statevars.dim <= 18
                    maxorder = 2;
                else
                    maxorder = 1;
                end
            end
            %shortcut if all fields are nonempty
            if ~isempty(obj.symrhs) && ~isempty(obj.Jacobian) && ~isempty(obj.Hessian)...
                    && ~isempty(obj.der3) && ~isempty(obj.der4) && ~isempty(obj.der5) && ...
                    ~isempty(obj.Jacobianp) && ~isempty(obj.Hessianp)
                return;
            end
            if obj.oldengine && ~obj.model.statevars.vector
 
                i_update_sym(obj, false, maxtime, maxorder);
                %                 if isempty(obj.sympars)
                %                     obj.sympars = symmodel.mycell2sym(obj.model.pars);
                %                 end
                %                 if isempty(obj.symstatevars)
                %                     obj.symstatevars = mysym(symmodel.mycell2sym(obj.model.statevars.names));
                %                 end
                %                 if isempty(obj.symrhs)
                %                     obj.symrhs = symmodel.struct2full(obj.model.rhs, true);
                %                 end
                return;
            end
            tstart = tic;
            if isempty(obj.symrhs)
                obj.calc_rhs;
            end
            %check if sizes of parameters have changed
            if obj.model.statevars.vector
                changed = false;
                for i = 1:length(obj.model.pars)
                    siz = evalin('base', sprintf('size(%s)', obj.model.pars{i}));
                    if any(siz ~= size(obj.sympars{i}))
                        changed = true;
                    end
                end
                %reset the model if any parameters have changed
                if changed
                    obj.reset;
                end
            end
            if isempty(obj.symrhs) && isempty(obj.errormsg.message) && ~maxtimereached
                obj.calc_rhs;
            end
            statevars1 = obj.symstatevars;
            symzero = mysym('0');
            if isempty(obj.errormsg.message)
                if obj.model.isimplicit
                    if isempty(obj.Jacobian_y) && maxorder > 0
                        obj.Jacobian_y.s = first_jacobian('Jacobian_y', obj.symrhs, statevars1);
                    end
                    symyp = mysym('g_X3', [obj.model.statevars.dim, 1]);
                    for i = 1:obj.model.statevars.dim
                        symyp(i) = mysym(sprintf('%s__p', obj.model.statevars.names{i}));
                    end
                    if isempty(obj.Jacobian_yp) && maxorder > 0
                        obj.Jacobian_yp.s = first_jacobian('Jacobian_yp', obj.symrhs, symyp);
                    end
                    %Jacobian we can use the general formula for implicit differentiation of a
                    %function R(x,y)=0
                    %
                    %dy/dx= -(dR/dx)/(dR/dy)
                    %comes from the generalized chain rule (multidimensional)
                    %
                    % 0 = R(x,y)
                    % d 0/dx = d(R(x,y)/dx
                    % 0 = d(R(x,y(x))/dx   %generalized chain rule
                    % 0 = dR/dy*dy(x)/dx + dR/dx*dx/dx
                    % 0 = dR/dy*dy(x)/dx + dR/dx*1 %solve
                    % dy(x)/dx=-(dR/dx)/(dR/dy)
                    %
                    %Another method is to calculate the implicit differntial and solve unknowns:
                    %
                    % mupad:
                    % solve(diff(R(y,yp(y)),y)=0,diff(yp(y),y))
                    %
                    % -D([1], R)(y, yp(y))/D([2], R)(y, yp(y))
                    % complex way of writing:
                    % -(dR/dy)/(dR/dyp)
                    %
                    if isempty(obj.Jacobian) && ~isempty(obj.Jacobian_yp) && ~isempty(obj.Jacobian_y)
                        jacy = obj.struct2full(obj.Jacobian_y, true);
                        jacyp = obj.struct2full(obj.Jacobian_yp, true);
                        %diagonal only!
                        jac = jacy;
                        for i = 1:size(jacy, 1)
                            for j = 1:size(jacy, 1)
                                %note that we need to take the diagonal of Jac_yp [dx/dx' dy/dx';dxd/dy' dy/dy']
                                jac(i, j) = -jacy(i, j) / jacyp(i, i);
                            end
                        end
                        obj.Jacobian.s = obj.full2struct(jac);
                    end

                else
                    if isempty(obj.Jacobian) && maxorder > 0
                        obj.Jacobian.s = first_jacobian('Jacobian', obj.symrhs, statevars1);
                    end
                    if isempty(obj.Hessian) && maxorder > 1
                        obj.Hessian.s = add_jaclevel('Hessian', obj.Jacobian, statevars1);
                    end
                    if isempty(obj.der3) && maxorder > 2
                        obj.der3.s = add_jaclevel('der3', obj.Hessian, statevars1);
                    end
                    if isempty(obj.der4) && maxorder > 3
                        obj.der4.s = add_jaclevel('der4', obj.der3, statevars1);
                    end
                    if isempty(obj.der5) && maxorder > 4
                        obj.der5.s = add_jaclevel('der5', obj.der4, statevars1);
                    end
                    if isempty(obj.Jacobianp) || isempty(obj.Hessianp)
                        pars1 = symmodel.mycell2sym(obj.model.parelems);
                    end
                    if isempty(obj.Jacobianp) && maxorder > 0
                        obj.Jacobianp.s = first_jacobian('Jacobianp', obj.symrhs, pars1);
                    end
                    if isempty(obj.Hessianp) && maxorder > 1
                        obj.Hessianp.s = add_jaclevel('Hessianp', obj.Jacobian, pars1, false);
                    end
                end
                if maxtimereached()
                    disp('Maximum computation time reached');
                end
            else
                objVar = inputname(1);
                fprintf('Cannot calculate analytic Jacobians\n%s <a href="matlab:%s.disp(''-?error'')">details</a>\n', obj.errormsg.message, objVar);
            end
        end
    end
end



function [rhs_no, diffvars] = getdiffvars(siz, indices)
    %first index is the rhs_no
    %other indices indicate the variables for differentiation (order is
    %not important (except for Hessianp where two kind of variables are used))
    a = cell(size(siz));
    [a{:}] = ind2sub(siz, indices);
    usubs = unique([a{1} sort([a{2:end}], 2)], 'rows');
    rhs_no = usubs(:, 1);
    diffvars = usubs(:, 2:end);
end

function siz = jacsize(field, dim, npar)
    switch field
        case {'Jacobian', 'Jacobian_y', 'Jacobian_yp'}
            siz = [dim, dim];
        case 'Hessian'
            siz = [dim, dim, dim];
        case 'der3'
            siz = [dim, dim, dim, dim];
        case 'der4'
            siz = [dim, dim, dim, dim, dim];
        case 'der5'
            siz = [dim, dim, dim, dim, dim, dim];
        case 'Jacobianp'
            siz = [dim, npar];
        case 'Hessianp'
            siz = [dim, dim, npar];
        case 'Sensitivp'
            siz = [dim, npar + dim];
    end
end

function c = mysym2cell(symeqn)
    try
        c = sym2cell(symeqn);
    catch err
        if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
            c = cell(size(symeqn));
            for i = 1:numel(c)
                c{i} = symeqn(i);
            end
        else
            rethrow(err);
        end
    end
end

function jac = condensejac(jac)
    %extra condensation if there are coincidally double elements
    if ~isempty(jac)
        eqn = {jac.unique.equation};
        [ueqn, ndx1, ndx2] = unique(eqn);
        if length(eqn) > length(ueqn)
            jac1 = jac.s;
            jac.unique = struct('equation', {}, 'sym', {}, 'indices', {});
            jac.unique(length(ndx1)).equation = '';
            for i = 1:length(ndx1)
                jac.unique(i) = jac1.unique(ndx1(i));
                ind = [jac1.unique(ndx2 == ndx2(ndx1(i))).indices];
                jac.unique(i).indices = ind';
            end
        end
    end
end



function eqn = mysym2str(symeqn, isvector)
    if nargin == 1
        isvector = false;
    end
    if numel(symeqn) > 1 && ~ischar(symeqn)
        eqn = cell(size(symeqn));
        for i = 1:numel(eqn)
            if iscell(symeqn)
                eqn{i} = mysym2str(symeqn{i}, isvector);
            else
                eqn{i} = mysym2str(symeqn(i), isvector);
            end
        end
        return;
    end
    eqn = char(symeqn);
    %vector models replace A__1_3 with A(1,3)
    if isvector
        eqn = regexprep(eqn, {'__([0-9]+)\>', '__([0-9]+)_([0-9]+)\>'}, {'($1)', '($1,$2)'});
    end
    %eqn = regexprep(eqn, '__p\>', '''');
    %remove piecewise
    f = regexp(eqn, '\<piecewise\>', 'once');
    if ~isempty(f)
        %replace piecewise with iif (slow)
        % if ~isempty(regexp(s,'\<piecewise\>', 'once'))
        eq = parsed_equation(eqn);
        f = find(strcmp(eq.fs, 'piecewise'), 1);
        while any(f)
            %get the arguments of piecewise (nested possible)
            [args, js] = eq.getfunctionpars(f);
            %args = regexprep(args, '[\[\]]', '');
            %uneven arguments are conditions, even are values (except
            %the last)
            conds = false(size(args));
            conds(1:2:end - 1) = true;
            %if there are 4 arguments often the second condition is the
            %opposite of the first, for efficiency can be removed.
            if numel(args) == 4
                if any(strcmp(args{3}, {'true', 'symtrue'})) || isequal(mystr2sym(args{1}), ~mystr2sym(args{3}))
                    args(3) = [];
                    conds(3) = [];
                end
            end
            if conds(end - 1)
                args{end + 1} = 'NaN';
                conds(end + 1) = false;
            end
            for i = 1:length(args)
                if conds(i)
                    args{i} = ['iif(' args{i}];
                end
            end
            thefunct = sprintf('%s,', args{:});
            thefunct = sprintf('%s%s', thefunct(1:end - 1), repmat(')', 1, sum(conds)));
            s = sprintf('%s%s%s', sprintf('%s', eq.fs{1:js(1, 1) - 3}), thefunct, sprintf('%s', eq.fs{js(end, 2) + 2:end}));
            eq = parsed_equation(s);
            f = find(strcmp(eq.fs, 'piecewise'), 1);
        end
        eqn = char(eq);
    end
end
