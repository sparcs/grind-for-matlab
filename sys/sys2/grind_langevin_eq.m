%FOKKERPLANCK   Analyse Fokker-Planck equation based on 1D differential equation
%   By using a Fokker-Planck equation you can analyse the time-evolution of 
%   the probability density function of a stochastic differential equation. 
%   This command only works for one-equation differential equations.
%   Assume the following stochastic differential equation:
%   dX = F(X) dt + sigma dW
%   where X is a state variable, F(X) is the deterministic part and dW is the 
%   Wiener process (Gaussian white noise) times the function sigma.
%   The Fokker-Planck PDE is then defined as:
%   dP/dt=- d(F(X) P)/dX + sigma^2/2*dP^2/d^2X
%   P is the probability density function.
%   We solve this PDE using MATLAB's pdepe function with absorbing or reflecting
%   boundaries. Use simtime to define the simulation time, the initial 
%   condition of the original model is used(g_fokkerplanck.p0 can be used 
%   to define other than default initial conditions)
%      
%   Usage:
%   FOKKERPLANCK - opens a dialog box in which you can enter all necessary 
%   information.
%   FOKKERPLANCK SIGMA - sigma is defined, for the rest use default values.
%   FOKKERPLANCK('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'bl' [absorbing | reflecting] - kind of left boundary (default reflecting)
%     'br' [absorbing | reflecting] - kind of right boundary (default reflecting)
%     'diffsigma' [number] - derivative of sigma to x (leave blanc for numerical solving) (default '')
%     'lim' [number and length(number)==2] - the limits of the statevariable (default defined in ax or [0 10])
%     'npoints' [integer>0] - the number of points for the probability density function (default=300)
%     'sigma' [number] - the standard deviation of the noise (default 1)
%   FOKKERPLANCK('-opt1','-opt2',...) - Valid command line <a href="matlab:commands func_args">options</a>:
%     '-p' - plot: redraw the figures of the last run.
%
%  
%   See also potential, plotdiff, <a href="matlab:help pdepe">pdepe</a>  
%
%   Reference page in Help browser:
%      <a href="matlab:commands('fokkerplanck')">commands fokkerplanck</a>

%   Copyright 2020 WUR
%   Revision: 1.2.1 $ $Date: 03-Jul-2020 15:21:48 $
classdef grind_langevin_eq < langevin_eq
    properties
        maxsurvtime = 200;
        sigma = '1';
        diffsigma = '';
    end
    methods
        function obj = grind_langevin_eq()
            hfun = i_getodehandle(1, '');
            obj.namex = i_statevars_names(1);
            d1 = @(x)hfun(0,x);
            obj.set('D1', d1);
        end
      
        function set(obj, varargin)
            if nargin > 1
                if ~isstruct(varargin{1})
                    args = struct(varargin{:});
                else
                    args = varargin{1};
                end
            end
            if isfield(args, 'sigma')
                obj.sigma = args.sigma;
             %   g=evalin('base',sprintf('@(%s)%s',obj.namex,obj.sigma));
                set@langevin_eq(obj, 'g', args.sigma);
                obj.DD2=[];
            end
            if isfield(args, 'diffsigma') %needs to be changed after sigma
                obj.diffsigma=args.diffsigma;
                if ~isempty(args.diffsigma)
                    obj.set('DD2',sprintf('(%s).*(%s)',obj.sigma,obj.diffsigma));
                end
            end
            if isfield(args, 'maxsurvtime')
                obj.maxsurvtime = args.maxsurvtime;
            end
            set@langevin_eq(obj, args);
        end
     
        function update(obj)
            %update all calculations using current GRIND settings
            %run separate commands for non-default runs
            global g_grind;
            obj.equilibria = obj.find_equilibria('deterministic'); %can be changed to 'effective' if you use ueff

            if isnan(g_grind.tstep)
                tstep=1000;
               % timespan = linspace(0, g_grind.ndays, 1000);
            else
                tstep=g_grind.tstep;
               % timespan = linspace(0, g_grind.ndays, g_grind.tstep);
            end
            obj.pdf(i_initvar);
            obj.runpdf(i_initvar, 'ntime', tstep, 'maxtime',g_grind.ndays);
            obj.mean_exit('all', obj.xtra.results.pdf);
            obj.survival('all', obj.maxsurvtime);
        end
        
  
        function [hax, res] = plot(obj, varargin)
            if nargin > 1
                 [hax1, res] = plot@langevin_eq(obj, varargin{:});
                  hax = hax1;
            else %default plots
                if isempty(obj.xtra) || ~(isfield(obj.xtra, 'results')&&isfield(obj.xtra.results,'pdf'))
                    obj.update;
                end
                hfig = i_makefig('fokkerplanck1');
                set(hfig, 'Name', 'Fokker-Planck pdf')
                %figure
                obj.plot(obj.xtra.results.pdfs, 't', 0);
                i_plotdefaults(hfig);
                canaddreplay = addreplaydata(get(hfig, 'children'));
                if canaddreplay
                    replayall;
                end
                hfig = i_makefig('fokkerplanck2');
                obj.plot('basinprob', obj.xtra.results.pdfs);

                set(hfig, 'Name', 'Fokker-Planck time plot')
                i_plotdefaults(hfig);
                if canaddreplay
                    replayall('-plot', 't');
                end
                hfig = i_makefig('fokkerplanck3');
                obj.plot(obj.xtra.results.mean_exit);
                i_plotdefaults(hfig);
                hfig = i_makefig('fokkerplanck4');
                obj.plot(obj.xtra.results.survival);
                i_plotdefaults(hfig);
                if nargout > 0
                    hax = [];
                    res = [];
                end
            end
        end
    end
end



function ok = addreplaydata(H)
    global g_langevin_eq;
    ok = false;
    if ~isempty(g_langevin_eq)
        ok = true;
        hax = findobj(H, 'type', 'axes');
        ud = get(hax, 'userdata');
        ud.replay.callback = @replaycallback;
        ud.replay.onstart = @onstart;
        ud.replay.onend = [];
        pdfs = g_langevin_eq.xtra.results.pdfs;
        ud.replay.settings = struct('tvar', 't', 'tlim', [pdfs.t(1) pdfs.t(1)], 'numt', length(pdfs.t));
        set(hax, 'userdata', ud);
    end
end
function onstart(hax)
    global g_langevin_eq;
    pdfs = g_langevin_eq.xtra.results.pdfs;
    if ishandle(hax)
        ud = get(hax, 'userdata');
        ud.replay.settings = struct('tvar', 't', 'tlim', [pdfs.t(1) pdfs.t(end)], 'numt', length(pdfs.t));
        set(hax, 'userdata', ud);
        i_figure(get(hax, 'parent'));
    end
end
function t = replaycallback(hax, avar, relt)
    global g_langevin_eq;
    t = [];
    if ishandle(hax) && isempty(avar) || strcmp(avar, 't')
        pdfs = g_langevin_eq.xtra.results.pdfs;
        ud = get(hax, 'userdata');
        t = ud.replay.settings.tlim(1) + relt * (ud.replay.settings.tlim(end) - ud.replay.settings.tlim(1));
        ser = get(hax, 'children');
        if isfield(ud, 'replay') && isfield(pdfs, 't')
            it = find(pdfs.t >= t, 1, 'first');
            set(ser(end), 'YData', [pdfs.pdfs(it, :, 1) 0 0]);
        end
    end
end



