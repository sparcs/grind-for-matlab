function yes = i_hastoolbox(toolname)
    %can simulate here that toolbox is not available
    yes=true;
    try
        if strcmp(toolname,'symbolic')
            yes=exist('syms','file');
        else
           toolboxdir(toolname);
        end
    catch err
        yes=false;
        if ~strcmpi(err.identifier,'MATLAB:toolboxdir:DirectoryNotFound')
           rethrow(err);
        end
    end
    %yes = exist(fullfile(matlabroot, 'toolbox', toolname), 'dir') == 7;
    %yes=false;
end

