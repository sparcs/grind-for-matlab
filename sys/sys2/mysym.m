classdef (InferiorClasses = {?sym}) mysym < sym
    methods
        function obj = mysym(varargin)
            obj = obj@sym(varargin{:});
%             if nargin==1&&isa(varargin{1},'sym')&&numel(varargin{1})>1
%                 c=varargin{1};
%                 obj = mysym(ones(size(c)));
%                 for i=1:length(obj)
%                     obj(i)=mysym(c(i));
%                 end
%             end
        end
        function m = max(a, b)
            if nargin == 1
                m = max@sym(a);
            else
                m = piecewise(a >= b, a, b);
            end
        end
        function m = min(a, b)
            if nargin == 1
                m = min@sym(a);
            else
                m = iif(a <= b, a, b);
            end
        end
        function m = mod(c, a)
            c=c/a;
            m = c-floor(c);
        end
        function m = iif(c, a, b)
           if numel(c)==1
                m = piecewise(c, a, b);
            else
                m=sym(zeros(size(c)));
                b2=sym(b);
                a2=sym(a);
                c2=sym(c);
                for i=1:numel(m)
                    if numel(a)==1
                        a1=a;
                    else
                        a1=a2(i);
                    end
                    if numel(b)==1
                        b1=b;
                    else
                        b1=b2(i);
                    end
                    c1=c2(i);
                    m(i)=piecewise(c1, a1, b1);
                end
            end
        end
        function varargout = boxcartrain(varargin) %#ok<STOUT>
            error('grind:mysym:notsupported','Function "boxcartrain" not supported for symbolic calculations')
        end
        function varargout = rednoise(varargin) %#ok<STOUT>
            error('grind:mysym:notsupported','Function "rednoise" not supported for symbolic calculations')
        end
        function varargout = i_djump(varargin) %#ok<STOUT>
            error('grind:mysym:notsupported','Function "djump" not supported for symbolic calculations')
        end
        function varargout = parlookup(varargin) %#ok<STOUT>
            error('grind:mysym:notsupported','Function "parlookup" not supported for symbolic calculations')
        end
        function res = lt(varargin)
            res = mysym(lt@sym(varargin{:}));
        end
        function res = gt(varargin)
            res = mysym(gt@sym(varargin{:}));
        end
        function res = ge(varargin)
            res = mysym(ge@sym(varargin{:}));
        end
        function res = le(varargin)
            res = mysym(le@sym(varargin{:}));
        end
        function res = mtimes(varargin)
            res = mysym(mtimes@sym(varargin{:}));
        end
        function res = mrdivide(varargin)
            res = mysym(mrdivide@sym(varargin{:}));
        end
        function res = times(varargin)
            res = mysym(times@sym(varargin{:}));
        end
        function res = rdivide(varargin)
            res = mysym(rdivide@sym(varargin{:}));
        end
        function res = eq(varargin)
            res = mysym(eq@sym(varargin{:}));
        end
        function res = mpower(varargin)
            res = mysym(mpower@sym(varargin{:}));
        end
        function res = power(varargin)
            res = mysym(power@sym(varargin{:}));
        end
        function res = plus(varargin)
            res = mysym(plus@sym(varargin{:}));
        end
        function res = minus(varargin)
            res = mysym(minus@sym(varargin{:}));
        end
        function res = atan(varargin)
            res = mysym(atan@sym(varargin{:}));
        end
        function res = sin(varargin)
            res = mysym(sin@sym(varargin{:}));
        end
        function res = cos(varargin)
            res = mysym(cos@sym(varargin{:}));
        end
        function res = tan(varargin)
            res = mysym(tan@sym(varargin{:}));
        end
        function res = exp(varargin)
            res = mysym(exp@sym(varargin{:}));
        end
        function res = log(varargin)
            res = mysym(log@sym(varargin{:}));
        end
        function res = subsref(varargin)
            res = mysym(subsref@sym(varargin{:}));
        end
        function res = subsindex(varargin)
            res = mysym(subsindex@sym(varargin{:}));
        end
        function res = subsasgn(varargin)
            res = mysym(subsasgn@sym(varargin{:}));
        end
        function res = horzcat(varargin)
            res = mysym(horzcat@sym(varargin{:}));
        end
        function res = complex(a, b)
            res = a + 1i * b;
        end
        function res = vertcat(varargin)
            res = mysym(vertcat@sym(varargin{:}));
        end
    end
end
