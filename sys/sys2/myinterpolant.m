classdef myinterpolant
    %use this when griddedInterpolant fails (that is much faster)
    %this failure is due to compatibily problems
    %it uses linear interpolation and NaN values outside the range
    properties
        GridVectors
        Values
    end
    methods
        function obj = myinterpolant(x, v)
            obj.GridVectors = {x};
            obj.Values = v;
        end
        function res = subsref(obj, A)
            if strcmp(A(1).type, '()')
                at=A.subs{1};
                if isempty(obj.Values)
                    res=NaN+zeros(size(at));
                elseif numel(at)==1
                    res = lookuptab(obj.GridVectors{1},obj.Values, at);
                else
                    res = interp1(obj.GridVectors{1}, obj.Values, at, 'linear', NaN);
                end
            else
                %call inherited subsref
                res = builtin('subsref', obj, A);
            end
        end
    end
end

function apar = lookuptab(tim,data, at)
    N=numel(tim);
    if ~isempty(data)
        if at < tim(1) || at > tim(N)
           apar = NaN .* zeros(1, size(data, 2));
        else
            i = 1;
            while (i < N) && tim(i) <= at
                i = i + 1;
            end
            %      i=find(tabl(:, 1) > key1,1);
            %     if isempty(i)
            %         i=N;
            %     end
            apar = data(i - 1, :) + (data(i, :) - data(i - 1, :)) ./ (tim(i) - tim(i - 1)) * (at - tim(i - 1));
            %    a = (tabl(i, 2:C) - tabl(i - 1, 2:C)) ./ (tabl(i, 1) - tabl(i - 1, 1));
            %    b = tabl(i - 1, 2:C) - a .* tabl(i - 1, 1);
            %    par = a .* key1 + b;
            %    par-par1
        end
    else
        apar = NaN;
    end
end
