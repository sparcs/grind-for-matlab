%RUN_PARALLEL   Try to run the model multiple times using parallel computing
%    Runs the model repetitive with different initial conditions  and/or parameters. For
%    this option the parallel computing toolbox is needed. Unfortunatly running
%    many models is dependent on global variables. These are not supported.
%    Examples:
%    Events ("setevent") are not supported as they often change global variables.
%    "onstart" is not executed.
%    Models with permanent variables are (yet) not supported.
%    Models that use "djump" are not yet supported (is certainly possible)
%    "rednoise", "dwiener" and "defextern" are now supported
%    
%        
%    Usage:
%      RUN_PARALLEL  - repeats a run 100 times
%      RUN_PARALLEL(n) - repeats a run n times
%    RUN_PARALLEL('argname',argvalue,...) - Valid argument name-value pairs [with type]:
%     'initial' [cell or number or function_handle] - each run can have a different initial state (empty-current state)
%     'n' [number] - Number of parallel runs if no variable was changed
%     'ndays' [number] - Number of time steps to run (default set by simtime)
%     'parnames' [string or cell] - Name(s) of parameters to change (empty=all, default empty)
%     'parvalues' [cell or number] - the values of the parameters to change
%     'tstep' [number] - Number of parallel runs if no variable was changed
%     'silent' [logical] - Show progress or not (default = false)
%     't0' [number] - Initial time (default t)
%
%    See also stabil, paranal, rungrid        
%
%   Reference page in Help browser:
%      <a href="matlab:commands('run_parallel')">commands run_parallel</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:39 $
function res = run_parallel(varargin)
    function cpar = getpar(j, cpar)
        if ~isempty(args.parvalues)
            if isempty(args.parnames)
                args.parnames = g_grind.pars;
            end
            ndx = ismember(g_grind.pars, args.parnames);
            if iscell(args.parvalues)
                cpar(ndx) = args.parvalues{j};
            else
                if g_grind.statevars.vector && size(args.parvalues, 2) > 1
                    error('grind:run_parallel:parvalues', 'The parameter values of a vector model should be a cell');
                end
                cpar(ndx) = num2cell(args.parvalues(j, :));
            end
        end

    end
    global g_grind t g_rednoise g_dwiener;
    fieldnams = {'parnames', 's#c', 'Name(s) of parameters to change (empty=all, default empty)', ''; ...
        'parvalues', 'c#n', 'the values of the parameters to change', NaN; ...
        'n', 'n', 'Number of parallel runs if no variable was changed', 100; ...
        'ndays', 'n', 'Number of time steps to run (default set by simtime)', g_grind.ndays; ...
        'tstep', 'n', 'Number of parallel runs if no variable was changed', g_grind.tstep; ...
        't0' , 'n', 'Initial time (default t)', t; ...
        'silent', 'l', 'Show progress or not (default = false)', false; ...
        'initial', 'c#n#f', 'each run can have a different initial state (empty-current state)', []}';
    args = i_parseargs(fieldnams, 'n', '', varargin);
    if ~isfield(args, 'parnames')
        args.parnames = {};
    end
    if ~isfield(args, 'silent')
        silent = true;
    else
        silent = args.silent;
    end
    if ~isfield(args, 'parvalues')
        args.parvalues = [];
    end
    if ~isfield(args, 'n')
        args.n = [100, 1];
    elseif numel(args.n) == 1
        args.n = [args.n 1];
    end
    if ~isfield(args, 't0')
        args.t0 = t;
    end
    if ~isfield(args, 'ndays')
        args.ndays = g_grind.ndays;
    end
    if ~isfield(args, 'tstep')
        args.tstep = g_grind.tstep;
    end
    if ~isnan(args.tstep) && args.tstep < 2
        args.tstep = 2;
    end
    n1 = [];
    if ~isfield(args, 'initial') || isempty(args.initial)
        initial = {i_initvar};
        n1 = size(initial);
    elseif ~isa(args.initial, 'function_handle')
        n1 = size(args.initial);
        initial = args.initial;
    else
        initial = args.initial;
    end

    if isnumeric(initial)
        initial = {initial};
    end
    if ~isempty(g_rednoise)
        g_rednoise.reset;
    end
    if ~isempty(g_dwiener)
        g_dwiener.reset(g_grind, args.t0);
    end
    par_model = parallel_model(g_grind, g_rednoise, g_dwiener);
    
    if iscell(args.parvalues) && length(args.parvalues) == 1
        % par_model.parvalues = args.parvalues{1};
        par_model = par_model.setparvalues(args.parvalues{1});
        args.parvalues{1} = [];
    elseif ~iscell(args.parvalues) && size(args.parvalues, 1) == 1
        cpar = par_model.parvalues;
%          par_model.parvalues = getpar(1, cpar);
        par_model = par_model.setparvalues(getpar(1, cpar));
        args.parvalues = {[]};
    elseif ~isempty(args.parvalues)
        if ~iscell(args.parvalues)
            siz = [size(args.parvalues, 1), 1];
        else
            siz = size(args.parvalues);
        end
        cpar = par_model.parvalues;
        n1 = prod(siz);
        parvalues = cell(n1, 1);
        for i = 1:n1
            parvalues{i} = getpar(i, cpar);
        end
    elseif numel(initial)>1
        siz=size(initial);
    else
        siz=args.n;
    end
    if ~isempty(n1)
        args.n = siz;
    end
    if isa(initial, 'function_handle')
        fun = initial;
        initial = cell(args.n);
        for i = 1:prod(args.n)
            initial{i} = fun();
        end
    end
    %solv = str2func(g_grind.solver.name);
    solv=g_grind.solver.solverhandle;
    opt = g_grind.solver.opt;
    gY = cell(args.n);
    gt = cell(args.n);
    t0 = args.t0;
    ndays = args.ndays;
    tstep = args.tstep;
    parvalues = args.parvalues;
    if isempty(parvalues)
        parvalues = {[]};
    end
    ranparallel = true;
    errorcode='';
    try
    %    assert(isempty(g_grind.onstart.funs), 'grind:run_parallel:onstart', 'onstart is not supported in run_parallel')
        assert(isempty(g_grind.event.events), 'grind:run_parallel:setevent', 'setevent is not supported in run_parallel')
        assert(isempty(g_grind.permanent), 'grind:run_parallel:defpermanent', 'defpermanent is not supported in run_parallel')
        assert(~g_grind.solver.isimplicit,'grind:run_parallel:dae', 'dae models are not supported in run_parallel')
        assert(~g_grind.solver.haslags,'grind:run_parallel:lags', 'dde (lag) models are not supported in run_parallel')
        parfor i = 1:prod(args.n)
            [gt{i}, gY{i}] = runstep(i, par_model, t0, ndays, tstep, initial, solv, opt, parvalues, silent);
        end
    catch err
        if strcmp(err.identifier, 'MATLAB:structRefFromNonStruct')
            fprintf('The model fails to run in parallel,\nProbably because the code is dependent on global variables\nError id:"%s"\n,%s\n', err.identifier, err.message);
        else
            fprintf('The model fails to run in parallel\nError id:"%s"\n%s\n', err.identifier, err.message);
        end
        errorcode=err.identifier;
        ranparallel = false;
        %try to run sequeuntial;
        for i = 1:prod(args.n)
            [gt{i}, gY{i}] = runstep(i, par_model, t0, ndays, tstep, initial, solv, opt, parvalues, silent);
        end
    end
    if numel(parvalues) == 1 && isempty(parvalues{1})
        parvalues = par_model.parvalues;
    end
    res = struct('t', {gt}, 'Y', {gY}, 'parvalues', {parvalues}, 'ranparallel', ranparallel,'errorcode',errorcode);
end
function [gt, gY] = runstep(i, par_model, t0, ndays, tstep, initial, solv, opt, parvalues, silent)
    if ~silent
        fprintf('Iteration %d\n', i);
    end
    [gt, gY] = par_model.run(t0, ndays, tstep, initial{min(i, length(initial))}, solv, opt, parvalues{min(i, length(parvalues))});
end
