function res = adjustplot(hfig, comm, varargin)
    %handy to adjust subplot positions or default appearance
    %general usage:
    %  adjustplot(gcf,'-command',parameters)
    %  adjustplot('-command')
    %
    %Commands/parameter
    %   '-alignaxes' align/resize axes of combined figure
    %   '-adjustpos': used internally
    %   '-b': remove ticks from the axes (does not
    %        automatically update with scaling of the figure)
    %   '-d': set defaults
    %   '-f'; fix scale
    %   '-1', par : make axes the same of subplots in a orientation
    %       (par='x','y','b' (both)
    %   '-move',[x,y] move all children by x,y
    %   '-p',spacing: shift subplots closer together (spacing = [spacingx
    %        spacingy])
    %   '-r',par): remove axes of a subplot in an orientation par='t'=remove titles only, 'tx'=remove x labels, 'ty' remove
    %        'y' y titles + values, 'x' titles + values 'b' both
    %   '-scale',fract: %scale children by fraction
    %   '-t',loc) add text right (loc='tr'), text left ('tl'), bottom
    %        left ('bl'), bottom right ('bl') 

   
    if nargin < 1
        hfig = gcf;
    elseif ~ishandle(hfig)
        if nargin >= 2
            varargin = [{comm} varargin];
            comm = hfig;
            hfig = gcf;
        elseif nargin == 1
            %varargin{1} = [];
            comm = hfig;
            hfig = gcf;
        end
    end
    res1 = [];
    %     if nargin < 3
    %         varargin{1} = [];
    %     end
    %if nargin < 2
    %    comm = '-d';
    %end
    switch comm
        case '-f' %fix scale
            if isempty(varargin) %xlim, ylim, clim
                varargin = {'xlim'};
            end
            kindscale = varargin{1};
            hs = getsubplotgrid(hfig);
            scal = zeros(numel(hs), 2);
            for i = 1:numel(hs)
                scal(i, :) = get(hs{i}(1), kindscale);
            end
            scal = [min(scal(:, 1)) max(scal(:, 2))];
            for i = 1:numel(hs)
                set(hs{i}(1), kindscale, scal);
            end
        case '-scale' %scale children by fraction
            ch = get(hfig, 'children');
            fraction = varargin{1};
            if numel(fraction) == 1
                fraction(2) = fraction(1);
            end
            for i = 1:length(ch)
                pos = get(ch(i), 'position');
                pos(3) = pos(3) .* fraction(1);
                pos(4) = pos(4) .* fraction(2);
                set(ch(i), 'position', pos);
            end
        case '-alignaxes' %align/resize axes of combined figure
            hs = getsubplotgrid(hfig);
            posi = cell(size(hs));
            for i = 1:size(hs, 1)
                for j = 1:size(hs, 2)
                    posi{i, j} = get(hs{i, j}(1), 'position');
                end
            end
            %allign/rescale rows
            %make size of the columns/rows uniform
            minh = inf;
            minw = Inf;
            for i = 1:numel(hs)
                if minh > posi{i}(4)
                    minh = posi{i}(4);
                end
                if minw > posi{i}(3)
                    minw = posi{i}(3);
                end
            end
            for i = 1:size(hs, 1)
                minx = Inf;
                %minh=Inf;
                for j = 1:size(hs, 2)
                    if minx > posi{i, j}(2)
                        minx = posi{i, j}(2);
                    end
                    %                     if minh>posi{i,j}(4)
                    %                         minh=posi{i,j}(4);
                    %                     end
                end
                for j = 1:size(hs, 2)
                    posi{i, j}(2) = minx;
                    posi{i, j}(4) = minh;
                end
            end
            %allign/rescale columns
            for j = 1:size(hs, 2)
                miny = Inf;
                %                 minw=Inf;
                for i = 1:size(hs, 1)
                    if miny > posi{i, j}(1)
                        miny = posi{i, j}(1);
                    end
                    %                     if minw>posi{i,j}(3)
                    %                         minw=posi{i,j}(3);
                    %                     end
                end
                for i = 1:size(hs, 1)
                    posi{i, j}(1) = miny;
                    posi{i, j}(3) = minw;
                end
            end
            for i = 1:numel(hs)
                set(hs{i}(1), 'position', posi{i});
            end
        case '-move' %move children
            ch = get(hfig, 'children');
            offset = varargin{1};
            if numel(offset) == 1
                offset(2) = 0;
            end
            for i = 1:length(ch)
                pos = get(ch(i), 'position');
                pos(1:2) = pos(1:2) + offset;
                set(ch(i), 'position', pos);
            end
        case '-getsubplotgrid'
            res1 = getsubplotgrid(hfig);
        case '-adjustpos'
            %used internally to reposition a list of elements of a figure
            %(like axes, legends and colorbars)
            %Usage:
            %adjustplot(hs,'-adjustpos',newpos);
            %hs can be a list of handles of elements that should be repositioned (or one handle)
            hs = hfig;
            hs = hs(~strcmpi(get(hs, 'type'), 'uimenu')); %remove uimenu as it has no position
            hs = hs(~strcmpi(get(hs, 'type'), 'UIControl')); %remove uicontrol as it has no position
            pos = varargin{1};
            [totpos, newpos] = totalpos(hs);
            siz = totpos(3:4);
            newsiz = pos(3:4);
            newpos(:, [1, 3]) = newpos(:, [1, 3]) .* newsiz(1) ./ siz(1);
            newpos(:, [2, 4]) = newpos(:, [2, 4]) .* newsiz(2) ./ siz(2);
            %shift pos
            newpos(:, 1) = newpos(:, 1) + pos(1);
            newpos(:, 2) = newpos(:, 2) + pos(2);
            for j = 1:length(hs)
                set(hs(j), 'position', newpos(j, :));
            end
        case '-b' %box without ticks, does not work if Inf is in xlim or ylim
            %not updated (could be done with listener)
            %ax=get(hfig,'currentaxes');
            axs = get(hfig, 'children');
            drawnow()
            for i = 1:length(axs)
                if strcmp(get(axs(i), 'Type'), 'axes')
                    yylim = get(axs(i), 'ylim');
                    xxlim = get(axs(i), 'xlim');
 
                    if strcmp(get(axs(i), 'YDir'), 'reverse')
                        iy = [2 1];
                    else
                        iy = [1 2];
                    end
                    if strcmp(get(axs(i), 'XDir'), 'reverse')
                        ix = [2 1];
                    else
                        ix = [1 2];
                    end
                    h = findobj(axs(i), 'tag', 'plot-box');
                    if isempty(h)
                        %first use only!! (else listener causes problem)
                        set(axs(i), 'box', 'off')
                        set(axs(i), 'ylim', yylim);
                        set(axs(i), 'xlim', xxlim);
                        oldhold = ~strcmp(get(axs(i), 'nextplot'), 'add');
                        hold(axs(i), 'on');
                        h = plot(axs(i), [xxlim(ix(1)), xxlim(ix(2)), xxlim(ix(2))], [yylim(iy(2)), yylim(iy(2)), yylim(iy(1))], 'k-', 'tag', 'plot-box', 'LineWidth', get(axs(i), 'LineWidth'));
                        set(get(get(h, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
                        if ~oldhold
                            hold(axs(i), 'off');
                        end
                        addlistener(axs(i), 'YLim', 'PostSet', @(src,evnt)adjustplot(hfig,'-b'));
                        addlistener(axs(i), 'XLim', 'PostSet', @(src,evnt)adjustplot(hfig,'-b'));
                    else
                        set(h, 'xdata', [xxlim(ix(1)), xxlim(ix(2)), xxlim(ix(2))], 'ydata', [yylim(iy(2)), yylim(iy(2)), yylim(iy(1))]);
                    end
                end
            end
            return;
        case '-d'
            if nargin < 3
                apen = struct('pen', '-', 'pen2', '-', 'markersize', 5, 'markerfacecolor', 'none', 'cycle', 0, ...
                    'i', 1, 'color', [0 0 1], 'color2', [0 0 1], 'drawcolor', [0.7500 0 0], 'colormap', [0 1 1; 1 0 1], ... 
                    'linewidth', 1, 'fontsize', 16, 'tickdir', 'out', 'box', 'on');
            else
                apen = varargin{1};
            end

            try
                hax = get(hfig, 'CurrentAxes');
            catch
                hax = findobj(hfig, 'type', 'axes');
            end
            if isempty(hax)
                hax = gca;
            end

            Cbar = findobj(hfig, 'tag', 'Colorbar');
            if ~isempty(Cbar)
                set(Cbar, 'FontSize', apen.fontsize);
            end
            set(hax, 'FontSize', apen.fontsize);
            set(hax, 'LineWidth', apen.linewidth);
            set(hax, 'TickDir', 'out');
            set(hax, 'TickLength', [0.015 0.025])
            if isempty(findobj(hfig, 'tag', 'plot-box'))
                set(hax, 'Box', apen.box);
            else
                adjustplot(hfig, '-b');
            end
            %set(H, 'TickDir', apen.tickdir);
            %            set(hax, 'Box', apen.box);
            t = findobj(hax, 'type', 'text');
            if ~isempty(t)
                if ~isfield(apen, 'fontsize')
                    apen.fontsize = 14;
                end
            
                set(t, 'fontsize', apen.fontsize);
            end

            t1 = [findobj(hfig, 'tag', 'conteq-text'); findobj(hfig, 'tag', 'contbif-text'); findobj(hfig, 'tag', 'label-text')];
            set(t1, 'fontsize', 10);
            if isempty(Cbar) || ~any(hax == Cbar)
                View = get(hax, 'view');
                if (apen.fontsize >= 14) && min(View == [0 90])
                    set(hax, 'units', 'normalized');
                    if ~isempty(Cbar)
                        P = [0.1500 0.1900 0.6455 0.7350];
                    else
                        P = [0.1300 0.1100 0.7750 0.8150];
                    end
                
                    pos1 = get(hax, 'position');
                    if (pos1(3) > 0.6) && (pos1(4) > 0.6)
                        set(hax, 'position', transform(P, apen.fontsize));
                        if ~isempty(Cbar)
                            P = [0.831375 0.13 0.058125 0.795];
                            set(Cbar, 'position', transform(P, apen.fontsize))
                        end
                    
                    end
                
                end
            end
        case '-t'
            if length(varargin) == 2
                atext = varargin{2};
                pos = varargin{1};
            else
                atext = varargin{1};
                pos = 'tl';
            end
            hs = getsubplotgrid(hfig);
            k = 1;
            for i = 1:size(hs, 1)
                for j = 1:size(hs, 2)
                    %hax = get(hfig, 'CurrentAxes');
                    if ischar(pos)
                        switch pos
                            case 'tl'
                                pos = [0.05 0.95];
                            case 'tr'
                                pos = [0.95 0.95];
                            case 'bl'
                                pos = [0.05 0.05];
                            case 'br'
                                pos = [0.95 0.05];
                        end
                    end
                    %                xlims = get(hs{i,j}(1), 'xlim');
                    %               ylims = get(hs{i,j}(1), 'ylim');
                    if iscell(atext)
                        atext1 = atext{k};
                        k = k + 1;
                    else
                        atext1 = atext;
                    end
                    text(hs{i, j}(1), pos(1), pos(2), atext1, 'Units', 'Normalized','tag','smalltext');
                    %text(hs{i, j}(1), xlims(1) + pos(1) * (xlims(2) - xlims(1)), ylims(1) + pos(2) * (ylims(2) - ylims(1)), atext1);
                end
            end
        case '-l'
            hs = getsubplotgrid(hfig);
            if isempty(varargin)
                orientation = 'b';
            else
                orientation = varargin{1};
            end
            [rows, cols] = size(hs);
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'x', 1)
                for i = 1:rows - 1
                    for j = 1:cols
                        if ishandle(hs{i, j}(1)) && (hs{i, j}(1) ~= 0)
                            adjustticklabels(hs{i, j}(1), 'X')
                        end
                    end
                end
            end
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'y', 1)
                for i = 1:rows
                    for j = 1:cols
                        if ishandle(hs{i, j}(1)) && (hs{i, j}(1) ~= 0)
                            adjustticklabels(hs{i, j}(1), 'Y')
                        end
                    end
                end
            end
        case '-r'
            if isempty(varargin) || isempty(varargin{1})
                orientation = 'b';
            else
                orientation = varargin{1};
            end
            hs = getsubplotgrid(hfig);
            if numel(hs) < 2
                disp('Too few axes');
                return;
            end
            removeticklabel = true;
            if strncmpi(orientation, 't', 1)
                removeticklabel = false;
                if strcontains(orientation, 'x')
                    orientation = 'x';
                elseif strcontains(orientation, 'y')
                    orientation = 'y';
                else
                    orientation = 'b';
                end
            end
            [rows, cols] = size(hs);
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'x', 1)
                for i = 1:rows - 1
                    for j = 1:cols
                        if ishandle(hs{i, j}(1)) && (hs{i, j}(1) ~= 0)
                            set(get(hs{i, j}(1), 'xlabel'), 'string', '')
                            %set(hs{i, j}(1), 'fontsize', 12)
                            if removeticklabel
                                set(hs{i, j}(1), 'XTickLabel', []);
                            end
                        end
                    end
                end
            end
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'y', 1)
                for i = 1:rows
                    for j = 2:cols
                        if ishandle(hs{i, j}(1)) && (hs{i, j}(1) ~= 0)
                            set(get(hs{i, j}(1), 'ylabel'), 'string', '')
                            %set(hs{i, j}(1), 'fontsize', 12)
                            if removeticklabel
                                set(hs{i, j}(1), 'YTickLabel', []);
                            end
                        end
                    end
                end
            end
            %             ndx = hs ~= 0;
            %             set(hs(ndx), 'fontsize', 12)
            %             aa = get(hs(ndx), 'xlabel');
            %             if iscell(aa)
            %                 aa = [aa{:}];
            %             end
            %             set(aa, 'fontsize', 12);
            %             aa = get(hs(ndx), 'ylabel');
            %             if iscell(aa)
            %                 aa = [aa{:}];
            %             end
            %             set(aa, 'fontsize', 12);
        case '-p'
 
 
            if isempty(varargin{1})
                spacing = 0.02;
            else
                spacing = varargin{1};
                if ischar(spacing)
                    spacing = str2double(spacing);
                end
            end
            set(hfig, 'PaperPositionMode', 'auto')
            set(hfig, 'units', 'normalized');
            sgtitleh = getappdata(gcf, 'SubplotGridTitle');
            if isempty(sgtitleh)
                height_title = 0;
            else
                height_title = 0.04;
            end
            figpos = get(hfig, 'position');
            vert = 0.5469; %normal size of figure
            hor = 0.41;
            [hs, colwidths] = getsubplotgrid(hfig);
            if isempty(hs)
                disp('No axes to adjust');
                return;
            elseif length(hs) == 1
                return;
            end

            if length(spacing) == 1
                spacing = spacing + zeros(2, 1);
            end
            [rows, cols] = size(hs);
            if cols <= 4 && rows <= 4
                newfigpos = [figpos(1:2), hor * cols / 2, vert * rows / 2];
            else
                rat = rows / cols;
                if rat < 1
                    newfigpos = [figpos(1:2), hor, vert * rat];
                else
                    newfigpos = [figpos(1:2), hor / rat, vert];
                end
            end
            if newfigpos(4) > 1
                newfigpos(2) = - 0.2;
            end
            set(hfig, 'position', newfigpos);
            for i = 1:rows
                for j = 1:cols
                    adjustplot(hs{i, j}, '-adjustpos', subplotpos(rows, cols, j, i, spacing, colwidths(i, j), height_title));
                end
            end
            movegui(gcf, 'center')
        otherwise
            error('adjustsubplots:unknown', 'Unknown command');
    end
    if nargout > 0
        res = res1;
    end
end

function [newpos, pos2] = totalpos(hs)
    %newpos is the total position of different handles
    %pos2 gives the relative position for each of the handles
    newpos=[];
    pos2 = get(hs, 'position');
    if iscell(pos2)
        pos2 = vertcat(pos2{:});
    end
    %shift pos to 0 0
    if size(pos2, 2) > 1
        newpos = [ min(pos2(:, 1)), min(pos2(:, 2)), 0 0];
        pos2(:, 1) = pos2(:, 1) - newpos(1);
        pos2(:, 2) = pos2(:, 2) - newpos(2);
        %find scales
        newpos(3:4) = [max(pos2(:, 1) + pos2(:, 3)), max(pos2(:, 2) + pos2(:, 4))];
    end
end

function [hs, colwidths] = getsubplotgrid(h)
    ch = num2cell(get(h, 'children'));
    hs = [];
    colwidths = [];
    axischild = true(size(ch));
    for i = 1:length(ch)
        try
            get(ch{i}, 'Axes');
        catch
            axischild(i) = false;
        end
    end
    axch = ch(axischild);
    ch = ch(~axischild);
    for i = 1:length(axch)
        hax = get(axch{i}, 'Axes');
        for j = 1:length(ch)
            if isequal(ch{j}, hax)
                ch{j} = [ch{j}, axch{i}];
            end
        end
    end
    sgtitleh = getappdata(h, 'SubplotGridTitle');
    if ~isempty(sgtitleh)
        for j = length(ch): -1:1
            if isequal(ch{j}, sgtitleh)
                ch(j) = [];
            end
        end
    end

    if ~isempty(ch)
        %         tags = get(ch, 'tag');
        %         ch = ch(~strcmpi(tags, 'legend') | ~strcmpi(tags, 'Colorbar'));
        %         types = get(ch, 'type');
        %         if ~iscell(types)
        %             types = {types};
        %         end
        poss = cell(size(ch));
        for i = 1:length(ch)
            poss{i} = totalpos(ch{i});
        end
        haspos=~cellfun(@isempty,poss);
        ch=ch(haspos);
        ipos = vertcat(poss{:});
        %         ipos = zeros(length(ch), 4);
        %         for i = length(ch): - 1:1
        %             if strcmp(types{i}, 'axes')
        %                 ipos(i, :) = poss{i};
        %             else
        %                 ipos(i, :) = [];
        %                 ch(i) = [];
        %                 types(i) = [];
        %             end
        %         end
        %we actually need to account for the spacing between the columns, a
        %bit complex?
        colwidth = floor(ipos(:, 3) ./ min(ipos(:, 3)) + 0.01); %the columns each figure spans
        ipos(:, 1) = ipos(:, 1) - min(ipos(:, 1));
        ipos(:, 2) = ipos(:, 2) - min(ipos(:, 2));
        tolx = mean(ipos(:, 3)) / 10;
        ipos(:, 1) = round(ipos(:, 1) / tolx) * tolx;
        tolx = mean(ipos(:, 4)) / 10;
        ipos(:, 2) = round(ipos(:, 2) / tolx) * tolx;
        colpos = sort(unique(sort(ipos(:, 1))), 'ascend');
        rowpos = sort(unique(sort(ipos(:, 2))), 'descend');
        hs = cell(length(rowpos), length(colpos));
        colwidth(colwidth > length(colpos)) = length(colpos);
        colwidths = zeros(length(rowpos), length(colpos));
        for i = 1:length(ch)
            %if strcmp(types{i}, 'axes')
            arow = find(rowpos == ipos(i, 2));
            acol = find(colpos == ipos(i, 1));
            hs(arow, acol) = ch(i);
            colwidths(arow, acol) = colwidth(i);
            set(ch{i}(1), 'tag', sprintf('subplot(%d,%d)', arow, acol));
            %end
        end
    end
end
% function [hs, colwidths] = getsubplotgrid(h)
%     ch = get(h, 'children');
%     hs = [];
%     colwidths = [];
%     if ~isempty(ch)
%         tags = get(ch, 'tag');
%         ch = ch(~strcmpi(tags, 'legend') | ~strcmpi(tags, 'Colorbar'));
%         types = get(ch, 'type');
%         if ~iscell(types)
%             types = {types};
%         end
%         poss = get(ch, 'position');
%         if ~iscell(poss)
%             poss = {poss};
%         end
%         ipos = zeros(length(ch), 4);
%         for i = length(ch): - 1:1
%             if strcmp(types{i}, 'axes')
%                 ipos(i, :) = poss{i};
%             else
%                 ipos(i, :) = [];
%                 ch(i) = [];
%                 types(i) = [];
%             end
%         end
%         %we actually need to account for the spacing between the columns, a
%         %bit complex?
%         colwidth = floor(ipos(:, 3) ./ min(ipos(:, 3)) + 0.01); %the columns each figure spans
%         colpos = sort(unique(sort(ipos(:, 1))), 'ascend');
%         rowpos = sort(unique(sort(ipos(:, 2))), 'descend');
%         hs = zeros(length(rowpos), length(colpos));
%         colwidth(colwidth > length(colpos)) = length(colpos);
%         colwidths = zeros(length(rowpos), length(colpos));
%         for i = 1:length(ch)
%             if strcmp(types{i}, 'axes')
%                 arow = find(rowpos == ipos(i, 2));
%                 acol = find(colpos == ipos(i, 1));
%                 hs(arow, acol) = ch(i);
%                 colwidths(arow, acol) = colwidth(i);
%                 set(ch(i), 'tag', sprintf('subplot(%d,%d)', arow, acol));
%             end
%         end
%     end
% end

function adjustticklabels(hax, orient)
    axis(hax, 'tight')
    newtick = get(hax, [orient 'tick']);
    tickdiff = (newtick(2) - newtick(1));
    newlim = [newtick(1) - tickdiff newtick(end) + tickdiff];
    axis(hax, 'manual')
    set(hax, [orient 'lim'], newlim);
    set(hax, [orient 'tick'], [newtick(1) - tickdiff newtick]);
end
function P = transform(P, fontsize)
    %P = [left, bottom, width, height]
    %where left and bottom define the distance from the lower-left corner of the screen
    if fontsize <= 18
        P(1) = P(1) + 0.01;
        P(2) = P(2) + 0.03;
        P(3) = P(3) - 0.02;
        P(4) = P(4) - 0.04;
    elseif fontsize <= 22
        P(2) = P(2) + 0.04;
        P(4) = P(4) - 0.04;
    elseif fontsize <= 28
        P(1) = P(1) + 0.02;
        P(3) = P(3) - 0.02;
        P(2) = P(2) + 0.08;
        P(4) = P(4) - 0.08;
    else
        P(1) = P(1) + 0.08;
        P(3) = P(3) - 0.08;
        P(2) = P(2) + 0.16;
        P(4) = P(4) - 0.16;
    end

    return
end
