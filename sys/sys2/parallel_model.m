classdef parallel_model
    %create a copy of a model that can run parallel as it uses no global
    %variables. (not possible for all kinds of models)
    % rednoise and dwiener are implemented.
    % can also (probably) be implemented for djump, externvars defpermanent
    % difficult to create for models with onstart, or events
    properties (SetAccess = protected)
        parvalues %cell with parameter values
        odefun %handle
        odehandle %isempty if the model has dynamic hidden parameters
        hidden_pars
        hiddenparvalues
        type %0=normal, 1=implicit, 2=haslags 3=discrete
    end
    methods
        
        function obj = setparvalues(obj, parvalues) %do this only if you need to run more replicaes 
            obj.parvalues = parvalues;
            if ~isempty(obj.odehandle)&&~isempty(obj.parvalues)
                obj.odehandle = obj.createhandle;
            else
                obj.odehandle = [];
            end
        end
        
        function obj = parallel_model(g_grind, g_rednoise, g_dwiener)
            %g_rednoise and g_dwiener should have been reset
            %sets the defaults using the current grind model
            %this can better not be done within a parfor loop (esp if there
            %are hidden parameters)
            %obj.definit = i_initval;
            %obj.defsolv = str2fun(g_grind.solver.name);
            %obj.defopts
            obj.parvalues = par(struct('opts', '-v','type','cell'));
            %obj.deftime = [t g_grind.ndays g_grind.tstep];
            %             obj.pars = g_grind.pars;
            if g_grind.solver.isimplicit
                obj.type = 1;
            elseif g_grind.solver.haslags
                obj.type = 1;
            elseif g_grind.solver.isdiffer
                obj.type = 3;
            else
                obj.type = 0;
            end
            obj.hidden_pars = g_grind.hidden_pars;
            obj.hiddenparvalues = cell(1, numel(obj.hidden_pars));
            for j = 1:length(g_grind.hidden_pars)
                switch g_grind.hidden_pars{j}
                    case 'g_rednoise'
                        rmod = rednoise_model(g_rednoise);
                        obj.hiddenparvalues{j} = rmod;
                    case 'g_dwiener'
                        rmod = dwiener_model(g_dwiener);
                        obj.hiddenparvalues{j} = rmod;
                    otherwise
                        obj.hiddenparvalues{j}=eval(g_grind.hidden_pars{j});
                end
            end
            if ischar(g_grind.odefile)
                obj.odefun = str2func(g_grind.odefile);
            else
                obj.odefun = g_grind.odefile;
            end
            hasdynamicpars = ~isempty(ismember(g_grind.hidden_pars, {'g_rednoise'}));
            if ~hasdynamicpars
                obj.odehandle = obj.createhandle;
            else
                obj.odehandle = [];
            end
        end
 
        function han = createhandle(obj, cpars, backwards)
            %create handle without using globals
            %not (yet) supported for all models
            if nargin < 2 || isempty(cpars)
                cpars = obj.parvalues;
            end
            if nargin < 3
                backwards = false;
            end
            %normal i_ru
            %give a cell array with all the parameter values to bind
            %(instead of reading it from the base workspace
            %hidden parameters should also be given
            hiddenpars = obj.hiddenparvalues;
            for j = 1:length(obj.hidden_pars)
                switch obj.hidden_pars{j}
                    case 'g_rednoise'
                        hiddenpars{j} = rednoise_model(obj.hiddenparvalues{j});
                end
            end
            han1 = obj.odefun;
            if ~backwards
                if obj.type == 1 %g_grind.solver.isimplicit
                    han = @(t,g_X1,g_X3)han1(t,g_X1,g_X3,cpars{:},hiddenpars{:});
                elseif obj.type == 2 %g_grind.solver.haslags
                    han = @(t,g_X1,g_lags)han1(t,g_X1,g_lags,cpars{:},hiddenpars{:});
                else
                    han = eval('@(t,g_X1)han1(t,g_X1,cpars{:},hiddenpars{:})');
                end
 
            else
                if obj.type == 1 %g_grind.solver.isimplicit
                    han = @(t,g_X1,g_X3)-han1(t,g_X1,-g_X3,cpars{:},hiddenpars{:});
                elseif obj.type == 3 % g_grind.solver.isdiffer
                    TOL = 1E-10;
                    MAXITS = 100;
                    odef = @(t,g_X1)han1(t,g_X1,cpars{:},hiddenpars{:});
                    han = @(t,y)newton4differ(t,y,odef,Jfun,TOL,MAXITS,1);
                elseif obj.type == 2 %g_grind.solver.haslags
                    han = @(t,g_X1)-han1(t,g_X1,[],cpars{:},hiddenpars{:});
                else
                    han = @(t,g_X1)-han1(t,g_X1,cpars{:},hiddenpars{:});
                end
 
            end
 
            %             if ~isempty(g_grind.solver.opt.Jacobian) %you need to take
            %             care of this elsewhere
            %                 enterjac('-update');
            %             end
        end
 
        function [gt, gY, nextrun] = run(obj, at, ndays, tstep, initval, solv, opts, parvalues)
            %runs grind model safely in parallel
            %Usage
            %[gt,gY]=obj.run(t, ndays, tstep, initval, solv, opts, parvalues)
            % t = t0
            % ndays = number of time units to run
            % tstep = number of time steps (NaN for all)
            % initval = vector (vertical) with initial conditions
            % solv = function handle to solver
            % opts = struct with options (optional)
            % parvalues = cell with parameters (optional)
            %
            %
            %Efficient subsequent runs (within one parfor step)
            %First run (save settings and odehandle in struct "nextrun"):
            %[gt,gY,nextrun]=obj.run(t, ndays, tstep, init, solv, opts, parvalues)
            %replicate of first run: 
            %[gt,gY]=obj.run(nextrun)
            %
            %append to previous run:
            %nextrun.initval=gY(end,:)';
            %nextrun.t=gt(end);
            %[gt,gY]=obj.run(nextrun)
            %
            %run with new parameters (a new odehandle is created):
            %nextrun.parvalues=newparvalues; %cell with all parameter values
            %[gt,gY,nextrun]=obj.run(nextrun);     
            if nargin == 2
                s = at;
                if isfield(s, 'odehandle')
                    han = s.odehandle;
                else
                    han = [];
                end
                at = s.t;
                ndays = s.ndays;
                tstep = s.tstep;
                initval = s.initval;
                solv = s.solv;
                if isfield(s, 'opts')
                    opts = s.opts;
                else
                    opts = [];
                end                 
                if isfield(s, 'parvalues') && ~isempty(s.parvalues)
                    %assume new parameter values if the field parvalues is
                    %filled;
                    parvalues = s.parvalues;
                    han = [];
                end
            else
                han = [];
                if nargin < 7
                    opts = [];
                end
                if nargin < 8 || isempty(parvalues)
                    han = obj.odehandle;
                    parvalues = obj.parvalues;
                end
            end
            if isempty(opts)
                %grind defaults
                opts = odeset('RelTol', 5e-5, 'AbsTol', 1e-7);
                opts.MaxStep = 0.1;
            end
            if ~isnan(tstep)
                ts = transpose(at:ndays / tstep:at + ndays);
                if length(ts) == 2
                    ts = [ts(1); ts(1) + (ts(2) - ts(1)) / 2; ts(2)];
                end
            else
                ts = [at; at + ndays];
            end
            if isempty(han)
                han = obj.createhandle(parvalues);
            end
            [gt, gY] = feval(solv, han, ts, initval, opts);
            if nargout > 2
                %nextrun is struct with odehandle and all input arguments
                %within a parfor run you can use this structure several times
                %for efficiency
                nextrun = struct('odehandle', han, 't', at, 'ndays', ndays, 'tstep', tstep, 'initval', initval, 'solv', solv, 'opts', opts);
            end
        end
    end
end

