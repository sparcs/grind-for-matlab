function dq = i_parfor_waitbar(varargin)
    % Example of usage (more limited then i_waitbar):
    %     wb=i_parfor_waitbar(0, n, 'Calculating');
    %     parfor i = 1:n
    %         .. do something
    %         i_parfor_waitbar(wb,i);
    %     end
    %     i_parfor_waitbar([]);
    %
    try
        if nargin == 2
            try
                send(varargin{:})
            catch err
                if ~strcmp(err.identifier, 'MATLAB:UndefinedFunction')
                    rethrow(err);
                else
                    i_waitbar(1);
                end
            end
        elseif nargin == 1 && isempty(varargin{1})
            i_waitbar(varargin{:})
        elseif (nargin >= 3)
            if isempty(gcp('nocreate'))
                parpool('local');
            end
            dq = parallel.pool.DataQueue;
            afterEach(dq, @(varargin)i_waitbar(1))
            varargin{3}=[varargin{3} ' (parfor)'];
            i_waitbar(varargin{:})
        end
    catch err
        if ~strcmp(err.identifier, 'MATLAB:UndefinedFunction')
            rethrow(err);
        else
            dq = i_waitbar(varargin{:});
        end
    end
end
