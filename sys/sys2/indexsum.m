function txtsum = indexsum(elem, indices)
    %indexsum(elem,indices)
    %  indices should be 'k=1..20'
    %  elem should be a string of the elements to sum (with square brackets)
    %    for instance
    %  indexsum('x[i,k]*y[k,j]','k=1..20')
    %  mupad can handle almost the same notation in differentiations:
    %  sum(x[i,k]*y[k,j],k=1..20)
    indx = regexp(indices, '[=]|[.][.]', 'split');
    patt = regexprep([elem '+'], ['(?<=[\[\,])' indx{1}], '%d');
    range = str2double(indx{2}):str2double(indx{3});
    txtsum = sprintf(patt, [range; 
        range]);
    txtsum = txtsum(1:end - 1);
    %make round brackets: 
    %regexprep(txtsum,{'(?<=[\[\,])i','(?<=[\[\,])j','[\[]([0-9ij#]*)[,]([0-9ij#]*)[\]]'},{'i','j','($1,$2)'})
end
