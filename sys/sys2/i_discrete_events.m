function [T, Y] = i_discrete_events(odefunct, tspan, Y0, options)
%solver for discrete event models, i.e. difference equations where the time
%step is dynamically changing.
%Such models should include an assignment to time t.
%if tspan is a vector of 2 all steps are saved.
%else tspan should include all time points for output.
    if (nargin < 4)
        nonNegative = [];
    else
        nonNegative = odeget(options, 'NonNegative', []);
    end
    anyNonNegative = ~isempty(nonNegative);
    haveOutputFcn = ~isempty(options.OutputFcn);
    outputFcn = options.OutputFcn;
    if haveOutputFcn
        feval(outputFcn, tspan, Y0, 'init');
    end
    %     if isempty(g_grind)
    %         iters = 1;
    %     else
    %         iters = g_grind.solver.iters;
    %     end
    endT = tspan(end);
    T0 = tspan(1);
 
    if size(Y0, 2) > size(Y0, 1)
        Y0 = transpose(Y0);
    end
    step = 2;
    fixedsteps = false;
    if numel(tspan) > 2
        Y = zeros(numel(tspan), numel(Y0));
        T = tspan;
        fixedsteps = true;
    else
        T = zeros(10000, 1);
        Y = zeros(10000, numel(Y0));
    end
    T(1) = T0;
    Y(1, :) = transpose(Y0);
    while T0 < endT
        [Y1, T1] = feval(odefunct, T0, Y0);
        if anyNonNegative
            ndx = nonNegative(Y1(nonNegative) <= 0);
            Y1(ndx) = max(Y1(ndx), 0);
        end
        if fixedsteps
            %save only when time exceeds the next step (we save Y0 = the previous step)
            while step <= length(T) && T1 > T(step)
                Y(step, :) = transpose(Y0);
                step = step + 1;
            end
        else
            %save all points
            if T1 > endT
                T(step) = endT;
                Y(step, :) = transpose(Y0);
                T = T(1:step);
                Y = Y(1:step, :);
            else
                T(step) = T1;
                Y(step, :) = transpose(Y1);
            end
            step = step + 1;
        end
        if haveOutputFcn
            if feval(outputFcn, T1, transpose(Y1), '')
                ndx = ~isnan(Y(:, 1));
                Y = Y(ndx, :);
                T = T(ndx);
                break;
            end
        end
        Y0 = Y1;
        T0 = T1;
    end
end


