function res = kroneckerDelta(i, j)
    if i == j
        res = 1;
    else
        res = 0;
    end
end
