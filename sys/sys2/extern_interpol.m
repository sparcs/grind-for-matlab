classdef extern_interpol
    properties
        name = ''
        default = 0
        dim1 = 1
        dim2 = 1
        cycle = true;
        tofloor = false; %do not interpolate within days (or time units)
        active = true;
    end
    properties (Dependent = true, Hidden = true)
        options
    end
    properties (Hidden = true)
        data = NaN
        nodatawarning = false;
        datarange = [];
        interpolant = [];
    end
    methods (Static=true)
        function c=toobject(c)
             for i=1:length(c)
                c{i}=extern_interpol(c{i});
             end
        end
            
        function c=tostruc(c)
            for i=1:length(c)
                c{i}=struct('name',c{i}.name,'default',c{i}.default,'dim1',...
                    c{i}.dim1,'dim2',c{i}.dim2,'options',c{i}.options,'data',c{i}.data,'nodatawarning',c{i}.nodatawarning);
            end
        end
    end
    
    methods
        function res = get.options(obj)
            res = struct('cycle', obj.cycle, 'tofloor', obj.tofloor, 'active', obj.active);
        end
        function obj=set.options(obj, values)
            obj.cycle = values.cycle;
            obj.tofloor = values.tofloor;
            obj.active = values.active;
        end
        function res = get.data(obj)
            if isequaln(obj.data, NaN)
                try
                    res = [obj.interpolant.GridVectors{1}, obj.interpolant.Values];
                catch
                    res = [];
                end
            else
                res = obj.data;
            end
        end
        %         function obj=set.data(obj, values)
        %             if isempty(values)
        %                 obj.interpolant = [];
        %                 obj.datarange = [];
        %             else
        %                 try
        %                     %problems: 
        %                     %(1) in old matlab versions 'extrapolation method
        %                     %    cannot be used
        %                     %(2) in some versions only one dimension is supported..
        %                     obj.interpolant = griddedInterpolant(values(:, 1), values(:, 2:end), 'linear', 'none');
        %                 catch
        %                     %a bit slower (but with rather fast search for one
        %                     %point)
        %                     obj.interpolant = myinterpolant(values(:, 1), values(:, 2:end));
        %                 end
        %                 obj.datarange = [values(1, 1), values(end, 1)];
        %             end
        %         end
       
        function obj=update_data(obj)
            dat=obj.data;
            if ~isequaln(dat, NaN)
                if isempty(dat)
                    obj.interpolant = [];
                    obj.datarange = [];
                else
                    try
                        %problems: 
                        %(1) in old matlab versions 'extrapolation method
                        %    cannot be used
                        %(2) in some versions only one dimension is supported..
                        obj.interpolant = griddedInterpolant(dat(:, 1), dat(:, 2:end), 'linear', 'none');
                    catch
                        %a bit slower (but with rather fast search for one
                        %point)
                        obj.interpolant = myinterpolant(dat(:, 1), dat(:, 2:end));
                    end
                    obj.datarange = [dat(1, 1), dat(end, 1)];
                    obj.data = NaN;
                end
            end
        end

        function res = getvalue(obj, at)
            %this is the basic function to get interpolated data from the
            %external variable
            %at can be a single time or a vector
            if isa(at, 'sym')
                res = sym(obj.name);
                return;
            end
            if size(at, 1) == 1 && size(at, 2) > 1
                at = at';
            end
            if ~obj.active
                res = evalin('base', obj.name);
                if numel(at) > 1
                    res = transpose(res(:));
                    res = repmat(res, numel(at), 1);
                end
                return;
            end
            %obj.update_data;
            if isempty(obj.datarange)
                if numel(at) == 1
                    res = obj.default;
                else
                    res = repmat(obj.default(:)', numel(at), 1);
                end
                return
            end
            if obj.tofloor
                at = floor(at);
            end
            if obj.cycle
                %cycle t outside of the datarange
                at = mod(at, obj.datarange(2) - obj.datarange(1)) + obj.datarange(1);
            end
            res = obj.interpolant(at);

            if numel(at) == 1
                if numel(res) == 1 && isnan(res)
                    res = obj.default;
                else
                    ndx = isnan(res);
                    res(ndx) = obj.default(ndx);
                    res = reshape(res, obj.dim1, obj.dim2);
                end
            else
                ndx = find(all(isnan(res), 2));
                res(ndx, :) = repmat(transpose(obj.default(:)), length(ndx), 1);
            end
        end
        
        function s = char(obj)
            valu=obj.getvalue(0);
            s=mat2str(valu);
        end
 
        function obj = extern_interpol(strct)
            %constructor, a structure is input (see defextern
            % the external variable can also be a matrix (not yet
            % implemented)
            obj.name = strct.name;
            if isfield(strct, 'default')
                if ischar(strct.default)
                    obj.default = evalin('base', strct.default);
                else
                    obj.default = strct.default;
                end
                if isfield(strct, 'data')
                    obj.data = strct.data;
                end
                if isfield(strct, 'dim1')
                    obj.dim1 = strct.dim1;
                else
                    obj.dim1 = size(obj.default, 1);
                end
                if isfield(strct, 'dim2')
                    obj.dim2 = strct.dim2;
                else
                    obj.dim2 = size(obj.default, 2);
                end
                if isfield(strct, 'active')
                    obj.active = strct.active;
                end
                if isfield(strct, 'nodatawarning')
                    obj.nodatawarning = strct.nodatawarning;
                end
                if isfield(strct, 'cycle')
                    obj.cycle = strct.cycle;
                end
                if isfield(strct, 'floor')
                    obj.tofloor = strct.floor;
                end
                obj=obj.update_data;
            end
        end
    end
end
