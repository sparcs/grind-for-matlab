%GRAPHPLOT   Display the state variables as a network (graph)
%  Make a plot of the interactions between state variables and show the values of the state
%   variables as color scale of the vertices. Use  <a href="matlab:help replayall">replayall</a> to view the time evolution.
%   
%  GRAPHPLOT('argname',argvalue,...) - Valid argument name-value pairs [with type]:
%     'J' [number] - matrix defining the network ([]=Jacobian)
%     'nodelabel' [nr | names | degree | outdegree | indegree | closeness | incloseness | outcloseness | betweenness | pagerank | eigenvector | hubs | authority | degree_rank | outdegree_rank | indegree_rank | closeness_rank | incloseness_rank | outcloseness_rank | betweenness_rank | pagerank_rank | eigenvector_rank | hubs_rank | authority_rank] - labels for nodes
%     'nodelabel_digits' [number] - number of digits for node label,2
%     'nodeweight' [number] - coloring of the nodes []=state variables
%     'rotation' [number] - angle of rotation of the graph plot
%     't' [number] - plot time step t, NaN=to end
%     'threshold' [number] - threshold for interaction in Jacobian (default=1E-10)
%     'type' [digraph | graph | auto] - type of graph digraph/graph
%   GRAPHPLOT('-opt1','-opt2',...) - Valid command line options:
%
%   See also replayall, vectplot, viewcells
%
%   Reference page in Help browser:
%      <a href="matlab:commands('graphplot')">commands graphplot</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:37 $
function graphplot(varargin)
    global g_Y g_t t g_grind;
    % g_t = (g_t(1):g_t(2):g_t(3))';

    fieldnams = {'J', 'n', 'matrix defining the network ([]=Jacobian)', [];  ...
        'type', 'e[digraph|graph|auto]', 'type of graph digraph/graph', 'auto';  ...
        'rotation', 'n', 'angle of rotation of the graph plot', [];  ...
        'nodelabel', 'e[nr|names|degree|outdegree|indegree|closeness|incloseness|outcloseness|betweenness|pagerank|eigenvector|hubs|authority|degree_rank|outdegree_rank|indegree_rank|closeness_rank|incloseness_rank|outcloseness_rank|betweenness_rank|pagerank_rank|eigenvector_rank|hubs_rank|authority_rank]', 'labels for nodes', 'nr';  ...
        'nodelabel_digits','n','number of digits for node label,2',2;...
        'nodeweight','n','coloring of the nodes []=state variables',[];...
        'threshold', 'n', 'threshold for interaction in Jacobian (default=1E-10)', 1E-10;  ...
        't', 'n', 'plot time step t, NaN=to end', nan}';
    args = i_parseargs(fieldnams, 't', '', varargin);
    if ~isfield(args, 't')
        args.t = 0;
    end
    if ~isfield(args, 'rotation')
        args.rotation = [];
    end
    if ~isfield(args, 'threshold')
        args.threshold = 1E-10;
    end
    
    N0 = i_initvar;
    if i_settingschanged(N0)
        i_ru(t, g_grind.ndays, N0, 1);
    end
    if ~isfield(args, 'J')
        args.J = i_calcjac;
    end
    M = abs(args.J) > args.threshold;
    if ~isfield(args, 'type')
        args.type = 'auto';
    end
    if ~isfield(args, 'thegraph') || isempty(args.thegraph) && ~isempty(args.M)
        if strcmp(args.type, 'auto')
            if issymmetric(M)
                thetype = 'graph';
            else
                thetype = 'digraph';
            end
        else
            thetype = args.type;
        end
        if strcmp(thetype, 'digraph')
            args.thegraph = digraph(setdiagon(M, 0));
        else
            if ~issymmetric(M)
                M = tril(M, -1) + tril(M, -1)';
            end
            args.thegraph = graph(setdiagon(M, 0));
        end
    end

    tndx = find(g_t >= args.t, 1);
    the_t = g_t(tndx);

    hfig = i_makefig('torus');
 
    

    htime = findobj(hfig, 'tag', 'timelabel');
    if isempty(htime)
        figure(hfig)
        hax1 = findobj(hfig, 'type', 'axes');
        h = plot(args.thegraph, 'NodeCdata', real(g_Y(tndx, :)), 'MarkerSize', 10, 'tag', 'graphplot');
        %    clim = get(hax1, 'CLim');
        htime = text(0.03, 0.05, sprintf('t=%d', round(the_t)), 'units', 'normalized', 'tag', 'timelabel');
        colormap parula
        hcol = colorbar;
        %  ylabel(hcol, colorbar_label);
        addreplaydata(hfig, args.thegraph);
        replayall
    else
        figure(hfig);
        hax1 = findobj(hfig, 'type', 'axes');
        h = findobj(hfig, 'tag', 'graphplot');
        clim = get(hax1, 'CLim');
        set(h, 'NodeCdata', real(g_Y(tndx, :)));
        set(hax1, 'Clim', clim);
        set(htime, 'string', sprintf('t=%d', round(the_t)));
    end

    if ~isempty(args.rotation) && abs(args.rotation) > 0
        rotate_series(h, args.rotation);
    end
    if isfield(args, 'nodeweight')&&~isempty(args.nodeweight)
        set(h, 'NodeCdata', args.nodeweight);
         clim = [min(args.nodeweight), max(args.nodeweight)];
         if clim(1)>=clim(2)
             clim(2)=clim(1)+0.1;
         end
        set(hax1, 'Clim', clim);
    end
      hax1 = findobj(hfig, 'type', 'axes');
    if isfield(args, 'nodelabel')
        ud = get(hax1, 'userdata');
        if isempty(ud)
            disp('c')
        end
        c=[];
        if ischar(args.nodelabel)
            rank=false;
            if strcontains(args.nodelabel,'_rank')
                rank=true;
                args.nodelabel=args.nodelabel(1:end-6);
            end
            switch args.nodelabel
                case 'nr'
                    c = 1:numel(N0);
                case 'names'
                    c = i_statevars_names;

                otherwise
                    try
                        c = centrality(ud.graph, args.nodelabel);
                    catch err
                        if strcmp(err.identifier, 'MATLAB:unrecognizedStringChoice')
                            if isa(ud.graph, 'graph')
                                error('grind:graphplot', '%s\n%s',err.message,'This option is only valid for digraphs');
                            else
                                error('grind:graphplot', '%s\n%s',err.message,'This option is only valid for graphs');
                            end
                        end
                    end
            end

            
        else
            c = args.nodelabel;
        end
        if isempty(c)
            disp('c')
        elseif isnumeric(c)
           % if isinteger(c{1})
          %  c = cellfun(@(x)int2str(x), num2cell(c), 'UniformOutput', false);
          %  else
            if rank
                [~,c]=sort(c);
            end
            if ~isfield(args, 'nodelabel_digits')
                args.nodelabel_digits=2;
            end
                
            c = cellfun(@(x)num2str(x,args.nodelabel_digits), num2cell(c), 'UniformOutput', false);
          %  end
        end
        set(h, 'nodeLabel', c);
    end
    
    %     N = find(abs(diff(sum(g_Y, 2))) < 1E-8, 1);
    %     if isempty(N)
    %         N = size(g_Y, 1);
    %     end
end


function addreplaydata(hfig, thegraph)
    global g_t; %#ok<GVMIS>
    %for i = 1:length(hfig)
    hax = findobj(hfig, 'type', 'axes');
    %  tags = get(hax, 'tag');
    %  hax = hax(~(strcmp(tags, 'legend') | strcmp(tags, 'Colorbar')));
    ud = get(hax, 'userdata');
    ud.replay.callback = @replaycallback;
    ud.replay.onstart = @onstart;
    ud.replay.onend = @onend;
    ud.replay.settings = struct('tvar', 't', 'tlim', [g_t(1) g_t(end)], 'numt', length(g_t));
    ud.replay.no = 1;
    ud.graph = thegraph;
    set(hax, 'userdata', ud);
    %end
end

function onend(hax, flag)
    global g_t;
    graphplot('t', g_t(end));
    hh = findobj(hax, 'tag', 'where');
    delete(hh);
end

function onstart(hax)
    global g_t; %#ok<GVMIS>
    if ishandle(hax)
        ud = get(hax, 'userdata');
        if ~isempty(g_t)
            ud.replay.settings = struct('tvar', 't', 'tlim', [g_t(1) g_t(end)], 'numt', length(g_t));
        end
        set(hax, 'userdata', ud);
        i_figure(get(hax, 'parent'));
    end
end
function t = replaycallback(hax, avar, relt)
    global g_grind; %#ok<GVMIS>
    t = [];
    if ishandle(hax) && isfield(g_grind, 'viewcells') && isempty(avar) || strcmp(avar, 't')
        ud = get(hax, 'userdata');
        t = ud.replay.settings.tlim(1) + relt * (ud.replay.settings.tlim(end) - ud.replay.settings.tlim(1));
        graphplot('t', t);

    end
    %*******************Replay paranal**************************
    %
    %
end

function rotate_series(h, theta, about_point)
    % rotate a plotted series around a point
    % rotate_series(h,theta,about_point)
    % theta = vector of max 3 rotation angles (radians):
    % theta(1) = angle to rotate around z-axis (so in x-y plane)
    % theta(2) = angle to rotate around y-axis (so in x-z plane)
    % theta(3) = angle to rotate around x-axis (so in y-z plane)
    % default theta(2)=0, theta(3)=0
    if nargin < 3
        about_point = [];
    end
    if isnumeric(h)
        %if h is a number we take the current gca, with the first numerical
        %series
        if nargin == 2
            about_point = theta;
        end
        theta = h;
        h = get(gca, 'children');
        h = findobj(h, '-property', 'xdata');
        %  h=h(1);
    elseif numel(h) == 1 && any(strcmp(get(h, 'type'), {'axes', 'figure'}))
        h = findobj(h, '-property', 'xdata');
    end
    for k = 1:length(h)
        data = [get(h(k), 'xdata'); get(h(k), 'ydata'); get(h(k), 'zdata')];
        if size(data, 1) == 2
            data = [data; zeros(1, size(data, 2))];
        end
        if isempty(about_point)
            about_point = mean(data, 2);
        end
        theta = [theta(:); zeros(3 - length(theta), 1)];
        about_point = [about_point(:); zeros(3 - length(about_point), 1)];
      
        %for simplicity change the coordinates to the about_point
        data = data - about_point;
        %rotate around z-axis (in x-y plane)
        A = [cos(theta(1)), -sin(theta(1)), 0;  ...
            sin(theta(1)), cos(theta(1)), 0;  ...
            0, 0, 1];
        data = A * data;
        %rotate around y-axis (in x-z plane)
        A = [cos(theta(2)), 0, sin(theta(2));  ...
            0, 1, 0;  ...
            -sin(theta(2)), 0, cos(theta(2))];
        data = A * data;
        %rotate around x-axis (in y-z plane)
        A = [1, 0, 0;  ...
            0, cos(theta(3)), sin(theta(3));  ...
            0, -sin(theta(3)), cos(theta(3))];
        data = A * data;
        
        %change the coordinates back
        data = data + about_point;
        set(h(k), 'xdata', data(1, :));
        set(h(k), 'ydata', data(2, :));
        set(h(k), 'zdata', data(3, :))
    end
end
