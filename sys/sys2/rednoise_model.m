classdef rednoise_model < handle
    %fast rednoise model to be used in GRIND (use command rednoise in model).
    % data could become very big if you have one set with very small time step
    % realy fast if all deltat are equal.
    
    %TODO
    %   -non-autonomous rednoise models (difficult)
    properties
        t0
        data
        buffsize
        T0
        Tinit
        alpha
        beta
        deltat
        funargs
        autonomous
        active
    end
    methods (Static=true)
        function lambda=alpha2lambda(alpha)
            lambda=1./(1-alpha);
        end
            
    end
    methods
        function obj = rednoise_model(amod)
            if nargin<1
                amod=[];
            end
            obj.clear;
            if ~isempty(amod)
                obj.active=amod.active;
                obj.t0=amod.t0;
                obj.Tinit=[];
                obj.buffsize=amod.buffsize;
                obj.T0=amod.T0;
                obj.alpha=amod.alpha;
                obj.beta=amod.beta;
                obj.deltat=amod.deltat;
                obj.funargs=amod.funargs;
                obj.autonomous=amod.autonomous;
            else
                obj.active=true;
                obj.reset;
            end
        end
        
               
        function reset(obj,Tinitial)
            global t g_grind;
            if nargin>1
                obj.Tinit=Tinitial;
            end
            obj.t0 = t;
            obj.data = [];
            if ~isempty(obj.funargs)
                obj.clear(true);
                if any([obj.funargs(:).hasaux])
                    %run aux variable and save the current values
                    funnames = g_grind.funcnames.names;
                    funvalues = cell(size(funnames));
                    vars = evalin('base', 'whos');
                    exists = ismember(funnames, {vars(:).name});
                    for i = 1:length(funnames)
                        if exists(i)
                            funvalues{i} = evalin('base', funnames{i});
                        end
                    end
                    evalin('base', g_grind.funcs);
                end
                args = obj.funargs;
                for i = 1:numel(args)
                    for j = 1:length(args(i).arguments)
                        if ~isempty(args(i).arguments{j}) && ischar(args(i).arguments{j})
                            args(i).arguments{j} = evalin('base', args(i).arguments{j});
                        end
                        if ~obj.autonomous
                            %nonautom=~cellfun('isempty', regexp(args.arguments, '\<t\>'));
                            for k = 1:length(args(i).functs)
                                if ~islogical(args(i).functs{k}) || args(i).functs{k}
                                    obj.funargs(i).functs{k} = evalin('base', sprintf('@(t)%s', obj.funargs(i).arguments{k}));
                                end
                            end
                        end
                    end
                    res = obj.addset(args(i).arguments{:});
                    obj.funargs(i).ndx = res;
                end
                if any([obj.funargs(:).hasaux])
                    for i = 1:length(funnames)
                        %reset the original state of the aux variables
                        if exists(i)
                            assignin('base', funnames{i}, funvalues{i});
                        else
                            evalin('base', sprintf('clear(''%s'');', funnames{i}));
                        end
                    end
                end
                obj.fillbuffer;
            end
        end
        
        function clear(obj, onreset)
            if nargin < 2
                onreset = false;
            end
            obj.data = [];
            obj.T0 = [];
            obj.alpha = [];
            obj.beta = [];
            obj.deltat = [];
            if ~onreset
                obj.t0 = 0;
                obj.buffsize = 1000;
                obj.active = true;
                obj.funargs = struct('iset', {}, 'ndx', {}, 'arguments', [], 'functs', [], 'hasaux', []);
                obj.autonomous = true;
            end

        end
        
        function eqnr = addargs(obj, args, hasaux)
            if nargin < 3
                hasaux = false;
            end
            if length(args) < 5
                iset = '1';
                args{5} = '1';
            else
                iset = args{5};
            end
            isetnr = str2double(iset);
            if isnan(isetnr)
                 error('grind:rednoise', 'Rednoise - set number "%s" cannot be a parameter or equation, it should be an unique integer',iset)
            end              
            if numel(isetnr) > 1
                error('grind:rednoise', 'Vector set number in rednoise no longer allowed, use unique number and make one of the other arguments a vector')
            end
            eqnr = length(obj.funargs) + 1;
            has_no_t = cellfun('isempty', regexp(args(2:end), '\<t\>'));
            has_iset = cellfun(@(x)any(find(strcmp(x,iset))==4), {obj.funargs(:).arguments}, 'UniformOutput', true);
            if any(has_iset)
                eqnr = find(has_iset);
                args1 = args(2:end);
                if length(args1) ~= length(obj.funargs(eqnr).arguments) || any(~strcmp(args1, obj.funargs(eqnr).arguments))
                    s = sprintf('%s,', args1{:});
                    s1 = sprintf('%s,', obj.funargs(eqnr).arguments{:});
                    error('grind:rednoise', 'Rednoise: if a set is used twice, the arguments should be exactly the same,\nCheck rednoise(t,%s) vs. rednoise(t,%s)', ...
                        s(1:end - 1), s1(1:end - 1))
                else
                    return
                end
            end

            autonom = all(has_no_t);
            obj.funargs(end + 1, :) = struct('iset', isetnr, 'ndx', [], 'arguments', {args(2:end)}, 'functs', {num2cell(~has_no_t)}, 'hasaux', hasaux);
            if ~autonom
                obj.autonomous = false;
                %warning('nonautonomous rednoise is not yet implemented')
            end
        end

        function ndx = addset(obj, T0, lambda, beta, iset, deltat)
            if nargin < 6 || isempty(deltat)
                deltat = 1;
            end
            if nargin < 5
                iset = 1;
            end
            ndx = numel(obj.T0) + 1;
            siz = max([numel(T0), numel(lambda), numel(beta), numel(iset), numel(deltat)]);
            ndx = ndx:ndx + (siz - 1);
            if siz > 1
                try
                    if numel(T0) == 1
                        T0 = T0 + zeros(1, siz);
                    else
                        ndx = reshape(ndx, size(T0));
                        T0 = T0(:).';
                    end
                    if numel(lambda) == 1
                        lambda = lambda + zeros(1, siz);
                    else
                        ndx = reshape(ndx, size(lambda));
                        lambda = lambda(:).';
                    end
                    if numel(beta) == 1
                        beta = beta + zeros(1, siz);
                    else
                        ndx = reshape(ndx, size(beta));
                        beta = beta(:).';
                    end
                    if numel(deltat) == 1
                        deltat = deltat + zeros(1, siz);
                    else
                        ndx = reshape(ndx, size(deltat));
                        deltat = deltat(:).';
                    end
                catch err
                    if strcmp(err.identifier, 'MATLAB:getReshapeDims:notSameNumel')
                        iset = int2str(iset);
                        eqnr = find(cellfun(@(x)any(find(strcmp(x,iset))==4), {obj.funargs(:).arguments}, 'UniformOutput', true));
                        s1 = sprintf('%s,', obj.funargs(eqnr).arguments{:});
                        error('grind:rednoise', 'The arguments (T0, lambda, beta) of rednoise are of different sizes\nrednoise(t,%s)', s1(1:end - 1))
                    else
                        rethrow(err)
                    end
                end
            end
            for i = ndx(end): -1:ndx(1)
                obj.T0(i) = T0(i - min(ndx) + 1);
                obj.alpha(i) = 1 - 1 / lambda(i - min(ndx) + 1);
                obj.beta(i) = beta(i - min(ndx) + 1);
                obj.deltat(i) = deltat(i - min(ndx) + 1);
                obj.data = [];
            end
            obj.deltat(obj.deltat == 0) = 1;
            if all(obj.deltat == obj.deltat(1))
                obj.deltat = obj.deltat(1);
            end
        end
        
        function res = getfunctvalue(obj, ts, aiset)
            %the same as getvalue but the data are not updated on errors. This is
            %therefore used in equations/aux vars after a run (and during reset)
            %the missing values are filled with NaN
            if nargin < 3
                aiset = 1;
            end
            eqnr = find([obj.funargs(:).iset] == aiset);
            if isempty(eqnr)
                error('grind:rednoise', 'Unknown set number');
            end
            res = obj.getvalue(ts, eqnr, false);
        end
    
        function res = getvalue(obj, ts, eqnr, update)
            if nargin < 3
                eqnr = 1;
            end
            if nargin < 4
                %if needed fill automatically the buffer
                update = true;
            end
            if isempty(obj.funargs)
                ndxs=eqnr;
            else
                ndxs = obj.funargs(eqnr).ndx;
            end
            if ~obj.active
                if isempty(obj.T0)
                    res = [];
                else
                    res = repmat(obj.T0(ndxs), length(ts), 1);
                end
            else
                try
                    if numel(obj.deltat) == 1
                        res = obj.data(floor((ts - obj.t0) ./ obj.deltat) + 1, ndxs);
                    elseif numel(ndxs) == 1
                        res = obj.data(floor((ts - obj.t0) ./ obj.deltat(ndxs)) + 1, ndxs);
                    elseif all(obj.deltat(ndxs) == obj.deltat(ndxs(1)))
                        %faster for large vectors, within the eqnr usually all deltat's are the same
                        res = obj.data(floor((ts - obj.t0) ./ obj.deltat(ndxs(1))) + 1, ndxs);
                    else
                        %muuch slower, cannot combine different isets
                        res = zeros(numel(ts), numel(ndxs));
                        for j = 1:length(ts)
                            %faster than vectorized this way:
                            res(j, :) = diag(obj.data(floor((ts(j) - obj.t0) ./ obj.deltat(ndxs)) + 1, ndxs));
                            %for k=1:length(ndxs)
                            %    res(j, k) = obj.data(floor((ts(j) - obj.t0) ./ obj.deltat(ndxs(k))) + 1, ndxs(k));
                            %end
                        end
                    end
                    if size(ts, 1) == 1 && size(ts, 2) > 1 && size(res, 1) > 1
                        res = res.';
                    end
                    if numel(res) > 1 && numel(ts) == 1
                        res = reshape(res, size(ndxs));
                    end
                catch err
                    if ~update
                        res = NaN + zeros(numel(ts), numel(ndxs));
                        for j = 1:length(ts)
                            for k = 1:numel(ndxs)
                                try
                                    res(j, k) = obj.data(floor((ts(j) - obj.t0) ./ obj.deltat(ndxs(k)) + 1), ndxs(k));
                                catch
                                end
                            end
                        end
                    else
                        if isempty(obj.data)||max(ts(:)) >= round(size(obj.data, 1) * min(obj.deltat)) - obj.t0 - obj.buffsize || min(ts(:)) < obj.t0
                            %prevents infinite loop!
                            obj.fillbuffer(ts);
                            res = obj.getvalue(ts, eqnr);

                        else
                            rethrow(err)
                        end
                    end
                end
            end
        end

        function fillbuffer(obj, ts)
            %fill the buffer with outcomes
            %model:
            % T(t) = alpha * (T(t-1) - T0) + T0 + beta*randn
            %backwards same model
            %
            function resul = runrednoise(nsteps, x0, direction)
                if nargin < 3
                    direction = 1;
                end
                siz = [nsteps, numel(obj.T0)];
                resul = zeros(siz);
                resul(1, :) = x0;
                if ~obj.autonomous
                    alpha1 = repmat(obj.alpha, siz(1), 1);
                    beta1 = repmat(obj.beta, siz(1), 1);
                    T01 = repmat(obj.T0, siz(1), 1);
                    tgrid = 0:direction:(nsteps - 1);
                    for j1 = 1:length(obj.funargs)
                        %run the functions with t
                        ndx1 = obj.funargs(j1).ndx;
                        t1 = tgrid .* obj.deltat(ndx1(1)) + obj.t0 + size(obj.data, 1) * obj.deltat(ndx1(1));
                        if ~islogical(obj.funargs(j1).functs{1})
                            T01(:, ndx1) = obj.funargs(j1).functs{1}(t1);
                        end
                        if ~islogical(obj.funargs(j1).functs{2})
                            alpha1(:, ndx1) = 1 - 1 ./ obj.funargs(j1).functs{2}(t1);
                        end
                        if ~islogical(obj.funargs(j1).functs{3})
                            beta1(:, ndx1) = obj.funargs(j1).functs{3}(t1);
                        end
                    end
                    gamma = (1 - alpha1) .* T01;
                    noise = beta1 .* randn(siz) + gamma;
                    for i1 = 2:siz(1)
                        resul(i1, :) = alpha1(i1 - 1, :) .* resul(i1 - 1, :) + noise(i1 - 1, :);
                    end
                else
                    gamma = (1 - obj.alpha) .* obj.T0;
                    if siz(2) > 1
                        noise = repmat(obj.beta, siz(1), 1) .* randn(siz) + repmat(gamma, siz(1), 1);
                    else
                        noise = obj.beta * randn(siz) + gamma;
                    end
                    resul(1, :) = x0;
                    for i1 = 2:siz(1)
                        resul(i1, :) = obj.alpha .* resul(i1 - 1, :) + noise(i1 - 1, :);
                    end
                end
            end
            if nargin < 2
                ts = 0;
            end
            t_0 = min(ts);
            if isempty(obj.data)
                obj.t0 = t_0;
                Tinitial=obj.Tinit;
                if isempty(Tinitial)
                    Tinitial=obj.T0;
                end
                obj.data = runrednoise(round((max(ts(:)) - obj.t0) / min(obj.deltat)) + obj.buffsize, Tinitial);
            else
                if obj.t0 > t_0
                    %run backwards
                    %obj.t0 = t_0;
                    dat = runrednoise(obj.t0 - t_0 + 1, obj.data(1, :), -1);
                    obj.t0 = t_0;
                    obj.data = [flipud(dat(2:end, :)); obj.data];
                end
                ndat = round((max(ts(:)) - obj.t0) / min(obj.deltat)) + obj.buffsize;
                if ndat > 0
                    dat = runrednoise(ndat, obj.data(end, :));
                    obj.data = [obj.data; dat(2:end, :)];
                end
            end
        end
    end
end

