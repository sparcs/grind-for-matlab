classdef sym_jacobian
    properties
        name = '';
        size = [];
        kind = 1 %1=empty, 2=structure, 3=function handle
        unique = struct('equation', {}, 'sym', {}, 'indices', []);
        handle = [];
        dx = []; %just for displaying
        dy = []; %just for displaying
    end
    properties (Dependent, Hidden)
        s
    end

    methods
 
        function obj = sym_jacobian(name)
            obj.name = name;
            obj.kind = 1;
        end
 
        function obj = set.s(obj, astruct)
            if isfield(astruct, 'ishandle') && astruct.ishandle
                obj.handle = astruct.handle;
                if isfield(astruct, 'size')
                    obj.size = astruct.size;
                end
                if isfield(astruct, 'name')
                    obj.name = astruct.name;
                end
            elseif isempty(astruct)
                obj = obj.reset;
                obj.kind = 2;
            else
                obj.kind = 2;
                obj.unique = astruct.unique;
                if isfield(astruct, 'size')
                    obj.size = astruct.size;
                end
                if isfield(astruct, 'name')
                    obj.name = astruct.name;
                end
            end
        end
 
        function astruct = get.s(obj)
            if isempty(obj)
                astruct = [];
            elseif obj.ishandle
                astruct = struct('size', obj.size, 'name', obj.name, 'ishandle', true, 'handle', obj.handle);
            else
                astruct = struct('size', obj.size, 'name', obj.name, 'ishandle', false, 'unique', obj.unique);
            end
        end
 
        function obj = reset(obj)
            obj.unique = struct('equation', {}, 'sym', {}, 'indices', []);
            obj.handle = [];
            obj.kind = 1;
        end
        
        function obj = setkind(obj, n)
            obj.kind = n;
        end
        
        function obj = set.handle(obj, ahandle)
            if ~isempty(ahandle)
                obj = obj.setkind( 3);
            elseif ishandle(obj)
                obj = obj.setkind(1);
            end
            obj.handle = ahandle;
        end
        
        function obj = set.unique(obj, aunique)
            if ishandle(obj)
                obj = obj.setkind(1);
            else
                obj = obj.setkind(2);
            end
            %             if ~isempty(aunique)
            %                 obj = obj.setkind( 2);
            %             elseif ~ishandle(obj)
            %                 obj = obj.setkind(1);
            %             end
            obj.unique = aunique;
        end
        
        function res = ishandle(obj)
            res = obj.kind == 3;
        end
        
        function res = isempty(obj)
            res = obj.kind == 1;
        end
 
        function h = funhandle(obj, parslist) %makes function handle assuming all parameters are in the base 
            if nargin < 2
                parslist = '';
            end
            switch obj.kind
                case 1
                    h = [];
                case 2
                    %te doen!!!: replace statevars in equations
                    c = {obj.unique(:).equation};
                    s1 = sprintf('%s,', c{:});
                    %binding the parameters from the base workspace to the
                    %equations
                    equats=evalin('base',sprintf('@(t,g_X1%s){%s}',parslist,s1(1:end - 1))); %#ok<NASGU>
                    %binding the current object to i_runsymstruc (can be
                    %improved)
                    h = eval(sprintf('@(t,g_X1%s)i_runsymstruc(obj.s,max(size(t,2),size(A,2)),equats(t,g_X1%s))', parslist,parslist));
                case 3
                    h = obj.handle;
            end
        end
 
        function res = sym(obj)
            switch obj.kind
                case 1
                    res = sym([]);
                case 2
                    res = struct2full(obj, true);
                case 3
                    res = [];
            end
        end
 
        function res = mysym(obj)
            res = mysym(obj.sym);
        end
 
        function res = cell(obj)
            switch obj.kind
                case 1
                    res = {};
                case 2
                    res = struct2full(obj, false);
                case 3
                    res = {};
            end
        end
        
        function obj1 = simplify(obj)
            obj1 = obj;
            if obj.kind == 2
                for i = 1:length(obj1.unique)
                    obj1.unique(i).sym = simplify(obj1.unique(i).sym);
                end
            end
        end
        
        function disp(obj)
            % function jac = plotjac(name, jac, dx, dy, dhess)
            %     if nargin < 5
            %         dhess = '';
            %     end
            if isempty(obj)
                fprintf('No %s defined\n', obj.name)
                %    elseif ishandle(jac)
                %        fprintf('Symbolic %s defined   @%s\n', name, func2str(jac))
            else
                s1 = sprintf('%d,', obj.size);
                fprintf('%s = zeros(%s)\n', obj.name, s1(1:end - 1))
                if obj.ishandle
                    disp(func2str(obj.handle))
                else
                    for i = 1:numel(obj.unique)
                        u = obj.unique(i);
                        for j = 1:size(u.indices) %#ok<CPROP>
                            c = {u.equation};
                            %c = i_enterjacdlg('jac2str', {u.equation});
                            if numel(obj.size) <= 2 && ~isempty(obj.dx)
                                [i1, j1] = ind2sub(obj.size, u.indices(j));
                                if isempty(obj.dy{i1})
                                    fprintf('%s = %s\n', obj.dx{j1}, c{1});
                                else
                                    fprintf('d(%s)/d(%s) = %s\n', obj.dx{i1}, obj.dy{j1}, c{1});
                                end
                            else
                                fprintf('%s(%s) = %s\n', obj.name, index2sub(obj.size, u.indices(j)), c{1})
                            end
                        end
                    end
                end

            end
            
        end
    end
end


function full = struct2full(jac, makesym)
    if nargin == 1
        makesym = false;
    end
    n = length(jac.unique);
    if makesym
        full = mysym(zeros(jac.size));
        for i = 1:n
            if ~isfield(jac.unique(i), 'sym')
                full(jac.unique(i).indices) = mysym(mystr2sym(jac.unique(i).equation));
            else
                full(jac.unique(i).indices) = jac.unique(i).sym;
            end
        end
    else
        full = cell(jac.size);
        full(:) = {'0'};
        for i = 1:n
            full(jac.unique(i).indices) = {jac.unique(i).equation};
        end
    end
end

function s = index2sub(siz, index)
    a = cell(size(siz));
    [a{1:end}] = ind2sub(siz, index);
    s = sprintf('%d,', a{:});
    s = s(1:end - 1);
end
