%FOKKERPLANCK   Analyse Fokker-Planck equation based on 1D differential equation
%   By using a Fokker-Planck equation you can analyse the time-evolution of 
%   the probability density function of a stochastic differential equation. 
%   This command only works for one-equation differential equations.
%   Assume the following stochastic differential equation (Langevin equation):
%   dX = F(X) dt + sigma dW
%   where X is a state variable, F(X) is the deterministic part and dW is the 
%   Wiener process (Gaussian white noise) times the function sigma.
%   The Fokker-Planck PDE is then defined as:
%   dP/dt=- d(F(X) P)/dX + sigma^2/2*dP^2/d^2X
%   P is the probability density function.
%   We solve this PDE using MATLAB's pdepe function with absorbing or reflecting
%   boundaries. Use simtime to define the simulation time, the initial 
%   condition of the original model is used(g_fokkerplanck.p0 can be used 
%   to define other than default initial conditions)
%   Additionally this function calculates the median exit time. 
%      
%   Usage:
%   FOKKERPLANCK - opens a dialog box in which you can enter all necessary 
%   information.
%   FOKKERPLANCK SIGMA - sigma is defined, for the rest use default values.
%   FOKKERPLANCK('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'diffsigma' [string] - derivative of sigma to x (leave blanc for numerical solving) (default '')
%     'domain' [number and length(number)==2] - the limits of the statevariable (default defined in ax or [0 10])
%     'maxsurvtime' [integer>0] - the time range for survival time (default 50)
%     'nx' [integer>0] - the number of points for the probability density function (default=300)
%     'sigma' [string or number] - the standard deviation of the noise - can be an equation (default 1)
%   FOKKERPLANCK('-opt1','-opt2',...) - Valid command line <a href="matlab:commands func_args">options</a>:
%     '-p' - plot: redraw the figures of the last run.
%
%  
%   See also potential, plotdiff, <a href="matlab:help pdepe">pdepe</a>  
%
%   Reference page in Help browser:
%      <a href="matlab:commands('fokkerplanck')">commands fokkerplanck</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:37 $
function fokkerplanck(varargin)
    evalin('base', 'global g_langevin_eq');
    global g_langevin_eq g_grind; %#ok<GVMIS>
    if isfield(g_grind, 'xaxis')
        theax = g_grind.xaxis.lim;
    else
        theax = [0 10];
    end
    fieldnams = {'sigma', 's#n', 'the standard deviation of the noise - can be an equation (default 1)', '1'; ...
        'diffsigma', 's', 'derivative of sigma to x (leave blanc for numerical solving) (default '''')', ''; ...
        'nx', 'i>0', 'the number of points for the probability density function (default=300)', 300; ...
        'domain', 'n&length(n)==2', 'the limits of the statevariable (default defined in ax or [0 10])', theax; ...
        'maxsurvtime', 'i>0', 'the time range for survival time (default 50)', 50}';
    args = i_parseargs(fieldnams, 'sigma', '-p', varargin);
    if g_grind.statevars.dim > 1 || g_grind.solver.isdiffer
        error('grind:fokkerplanck:nodiffer', 'This command works only for 1D differential equations');
    end
    if g_grind.solver.nonautonomous || g_grind.solver.isstochastic && ~(isfield(g_grind.solver, 'dwiener') && i_split_sde) 
        error('grind:fokkerplanck:nonautonomous', 'This command works only for autonomous deterministic 1D differential equations');
    end
    if any(strcmp(args.opts, '-p'))
        g_langevin_eq.plot;
        return;
    end
    if isempty(g_langevin_eq)
        g_langevin_eq=grind_langevin_eq;
        g_langevin_eq.set('domain',theax);
    end
    if i_split_sde
        var=i_statevars_names(1);
        g_langevin_eq.set('sigma',vectorize(regexprep(g_grind.syms.diffusion,'\<g_X1\(1,:\)\>',var))...
            ,'f',vectorize(regexprep(g_grind.syms.drift,'\<g_X1\(1,:\)\>',var)));
    end
    if nargin == 0
        args = i_parseargdlg('fokkerplanck', fieldnams, {}, '', struct('sigma',g_langevin_eq.sigma,...
            'diffsigma',g_langevin_eq.diffsigma,'nx',g_langevin_eq.nx,'domain',g_langevin_eq.domain,'maxsurvtime',g_langevin_eq.maxsurvtime));
        if isempty(args)
            error('grind:fokkerplanck:canceled', 'Canceled by user');
        end
    end 
    g_langevin_eq.set(args);
    g_langevin_eq.update;
    g_langevin_eq.plot;
end
