%TORUS   Polar coordinates plot 
%  Create a time plot using polar coordinates. The first two axis of the phase plane.
%
%   Usage:
%   TORUS - create a torus plot with a period of 365 time units. The x axis starts at -1.
%   TORUS PERIOD XSTART - create torus plot with a period of PERIOD steps. The x axis 
%   starts at -XSTART.           
%   TORUS('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'hax' [general] - handle of the axis to write
%     'period' [number>0] - the period of the torus.
%     't' [number] - plot till time t, NaN=to end
%     'xstart' [number] - the x axis starts at -XSTART
%   
%   
%   See also ru, ax, replayall    
%
%   Reference page in Help browser:
%      <a href="matlab:commands('torus')">commands torus</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:39 $
function torus(varargin)
    %(period,increment)
    global g_t g_Y t g_grind;
    fieldnams = {'period', 'n>0', 'the period of the torus.', 365;  ...
        'xstart', 'n', 'the x axis starts at -XSTART', 1;  ...
        'hax', '', 'handle of the axis to write', [];  ...
        't', 'n', 'plot till time t, NaN=to end', nan}';
    args = i_parseargs(fieldnams, 'period,xstart', '', varargin);
    i_parcheck;
    if ~isfield(args, 'period')
        args.period = 365;
    end
    if ~isfield(args, 'xstart')
        args.xstart = 1;
    end
    if ~isfield(args, 'hax')
        args.hax = [];
    end

    N0 = i_initvar;
    if i_settingschanged(N0)
        i_ru(t, g_grind.ndays, N0, 1);
    end
    iX = i_getno(g_grind.xaxis.var);
    iY = i_getno(g_grind.yaxis.var);
    if ~iX.isvar || ~iY.isvar
        error('GRIND:torus:NoStatevars', 'Error: there are no state variables on the axes, use ax to set the first 2 axes');
    end
    if isempty(args.hax)
       hfig = i_makefig('torus');
       args.hax=gca;
    else
        hfig=get(args.hax,'parent');
    end
    hld = ishold(args.hax);
    
    if isfield(args, 't')
        ft = find(g_t >= args.t);
        if isempty(ft)
            ft = length(g_t);
        end
        ts = g_t(1:ft, :);
        Ys = g_Y(1:ft, :);
        [X1, Y1, Z1] = toruscoord(ts, Ys(:, iX.no), Ys(:, iY.no), args.period, args.xstart);
        hh = findobj(hfig, 'tag', 'dataline');
        if isempty(hh)
            [X2, Y2, Z2] = toruscoord(g_t, g_Y(:, iX.no), g_Y(:, iY.no), args.period, args.xstart);
            plottorus(hfig, X2, Y2, Z2, args);
        end
        set(hh, 'XData', X1, 'YData', Y1, 'ZData', Z1);
        hh = findobj(hfig, 'tag', 'where');
        
        if isempty(hh)
            hold(args.hax,'on');
            plot3(args.hax,X1(end), Y1(end), Z1(end), 'ro', 'MarkerFaceColor', 'r', 'tag', 'where');
        else
            set(hh, 'XData', X1(end), 'YData', Y1(end), 'ZData', Z1(end));
        end
        return;
    end
    [X1, Y1, Z1] = toruscoord(g_t, g_Y(:, iX.no), g_Y(:, iY.no), args.period, args.xstart);
    plottorus(hfig, X1, Y1, Z1, args);
    us1.period = args.period;
    us1.increment = args.xstart;
    set(hfig, 'userdata', us1);
    addreplaydata(hfig)
    if ~hld
        hold off;
    end
end

function plottorus(hfig, X1, Y1, Z1, args)
    global g_grind;
    hax=findobj(hfig,'type','axes');
    if isempty(hax)
        hax=gca;
    end
    plot3(hax,X1, Y1, Z1, 'tag', 'dataline');
    i_plotdefaults(hfig);
    if ~isoctave && verLessThan('matlab', '8.4.0')
        set(hax, 'drawmode', 'fast');
    else
        set(hax, 'SortMethod', 'depth');
    end
    hold(hax,'on');
 %  hold on;
    box(hax,'off');
    lims = max(abs([get(hax, 'xlim'), get(hax, 'ylim')]));
    zlim = get(hax, 'zlim');
    plot3(hax,[0; 0], [0; 0], get(hax, 'zlim'), 'k')
    plot3(hax,[0; 0], [ - lims, lims], [zlim(1), zlim(1)], 'k');
    plot3(hax,[ -lims, lims], [0; 0], [zlim(1), zlim(1)], 'k');
    xlabel(hax,['sin(t)*' g_grind.xaxis.var]);
    ylabel(hax,['cos(t)*' g_grind.xaxis.var]);
    zlabel(hax,g_grind.yaxis.var);
    t1 = 0:0.05:6.5;
    plot3(hax,sin(t1) * lims, cos(t1) * lims, zlim(1) * ones(1, length(t1)), 'k');
    plot3(hax,sin(t1) * args.xstart, cos(t1) * args.xstart, zlim(1) * ones(1, length(t1)), 'k');
end

function [X1, Y1, Z1] = toruscoord(ts, X, Y, period, xstart)
    ts = ts / period * 2 * pi;
    X1 = sin(ts) .* (xstart + X);
    Y1 = cos(ts) .* (xstart + X);
    Z1 = Y;
end

function addreplaydata(hfig)
    global g_t; %#ok<GVMIS>
    %for i = 1:length(hfig)
        hax = findobj(hfig, 'type', 'axes');
      %  tags = get(hax, 'tag');
      %  hax = hax(~(strcmp(tags, 'legend') | strcmp(tags, 'Colorbar')));
        ud = get(hax, 'userdata');
        ud.replay.callback = @replaycallback;
        ud.replay.onstart = @onstart;
        ud.replay.onend =  @onend;
        ud.replay.settings = struct('tvar', 't', 'tlim', [g_t(1) g_t(end)], 'numt', length(g_t));
        ud.replay.no = 1;
        set(hax, 'userdata', ud);
    %end
end

function onend(hax,flag)
global g_t;
   torus('t',g_t(end),'hax',hax);
   hh = findobj(hax, 'tag', 'where');
   delete(hh);

end

function onstart(hax)
    global g_t; %#ok<GVMIS>
    if ishandle(hax)
        %    N0 = i_initvar;
        %    if i_settingschanged(N0, g_grind.ndays)
        %       i_ru(t, g_grind.ndays, N0, 1);
        %    end
        ud = get(hax, 'userdata');
        if ~isempty(g_t)
            ud.replay.settings = struct('tvar', 't', 'tlim', [g_t(1) g_t(end)], 'numt', length(g_t));
        end
        set(hax, 'userdata', ud);
        i_figure(get(hax, 'parent'));
    end
end
function t = replaycallback(hax, avar, relt)
   % global g_grind; %#ok<GVMIS>
    t = [];
    if ishandle(hax) && strcmp(avar,'t') %|| strcmp(avar, g_paranal.run.pars{1})
        ud = get(hax, 'userdata');
        if ~isfield(ud,'replay')
            disp('');
        end
        t = ud.replay.settings.tlim(1) + relt * (ud.replay.settings.tlim(end) - ud.replay.settings.tlim(1));
        torus('t',t,'hax',hax);
%         ser = get(hax, 'children');
%         if isfield(ud, 'replay')
%             i = ud.replay.no;
%             A1 = real(reshape(outfun(g_grind.viewcells.vars{i}.name, 'times', t), g_grind.viewcells.vars{i}.dims));
%             if g_grind.viewcells.vars{i}.dims(2) == 1
%                 A1 = repmat(A1, 1, 3);
%             end
%             if g_grind.viewcells.vars{i}.dims(1) == 1
%                 A1 = repmat(A1, 3, 1);
%             end
%             if length(ser) == 1
%                 set(ser, 'CData', A1);
%                 if isprop(ser, 'ZData')
%                     set(ser, 'ZData', A1);
%                 end
%             else
%                 set(ser{1}, 'CData', A1);
%                 if isprop(ser, 'ZData')
%                     set(ser(1), 'ZData', A1);
%                 end
%             end
%         end
    end
    %*******************Replay paranal**************************
    %
    %
end

