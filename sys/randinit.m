%RANDINIT   Random initial values
%   Set random initial values. By default the initial values are uniformly distributed,
%   but you can also use uneven distribution (=beta(0.4, 0.4)) or a user function.
%  
%   Usage:
%   RANDINIT - chooses random initial points between 0 and 100.
%   RANDINIT MAX - chooses random initial points between 0 and MAX.
%   RANDINIT [MIN MAX] - chooses random initial points between MIN and MAX.
%   RANDINIT VAR - chooses random initial points of state variable VAR between 0 and 100.
%   RANDINIT VAR [MIN MAX] - chooses random initial points of VAR between MIN and MAX.
%   RANDINIT VAR MAX - chooses random initial points of VAR between 0 and MAX.
%   RANDINIT('method',@(dim1,dim2)(randi(2,dim1,dim2)-1)*2-1) - use a different distribution 
%   for the initial conditions. The user function should have 2 arguments like the function rand(dim1, dim2)
%   RANDINIT('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'method' [uniform | uneven | findeqs | simple_uneven or function_handle] - method to draw the initial points (uniform, uneven or user function)
%     'range' [double and length(double)<=2] - range for the initial points
%     'var' [state variable or empty] - state variable (empty=all)
%      
%
%   See also ke, val, findeqs
%
%   Reference page in Help browser:
%      <a href="matlab:commands('randinit')">commands randinit</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:38 $
function res = randinit(varargin)
    %(VarName,range)
    global g_grind g_Y; %#ok<GVMIS>
    fieldnams = {'var', 'v#E', 'state variable (empty=all)', [];  ...
        'method', 'e[uniform|uneven|findeqs|simple_uneven]#f', 'method to draw the initial points (uniform, uneven or user function)', 'uniform';  ...
        'range', 'd&length(d)<=2', 'range for the initial points', [0 100]}';
    if nargin == 1 && isstruct(varargin{1})
        %this is used  by findeqs for speed 
        args = varargin{1};
    else
        args = i_parseargs(fieldnams, 'if(argtype(1,''e[uniform|uneven|findeqs|simple_uneven]'')),deffields=''method,range'';elseif(argtype(1,''d'')),deffields=''range'';else,deffields=''var,range'';end;', '', varargin);
        i_parcheck;
    end
    if ~isfield(args, 'var')
        args.var = [];
    end
    if ~isfield(args, 'range') || isempty(args.range)
        args.range = [0 100];
    end
    if ~isfield(args, 'method')
        args.method = 'uniform';
    end

    if numel(args.range) == 1
        args.range = [0 args.range];
    end

    if ischar(args.method)
        switch args.method
            case 'uniform'
                args.method = @(dim1,dim2)drawuniform(dim1,dim2,args.range(1), args.range(2));
            case 'uneven'
                args.method = @(dim1,dim2)drawbetarnd(dim1,dim2,args.range(1), args.range(2));
            case 'simple_uneven'
                %for testing without stats toolbox
                args.method = @(dim1,dim2)drawsimple_uneven(dim1,dim2,args.range(1), args.range(2));
            case 'findeqs'
                args.method = @(dim1,dim2)drawfindeqs(dim1, dim2, args.range(2));
        end
    end
 
    %args.method is used to draw the full vector allways
    N1 = args.method(g_grind.statevars.dim, 1);
    if isempty(args.var)
        N0 = N1;
    else
        N0 = i_initvar;
        [sfrom, sto] = i_statevarnos(args.var);
        if ~isempty(sfrom)
            N0(sfrom:sto) = N1(sto - sfrom + 1);
        end
    end
    if nargout == 0
        i_keep(N0);
        g_Y = [];
    else
        %if there are output arguments the new condition will NOT be set.
        res = N0;
    end
end

function N0 = drawfindeqs(dim1, dim2, scale)
    f = rand(dim1, dim2) < 0.1;
    f2 = rand(dim1, dim2) < 0.1;
    f3 = rand(dim1, dim2) < 0.05;
    N0 = rand(dim1, dim2) * scale;
    N0(f) = 0.001 * N0(f);
    N0(f2) = 100 * N0(f2);
    N0(f3) = -N0(f3);
end


function N0 = drawsimple_uneven(dim1, dim2, minrange, maxrange)
    N0 = drawuniform(dim1, dim2, minrange, maxrange);
    ndx = rand(size(N0)) < 0.2;
    N0(ndx) = N0(ndx) .* 0.1;
    ndx = rand(size(N0)) < 0.1;
    N0(ndx) = N0(ndx) .* 0.01;
end

function N0 = drawbetarnd(dim1, dim2, minrange, maxrange)
    try
        N0 = betarnd(0.4, 0.4, dim1, dim2) * (maxrange - minrange) + minrange;
    catch err
        %if the statistics toolbox is not present we offer an
        %alternative
        if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
            N0 = drawsimple_uneven(dim1, dim2, minrange, maxrange);
        else
            rethrow(err)
        end
    end
end


