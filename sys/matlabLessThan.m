function res = matlabLessThan(verparts)
    %matlabLessThan
    %This function is similar as verLessThan('matlab',..), but significantly
    %faster. It only checks the version in two levels.
    %usage:
    %if matlabLessThan(verparts)
    %    ...
    %endif
    %verparts should be a 2 x 1 vector with version numbers
    % for instance
    % if matlabLessThan([8 8])
    %
    persistent curparts
    if isempty(curparts)
        %slowest part
        curparts = sscanf(version, '%d.%d')';
    end
    if verparts(1) ~= curparts(1)
        res = curparts(1) < verparts(1);
    else
        res = curparts(2) < verparts(2);
    end
end

