%ONSTART   Function(s) that should be run once before each simulation
%   If some initiation should be done before each simulation, use onstart. 
%   This function is run before each new simulation. Similar things can be
%   done with <a href="matlab:help setevent">setevent</a>. Some functions (for instance
%   dwiener or rednoise) include onstart functions that cannot be removed as
%   the need to run.
%  
%   Usage:
%   ONSTART FUN - run function FUN before each run (FUN may also be a single line of 
%   commands). (if there was already another function the new function is added)
%   ONSTART('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'fun' [function_handle or string] - function/single line of code to be run before starting each run
%   ONSTART('-opt1','-opt2',...) - Valid command line <a href="matlab:commands func_args">options</a>:
%     '-c' - clear all commands (except the necessary).
%     '-d' - set the necessary onstart functions
%     '-l' - list the commands.
%     '-nowarning' - suppress the warning that double functions are ignored.
%  
%   See also setevent, modelpanel
%
%   Reference page in Help browser:
%      <a href="matlab:commands('onstart')">commands onstart</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:38 $
function onstart(varargin)
    global g_grind;
    fieldnams = {'fun', 'f#s', 'function/single line of code to be run before starting each run', ''}';
    args = i_parseargs(fieldnams, 'fun(+)', '-l,-c,-d,-nowarning', varargin);
    if nargin == 0
        for j = 1:length(g_grind.onstart.funs)
            if ischar(g_grind.onstart.funs{j})
                evalin('base', g_grind.onstart.funs{j});
            else
                feval(g_grind.onstart.funs{j});
            end
        end
        return;
    end
    if any(strcmp(args.opts, '-d'))
        %default initializations
        if isfield(g_grind,'implicdisp')&&~isempty(g_grind.implicdisp)
            onstart(@()implicitdisperse);
        end
        if g_grind.solver.isstochastic && g_grind.solver.eulerneeded
            if any(strcmp('g_dwiener', g_grind.hidden_pars))
                onstart(@()dwiener('-i'), '-nowarning');
            end
            if hasfunction(g_grind.model,'djump')
                onstart(@()djump('-i'), '-nowarning');
            end
        end
        if g_grind.solver.isstochastic&&any(strcmp('g_rednoise', g_grind.hidden_pars))
            onstart(@()rednoise('-u'),'-nowarning');
        end
    end

    if any(strcmp(args.opts, '-c'))
        g_grind.onstart.funs = {};
        onstart('-d');
        return;
    end
    if any(strcmp(args.opts, '-l'))
        if isempty(g_grind.onstart.funs)
            disp('no onstart commands');
        else
            for i = 1:length(g_grind.onstart.funs)
                fprintf('onstart %s\n', char(g_grind.onstart.funs{i}))
            end
        end
        return;
    end
    if isfield(args, 'fun')
        if iscell(args.fun) && length(args.fun) > 1
            args.fun = strtrim(sprintf('%s ', args.fun{:}));
            if ~strcontains(args.fun, ';')
                args.fun = sprintf('%s;', args.fun);
            end
        elseif iscell(args.fun)
            args.fun = args.fun{1};
        end
        i = length(g_grind.onstart.funs) + 1;
        ndx = cellfun(@(x)any(strcmp(char(x),char(args.fun))), g_grind.onstart.funs);
        if any(ndx)
            i = find(ndx, 1);
            if ~any(strcmp(args.opts, '-nowarning'))
                warning('MATLAB:onstart:alreadyin', 'Function "%s" was already in onstart', char(args.fun))
            end
        end
        
        g_grind.onstart.funs{i} = args.fun;
        if ischar(g_grind.onstart.funs{i})
            evalin('base', g_grind.onstart.funs{i});
        else
            feval(g_grind.onstart.funs{i});
        end
    end
end

function res = hasfunction(model, fun)
    aa = regexp(model, ['([%].*)|((?<![a-zA-Z1-9_\)\]\}])[\''][^'']*[''])|(["][^"]*["])|(\<' fun '\()'], 'match');
    res = any(cellfun(@(x)any(strcmp(x,[fun '('])), aa));
end
