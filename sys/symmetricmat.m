function M = symmetricmat(A, down2up)
    M = A;
    siz = size(M, 1);
    if siz ~= size(M, 2)
        error('GRIND:symmetricmat:square', 'Matrix must be square');
    end
    if (nargin > 1) && down2up
        M = max(tril(A), triu(A'));
    else
        M = max(tril(A'), triu(A));
    end
end
