%function generic_ews
% generic_ews is used to estimate statistical moments within rolling
% windows along a timeserie (based on the R early warning signals toolbox,
% but simplified)
%
% Usage
%
% generic_ews(timeseries, option pairs)
% generic_ews(time,timeseries, option pairs)
%    for example:
%    generic_ews(timeseries, 'winsize', 50, 'detrending', 'no', ...
%       'bandwidth', [], 'logtransform', false, 'interpolate', false)
%
% Arguments
%
% timeseries   - a numeric vector of the observed univariate timeseries values or a numeric
%                matrix of observed timeseries values. If you use a tabel or dataset
%                you can add time as a column named t or time
%
% time         - a numeric vector of the time index (numeric)
%
% datacolumn   - column number with the data (default first column (excluding time))
%
% winsize	   - is the size of the rolling window expressed as percentage of the timeseries length
%                (must be numeric between 0 and 100). Default is 50%.
%
% bandwidth	   - is the bandwidth used for the Gaussian kernel when gaussian filtering is applied.
%                It is expressed as percentage of the timeseries length (must be numeric between 0 and 100)
%                Alternatively it can be given by the optimal bandwidth suggested by Bowman and Azzalini (1997) Default).
%
% detrending   - the timeseries can be detrended/filtered prior to analysis (can be a cell for each data column).
%                There are four options: 'no'= no detrending, 'gaussian' =
%                gaussian filtering, 'movmean' = moving average (statistics
%                toolbox), 'poly' = polynomical (default degree=4, see
%                polydegree), 'linear' = linear detrending, or 'first-diff' = first-differencing. Default is 'gaussion' detrending.
%
% indicators   - cell of strings with the names of the indicators can be one of the following strings:
%                'AR' - autocorrelation of residuals
%                'acf' - autocorrelation function of residuals
%                'std' - standard deviation of residuals
%                'DFA' - detrended fluctuation analysis
%                'skewness' - skewness of residuals
%                'cv' - coefficient of variation of original data
%                'abscorr' - the average of the absolute correlation with all other columns
%                default: {'AR', 'acf', 'std', 'skewness'}
%
%                You can also specify custom functions as a function handle
%                or a struct with the type of function (see register_EWS_functions in the code). 
%                The function handle should take 2 arguments: x and trend and return the value.
%                example: 
%                >> generic_ews(data, 'indicators',{'AR','std',@(x,trend)autoregression(x-trend,2)}
%
%
%
% figures      - cell of strings with figures of the combined figure {'original data', 'residuals'; 
%               'indicator 1'...'indicator 4'}  'original data 2' gives the
%               original data of column 2, 'residuals 2' the residuals of
%               col 2, no number = first datacolumn
%
% logtransform - if TRUE data are logtransformed prior to analysis as log(X+1). Default is FALSE.
%
% interpolate  - If TRUE linear interpolation is applied to produce a timeseries of equal length as the original. Default is FALSE (assumes there are no gaps in the timeseries).
% polydegree   - degree of polynomial for detrending (default = 4)      
% arlag        - The lag for autoregression. Default = 1
% DFAwin       - the maximum window size for detrended fluctuation analysis. Default = 100,...
% DFAorder     - the order of the detrended fluctuation analysis. Default = 1,...
% silent       - If silent=true then the figure is not drawn, default = false
% ebisuzaki    - number of runs for a null model (using the Ebisuzaki method), default = 0
% violinplot   - make violin plot of the taus of the null model, default = true
% title        - use text as title of the figure, default =''
% corrcolumns  - column number with the variable for cross correlation
% bandwidth_unit - unit of bandwidth (see units of winsize)
% winsize_unit - units of the  winsize - if different from '%' the absolute
%                units of the time series are assumed. If % the real size
%                is calculated from the length of the time series
% cv           - (depreciated) if true use cv instead of standard deviation default = false (use now indicators field)
%
%
%not yet supported in the MATLAB version:
% AR_n	       - If TRUE the best fitted AR(n) model is fitted to the data. Default is FALSE.
%
% powerspectrum	-If TRUE the power spectrum within each rolling window is plotted. Default is FALSE.
%
% Options:
% s=generic_ews('-defaultopts') - create a struct s with the default
%               options (used internally)
% generic_ews('-f',res) determine the Fisher combined probability plots of 
%               the results res. res should be a struct array or cell array of earlier results
%
% generic_ews returns a structure with the following fields:
% timeseries - original time series
% EWSopt     - structure with all used options (including the time index)
% trend      - trend in the time series (depending on the detrending option)
% colnames   - names of columns of the results (indicator names made unique, e.g. if 
%              two AR indicators are saved they are named AR and AR_1)
% indicators - the resulting 
% taus       - Kendall tau values for each indicator
% pvalues    - p values of the tau values (only if an ebizuzsaki analysis was
%              done)
% ebitaus    - the tau values for each of the ebizuzsaki iterations
%              done)
% description  - string with main options
%
%
% In addition, generic_ews returns a plots. The  plot contains the original data,
% the detrending/filtering applied and the residuals (if selected), and all the moment statistics.
% For each statistic trends are estimated by the nonparametric Kendall tau correlation (if statistics toolbox
% is present).
% Not supported: The second plot, if asked, quantifies resilience indicators fitting AR(n) selected by the
% Akaike Information Criterion. The third plot, if asked, is the power spectrum estimated
% by spec.ar for all frequencies within each rolling window.
%
% Author(s)
%
% Vasilis Dakos vasilis.dakos@gmail.com
% MATLAB version by Egbert van Nes
%
% References
%
% Ives, A. R. (1995). "Measuring resilience in stochastic systems." Ecological Monographs 65: 217-233
%
% Dakos, V., et al (2008). "Slowing down as an early warning signal for abrupt climate change." Proceedings of the National Academy of Sciences 105(38): 14308-14312
%
% Dakos, V., et al (2012)."Methods for Detecting Early Warnings of Critical Transitions in Time Series Illustrated Using Simulated Ecological Data." PLoS ONE 7(7): e41010. doi:10.1371/journal.pone.0041010
%
%
function result = generic_ews(timeseries, varargin)
    if nargin == 0
        error('grind:generic_ews:notenoughargs', 'Not enough input arguments, at least the timeseries is needed (see <a href="matlab:help(''generic_ews'')">help generic_ews</a>)');
    end
    if ischar(timeseries)
        if strcmp(timeseries, '-defaultopts')
            %the default options
            result = struct('winsize', 50, ...
                'detrending', 'gaussian', ... %  = c("no", "gaussian", "linear", "poly", "first-diff", "movmean",
                'bandwidth', 10, ...
                'time', [], ...
                'datacolumn', 1, ... %per indicator or one for all
                'corrcolumns', [], .... %empty is all (except datacolumn)
                'silent', false, ...
                'logtransform', false, ...
                'arlag', 1, ...
                'indicators', {{'AR', 'acf', 'std', 'skewness'}}, ... %other options cv, abscorr, crosscorr, mean, DFA
                'figures', {{'original data', 'residuals'; 'indicator 1', 'indicator 2'; 'indicator 3', 'indicator 4'; 'indicator 5', 'indicator 6'}}, ...
                'cv', false, ...
                'polydegree', 4, ...
                'DFAwin',100,...
                'DFAorder',1,...
                'interpolate', false, ...
                'AR_n', false, ...
                'powerspectrum', false, ...
                'bandwidth_unit', '%', ...
                'winsize_unit', '%', ...
                'ebisuzaki', 0, ...  %number of nullmodel (Ebisuzaki) runs (0=none)
                'violinplot', true, ...
                'title', '');
            return;
        elseif strcmp(timeseries, '-funhandles')
            if nargin==1
                options='-all';
            elseif iscell(varargin{1})
                options=struct('indicators',{varargin(1)});
            elseif ischar(varargin{1})&&~strcmp(varargin{1},'-all')
                options=struct('indicators',{varargin(1)});
            elseif isfield(varargin{1},'EWSopt')
                options=varargin{1}.EWSopt;
            else
                options=varargin{1};
            end
            result=register_EWS_functions(options);
            return;
        elseif strncmpi(timeseries, '-f', 2)
            %table with fisher combined probabilities
            res = varargin{1};
            if ~iscell(res)
                %if not a cell make a cell for convenience
                res1 = varargin{1};
                res = cell(size(res));
                for i = 1:numel(res1)
                    res{i} = res1(i);
                end
            end
            %ebitaus = zeros(res{1}.EWSopt.ebisuzaki, length(res));
            titles = cell(length(res), 1);
            colnames = res{1}.colnames;
            pvalues = zeros(length(res), length(res{1}.colnames));
            fisher(1, length(res{1}.colnames)) = struct('p', [], 'chi', [], 'df', []);
            %  fisher = cell(1, length(res{1}.colnames));
            taus = zeros(length(res), length(res{1}.colnames));
            silent = res{1}.EWSopt.silent | nargout > 0;
            for j = 1:length(res{1}.colnames)
                if ~silent
                    fprintf( '\n=======  %s  ========\n', res{1}.colnames{j});
                    fprintf( 'Title\tKendall-tau %s\tp-value %s\n', res{1}.colnames{j}, res{1}.colnames{j});
                end
                for i = 1:numel(res)
                    %ebitaus(:, i) = res{i}.ebitaus(j, :)';
                    titles{i} = res{i}.EWSopt.title;
                    if isempty(titles{i})
                        titles{i} = sprintf('data set %d', i);
                    end
                    taus(i, j) = res{i}.taus(j);
                    pvalues(i, j) = res{i}.pvalues(j); %(sum(taus(i) < ebitaus(:, i)) + 1) / size(ebitaus, 1);
                    if ~silent
                        fprintf( '%s\t%.3g\t%.3g\n', titles{i}, taus(i, j), pvalues(i, j));
                    end
                end
                fisher1 = fishercombine(pvalues(:, j));
                if ~silent
                    regimes = 'Fisher combined'; %sprintf('%s, ', titles{:});
                    fprintf( '%s\tX2(%d)=%.3g\t%.3g\n', regimes, fisher1.df, fisher1.chi, fisher1.p);
                end
                fisher(1, j) = fisher1;
            end
            if nargout > 0
                result.titles = titles;
                result.colnames = colnames;
                result.taus = taus;
                result.pvalues = pvalues;
                %   result.fisherp = fisherp;
                result.fisher = fisher;
            end
            return;
        end
    end
    if ~((isfield(timeseries, 'timeseries') || isfield(timeseries, 'inds')) && isfield(timeseries, 'taus')) % if these fields exist it is a previously saved run
        [timeseries, EWSopt] = initialize_settings(timeseries, varargin);
       
        taus = zeros(length(EWSopt.indicators), 1);
        trend = zeros(size(timeseries));
        for i = 1:size(timeseries, 2)
            detrending = getcolumn(EWSopt.detrending, i, true);
            if strcmpi(detrending, 'first-diff')
                timeseries(:, i) = [nan; diff(timeseries(:, i))];
            elseif strcmpi(detrending, 'linear')
                p = polyfit(EWSopt.time, timeseries(:, i), 1);
                trend(:, i) = polyval(p, EWSopt.time);
            elseif strcmpi(detrending, 'poly')
                p = polyfit(EWSopt.time, timeseries(:, i), EWSopt.polydegree);
                trend(:, i) = polyval(p, EWSopt.time);
            elseif strcmpi(detrending, 'movmean')
                bw = getcolumn(EWSopt.bandwidth, i);
                if isempty(bw)
                    bw = [4 0];
                end
                try
                    trend(:, i) = movmean( timeseries(:, i), bw);
                catch
                    trend(:, i) = movmean1( timeseries(:, i), bw);
                end
            elseif strcmpi(detrending, 'gaussian')
                bw = getcolumn(EWSopt.bandwidth, i);
                if strcmp(EWSopt.bandwidth_unit{i}, '%')
                    if ~isempty(bw)
                        EWSopt.absbandwidth = bw / 100 * (max(EWSopt.time) - min(EWSopt.time));
                    else
                        EWSopt.absbandwidth = [];
                    end
                else
                    if iscell(EWSopt.bandwidth)
                        EWSopt.absbandwidth = EWSopt.bandwidth{i};
                    else
                        EWSopt.absbandwidth = EWSopt.bandwidth;
                    end
                end
                trend(:, i) = ksmooth(EWSopt.time, timeseries(:, i), getcolumn(EWSopt.absbandwidth, i), detrending);
            end
        end


        % ndx = ~isnan(trend(:, 1));
        if strcmp(EWSopt.winsize_unit, '%')
            if EWSopt.winsize > 100
                EWSopt.winsize = 100;
            end
            if EWSopt.winsize < 0.1
                EWSopt.winsize = 0.1;
            end
            EWSopt.avwin = EWSopt.winsize / 100 * (max(EWSopt.time) - min(EWSopt.time));
        else
            EWSopt.avwin = EWSopt.winsize;
        end
 
        EWSfunctions = register_EWS_functions(EWSopt);

        indicators = cell(size(EWSopt.indicators));
        for i1 = 1:length(indicators)
            indicators{i1} = zeros(size(timeseries, 1), 1) .* NaN;
        end
        ts = EWSopt.time;
        winsize = EWSopt.avwin;
        tstart = ts - winsize;
        f = find(tstart >= EWSopt.time(1), 1);
        if isempty(f)
            error('not enough data')
        end
        tstart = tstart - (tstart(f) - EWSopt.time(1)); %allign with time lags
        tstart(tstart < EWSopt.time(1)) = NaN;
        notrend = false;
        for i1 = 1:length(ts)
            if ~isnan(tstart(i1)) && ~isnan(trend(i1, 1))
                ndx = ts >= tstart(i1) & ts < ts(i1) & ~isnan(trend(i1, 1));
                timser = timeseries(ndx, :);
                if isempty(trend)
                    tren = zeros(size(timser));
                else
                    tren = trend(ndx, :);
                end
                for j = 1:length(EWSopt.indicators)
                    %*type 1: simple function without detrending       : fun(timeseries)
                    % type 2: simple function with detrending          : fun(timeseries-trend)
                    % type 3: AR function with detrending              : fun(timeseries-trend,arlag)
                    %*type 4: crosscorrelation 2 columns wo detrending : fun(timeseries,datacolumn,corrcolumn) indexes of columns
                    % type 5: both trend and timeseries given          : fun(timeseries,trend)
                    switch EWSfunctions(j).type
                        case 2
                            indicators{j}(i1) = EWSfunctions(j).function(timser(:, getcolumn(EWSopt.datacolumn, j)) - tren(:, getcolumn(EWSopt.datacolumn, j)));
                        case 3
                            indicators{j}(i1) = EWSfunctions(j).function(timser(:, getcolumn(EWSopt.datacolumn, j)) - tren(:, getcolumn(EWSopt.datacolumn, j)), getcolumn(EWSopt.arlag, j));
                        case 5
                            indicators{j}(i1) = EWSfunctions(j).function(timser(:, getcolumn(EWSopt.datacolumn, j)), tren(:, getcolumn(EWSopt.datacolumn, j)));
                        otherwise
                            notrend = true;
                    end
                end
            end
        end
        if notrend
            for i1 = 1:length(ts)
                if ~isnan(tstart(i1))
                    ndx = ts >= tstart(i1) & ts < ts(i1) ;
                    timser = timeseries(ndx, :);             
                    for j = 1:length(EWSopt.indicators)
                        %type 1: (simple function without detrending) : fun(timeseries)
                        %type 4: crosscorrelation 2 columns           : fun(timeseries,datacolumn,corrcolumn) indexes of columns
                        %type 6: DFA
                        switch EWSfunctions(j).type
                            case 1
                                indicators{j}(i1) = EWSfunctions(j).function(timser(:, getcolumn(EWSopt.datacolumn, j)));
                            case 4
                                indicators{j}(i1) = EWSfunctions(j).function(timser, getcolumn(EWSopt.datacolumn, j), getcolumn(EWSopt.corrcolumns, j, true));
                            case 6
                                indicators{j}(i1) = EWSfunctions(j).function(timser(:,getcolumn(EWSopt.datacolumn, j)), getcolumn(EWSopt.DFAwin, j, true), getcolumn(EWSopt.DFAorder, j, true));
                        end
                    end
                end
            end
        end
        hasstats = exist('corr', 'file') ~= 0;
        if hasstats
            %we cannot use p values as the data are not independent
            taus = zeros(size(EWSopt.indicators));
            for i = 1:length(taus)
                taus(i) = corr(EWSopt.time, indicators{i}, 'type', 'Kendall', 'rows', 'complete');
            end
        end
        ebitaus = [];
    else %plot previous data
        hasstats = exist('corr', 'file') ~= 0;
        res = timeseries;
        EWSopt = res.EWSopt;
        EWSopt = mergeoptions(EWSopt, varargin);
        if ~isfield(EWSopt, 'indicators')
            EWSopt.indicators = {'AR', 'acf', 'std', 'skewness'};
            if EWSopt.cv
                EWSopt.indicators{3} = 'cv';
            end
        end
        EWSfunctions = register_EWS_functions(EWSopt);

        %         if isa(res.inds, 'table')
        %             inds = table2array(res.inds(:, makeunique(EWSopt.indicators)));
        %         else
        %             inds = res.inds(:, 4:end);
        %         end
        taus = res.taus;
        ebitaus = res.ebitaus;
        if ~isfield(res, 'inds')
            trend = res.trend;
            if isempty(trend)
                trend = zeros(size(res.timeseries));
            end
            timeseries = res.timeseries;
            indicators = num2cell(res.indicators, 1);
        elseif isa(res.inds, 'table') %old version
            if ~any(strcmp(res.inds.Properties.VariableNames, 'trend'))
                trend = zeros(size(res.inds.Variable));
            else
                trend = res.inds.trend;
            end
            timeseries = res.inds.Variable;
            inds = table2array(res.inds(:, makeunique({EWSfunctions.name})));
            indicators = num2cell(inds, 1);
        end
    end


    pvalues = [];

    if EWSopt.ebisuzaki > 0 && isempty(ebitaus)
        nulldata = zeros(size(timeseries, 1), size(timeseries, 2), EWSopt.ebisuzaki);
        for i = 1:size(timeseries, 2)
            nulldata(:, i, :) = ebisuzaki(timeseries(:, i), EWSopt.ebisuzaki);
        end
        %nulldata = ebisuzaki(timeseries(:, EWSopt.datacolumn), EWSopt.ebisuzaki);
        EWSopt1 = EWSopt;
        EWSopt1.ebisuzaki = 0;
        EWSopt1.silent = true;
        if isempty(ebitaus)
            ebitaus = zeros(numel(taus), EWSopt.ebisuzaki);
            for i = 1:EWSopt.ebisuzaki
                res = generic_ews(nulldata(:, :, i), EWSopt1);
                ebitaus(:, i) = res.taus;
            end
        end
    end
    if ~isempty(ebitaus)
        pvalues = zeros(size(EWSopt.indicators));
        nebisuzaki = size(ebitaus, 2);
        for j = 1:length(pvalues)
            pvalues(j) = (sum(taus(j) < ebitaus(j, :)) + 1) / nebisuzaki;
        end
        fprintf('Surrogate data analysis  %s\n', EWSopt.title);
        fprintf('Number of surrogate data sets: %d\n', nebisuzaki);
        for i = 1:length(pvalues)
            fprintf('Tau %s = %g, pvalue = %g\n', EWSopt.indicators{i}, taus(i), pvalues(i));
        end
        if ~EWSopt.silent && (~isfield(EWSopt, 'violinplot') || EWSopt.violinplot)
            oldfig = get(0, 'currentfigure');
            figure;
            try
                violinplot(ebitaus', (1:length(pvalues)), 'bandwidth', 0.1, 'categorynames', EWSopt.indicators);
            catch
            end
            i_plotdefaults;
            hold on
            plot((1:length(pvalues)), taus, 'ro');
            ylabel('Kendall \tau')
            title('Surrogate data')
            if ~isempty(oldfig)
                figure(oldfig);
            end
        end
    end
    if ~EWSopt.silent && ~isempty(EWSopt.figures)
        %         if ~isfield(EWSopt, 'figures')
        %             EWSopt.figures = {'original data', 'residuals'; 'indicator 1', 'indicator 2'; 'indicator 3', 'indicator 4'};
        %         end
        xlims = [min(EWSopt.time), max(EWSopt.time)];
        figure;
        figs = EWSopt.figures'; %strange of MATLAB that this is different
        for i = 1:numel(EWSopt.figures)
            h = subplot(size(EWSopt.figures, 1), size(EWSopt.figures, 2), i);
            if strncmp(figs{i}, 'original data', 13)
                col = str2double(regexp(figs{i}, '[0-9]*', 'match', 'once'));
                if isnan(col)
                    col = getcolumn(EWSopt.datacolumn, 1);
                end
                plot(EWSopt.time, timeseries(:, col), 'k-', 'Tag', figs{i});
                xlim(xlims)
                adjustticklabels(gca, 'Y');
                if ~isempty(trend)
                    hold on;
                    plot(EWSopt.time, trend(:, col), 'r-', 'Tag', 'trend');
                    hold off;
                end
                if ~isempty(EWSopt.title)
                    text(0.1, 0.9, EWSopt.title, 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'toptext')
                    set(gcf, 'name', EWSopt.title)
                end
                xlabel('Time');
                % ylabel('variable and trend');
            elseif strncmp(figs{i}, 'residuals', 9)
                col = str2double(regexp(figs{i}, '[0-9]*', 'match', 'once'));
                if isnan(col)
                    col = getcolumn(EWSopt.datacolumn, 1);
                end
                if ~isempty(trend)
                    h = stem(EWSopt.time, timeseries(:, col) - trend(:, col), 'k.', 'Tag', figs{i});
                    adjustticklabels(gca, 'Y');
                    text(0.1, 0.9, 'residuals', 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'toptext')
                    set(h, 'markersize', 1);
                else
                    plot(EWSopt.time, timeseries(:, col), 'k-', 'Tag', figs{i});
                    text(0.1, 0.9, 'No residuals - no detrending', 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'text1')
                end
                xlabel('Time');
                xlim(xlims);
            else
                uinds = makeunique({EWSfunctions.name});
                iind = find(strcmp(figs{i}, uinds));
                if isempty(iind)
                    iind = str2double(regexp(figs{i}, '[0-9]*', 'match', 'once'));
                end
                if iind <= length(EWSopt.indicators)
                    sindicator = uinds{iind};
                    if any(strcmp(EWSopt.indicators{iind}, {'AR', 'acf'}))
                        sindicator = sprintf('%s(%d)', lower(sindicator), EWSopt.arlag);
                    elseif strcmp(EWSopt.indicators{iind}, 'std')
                        sindicator = 'standard deviation';
                    end
                    plot(EWSopt.time, indicators{iind}, 'k-', 'tag', uinds{iind});
                    adjustticklabels(gca, 'Y');
                    text(0.1, 0.9, sindicator, 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'toptext')
                    if hasstats
                        if ~isempty(pvalues)
                            text(0.1, 0.1, sprintf('{\\tau} = %5.3g (p =%5g)', taus(iind), pvalues(iind)), 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'bottomtext');
                        else
                            text(0.1, 0.1, sprintf('{\\tau} = %5.3g', taus(iind)), 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'bottomtext');
                        end
                    end
                    xlabel('Time');
                    xlim(xlims);
                else
                    delete(h);
                end
            end
            movegui(gcf, 'center');
        end
        ch = findobj(gcf, 'type', 'axes');
        set(ch, 'TickDir', 'out');
        if numel(figs) > 1
            adjustpos([0, 0.07]);
            removeaxis('x');
        end
        xlim(xlims);
    end
    if nargout > 0
        %        indic = makeunique(EWSopt.indicators);
        %         if ~isoctave && verLessThan('matlab', '8.4.0')
        %             properties = struct('VarNames', indic , 'Description', sprintf('data generated with generic_ews\nwinsize = %g, detrending = %s, bandwidth = %g, logtransform = %d, arlag = %d, interpolate = %d', EWSopt.winsize, getcolumn(EWSopt.detrending, 1), getcolumn(EWSopt.bandwidth, 1), EWSopt.logtransform, EWSopt.arlag, EWSopt.interpolate));
        %             inds = struct('time', EWSopt.time, 'timeseries', timeseries, 'trend', trend, 'indicators', [indicators{:}], 'Properties', properties);
        %         else
        %             %  [~,ndx]=unique(EWSopt.indicators);
        %             if ~isempty(trend)
        %                 ndx = ~isnan(trend(:, 1));
        %                 for i = 1:length(indicators)
        %                     inds = zeros(size(ndx)) + NaN;
        %                     if numel(indicators{i}) == numel(ndx)
        %                         inds = indicators{i};
        %                     else
        %                         inds(ndx) = indicators{i};
        %                     end
        %                     indicators{i} = inds;
        %                 end
        %                 inds = table(EWSopt.time, timeseries, trend, indicators{:});
        %                 inds.Properties.VariableNames = [{'Time', 'Variable', 'trend'}, indic];
        %             else
        %                 inds = table(EWSopt.time, timeseries, indicators{:});
        %                 inds.Properties.VariableNames = [{'Time', 'Variable'}, indic];
        %             end
        % 
        %             inds.Properties.Description = sprintf('data generated with generic_ews\nwinsize = %g, detrending = %s, bandwidth = %g, logtransform = %d, arlag = %d, interpolate = %d', EWSopt.winsize, getcolumn(EWSopt.detrending, 1), getcolumn(EWSopt.bandwidth, 1), EWSopt.logtransform, EWSopt.arlag, EWSopt.interpolate);
        %         end
        if isempty(getcolumn(EWSopt.bandwidth, 1))
            % optimal bandwidth suggested by Bowman and Azzalini (1997) p.31
            n = size(timeseries, 1);
            hx = median(abs(EWSopt.time - median(EWSopt.time))) / 0.6745 * (4 / 3 / n)^0.2;
            hy = median(abs(timeseries(:, getcolumn(EWSopt.datacolumn, 1)) - median(timeseries(:, getcolumn(EWSopt.datacolumn, 1))))) / 0.6745 * (4 / 3 / n)^0.2;
            EWSopt.bandwidth = sqrt(hy * hx);
        end
        if nargout == 1
            descr = sprintf('data generated at %s with generic_ews\nwinsize = %g, detrending = %s, bandwidth = %g, logtransform = %d, arlag = %d, interpolate = %d', ... 
                datestr(now()), EWSopt.winsize, getcolumn(EWSopt.detrending, 1), getcolumn(EWSopt.bandwidth, 1), EWSopt.logtransform, EWSopt.arlag, EWSopt.interpolate);
            result = struct('timeseries', timeseries, ...
                'EWSopt', EWSopt, ...
                'trend', trend, ...
                'colnames', {makeunique({EWSfunctions.name})} , ...
                'indicators', [indicators{:}], ...
                'taus', taus, ...
                'pvalues', pvalues, ...
                'ebitaus', ebitaus, ...               
                'description', descr);
        end
    end

    %end
end

function functions = register_EWS_functions(EWSopt)
    % Register new indicators here!
    %type 1: (simple function without detrending) : fun(timeseries)
    %type 2: simple function with detrending      : fun(timeseries-trend)
    %type 3: AR function with detrending          : fun(timeseries-trend,EWSopt.arlag)
    %type 4: crosscorrelation 2 columns no detrend: fun(timeseries,datacolumn,corrcolumn) indexes of columns
    %type 5: both trend and timeseries given      : fun(timeseries,trend)
    %type 6: DFA no detrending 2 arguments        : fun(timeseries,EWSopt.DFAwin,EWSopt.DFAorder)
    %default user defined function type = 5
    allfuncs(1) = struct('name', 'AR', 'type', 3, 'function', @autoregression);
    allfuncs(2) = struct('name', 'acf', 'type', 3, 'function', @acf);
    allfuncs(3) = struct('name', 'cv', 'type', 1, 'function', @(x)std(x)/mean(x));
    allfuncs(4) = struct('name', 'std', 'type', 2, 'function', @std);
    allfuncs(5) = struct('name', 'skewness', 'type', 2, 'function', @skewness);
    allfuncs(6) = struct('name', 'abscorr', 'type', 4, 'function', @(y,j1,j2)meancorr(y,j1,j2,true));
    allfuncs(7) = struct('name', 'crosscorr', 'type', 4, 'function', @(y,j1,j2)meancorr(y,j1,j2,false));
    allfuncs(8) = struct('name', 'mean', 'type', 1, 'function', @mean);
    allfuncs(9) = struct('name', 'DFA', 'type', 6, 'function', @DFA_fun);
    if ischar(EWSopt)&&strcmp(EWSopt,'-all')
        functions=allfuncs;
        return;
    end
    names = {allfuncs.name};
    for i1 = length(EWSopt.indicators): -1:1
        if ischar(EWSopt.indicators{i1})
            functions(i1) = allfuncs(strcmp(EWSopt.indicators{i1}, names));
        elseif isstruct(EWSopt.indicators{i1})
           if any(strcmp(EWSopt.indicators{i1}.name,names))
                error('generic_ews:unique_name','Use unique name for user-defined functions');
            end
            functions(i1) = EWSopt.indicators{i1};
        else
            nam=regexprep(char(EWSopt.indicators{i1}), '[@]?[(][^)]*[)]', '');
            if any(strcmp(nam,names))
                error('generic_ews:unique_name','Use unique name for user-defined functions');
            end
            functions(i1) = struct('name', nam, 'type', 5, 'function', EWSopt.indicators{i1});
        end
    end
end



function [timeseries, EWSopt] = initialize_settings(timeseries, args)
    if isa(timeseries, 'table')
        %the time series may be a table , it can then have a column with
        %time, the name of that column should be t or Time or time
        ndx = ~(strcmp('t', timeseries.Properties.VariableNames) | strcmpi('time', timeseries.Properties.VariableNames));
        if any(ndx)
            % the time is added to the options
            args = [args, 'time', timeseries.t];
            timeseries = timeseries{:, ndx};
        else
            timeseries = timeseries{:, :};
        end
    elseif isa(timeseries, 'dataset')
        %the time series may be a dataset, it can then have a column with
        %time, the name of that column should be t or Time or time
        ndx = ~(strcmp('t', timeseries.Properties.VarNames) | strcmpi('time', timeseries.Properties.VarNames));
        if any(ndx)
            % the time is added to the options
            args = [args, 'time', timeseries.t];
            timeseries = double(timeseries(:, ndx));
        else
            timeseries = double(timeseries(:, :));
        end
    else
        timeseries = double(timeseries);
    end

    if ~exist('i_use', 'file')
        addpath([grindpath, filesep, 'sys2']);
    end
    if isempty(timeseries)
        error('grind:generic_ews:empty', 'Empty time series');
    end
    EWSopt = generic_ews('-defaultopts');
    EWSopt = mergeoptions(EWSopt, args);
    if ischar(EWSopt.bandwidth_unit)
        EWSopt.bandwidth_unit = {EWSopt.bandwidth_unit};
    end
    if numel(EWSopt.bandwidth_unit) == 1 && size(timeseries, 2) > 1
        EWSopt.bandwidth_unit = repmat(EWSopt.bandwidth_unit, 1, size(timeseries, 2));
    end

    if EWSopt.powerspectrum || EWSopt.AR_n
        error('Option not yet supported');
    end
    if isempty(EWSopt.time)
        EWSopt.time = transpose(linspace(1, size(timeseries, 1), size(timeseries, 1)));
    end

    if EWSopt.interpolate
        ts = transpose(linspace(EWSopt.time(1), EWSopt.time(end), size(timeseries, 1)));
        timeseries = interp1(EWSopt.time, timeseries, ts);
        EWSopt.time = ts;
    end
    for i = 1:size(timeseries, 2)
        if getcolumn(EWSopt.logtransform, i)
            timeseries(:, i) = log(timeseries(:, i) + 1);
        end
    end

    EWSopt.datalength = max(EWSopt.time) - min(EWSopt.time);
    if EWSopt.cv
        %depreciated
        f = strcmp(EWSopt.indicators, 'std');
        EWSopt.indicators(f) = {'cv'};
    end
end
function EWSopt = mergeoptions(EWSopt, arg)
    f = fieldnames(EWSopt);
    if ~isempty(arg) && isstruct(arg{1})
        %all options can be entered as struct
        if exist('mergestructs', 'file') == 0
            initgrind;
        end
        %replace common fields in EWSopt
        EWSopt = mergestructs(EWSopt, arg{1});
    else
        for i = 1:2:length(arg)
            if ~any(strcmp(f, arg{i}))
                s = sprintf('"%s", ', f{:});
                error('Unknown option for generic_ews: %s\nValid options are: %s', arg{i}, s(1:end - 1));
            end
            EWSopt.(arg{i}) = arg{i + 1};
        end
    end
end

function res = getcolumn(col, i, multcell)
    if nargin < 3
        multcell = false;
    end
    if multcell && ~iscell(col)
        col = {col};
    end
    if isempty(col)
        res = col;
    elseif iscell(col)
        if numel(col) == 1
            res = col{1};
        else
            res = col{i};
        end
    else
        if numel(col) == 1
            res = col(1);
        else
            res = col(i);
        end
    end
end

function res = meancorr(x, datacolumm, corrcolumns, doabs)
    if size(x, 2) == 1
        res = NaN;
        return;
    end
    res = corr(x);
    res = res(datacolumm, :);
    if isempty(corrcolumns)
        res(:, datacolumm) = [];
    else
        res = res(:, corrcolumns);
    end
    if doabs
        res = mean(abs(res));
    else
        res = mean(res);
    end
end
function indic = makeunique(indic)
    uindic = unique(indic);
    if length(uindic) < length(indic)
        for i = 1:length(uindic)
            f = find(strcmp(uindic{i}, indic));
            for j = 2:length(f)
                indic{f(j)} = sprintf('%s_%d', indic{f(j)}, j - 1);
            end
        end
    end
end
function b = linregres(x, y) %#ok<DEFNU>
    %b=(sum(x.*y)-1/length(x)*sum(x)*sum(y))/(sum(x.^2)-1/length(x)*sum(x).^2);
    b = sum(x .* y) ./ (sum(x.^2)); %without intercept
    %intercept
    %a=mean(y)-b*mean(x);
end
function res = skewness(x, flag)
    if nargin < 2
        flag = 0;
    end
    x = x - mean(x);
    s2 = mean(x.^2); % this is the biased variance estimator
    m3 = mean(x.^3);
    res = m3 ./ s2.^(1.5);
    if flag == 0
        n = length(x);
        if n > 3
            n(n < 3) = NaN; % bias correction is not defined for n < 3.
            res = res .* sqrt((n - 1) ./ n) .* n ./ (n - 2);
        end
    end
end
function res = acf(X, alag)
    if nargin < 2
        alag = 1;
    end
    %autocorrelation function like in R
    n = length(X);
    s = var(X);
    mu = mean(X);
    Xt = X(1:end - alag);
    Xtk = X(1 + alag:end);
    res = 1 / (n - 1) / s * sum((Xt - mu) .* (Xtk - mu));
end
function res = ksmooth(x, y, bandwidth, method)
    if nargin < 3
        bandwidth = [];
    end
    if nargin < 4
        method = 'g';
    end
    if isempty(x)
        res = x;
        return
    end
    n = length(x);
    if isempty(bandwidth)
        % optimal bandwidth suggested by Bowman and Azzalini (1997) p.31
        hx = median(abs(x - median(x))) / 0.6745 * (4 / 3 / n)^0.2;
        hy = median(abs(y - median(y))) / 0.6745 * (4 / 3 / n)^0.2;
        h = sqrt(hy * hx);
        fprintf('bandwidth smoothing set to %g\n', h);
        if h < sqrt(eps) * n
            error('GRIND:outf:LittleVar', 'There is not enough variation in the data. Regression is meaningless.')
        end
    else
        h = bandwidth;
    end
    switch lower(method(1))
        case 'g' %"Normal" = Gaussian kernel function
            h = h * 0.36055512754640; %variance of 0.13 to get quartiles at + /  -  0.25 * h (comparable with ksmooth (R))
            res = ones(size(y, 1), 1);
            for k = 1:n
                xx = abs(x(k) - x) / h;
                z = exp( - xx(xx < 4).^2 / 2); %x < 4 is more efficient for long time series (negligible effect)
                res(k) = sum(z .* y(xx < 4)) / sum(z);
            end
            %remove tails
            x1 = (x(end) - x) > h / 2 & (x - x(1)) > h / 2;
            res(~x1) = NaN;
        case 'b' %"box" = moving average
            d = h / 2; % 0.25 quartiles
            res = cell(1, n);
            for k = 1:n
                xx = abs(x(k) - x) / h;
                z = xx < d;
                res(k) = sum(z .* y) / sum(z);
            end
    end
end

function res = movmean1( timeseries, bw)
    %simple remake of movmean (for older MAT
    if length(bw) == 1
        if mod(bw, 2) == 0
            bw = [bw / 2 bw / 2 - 1];
        else
            bw = [(bw - 1) / 2 (bw - 1) / 2];
        end
    end
    starti = (1:length(timeseries)) - bw(1);
    endi = (1:length(timeseries)) + bw(2);
    starti(starti < 1) = 1;
    endi(endi > length(timeseries)) = length(timeseries);
    res = zeros(size(timeseries));
    for i1 = 1:length(timeseries)
        res(i1) = mean(timeseries(starti(i1):endi(i1)));
    end
end

function removeaxis(orientation)
    hs = getsubplotgrid(gcf);
    if nargin == 0
        orientation = 'b';
    end
    [rows, cols] = size(hs);
    if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'x', 1)
        for i = 1:rows - 1
            for j = 1:cols
                if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                    set(get(hs(i, j), 'xlabel'), 'string', '')
                    set(hs(i, j), 'XTickLabel', []);
                end
            end
        end
        for i = 2:rows
            for j = 1:cols
                if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                    set(get(hs(i, j), 'title'), 'string', '')
                end
            end
        end
    end
    if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'y', 1)
        for i = 1:rows
            for j = 2:cols
                if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                    set(get(hs(i, j), 'ylabel'), 'string', '')
                    set(hs(i, j), 'YTickLabel', []);
                end
            end
        end
    end
    set(hs(hs ~= 0), 'fontsize', 12)
    aa = get(get(gcf, 'children'), 'xlabel');
    if ~iscell(aa)
        aa = {aa};
    end
    for i = 1:length(aa)
        set(aa{i}, 'fontsize', 12);
    end
    aa = get(get(gcf, 'children'), 'ylabel');
    if ~iscell(aa)
        aa = {aa};
    end
    for i = 1:length(aa)
        set(aa{i}, 'fontsize', 12);
    end
end
function adjustpos(spacing)
    set(gcf, 'PaperPositionMode', 'auto')
    set(gcf, 'units', 'normalized');
    figpos = get(gcf, 'position');
    vert = 0.5469; %normal size of figure
    hor = 0.41;
    hs = getsubplotgrid(gcf);
    if nargin == 0
        spacing = 0.02;
    end
    if length(spacing) == 1
        spacing = spacing + zeros(2, 1);
    end
    [rows, cols] = size(hs);
    if cols < 4 && rows <= 4
        set(gcf, 'position', [figpos(1:2) hor * cols / 4 vert * rows / 4]);
    else
        rat = rows / cols;
        if rat < 1
            set(gcf, 'position', [figpos(1:2) hor vert * rat]);
        else
            set(gcf, 'position', [figpos(1:2) hor / rat vert]);
        end
    end
    for i = 1:rows
        for j = 1:cols
            h = findobj(gcf, 'tag', sprintf('subplot(%d,%d)', i, j));
            if any(ishandle(h)) && any(h ~= 0)
                set(h, 'position', subplotpos(rows, cols, j, i, spacing));
            end
        end
    end
end
function hs = getsubplotgrid(h)
    ch = get(h, 'children');
    tags = get(ch, 'tag');
    ch = ch(~strcmp(tags, 'legend'));
    types = get(ch, 'type');
    poss = get(ch, 'position');
    if length(ch) == 1
        poss = {poss};
        types = {types};
    end
    ipos = zeros(length(ch), 4);
    for i = length(ch): - 1:1
        if iscell(types) && strcmp(types{i}, 'axes')
            ipos(i, :) = poss{i};
        elseif ischar(types) && strcmp(types, 'axes')
            ipos(i, :) = poss{i};
        else
            ipos(i, :) = [];
        end
    end
    colpos = sort(unique(sort(ipos(:, 1))), 'ascend');
    rowpos = sort(unique(sort(ipos(:, 2))), 'descend');
    hs = zeros(length(rowpos), length(colpos));
    for i = 1:length(ch)
        if strcmp(types{i}, 'axes')
            arow = find(rowpos == ipos(i, 2));
            acol = find(colpos == ipos(i, 1));
            hs(arow, acol) = ch(i);
            set(ch(i), 'tag', sprintf('subplot(%d,%d)', arow, acol));
        end
    end
end

function adjustticklabels(hax, orient)
    %     axis(hax, 'tight')
    %     newtick = get(hax, [orient 'tick']);
    %     tickdiff = (newtick(2) - newtick(1));
    %     newlim = [newtick(1) - tickdiff newtick(end) + tickdiff];
    %     axis(hax, 'manual')
    %     set(hax, [orient 'lim'], newlim);
    %     set(hax, [orient 'tick'], [newtick(1) - tickdiff newtick]);
end

function [A, stats] = DFA_fun(data, pts, order)

    % -----------------------------------------------------
    % DESCRIPTION:
    % Function for the DFA analysis.

    % INPUTS: 
    % data: a one-dimensional data vector.
    % pts: sizes of the windows/bins at which to evaluate the fluctuation
    % order: (optional) order of the polynomial for the local trend correction.
    % if not specified, order == 1;

    % OUTPUTS: 
    % A: a 2x1 vector. A(1) is the scaling coefficient "alpha",
    % A(2) the intercept of the log-log regression, useful for plotting (see examples).
    % F: A vector of size Nx1 containing the fluctuations corresponding to the
    % windows specified in entries in pts.
    % -----------------------------------------------------

    % Checking the inputs
    if nargin < 3
        order = 1;
    end
    if nargin < 2
        pts = round(numel(data) / 10);
        if pts < 50
            pts = 50;
        end
    end
    if numel(pts) == 1 
        pts = unique(round(logspace(log10(5), log10(pts), 50)));
    end
    if numel(pts) == 2
        pts = unique(round(logspace(log10(pts(1)), log10(pts(2)), 50)));
    end
    sz = size(data);
    if sz(1) < sz(2)
        data = data';
    end

    exit = 0;

    if min(pts) == order + 1
        disp(['WARNING: The smallest window size is ' num2str(min(pts)) '. DFA order is ' num2str(order) '.'])
        disp('This severly affects the estimate of the scaling coefficient')
        disp('(If order == [] (so 1), the corresponding fluctuation is zero.)')
    elseif min(pts) < (order + 1)
        disp(['ERROR: The smallest window size is ' num2str(min(pts)) '. DFA order is ' num2str(order) ':'])
        disp(['Aborting. The smallest window size should be of ' num2str(order + 1) ' points at least.'])
        exit = 1;
    end

    if exit == 1
        return
    end


    % DFA
    npts = numel(pts);

    F = zeros(npts, 1);
    N = length(data);


    for h = 1:npts
     
        w = pts(h);
     
        n = floor(N / w);
        Nfloor = n * pts(h);
        D = data(1:Nfloor);
     
        y = cumsum(D - mean(D));
     
        bin = 0:w:(Nfloor - 1);
        vec = 1:w;
     
        coeff = arrayfun(@(j) polyfit(vec',y(bin(j) + vec),order), 1:n, 'uni', 0);
        y_hat = cell2mat(cellfun(@(y) polyval(y,vec), coeff, 'uni', 0));
        F(h) = mean((y - y_hat').^2)^0.5;
     
    end
    y = log(F)';
    x = log(pts);
    if nargout == 0
        figure
        plot(x, y, 'o')
    end
    [A, stat] = polyfit(x, y, 1);
    if nargout == 2
        [A1, stat1] = polyfit(x(1:round(length(x) / 2)), y(1:round(length(x) / 2)), 1);
        stats.R2_half = 1 - (stat1.normr / norm(y(1:round(length(x) / 2)) - mean(y(1:round(length(x) / 2)))))^2;
        stats.R2 = 1 - (stat.normr / norm(y - mean(y)))^2;
        stats.F = F';
        stats.pts = pts;
        stats.fit = A;
        stats.fit_half = A1;
    end
    A = A(1);
end





