%function generic_ews_sens
% make heatmaps of the sensitivity of the tau values of generic_ews of the
% window size and bandwithd
%
% Usage
%
% generic_ews_sens(timeseries, option pairs)
%
% all options pairs: (with defaults)
% res= generic_ews_sens(timeseries, 'winsize', (5:5:90), 'detrending', 'no', ...
%     'bandwidth', (1:1:20), 'logtransform', false, 'interpolate', false)
% To draw plots:
% generic_ews_sens('-p',res) plot all results.
% generic_ews_sens('-p',res,'indicators',{'AR'},'units',unit) - some options
% can be changed for plotting:
%     - 'indicators' the indicators to be plotted
%     - 'defaultpos' position of the default settings
%     - 'units' the units for the bandwidth and window size in the plot
%     - 'title' title of the figure, default the indicator
%  other changed settings are ignored
%
% generic_ews_sens('-f',res) determine the Fisher combined probability plots of 
%        the results res. res should be a struct array or cell array of earlier results
% s=generic_ews_sens('-defaultopts') - create a struct s with the default
% settings (used internally)
%
%
% Arguments (only those different from generic_ews)
%
% winsize	   - an array with the window sizes to be tested (in % of the
%                size of the time series)
%
% bandwidth	   - an array with the bandwidths to be tested (in % of the
%                size of the time series)
% defaultpos   - position of a star with the default settings (bandwidth,winsize) (optional)
% bandwidth_unit - unit of bandwidth (see units of winsize)
% winsize_unit - units of the  winsize - if different from '%' the absolute
%                units of the time series are assumed. If % the real size
%                is calculated from the length of the time series
% distribution - way of selecting bandwidth/winsize (regular/uniform)
%                (default regular)
% ndraws       - number of draws from distribution (ignored when regular)
%                (default = 1000)
%
%
% generic_ews_sens returns a struct with the fields:
%
%     bandwidths:   matrix with all bandwidth tested
%     winsizes:     matrix with the window sizes
%     taus:         3D matix with the taus (taus(:,:,1)=AR1,
%                   taus(:,:,2)=acf, taus(:,:,3)=standard dev/CV, taus(:,:,4)=skewness)
%     EWSopt        the options used
%     inds          the timeseries and the lastly calculated indicators (see generic_ews)
%     colnames      the names of the columns (see generic_ews)
%
%
%
% Author(s)
%
% Vasilis Dakos vasilis.dakos@gmail.com
% MATLAB version by Egbert van Nes
%
% References
%
% Dakos, V., et al (2008). "Slowing down as an early warning signal for abrupt climate change." Proceedings of the National Academy of Sciences 105(38): 14308-14312
%
% Dakos, V., et al (2012)."Methods for Detecting Early Warnings of Critical Transitions in Time Series Illustrated Using Simulated Ecological Data." PLoS ONE 7(7): e41010. doi:10.1371/journal.pone.0041010
%
%
function [res] = generic_ews_sens(timeseries, varargin)
    if isfield(timeseries, 'timeseries')
        varargin = [{timeseries} varargin];
        timeseries = '-p';
    end
    if nargin > 0 && ischar(timeseries)
        if strncmp(timeseries, '-p', 2)
            %plot the figures
            if numel(varargin{1}) > 1
                res = varargin{1};
                for i = 1: numel(varargin{1})
                    generic_ews_sens('-p', res(i), varargin{2:end});
                end
                return;
            end
            EWSopt = varargin{1}.EWSopt;
            if length(varargin) > 1
                EWSopt = mergeoptions(EWSopt, varargin(2:end));
            end
            results = varargin{1};
            %bandwidths = varargin{1}.bandwidths;
            %winsizes = varargin{1}.winsizes;
            taus = varargin{1}.taus;
            ebitaus = varargin{1}.ebitaus;
            colnames = varargin{1}.colnames;
            %EWSopt.indicators = varargin{1}.colnames;
            if ischar( EWSopt.indicators)
                EWSopt.indicators = {EWSopt.indicators};
            end
            figs = EWSopt.indicators;
            if ischar(figs)
                figs = {figs};
            elseif isnumeric(figs)
                figs = EWSopt.indicators(figs);
            end
            if ischar(EWSopt.bandwidth_unit)
                EWSopt.bandwidth_unit = {EWSopt.bandwidth_unit};
            end
            if numel(EWSopt.bandwidth_unit) == 1 && size(timeseries, 2) > 1
                EWSopt.bandwidth_unit = repmat(EWSopt.bandwidth_unit, 1, size(timeseries, 2));
            end
            %             bw_unit = EWSopt.bandwidth_unit;
            %             win_unit = EWSopt.winsize_unit;
            %             if isfield(EWSopt, 'units')
            %                 units = EWSopt.units;
            %             else
            %                 units = {'%', '%'};
            %             end
            %             if ischar(units)
            %                 units = {units, units};
            %             end
            %             if length(units) == 1
            %                 units = [units units];
            %             end
            %             if any(strcmp(bw_unit{1}, '%'))
            %                 bandwidths = bandwidths .* EWSopt.datalength ./ 100;
            %             end
            %             if strcmp(win_unit, '%')
            %                 winsizes = winsizes .* EWSopt.datalength ./ 100;
            %             end
            if isfield(EWSopt, 'defaultpos')
                defaultpos = EWSopt.defaultpos;
                if iscell(defaultpos)
                    defaultpos = [defaultpos{:}];
                end
            else
                defaultpos = [];
            end
            titles = colnames;
            funcs=generic_ews('-funhandles',EWSopt);
            funcnames={funcs.name};
            for i = 1:numel(funcnames)
                fignr = find(strcmp(colnames, funcnames{i}));
                if isempty(fignr)
                    error('grind:generic_ews_sens:unknowindicator', 'indicator %s unknown', funcnames{i});
                end
                if ~isempty(EWSopt.title)
                    atitle = EWSopt.title;
                else
                    atitle = titles{fignr};
                end
                plot_sensitiv(results, taus(:, :, fignr), fignr, [], [], defaultpos, atitle, EWSopt);
            end
            if ~isempty(ebitaus)
                if ~isfield(EWSopt,'distribution')||strcmp(EWSopt.distribution, 'regular')
                    titles = colnames;
                    levels = 0:0.05:0.3;
                    for i = 1:numel(funcnames)
                        fignr = find(strcmp(colnames, funcnames{i}));
                        if ~isempty(EWSopt.title)
                            atitle = ['p-value''s ' EWSopt.title ];
                        else
                            atitle = ['p-value''s ' titles{fignr}];
                        end
                        pvalues = getpvalues(taus(:, :, fignr), ebitaus(:, :, fignr, :));
                        plot_sensitiv(results, pvalues, fignr, [0 0.2], levels, defaultpos, atitle, EWSopt);
                    end
                else
                    for i = 1:numel(funcnames)
                        fignr = find(strcmp(colnames, funcnames{i}));
                        if ~isempty(EWSopt.title)
                            atitle = ['p-value''s ' EWSopt.title ];
                        else
                            atitle = ['p-value''s ' titles{fignr}];
                        end
                        pvalues = getpvalues(taus(:, :, fignr), ebitaus(:, :, fignr, :));
                        figure;
                        violinplot(pvalues, ones(size(pvalues)));
                        title(atitle)
                    end
                end
            end
            return;
        elseif strcmp(timeseries, '-setcolor')
            c1 = findobj(gcf, 'tag', 'perc_fill');
            if ~isempty(c1)
                %something wrong with this code... (it works only once)
                colors = get(c1, 'FaceColor');
                colors = vertcat(colors{:});
                [~, i] = max(var(colors, 1));
                rangecolors = 1 - (colors - max(colors(:, i))) ./ (min(colors(:, i) - max(colors(:, i))));
                mincolor = varargin{1};
                if ischar(mincolor)
                    colorcodes = {[1 1 0], 'y'; ...
                        [1 0 1], 'm'; ...
                        [0 1 1], 'c'; ...
                        [1 0 0], 'r'; ...
                        [0 1 0], 'g'; ...
                        [0 0 1], 'b'; ...
                        [1 1 1], 'w'; ...
                        [0 0 0], 'k'};
                    ndx = strcmp(mincolor, colorcodes(:, 2));
                    mincolor = colorcodes{ndx,1};
                end
                maxcolor = ones(1, 3);
                for i = 1:length(c1)
                    set(c1(i), 'facecolor', (mincolor + (maxcolor - mincolor) * (rangecolors(i))));
                end
            else
                error('generic_ews_sens:notfound', 'series not found');
            end
            return;
        elseif strncmpi(timeseries, '-f', 2) %fisher combine previous results
            inputs1 = varargin{1};
            inputs1 = generic_ews_sens('-add_pvalues', inputs1);
            EWSopt = inputs1(1).EWSopt;
            if length(varargin) > 1
                EWSopt = mergeoptions(EWSopt, varargin(2:end));
            end
            ind_ndx = find(ismember(inputs1(1).colnames, EWSopt.indicators));
            if isempty(ind_ndx)
                error('generic_ews_sens:fisher', 'None of the indicators were found');
            end
            res1.pvalues = {inputs1(:).pvalues};
            res1.fisher_pvalues = ones(size(inputs1(1).taus, 1), size(inputs1(1).taus, 2), length(ind_ndx));
            res1.fisher_chi = ones(size(res1.fisher_pvalues));
            titles = inputs1(1).colnames;
            levels = 0:0.05:0.3;
            if iscell(EWSopt.defaultpos)
                defaultpos = [EWSopt.defaultpos{:}];
            else
                defaultpos = EWSopt.defaultpos;
            end
            for k = 1:length(ind_ndx) %loop the different indicators
                for f1 = 1:size(inputs1(1).taus, 1)
                    for f2 = 1:size(inputs1(1).taus, 2)
                        ps = zeros(size(inputs1));
                        for i = 1:numel(inputs1)
                            ps(i) = inputs1(i).pvalues(f1, f2, ind_ndx(k));
                        end
                        fisher1 = fishercombine(ps);
                        res1.fisher_pvalues(f1, f2, k) = fisher1.p;
                        res1.fisher_chi(f1, f2, k) = fisher1.chi;
                    end
                end
                res1.fisher_df = fisher1.df;
            end
            res1.hfigs = [];
            for k = 1:length(ind_ndx)
                if ~isempty(EWSopt.title)
                    atitle = ['Fisher p ' EWSopt.title ];
                else
                    atitle = ['Fisher p ' titles{ind_ndx(k)}];
                end
                res1.hfigs(k) = plot_sensitiv(inputs1(1), res1.fisher_pvalues(:, :, k), ...
                    k, [0 0.3], levels, defaultpos, atitle, EWSopt);
            end
            for i = 1:length(res1.pvalues)
                res1.pvalues{i} = res1.pvalues{i}(:, :, ind_ndx);
            end
            if nargout > 0
                res = res1;
            end
            return;
        elseif strcmp(timeseries, '-add_pvalues') %get the p-values
            res = varargin{1};
            res(1).pvalues = [];
            if numel(res) > 1
                for i = numel(res): -1:1
                    res(i) = generic_ews_sens('-add_pvalues', res(i));
                end
            else
                res.pvalues = ones(size(res.taus));
                for k = 1:size(res(1).taus, 3)
                    res.pvalues(:, :, k) = getpvalues(res.taus(:, :, k), res.ebitaus(:, :, k, :));
                end
            end
            return
        elseif strcmp(timeseries, '-defaultopts')
            res = generic_ews('-defaultopts');
            %default winsizes and bandwidths
            res.winsize = (5:5:90);
            res.bandwidth = (1:1:20);
            res.defaultpos = {[]};
            res.distribution = 'regular';
            res.ndraws = 1000;
            return;
        end
    end
    EWSopt = generic_ews_sens('-defaultopts');
    EWSopt = mergeoptions(EWSopt, varargin);
    sens_opts = EWSopt;
    
    %override the value of silent to suppress too many figures
    EWSopt.silent = true;
    if exist('i_waitbar', 'file') == 0
        initgrind;
    end
    if strcmp(EWSopt.distribution, 'regular')
        inds = [];
        taus = zeros(numel(EWSopt.winsize), numel(EWSopt.bandwidth), numel(EWSopt.indicators));
        ebitaus = zeros(numel(EWSopt.winsize), numel(EWSopt.bandwidth), numel(EWSopt.indicators), EWSopt.ebisuzaki);
        [bandwidths, winsizes] = meshgrid(EWSopt.bandwidth, EWSopt.winsize);
        wb = i_parfor_waitbar(0, size(winsizes, 1), 'Calculating sensitivity');
        parfor i = 1:size(winsizes, 1)
            taus_loc = taus(i, :, :);
            ebitaus_loc = ebitaus(i, :, :, :);
            for j = 1:size(winsizes, 2)
                EWSopt1 = EWSopt;
                EWSopt1.winsize = winsizes(i, j);
                EWSopt1.bandwidth = bandwidths(i, j);
                res2 = generic_ews(timeseries, EWSopt1);
                taus_loc(1, j, :) = res2.taus;
                if ~isempty(res2.ebitaus)
                    ebitaus_loc(1, j, :, :) = res2.ebitaus;
                end
            end
            i_parfor_waitbar(wb, i);
            taus(i, :, :) = taus_loc;
            ebitaus(i, :, :, :) = ebitaus_loc;
        end
    elseif strcmp(EWSopt.distribution, 'uniform')
        taus = zeros(1, EWSopt.ndraws, numel(EWSopt.indicators));
        inds = zeros(EWSopt.ndraws, size(timeseries, 1), numel(EWSopt.indicators));
        ebitaus = zeros(1, EWSopt.ndraws, numel(EWSopt.indicators), EWSopt.ebisuzaki);
        bandwidths = min(EWSopt.bandwidth) + rand(EWSopt.ndraws, 1) * (max(EWSopt.bandwidth) - min(EWSopt.bandwidth));
        winsizes = min(EWSopt.winsize) + rand(EWSopt.ndraws, 1) * (max(EWSopt.winsize) - min(EWSopt.winsize));
        wb = i_parfor_waitbar(0, EWSopt.ndraws, 'Calculating sensitivity');
        for i = 1:EWSopt.ndraws
            EWSopt1 = EWSopt;
            EWSopt1.winsize = winsizes(i);
            EWSopt1.bandwidth = bandwidths(i);
            res2 = generic_ews(timeseries, EWSopt1);
            taus(1, i, :) = res2.taus;
            if ~isempty(res2.ebitaus)
                ebitaus(1, i, :, :) = res2.ebitaus;
            end
            inds(i, :, :) = res2.indicators;
            i_parfor_waitbar(wb, i);
        end
    end
    EWSopt1 = EWSopt;
    EWSopt1.winsize = winsizes(1);
    EWSopt1.bandwidth = bandwidths(1);
    EWSopt1.ebisuzaki = 0;

    inputs1 = generic_ews(timeseries, EWSopt1);
    EWSopt1.ebisuzaki = EWSopt.ebisuzaki;
    i_parfor_waitbar([]);
    %save the basic data per run
    if ~isempty(inds)
        inputs1.indicators = inds;
    end
    inputs1.bandwidths = bandwidths;
    inputs1.winsizes = winsizes;
    inputs1.taus = taus;
    if EWSopt.ebisuzaki > 0
        inputs1.ebitaus = ebitaus;
    else
        inputs1.ebitaus = [];
    end
    inputs1.EWSopt = inputs1.EWSopt;
    inputs1.EWSopt.winsize = sens_opts.winsize;
    inputs1.EWSopt.bandwidth = sens_opts.bandwidth;
    inputs1.EWSopt.silent = sens_opts.silent;
    %   res1.indicators = res1.indicators;
    %   res1.colnames = res1.colnames;
    if nargout > 0
        res = inputs1;
    end
end
function hfig = plot_sensitiv(results, x, fignr, climit, levels, defaultpos, atitle, EWSopt)
    hfig = figure;
    %     pcolor(bandwidths, winsizes, x);
    %     shading flat
    if isfield(EWSopt,'distribution')&&strcmp(EWSopt.distribution, 'uniform')
        hold on
        percentiles = 0.05:0.025:0.95;
        maxcolor = [0.1 0.1 0.1];
        mincolor = [0.9 0.9 0.9];
        cs = 0:find(abs(percentiles - 0.5) < 0.01) - 1;
        cs = cs ./ (max(cs(:)));
        cs = [cs(1:end - 1), fliplr(cs)];
        data = quantile(results.indicators(:, :, fignr), percentiles)';
        ndx = ~isnan(data(:, 1));
        for i = 1:length(percentiles) - 1
            c = mincolor + cs(i) .* (maxcolor - mincolor);
            fill([EWSopt.time(ndx); flipud(EWSopt.time(ndx))], [data(ndx, i); flipud(data(ndx, i + 1))], c);
        end
        set(get(gca, 'children'), 'EdgeColor', 'none', 'tag', 'perc_fill')
        plot(EWSopt.time, quantile(results.indicators(:, :, fignr), 0.5), 'color', maxcolor, 'tag', 'median');
        xlabel('Time');
        
        xlims = [min(EWSopt.time), max(EWSopt.time)];
        xlim(xlims)
        text(0.1, 0.9, results.colnames{fignr}, 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'toptext')
        i_plotdefaults;
        %plot(EWSopt.time,results.indicators(:, :, fignr))
    else
        if ~isempty(levels)
            contourf(results.bandwidths, results.winsizes, x, levels);
        else
            contourf(results.bandwidths, results.winsizes, x);
        end
        if ~isempty(climit)
            set(gca, 'clim', climit)
        end
        if ~isempty(defaultpos)
            hold on
            h1 = plot(defaultpos(1), defaultpos(2), 'k*');
            set(h1, 'markersize', 6);
            hold off
        end
        i_plotdefaults;
        xlim([min(results.bandwidths(:)) max(results.bandwidths(:))]);
        ylim([min(results.winsizes(:)) max(results.winsizes(:))]);
        xlabel(sprintf('filtering bandwidth (%s)', EWSopt.bandwidth_unit{1}))
        ylabel(sprintf('sliding window (%s)', EWSopt.winsize_unit));
        h = colorbar;
        i_plotdefaults;
        ylabel(h, atitle);
    end
 %   title(atitle);
    set(hfig, 'tag', atitle);
end
function pvalues = getpvalues(taus, ebitaus)
    nebisuzaki = size(ebitaus, 4);
    pvalues = zeros(size(taus));
    for i1 = 1:size(taus, 1)
        for j1 = 1:size(taus, 2)
            pvalues(i1, j1) = (sum(taus(i1, j1) < ebitaus(i1, j1, 1, :)) + 1) / nebisuzaki;
        end
    end
end
function EWSopt = mergeoptions(EWSopt, arg)
    f = fieldnames(EWSopt);
    if ~isempty(arg) && isstruct(arg{1})
        %all options can be entered as struct
        if exist('mergestructs', 'file') == 0
            initgrind;
        end
        %replace common fields in EWSopt
        EWSopt = mergestructs(EWSopt, arg{1});
    else
        for i = 1:2:length(arg)
            if strcmp(arg{i},'units')
                EWSopt.bandwidth_unit = arg{i + 1};
                EWSopt.winsize_unit = arg{i + 1};
            elseif ~any(strcmp(f, arg{i}))
                s = sprintf('"%s", ', f{:});
                error('Unknown option for generic_ews_sens: %s\nValid options are: %s', arg{i}, s(1:end - 1));
            end
            EWSopt.(arg{i}) = arg{i + 1};
        end
    end
end
