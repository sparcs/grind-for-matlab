unit SymbolDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TSymbolDialog = class(TForm)
    SymbolCombo: TComboBox;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetColumn(s: string; notList: TListBox);
    function SymbolID(s: string): integer;
  end;

var
  SymbolDialog: TSymbolDialog;

implementation

uses GrindComps, visgrind;
{$R *.DFM}

procedure TSymbolDialog.SetColumn(s: string; notList: TListBox);
var
  i: integer;
  list: TList;

  function HasSymbol(symb: string): boolean;
  var
    j: integer;
  begin
    with notList do
    begin
      j := 0;
      while (j < Items.Count) and
        (trim(copy(Items[j], pos(' --> ', Items[j]) + 5, length(Items[j])))
        <> symb) do
        inc(j);
      Result := j < Items.Count;
    end;
  end;

begin
  SymbolCombo.Items.Clear;
  if not HasSymbol('t') then
    SymbolCombo.Items.Add('t');
  list := MainForm.GrindModel.CompsOfType(ctStatevar);
  for i := 0 to list.Count - 1 do
    if not HasSymbol(TGrindComponent(list.Items[i]).Symbol) then
      SymbolCombo.Items.AddObject(TGrindComponent(list.Items[i]).Symbol,
        TGrindComponent(list.Items[i]));
  list := MainForm.GrindModel.CompsOfType(ctExternvar);
  for i := 0 to list.Count - 1 do
    if not HasSymbol(TGrindComponent(list.Items[i]).Symbol) then
      SymbolCombo.Items.AddObject(TGrindComponent(list.Items[i]).Symbol,
        TGrindComponent(list.Items[i]));
  if pos(': ', s) > 0 then
    s := copy(s, pos(': ', s) + 2, length(s));
  i := SymbolID(s);
  if i < 0 then
  begin
    i := 0;
    while (i < SymbolCombo.Items.Count) and
      (SymbolCombo.Items[i] <> copy(s, 1, length(SymbolCombo.Items[i]))) do
      inc(i);
  end;
  if i = SymbolCombo.Items.Count then
    SymbolCombo.ItemIndex := 0
  else
    SymbolCombo.ItemIndex := i;
end;

function TSymbolDialog.SymbolID(s: string): integer;
begin
  Result := 0;
  while (Result < SymbolCombo.Items.Count) and
    (s <> SymbolCombo.Items[Result]) do
    inc(Result);
  if Result >= SymbolCombo.Items.Count then
    Result := -1;
end;

end.
