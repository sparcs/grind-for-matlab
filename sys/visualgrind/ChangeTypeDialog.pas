unit ChangeTypeDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, GrindComps;

type
  TChangeTypeDlg = class(TForm)
    Label1: TLabel;
    TypesList: TListBox;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
  private
    function GetTheType: TCompType;
    procedure SetTheType(const Value: TCompType);
    { Private declarations }
  public
    property TheType: TCompType read GetTheType write SetTheType;
    { Public declarations }
  end;

var
  ChangeTypeDlg: TChangeTypeDlg;

implementation

{$R *.DFM}
{ TChangeTypeDlg }

function TChangeTypeDlg.GetTheType: TCompType;
begin
  Result := TCompType(TypesList.ItemIndex);
end;

procedure TChangeTypeDlg.SetTheType(const Value: TCompType);
begin
  TypesList.ItemIndex := integer(Value);
end;

end.
