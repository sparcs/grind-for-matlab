unit FileLst;

interface

uses Menus;

const
  MaxListednames = 10;
  MaxLenDisplayed = 50;

type
  TFileTypes = record
    FileName: string;
    FileType: Integer;
  end;

  TFileTypesAr = array [0 .. MaxListednames - 1] of TFileTypes;

  TFileListEvent = procedure(Sender: TObject; FileName: string;
    FileType: Integer) of object;

  TFileList = class
  private
    FMaxListedFiles: Integer;
  protected
    FMenu: TMenuItem;
    First: Integer;
    FOnClick: TFileListEvent;
    ItemsCount: Integer;
    Files: TFileTypesAr;
    procedure SetMaxListedFiles(aN: Integer);
    procedure ChangeMenu;
    procedure DisposeFileList;
    procedure FileListExecute(Sender: TObject);
  public
    constructor Create(aMenu: TMenuItem; aMaxListedFiles: Integer;
      AOnClick: TFileListEvent);
    destructor Destroy; override;
    procedure SaveFileNames(var f: textfile);
    procedure LoadFileNames(var f: textfile);
    procedure AddFile(aFileName: string; aFileType: Integer);
    property OnClick: TFileListEvent read FOnClick write FOnClick;
    property Menu: TMenuItem read FMenu write FMenu;
    property MaxListedFiles: Integer read FMaxListedFiles
      write SetMaxListedFiles;
  end;

implementation

uses SysUtils;

const
  NilType: TFileTypes = (FileName: ''; FileType: 0);
  Max_Path = 255;

  { -------------------------------------
    TFileList
    -------------------------------------- }

constructor TFileList.Create(aMenu: TMenuItem; aMaxListedFiles: Integer;
  AOnClick: TFileListEvent);
var
  I: Integer;
begin
  Menu := aMenu;
  OnClick := AOnClick;
  First := 0;
  ItemsCount := aMenu.Count;
  FMaxListedFiles := aMaxListedFiles;
  for I := 0 to MaxListednames - 1 do
    Files[I] := NilType;
end;

procedure TFileList.SetMaxListedFiles(aN: Integer);
var
  OudFiles: TFileTypesAr;
  J, K: Integer;
begin
  if aN <> FMaxListedFiles then
  begin
    if aN > MaxListednames then
      aN := MaxListednames;
    Move(Files, OudFiles, SizeOf(Files));
    for J := 0 to MaxListednames - 1 do
      Files[J] := NilType;
    J := First;
    ChangeMenu;
    K := 0;
    while OudFiles[J].FileName <> '' do
    begin
      if K < aN then
        Files[K] := OudFiles[J];
      OudFiles[J] := NilType;
      Inc(J);
      Inc(K);
      if J >= FMaxListedFiles then
        J := 0;
    end;
    FMaxListedFiles := aN;
    First := 0;
    ChangeMenu;
  end;
end;

procedure TFileList.AddFile(aFileName: string; aFileType: Integer);
var
  I: Integer;
  H, H2: Integer;
begin
  if (aFileName <> '') and (MaxListedFiles > 0) then
  begin
    aFileName := LowerCase(aFileName);
    if First > 0 then
      Dec(First)
    else
      First := MaxListedFiles - 1;
    H := -1;
    for I := 0 to MaxListedFiles - 1 do
      if (Files[I].FileName <> '') and (Files[I].FileName = aFileName) then
        with Files[I] do
        begin
          H := I;
          if FileName <> aFileName then
            FileName := '';
          Files[I] := NilType;
        end;
    if H >= 0 then
    begin
      while H <> First do
      begin
        H2 := H;
        Inc(H);
        if H >= MaxListedFiles then
          H := 0;
        Files[H2] := Files[H];
      end;
      Files[First] := NilType;
    end;
    with Files[First] do
    begin
      FileName := aFileName;
      FileType := aFileType;
    end;
    ChangeMenu;
  end;
end;

function StrShortenFileName(Source, CurPath: string; MaxLen: Integer): string;
var
  LenCurPath: Integer;
  P, P2: Integer;
  function rPos(substr: char; s: string): Integer;
  begin
    Result := length(s);
    while (Result > 0) and (s[Result] <> substr) do
      Result := Result - 1;
  end;

begin
  if CurPath = '' then
    CurPath := GetCurrentDir;
  LenCurPath := length(CurPath);

  if (length(Source) + 1 > length(CurPath)) and
    (CurPath = copy(Source, 1, LenCurPath)) and
    (pos('\', copy(Source, LenCurPath + 1, length(Source))) = 0) then
  begin
    if Source[LenCurPath] = '\' then
      Inc(LenCurPath);
    Result := copy(Source, 1, LenCurPath);
  end
  else
    Result := Source;
  if length(Result) > MaxLen then
  begin
    P := pos('\', Result);
    P2 := rPos('\', Result);
    if (P <> 0) and (P2 <> 0) and (P < P2) then
      Result := copy(Result, 1, P) + '...' + copy(Result, P2, length(Result));
  end;
  if length(Result) > MaxLen then
    Result := copy(Result, 1, MaxLen) + '...';
end;

procedure TFileList.ChangeMenu;
var
  Comm, CurPath: string;
  I, J: Word;
  V: string;
  Item: TMenuItem;

  procedure AppendMenuItem(s: string);
  var
    NewItem: TMenuItem;
  begin
    NewItem := TMenuItem.Create(Menu);
    NewItem.Caption := s;
    if s <> '-' then
      NewItem.OnClick := FileListExecute;
    Menu.Add(NewItem);
  end;

begin
  for I := Menu.Count - 1 downto ItemsCount + 1 do
  begin
    Item := Menu.Items[I];
    Menu.Delete(I);
    Item.Free;
  end;
  J := First;
  CurPath := GetCurrentDir;;
  for I := 1 to MaxListedFiles do
    if Files[J].FileName <> '' then
    begin
      V := IntToStr(I);
      Comm := '&' + V + ' ' + StrShortenFileName(Files[J].FileName, CurPath,
        MaxLenDisplayed);
      if I = 1 then
        AppendMenuItem('-');
      AppendMenuItem(string(Comm));
      Inc(J);
      if J >= MaxListedFiles then
        J := 0;
    end;
end;

procedure TFileList.DisposeFileList;
var
  I: Integer;
begin
  for I := 0 to MaxListedFiles - 1 do
    with Files[I] do
    begin
      FileName := '';
      Files[I] := NilType;
    end;
end;

destructor TFileList.Destroy;
begin
  DisposeFileList;
  inherited Destroy;
end;

procedure TFileList.FileListExecute(Sender: TObject);
var
  s: string;
  P: Integer;
  J, Err: Integer;
begin
  P := pos(' ', TMenuItem(Sender).Caption);
  s := copy(TMenuItem(Sender).Caption, 2, P - 2);
  Val(s, J, Err);
  J := First + J - 1;
  if J >= MaxListedFiles then
    Dec(J, MaxListedFiles);
  if (Err = 0) and Assigned(OnClick) then
    OnClick(Sender, Files[J].FileName, Files[J].FileType);
end;

procedure TFileList.LoadFileNames(var f: textfile);
var
  I: Integer;
  s: string;
begin
  if not eof(f) then
  begin
    readln(f, s);
    ItemsCount := strtoint(s);
    readln(f, First);
    for I := 0 to ItemsCount - 1 do
    begin
      Files[I] := NilType;
      readln(f, Files[I].FileType);
      readln(f, Files[I].FileName);
      if not fileexists(Files[I].FileName) then
        Files[I] := NilType;
    end;
    ChangeMenu;
  end;
end;

procedure TFileList.SaveFileNames(var f: textfile);
var
  I: Integer;
begin
  writeln(f, ItemsCount);
  writeln(f, First);
  for I := 0 to ItemsCount - 1 do
  begin
    writeln(f, Files[I].FileType);
    writeln(f, Files[I].FileName);
  end;
end;

end.
