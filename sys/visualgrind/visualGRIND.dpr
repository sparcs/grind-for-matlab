program visualGRIND;
{$R *.res}

uses
  Forms,
  visgrind in 'visgrind.pas' {MainForm},
  CompDialog in 'CompDialog.pas' {ComponentDlg},
  ModelDialog in 'ModelDialog.pas' {ModelDlg},
  ParseClass in 'ParseClass.pas',
  ParseExpr in 'ParseExpr.pas',
  oObjects in 'oObjects.pas',
  SelectDlg in 'SelectDlg.pas' {SelectDialog},
  StylesDlg in 'StylesDlg.pas' {StylesDialog},
  FileLst in 'FileLst.pas',
  prgTools in 'prgTools.pas',
  ErrorDialog in 'ErrorDialog.pas' {ErrorDlg},
  NoInifileDlg in 'NoInifileDlg.pas' {NoInifileDialog},
  MultiLineDlg in 'MultiLineDlg.pas' {MultilineDialog},
  CompFrm in 'CompFrm.pas' {CompFrame: TFrame},
  FindFileDlg in 'FindFileDlg.pas' {FindFileDialog},
  ChangeTypeDialog in 'ChangeTypeDialog.pas' {ChangeTypeDlg},
  DataFileDlg in 'DataFileDlg.pas' {DataFileDialog},
  SymbolDlg in 'SymbolDlg.pas' {SymbolDialog},
  reorderdlg in 'reorderdlg.pas' {ReorderDialog},
  AboutDlg in 'AboutDlg.pas' {AboutBox},
  GRindcomps in 'GRindcomps.pas',
  SymbolUsedDialog in 'SymbolUsedDialog.pas' {SymbolErrorDlg},
  MatrixDlg in 'MatrixDlg.pas' {MatrixDialog},
  MemoLongStrings in 'MemoLongStrings.pas',
  UnitDlg in 'UnitDlg.pas' {UnitDialog};

//{$R *.RES}

begin
  { Here is a typical simple use of MemCheck (works in most cases)...
    dd the MemCheck unit to your project (in the uses clause of the DPR).
    In the first line after the begin of your project (in the dpr file),
    call MemChk. Compile (build all) your project without optimization,
    with stack frames, with TD32 debug info (see other question about project options). Run your program. When your program terminates, MemCheck will create a log file in your temp directory. This file contains the list of Memory leaks.
    MemCheck will also raise an exception at the place where a leak occured. When you press F9, it will raise an exception at the next memory leak's place, and so on. What if your program terminates and nothing happens ? That means there are no memory leaks.
  }
  // MemChk;


  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TComponentDlg, ComponentDlg);
  Application.CreateForm(TModelDlg, ModelDlg);
  Application.CreateForm(TSelectDialog, SelectDialog);
  Application.CreateForm(TStylesDialog, StylesDialog);
  Application.CreateForm(TErrorDlg, ErrorDlg);
  Application.CreateForm(TNoInifileDialog, NoInifileDialog);
  Application.CreateForm(TMultilineDialog, MultilineDialog);
  Application.CreateForm(TFindFileDialog, FindFileDialog);
  Application.CreateForm(TChangeTypeDlg, ChangeTypeDlg);
  Application.CreateForm(TDataFileDialog, DataFileDialog);
  Application.CreateForm(TSymbolDialog, SymbolDialog);
  Application.CreateForm(TReorderDialog, ReorderDialog);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TSymbolErrorDlg, SymbolErrorDlg);
  Application.CreateForm(TMatrixDialog, MatrixDialog);
  Application.CreateForm(TUnitDialog, UnitDialog);
  Application.Run;

end.
