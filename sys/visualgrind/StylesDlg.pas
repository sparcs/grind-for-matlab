unit StylesDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ColorGrd, GrindComps, Buttons;

type
  TStylesDialog = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    StyleCombo: TComboBox;
    Width: TLabel;
    Height: TLabel;
    WidthEdit: TEdit;
    WidthUpDown: TUpDown;
    HeightUpDown: TUpDown;
    HeightEdit: TEdit;
    TextCheck: TCheckBox;
    SymbolCheck: TCheckBox;
    PenLabel: TLabel;
    PenColorGrid: TColorGrid;
    Pen2ColorGrid: TColorGrid;
    FillColorGrid: TColorGrid;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    PenStyleCombo: TComboBox;
    FillStyleCombo: TComboBox;
    ShapeCombo: TComboBox;
    Label6: TLabel;
    PenWidthEdit: TEdit;
    PenWidthUpDown: TUpDown;
    Label7: TLabel;
    ShapeGroup: TGroupBox;
    Pen1Group: TGroupBox;
    BrushGroup: TGroupBox;
    Pen2Group: TGroupBox;
    Label8: TLabel;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    Button1: TButton;
    FullList: TComboBox;
    ShortList: TComboBox;
    TabSheet8: TTabSheet;
    Button2: TButton;
    FontDialog1: TFontDialog;
    BitBtn1: TBitBtn;
    UserFunctionTab: TTabSheet;
    procedure PageControl1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure StyleComboChange(Sender: TObject);
    procedure ShapeComboChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    aStyles: TComponentStyles;
    currentStyle: integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StylesDialog: TStylesDialog;

implementation

uses visgrind;

{$R *.DFM}

procedure TStylesDialog.PageControl1Change(Sender: TObject);

  procedure SetStyle(n: integer);
  begin
    with aStyles[TCompType(n)] do
    begin
      WidthUpDown.Position := Width;
      HeightUpDown.Position := Height;
      PenWidthUpDown.Position := PenWidth;
      if TCompType(n) in [ctStatevar, ctPermanentvar, ctAuxilvar, ctParameter, ctExternvar,
        ctCloud, ctUserFunction] then
      begin
        ShapeCombo.Items.Assign(ShortList.Items);
        ShapeCombo.Enabled := True;
      end
      else
      begin
        ShapeCombo.Items.Assign(FullList.Items);
        ShapeCombo.Enabled := False;
      end;
      ShapeCombo.ItemIndex := integer(shape);
      SymbolCheck.Checked := showsymbol;
      TextCheck.Checked := showtext;
      PenStyleCombo.ItemIndex := integer(penstyle);
      FillStyleCombo.ItemIndex := integer(brushstyle);
      PenColorGrid.ForegroundIndex := PenColorGrid.ColorToIndex(pencolor);
      FillColorGrid.ForegroundIndex := FillColorGrid.ColorToIndex(brushcolor);
      Pen2ColorGrid.ForegroundIndex := Pen2ColorGrid.ColorToIndex(pencolor2);
    end;
  end;

begin
  ShapeGroup.parent := PageControl1.ActivePage;
  Pen1Group.parent := PageControl1.ActivePage;
  BrushGroup.parent := PageControl1.ActivePage;
  Pen2Group.parent := PageControl1.ActivePage;
  SetStyle(PageControl1.ActivePageIndex);
  ShapeComboChange(Sender);
end;

procedure TStylesDialog.FormShow(Sender: TObject);
begin
  Application.HintColor := clInfoBk;
  StyleCombo.ItemIndex := MainForm.StylesCombo.ItemIndex;
  currentStyle := StyleCombo.ItemIndex;
  aStyles := MainForm.GrindModel.StyleSchemes[currentStyle];
  PageControl1Change(Sender);
end;

procedure TStylesDialog.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  begin
    with aStyles[TCompType(PageControl1.ActivePageIndex)] do
    begin
      Width := WidthUpDown.Position;
      Height := HeightUpDown.Position;
      shape := TShapeKind(ShapeCombo.ItemIndex);
      showsymbol := SymbolCheck.Checked;
      showtext := TextCheck.Checked;
      PenWidth := PenWidthUpDown.Position;
      penstyle := TPenStyle(PenStyleCombo.ItemIndex);
      brushstyle := TBrushStyle(FillStyleCombo.ItemIndex);
      pencolor := PenColorGrid.ForegroundColor;
      brushcolor := FillColorGrid.ForegroundColor;
      pencolor2 := Pen2ColorGrid.ForegroundColor;
    end;
  end;
end;

procedure TStylesDialog.OKBtnClick(Sender: TObject);
var
  Allow: Boolean;
begin
  PageControl1Changing(Sender, Allow);
  MainForm.GrindModel.StyleSchemes[currentStyle] := aStyles;
  MainForm.StylesCombo.ItemIndex := currentStyle;
  MainForm.GrindModel.Styles := aStyles;
  Hide;
end;

procedure TStylesDialog.CancelBtnClick(Sender: TObject);
begin
  Hide;
end;

procedure TStylesDialog.StyleComboChange(Sender: TObject);
var
  Allow: Boolean;
begin
  PageControl1Changing(Sender, Allow);
  MainForm.GrindModel.StyleSchemes[currentStyle] := aStyles;
  currentStyle := StyleCombo.ItemIndex;
  aStyles := MainForm.GrindModel.StyleSchemes[currentStyle];
  PageControl1Change(Sender);
end;

procedure TStylesDialog.ShapeComboChange(Sender: TObject);
begin
  Pen2Group.Visible := ShapeCombo.ItemIndex
    in [integer(skValve), integer(skTrain)];
end;

procedure TStylesDialog.Button1Click(Sender: TObject);
begin
  MainForm.GrindModel.SetDefaultStyles(aStyles, currentStyle);
  PageControl1Change(Sender);
end;

procedure TStylesDialog.FormActivate(Sender: TObject);
begin
  Position := poMainFormCenter;
end;

procedure TStylesDialog.Button2Click(Sender: TObject);
begin
  FontDialog1.Font.Assign(MainForm.ThePaintBox.Font);
  if FontDialog1.Execute then
  begin
    MainForm.ThePaintBox.Font.Assign(FontDialog1.Font);
    // MainForm.MemBitmap.Canvas.Font.Assign(FontDialog1.Font);
    MainForm.TextEdit.Font.Assign(FontDialog1.Font);
    MainForm.SymbolEdit.Font.Assign(FontDialog1.Font);
  end
end;

procedure TStylesDialog.BitBtn1Click(Sender: TObject);
begin
  MainForm.HelpTopic('stylesdlg');
end;

end.
