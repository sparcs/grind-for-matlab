unit UnitDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TUnitDialog = class(TForm)
    UnitEdit: TEdit;
    UnitCombo: TComboBox;
    Label2: TLabel;
    Label4: TLabel;
    OKButton: TButton;
    Button1: TButton;
    UsedList: TListBox;
    UsedLabel: TLabel;
    BasicList: TListBox;
    Label6: TLabel;
    UnitButton: TButton;
    Button7: TButton;
    PrefixList: TListBox;
    Label1: TLabel;
    Label3: TLabel;
    PowerList: TListBox;
    PrevLabel: TLabel;
    NextLabel: TLabel;
    WarningLabel: TLabel;
    Label5: TLabel;
    StyleCombo: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure UsedListClick(Sender: TObject);
    procedure UnitEditClick(Sender: TObject);
    procedure PrefixListClick(Sender: TObject);
    procedure UnitButtonClick(Sender: TObject);
    procedure UnitComboChange(Sender: TObject);
    procedure BasicListClick(Sender: TObject);
    procedure StyleComboChange(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
  private
    procedure UpdateLists(subunit: string);
    procedure UpdatePowerList;
    function CaseIndexOf(list: TStrings; s: string): integer;
    procedure splitunit(var prefix, theunit, power: string;
      const subunit: string);
    procedure findsubunit(s: string; sel: integer; var i1, i2: integer);
    procedure readstyle;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UnitDialog: TUnitDialog;

implementation

uses grindcomps, visgrind, strUtils;
{$R *.dfm}

const
  basic_units: array [0 .. 6] of string = ('m', 'g', 's', 'A', 'K',
    'mol', 'cd');
  basic_hints: array [0 .. 7] of string = ('meter - length', 'gram - mass',
    'second - time', 'Ampere - electic current', 'Kelvin - temperature',
    'mole - amount of substance', 'candela - lumious intensity', '');
  units_no_prefix: array [0 .. 6] of string = ('cd', 'ha', 'Gy', 'Pa', 'yd',
    'day', 'days');
  prefix_hints: array [0 .. 21] of string = ('yotta - 10^24', 'zetta - 10^21',
    'exa - 10^18', 'peta - 10^15', 'tera - 10^12', 'giga - 10^9', 'mega - 10^6',
    'kilo - 10^3', 'hecto - 10^2', 'deca - 10^1', '', 'deci - 10^-1',
    'centi - 10^-2', 'milli - 10^-3', 'micro - 10^-6', 'micro - 10^-6',
    'nano - 10^-9', 'pico - 10^-12', 'femto - 10^-15', 'atto - 10^-18',
    'zepto - 10^-21', 'yocto - 10^-24');

type
  TUnitStyle = (scientific, division, dots, brackets);
  charset = set of widechar;

function chartrim(s: string; chset: charset): string;
var
  j: integer;
begin
  Result := s;
  if s <> '' then
  begin
    j := length(s);
    while (j > 0) and charInSet(s[j], chset) do
      j := j - 1;
    Result := copy(Result, 1, j);
    j := 1;
    while (j < length(Result)) and charInSet(Result[j], chset) do
      j := j + 1;
    Result := copy(Result, j, length(Result));
  end;
end;

function cleanupunit(s: string): string;
begin
  Result := chartrim(s, [' ', '.']);
  Result := StringReplace(Result, '^', '', [rfReplaceAll]);
  while pos('  ', Result) > 0 do
    Result := ReplaceStr(Result, '  ', ' ');
  while pos('..', Result) > 0 do
    Result := ReplaceStr(Result, '..', '.');
  Result := StringReplace(Result, ' /', '/', [rfReplaceAll]);
  Result := StringReplace(Result, '/ ', '/', [rfReplaceAll]);
  Result := StringReplace(Result, '( ', '(', [rfReplaceAll]);
  Result := StringReplace(Result, ' )', ')', [rfReplaceAll]);
  Result := StringReplace(Result, ') ', '(', [rfReplaceAll]);
  Result := StringReplace(Result, ' (', ')', [rfReplaceAll]);
  if Result = 'oC' then Result:='�C';
end;

procedure TUnitDialog.UpdatePowerList;
var
  ndx, i: integer;
begin
  case TUnitStyle(StyleCombo.ItemIndex) of
    division:
      begin
        PowerList.Items.BeginUpdate;
        ndx := PowerList.ItemIndex;
        PowerList.Items.Clear;
        for i := 6 downto 2 do
          PowerList.Items.Append(IntToStr(i));
        PowerList.Items.Append(' ');
        PowerList.Items.Append('/#');
        for i := -2 downto -6 do
          PowerList.Items.Append('/#' + IntToStr(-i));
        PowerList.ItemIndex := ndx;
        PowerList.Items.EndUpdate;
      end;
    scientific, dots:
      begin
        PowerList.Items.BeginUpdate;
        ndx := PowerList.ItemIndex;
        PowerList.Items.Clear;
        for i := 6 downto 2 do
          PowerList.Items.Append(IntToStr(i));
        PowerList.Items.Append(' ');
        for i := -1 downto -6 do
          PowerList.Items.Append(IntToStr(i));
        PowerList.ItemIndex := ndx;
        PowerList.Items.EndUpdate;
      end;
  end;
end;

procedure TUnitDialog.findsubunit(s: string; sel: integer; var i1, i2: integer);
begin
  if sel < 0 then
    sel := length(s) - 1;
  if sel = 0 then
    sel := 1;
  if sel > length(s) then
    sel := length(s);
  if charInSet(s[sel], [' ', '.']) then
  begin
    if sel >= length(s) - 1 then
    begin
      i1 := length(s);
      i2 := i1;
      exit;
    end
    else
      sel := sel + 1;
  end;
  i1 := sel;
  i2 := sel;
  if s[i2] = '/' then
    i2 := i2 + 1;
  while (i1 > 1) and not charInSet(s[i1], [' ', '.', '/', ']', '[', '(']) do
    i1 := i1 - 1;
  if charInSet(s[i1], [' ', '.', '(', '[', ']']) then
    i1 := i1 + 1;
  while (i2 < length(s)) and not charInSet(s[i2],
    [' ', '.', '/', '[', ']', ')']) do
    i2 := i2 + 1;
  if charInSet(s[i2], [' ', '.', '/', ')', '[', ']']) then
    i2 := i2 - 1;
end;

procedure TUnitDialog.splitunit(var prefix, theunit, power: string;
  const subunit: string);
var
  i, j, lenprefix: integer;
  found: boolean;
  // prefix, theunit, power: string;
begin
  theunit := subunit;
  prefix := ' ';
  power := ' ';
  if length(theunit) > 1 then
  begin
    i := 1;
    while (i < length(theunit)) and not charInSet(theunit[i],
      ['0' .. '9', '-']) do
      i := i + 1;
    if charInSet(theunit[i], ['0' .. '9', '-']) then
    begin
      power := copy(theunit, i, length(theunit));
      theunit := copy(theunit, 1, i - 1);
    end;
    if pos('/', theunit) > 0 then
    begin
      theunit := copy(theunit, pos('/', theunit) + 1, length(theunit));
      if power = '' then
        power := '/#'
      else
        power := '/#' + power;
    end;
    if (length(theunit) > 1) and charInSet(theunit[1],
      ['Y', 'Z', 'E', 'P', 'T', 'G', 'M', 'k', 'h', 'd', 'c', 'm', '�', 'n',
      'p', 'f', 'a', 'z', 'y']) then
    begin
      lenprefix := 1;
      if (length(theunit) > 2) and charInSet(theunit[1], ['d', 'm']) then
      begin
        if (copy(theunit, 1, 2) = 'da') or (copy(theunit, 1, 2) = 'mu') then
          lenprefix := 2;
      end;
      found := false;
      for j := 0 to length(units_no_prefix) - 1 do
        if units_no_prefix[j] = theunit then
        begin
          found := true;
          break;
        end;
      if not found then
      begin
        prefix := copy(theunit, 1, lenprefix);
        theunit := copy(theunit, lenprefix + 1, length(theunit));
        with BasicList do
          i := Items.IndexOf(theunit);
        if i < 0 then
          with UnitCombo do
            i := CaseIndexOf(Items, theunit);
        if i < 0 then
        begin
          theunit := prefix + theunit;
          prefix := '';
        end;
      end;
    end;
  end;
  if AnsiCompareStr(theunit, '-') = 0 then
    theunit := ' ';
end;

procedure TUnitDialog.StyleComboChange(Sender: TObject);
var
  i, i1, i2, posbr1, posbr2: integer;
  s, snew, sbrack, subunit, prefix, theunit, power: string;
begin
  s := UnitEdit.Text;
  i := 1;
  snew := '';
  sbrack := '';
  posbr1 := pos('/(', s);
  posbr2 := pos(')', s);
  while (i < length(s)) do
  begin
    findsubunit(s, i, i1, i2);
    subunit := copy(s, i1, i2 - i1 + 1);
    splitunit(prefix, theunit, power, subunit);
    if (i1 > posbr1) and (i1 < posbr2) then
      power := '/#' + power;
    if chartrim(power, [' ', '.']) = '/#' then
      power := '-1'
    else if power[1] = '/' then
      power := '-' + copy(power, 3, length(power));
    case TUnitStyle(StyleCombo.ItemIndex) of
      scientific:
        snew := snew + chartrim(prefix, [' ', '.']) +
          chartrim(theunit, [' ', '.']) + chartrim(power, [' ', '.']) + ' ';
      dots:
        snew := snew + chartrim(prefix, [' ', '.']) +
          chartrim(theunit, [' ', '.']) + chartrim(power, [' ', '.']) + '.';
      brackets:
        begin
          if power = '-1' then
            sbrack := chartrim(sbrack, [' ', '.']) + ' ' +
              chartrim(prefix, [' ', '.']) + chartrim(theunit, [' ', '.']) + ' '
          else if power[1] = '-' then
            sbrack := trim(sbrack) + ' ' + chartrim(prefix, [' ', '.']) +
              chartrim(theunit, [' ', '.']) +
              copy(power, 2, length(power)) + ' '
          else if theunit <> '' then
            snew := snew + chartrim(prefix, [' ', '.']) +
              chartrim(theunit, [' ', '.']) + power + ' ';
        end;
      division:
        begin
          if power = '-1' then
            snew := chartrim(snew, [' ', '.']) + '/' +
              chartrim(prefix, [' ', '.']) + chartrim(theunit, [' ', '.']) + ' '
          else if power[1] = '-' then
            snew := trim(snew) + '/' + chartrim(prefix, [' ', '.']) +
              chartrim(theunit, [' ', '.']) +
              copy(power, 2, length(power)) + ' '
          else if theunit <> '' then
            snew := snew + chartrim(prefix, [' ', '.']) +
              chartrim(theunit, [' ', '.']) + power + ' ';
        end;
    end;
    if i2 > i then
      i := i2 + 1
    else
      i := i + 1;
  end;
  snew := ReplaceStr(snew, '(', '');
  snew := ReplaceStr(snew, ')', '');
  sbrack:=trim(sbrack);
  if sbrack <> '' then
  begin
    if pos(' ', sbrack) > 0 then
      snew := trim(snew) + '/(' + sbrack + ')'
    else
      snew := trim(snew) + '/' + sbrack;
  end;
  UnitEdit.Text := cleanupunit(snew);
  UpdatePowerList;
end;

procedure TUnitDialog.BasicListClick(Sender: TObject);
begin
  WarningLabel.Caption := '';
  UnitCombo.Text := '';
  if (BasicList.ItemIndex >= 0) and (BasicList.ItemIndex < 8) then
    BasicList.Hint := basic_hints[BasicList.ItemIndex];
  PrefixListClick(Sender);
end;

procedure TUnitDialog.Button7Click(Sender: TObject);
begin
  MainForm.HelpTopic('edit_units');
end;

function TUnitDialog.CaseIndexOf(list: TStrings; s: string): integer;
// it seems impossible to search with case sensitivity in Listbox
var
  i: integer;
begin
  Result := -1;
  for i := 0 to list.Count - 1 do
  begin
    // %   j:= AnsiCompareStr(list[i], s);
    if AnsiCompareStr(trim(list[i]), trim(s)) = 0 then
    begin
      Result := i;
      exit;
    end;
  end;
end;
procedure TUnitDialog.ReadStyle;
begin
  UnitEdit.Text := cleanupunit(UnitEdit.Text);
  if pos('/', UnitEdit.Text) <= 0 then
  begin
    if pos('.', UnitEdit.Text) > 0 then
      StyleCombo.ItemIndex := integer(dots)
    else
      StyleCombo.ItemIndex := integer(scientific)
  end
  else if pos('(', UnitEdit.Text) > 0 then
    StyleCombo.ItemIndex := integer(brackets)
  else
    StyleCombo.ItemIndex := integer(division);
  UpdatePowerList;

end;

procedure TUnitDialog.FormShow(Sender: TObject);
var
  i: integer;
begin
  PrevLabel.Caption := '';
  NextLabel.Caption := '';
  readStyle;
  if UnitEdit.Text = '' then
    UpdateLists(' ')
  else
    UnitEditClick(Sender);
  UsedList.Items.BeginUpdate;
  UsedList.Items.Clear;
  for i := 0 to MainForm.Grindmodel.Components.Count - 1 do
    if TGrindComponent(MainForm.Grindmodel.Components.Items[i]).theunit <> ''
    then
    begin
      if CaseIndexOf(UsedList.Items,
        TGrindComponent(MainForm.Grindmodel.Components.Items[i]).theunit) = -1
      then
        UsedList.Items.Add(TGrindComponent(MainForm.Grindmodel.Components.Items
          [i]).theunit)
    end;
  UsedList.Visible := UsedList.Items.Count > 0;
  UsedLabel.Visible := UsedList.Items.Count > 0;
  UsedList.Items.EndUpdate;
end;

procedure TUnitDialog.OKButtonClick(Sender: TObject);
begin
   UnitEdit.Text:=cleanupunit(UnitEdit.Text);
end;

procedure TUnitDialog.PrefixListClick(Sender: TObject);
var
  prefix, theunit, power: string;
  sel: integer;
begin
  if PrefixList.ItemIndex < 0 then
    prefix := ''
  else
  begin
    PrefixList.Hint := prefix_hints[PrefixList.ItemIndex];
    prefix := chartrim(PrefixList.Items[PrefixList.ItemIndex], [' ', '.']);
  end;
  if BasicList.ItemIndex >= 0 then
    theunit := chartrim(BasicList.Items[BasicList.ItemIndex], [' ', '.'])
  else if WarningLabel.Caption = '' then
    theunit := chartrim(UnitCombo.Text, [' ', '.'])
  else
  begin
    theunit := copy(WarningLabel.Caption, pos('"', WarningLabel.Caption) + 1,
      length(WarningLabel.Caption));
    if pos('"', theunit) > 0 then
      theunit := copy(theunit, 1, pos('"', theunit) - 1);
  end;
  if PowerList.ItemIndex < 0 then
    power := ''
  else
    power := chartrim(PowerList.Items[PowerList.ItemIndex], [' ', '.']);
  sel := UnitEdit.SelStart;
  if (StyleCombo.ItemIndex = integer(scientific)) or (pos('/', power) = 0) then
  begin
    if (PrevLabel.Caption <> '') and
      (PrevLabel.Caption[length(PrevLabel.Caption)] <> ' ') then
      PrevLabel.Caption := PrevLabel.Caption + ' ';
    if (NextLabel.Caption <> '') and
      (not charInSet(NextLabel.Caption[1], ['.', ' ', '/'])) then
      NextLabel.Caption := ' ' + NextLabel.Caption;
    UnitEdit.Text := PrevLabel.Caption + prefix + theunit + power +
      NextLabel.Caption;
  end
  else
    UnitEdit.Text := PrevLabel.Caption + '/' + prefix + theunit +
      copy(power, 3, length(power)) + NextLabel.Caption;
  UnitEdit.SelStart := sel;
  UnitEdit.SelLength := 0;
end;

procedure TUnitDialog.UpdateLists(subunit: string);
var
  prefix, theunit, power: string;
begin
  splitunit(prefix, theunit, power, subunit);
  WarningLabel.Caption := '';
  with PrefixList do
    ItemIndex := CaseIndexOf(Items, prefix);
  with BasicList do
    ItemIndex := Items.IndexOf(theunit); // all units are unique in case
  if BasicList.ItemIndex < 0 then
  begin
    if CaseIndexOf(UnitCombo.Items, theunit) = -1 then
    begin
      WarningLabel.Caption := format('Unknown unit "%s"', [theunit]);
      UnitCombo.Text := '';
    end
    else
      UnitCombo.Text := theunit;
  end
  else
    UnitCombo.Text := '';
  with PowerList do
    ItemIndex := CaseIndexOf(Items, power);
end;

procedure TUnitDialog.UnitButtonClick(Sender: TObject);
var
  s: string;
begin
  if StyleCombo.ItemIndex = integer(dots) then
  begin
    s := chartrim(UnitEdit.Text, [' ', '.']);
    while (length(s) > 0) and (s[length(s)] = '.') do
      s := copy(s, 1, length(s) - 1);
    UnitEdit.Text := s + '.';
  end
  else
    UnitEdit.Text := chartrim(UnitEdit.Text, [' ', '.']) + ' ';
  UnitEdit.SelStart := length(UnitEdit.Text);
  UnitEditClick(Sender);
end;

procedure TUnitDialog.UnitComboChange(Sender: TObject);
begin
  BasicList.ItemIndex := -1;
  WarningLabel.Caption := '';
  PrefixListClick(Sender);
end;

procedure TUnitDialog.UnitEditClick(Sender: TObject);
var
  i1, i2, origsel: integer;
  subunit: string;
begin
  with UnitEdit do
  begin
    if trim(Text) = '' then
    begin
      Text := ' ';
      PrevLabel.Caption := '';
      NextLabel.Caption := '';
      UpdateLists(' ');
    end
    else
    begin
      origsel := SelStart;
      findsubunit(Text, SelStart, i1, i2);
      if (i1 > length(Text)) and (i2 = i1) then
      begin
        PrevLabel.Caption := Text;
        NextLabel.Caption := '';
        UpdateLists(' ');
        SelStart := origsel;
        exit;
      end
      else
        PrevLabel.Caption := copy(Text, 1, i1 - 1);
      NextLabel.Caption := copy(Text, i2 + 1, length(Text));
      subunit := copy(Text, i1, i2 - i1 + 1);
      if subunit=')' then
      begin
         NextLabel.Caption:=copy(Text, i2, length(Text));
         subunit:='';
      end;
      UpdateLists(subunit);
      SelStart := origsel;
      SetFocus;
    end;
  end;
end;

procedure TUnitDialog.UsedListClick(Sender: TObject);
begin
  UnitEdit.Text := UsedList.Items[UsedList.ItemIndex];
  readstyle;
end;

end.
