object StylesDialog: TStylesDialog
  Left = 738
  Top = 106
  Caption = 'Customize styles'
  ClientHeight = 510
  ClientWidth = 621
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label8: TLabel
    Left = 16
    Top = 16
    Width = 99
    Height = 13
    Caption = 'Select style to adapt:'
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 64
    Width = 617
    Height = 393
    ActivePage = TabSheet1
    TabOrder = 0
    OnChange = PageControl1Change
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = 'State variable'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Pen1Group: TGroupBox
        Left = 280
        Top = 8
        Width = 257
        Height = 169
        Caption = 'Line style'
        TabOrder = 0
        object PenLabel: TLabel
          Left = 8
          Top = 24
          Width = 46
          Height = 13
          Caption = 'Pen Color'
        end
        object Label4: TLabel
          Left = 8
          Top = 80
          Width = 45
          Height = 13
          Caption = 'Pen Style'
        end
        object Label7: TLabel
          Left = 8
          Top = 120
          Width = 47
          Height = 13
          Caption = 'Pen width'
        end
        object PenWidthEdit: TEdit
          Left = 96
          Top = 120
          Width = 113
          Height = 21
          TabOrder = 2
          Text = '1'
        end
        object PenWidthUpDown: TUpDown
          Left = 209
          Top = 120
          Width = 15
          Height = 21
          Associate = PenWidthEdit
          Max = 10
          Position = 1
          TabOrder = 3
        end
        object PenStyleCombo: TComboBox
          Left = 96
          Top = 80
          Width = 129
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          Items.Strings = (
            'Solid'
            'Dash'
            'Dot'
            'DashDot'
            'DashDotDot'
            'None')
        end
        object PenColorGrid: TColorGrid
          Left = 96
          Top = 24
          Width = 128
          Height = 32
          ClickEnablesColor = True
          GridOrdering = go8x2
          BackgroundEnabled = False
          TabOrder = 0
        end
      end
      object BrushGroup: TGroupBox
        Left = 280
        Top = 184
        Width = 257
        Height = 169
        Caption = 'Fill style'
        TabOrder = 1
        object Label3: TLabel
          Left = 8
          Top = 24
          Width = 41
          Height = 13
          Caption = 'Fill color:'
        end
        object Label5: TLabel
          Left = 8
          Top = 88
          Width = 39
          Height = 13
          Caption = 'Fill style '
        end
        object FillStyleCombo: TComboBox
          Left = 96
          Top = 80
          Width = 129
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          Items.Strings = (
            'Solid'
            'None'
            'Horizontal'
            'Vertical'
            'Forward diagonal'
            'Backward diagonal'
            'Cross'
            'Diagonal cross')
        end
        object FillColorGrid: TColorGrid
          Left = 96
          Top = 24
          Width = 128
          Height = 32
          GridOrdering = go8x2
          BackgroundIndex = 1
          BackgroundEnabled = False
          TabOrder = 0
        end
      end
      object ShapeGroup: TGroupBox
        Left = 16
        Top = 8
        Width = 257
        Height = 249
        Caption = 'Shape'
        TabOrder = 2
        object Width: TLabel
          Left = 8
          Top = 88
          Width = 28
          Height = 13
          Caption = 'Width'
        end
        object Height: TLabel
          Left = 8
          Top = 120
          Width = 31
          Height = 13
          Caption = 'Height'
        end
        object Label6: TLabel
          Left = 8
          Top = 40
          Width = 31
          Height = 13
          Caption = 'Shape'
        end
        object ShapeCombo: TComboBox
          Left = 104
          Top = 32
          Width = 89
          Height = 21
          Style = csDropDownList
          TabOrder = 6
          OnChange = ShapeComboChange
        end
        object WidthEdit: TEdit
          Left = 104
          Top = 80
          Width = 73
          Height = 21
          TabOrder = 0
          Text = '20'
        end
        object WidthUpDown: TUpDown
          Left = 177
          Top = 80
          Width = 15
          Height = 21
          Associate = WidthEdit
          Min = 5
          Position = 20
          TabOrder = 1
        end
        object HeightUpDown: TUpDown
          Left = 177
          Top = 120
          Width = 15
          Height = 21
          Associate = HeightEdit
          Min = 5
          Position = 20
          TabOrder = 2
        end
        object HeightEdit: TEdit
          Left = 104
          Top = 120
          Width = 73
          Height = 21
          TabOrder = 3
          Text = '20'
        end
        object TextCheck: TCheckBox
          Left = 40
          Top = 200
          Width = 161
          Height = 17
          Caption = 'Show additional text'
          TabOrder = 4
        end
        object SymbolCheck: TCheckBox
          Left = 40
          Top = 168
          Width = 161
          Height = 17
          Caption = 'Show symbol'
          TabOrder = 5
        end
        object FullList: TComboBox
          Left = 160
          Top = 56
          Width = 89
          Height = 21
          Style = csDropDownList
          TabOrder = 7
          Visible = False
          OnChange = ShapeComboChange
          Items.Strings = (
            'None'
            'Rectangle'
            'RoundRect'
            'Ellipse'
            'Vertical Bar'
            'Horizontal Bar'
            'Cloud'
            'Valve'
            'Train'
            'Connector')
        end
        object ShortList: TComboBox
          Left = 168
          Top = 204
          Width = 89
          Height = 21
          Style = csDropDownList
          TabOrder = 8
          Visible = False
          OnChange = ShapeComboChange
          Items.Strings = (
            'None'
            'Rectangle'
            'RoundRect'
            'Ellipse'
            'Vertical Bar'
            'Horizontal Bar'
            'Cloud')
        end
      end
      object Pen2Group: TGroupBox
        Left = 16
        Top = 264
        Width = 257
        Height = 89
        Caption = 'Line style valve/train'
        TabOrder = 3
        object Label2: TLabel
          Left = 12
          Top = 26
          Width = 55
          Height = 13
          Caption = 'Pen Color 2'
        end
        object Pen2ColorGrid: TColorGrid
          Left = 98
          Top = 20
          Width = 128
          Height = 32
          GridOrdering = go8x2
          BackgroundEnabled = False
          TabOrder = 0
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Auxiliary variable'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet3: TTabSheet
      Caption = 'Parameter'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet8: TTabSheet
      Caption = 'Externvar'
      ImageIndex = 7
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet4: TTabSheet
      Caption = 'Connector'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet5: TTabSheet
      Caption = 'Continuous flow'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet6: TTabSheet
      Caption = 'Discrete flow'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet7: TTabSheet
      Caption = 'Cloud'
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object UserFunctionTab: TTabSheet
      Caption = 'Function'
      ImageIndex = 8
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
  object StyleCombo: TComboBox
    Left = 128
    Top = 16
    Width = 137
    Height = 21
    Style = csDropDownList
    TabOrder = 1
    OnChange = StyleComboChange
    Items.Strings = (
      'Default'
      'Forrester'
      'Style 1'
      'Style 2')
  end
  object OKBtn: TBitBtn
    Left = 136
    Top = 472
    Width = 105
    Height = 33
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 2
    OnClick = OKBtnClick
  end
  object CancelBtn: TBitBtn
    Left = 264
    Top = 472
    Width = 105
    Height = 33
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 3
    OnClick = CancelBtnClick
  end
  object Button1: TButton
    Left = 280
    Top = 16
    Width = 97
    Height = 25
    Caption = 'Reset to default'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 496
    Top = 16
    Width = 99
    Height = 25
    Caption = 'Change font'
    TabOrder = 5
    OnClick = Button2Click
  end
  object BitBtn1: TBitBtn
    Left = 392
    Top = 472
    Width = 105
    Height = 33
    Kind = bkHelp
    NumGlyphs = 2
    TabOrder = 6
    OnClick = BitBtn1Click
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 464
    Top = 16
  end
end
