object NoInifileDialog: TNoInifileDialog
  Left = 357
  Top = 236
  Caption = 'No file name entered'
  ClientHeight = 193
  ClientWidth = 641
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 64
    Width = 89
    Height = 13
    Caption = 'Name of the model'
  end
  object InifileEdit: TEdit
    Left = 192
    Top = 64
    Width = 361
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 568
    Top = 64
    Width = 41
    Height = 25
    Caption = '...'
    TabOrder = 1
    OnClick = Button1Click
  end
  object BitBtn1: TBitBtn
    Left = 408
    Top = 136
    Width = 97
    Height = 33
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object BitBtn3: TBitBtn
    Left = 280
    Top = 136
    Width = 105
    Height = 33
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 3
  end
  object SaveDialog1: TSaveDialog
    Left = 472
    Top = 16
  end
end
