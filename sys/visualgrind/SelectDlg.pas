unit SelectDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TSelectDialog = class(TForm)
    Label1: TLabel;
    ListBox1: TListBox;
    BitBtn1: TBitBtn;
    EditBtn: TBitBtn;
    DeleteBtn: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EditBtnClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ListBox1MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure CleanUp;
    { Public declarations }
  end;

var
  SelectDialog: TSelectDialog;

implementation

uses GrindComps, visgrind, CompDialog,System.Types;
{$R *.DFM}

procedure TSelectDialog.FormShow(Sender: TObject);
var
  i: Integer;
  comp: TGRindComponent;
  s: string;
begin
  Application.HintColor := clInfoBk;
  with ListBox1 do
  begin
    Items.Clear;
    for i := 0 to MainForm.GrindModel.Components.count - 1 do
    begin
      comp := TGRindComponent(MainForm.GrindModel.Components.Items[i]);
      s := comp.Typestr + ': ' + comp.symbol;
      if comp.Description <> '' then
        s := s + ' = ' + comp.Description
      else if (comp.From <> nil) and (comp.aTo <> nil) then
        s := s + ' connects ' + comp.From.symbol + ' with ' + comp.aTo.symbol
      else if comp.Expression <> '' then
        s := s + ' = ' + comp.Expression;
      Items.AddObject(s, comp);
    end;
  end;
end;

procedure TSelectDialog.FormHide(Sender: TObject);
var
  List: TList;
  i: Integer;
begin
  List := TList.create;
  try
    with ListBox1 do
    begin
      for i := 0 to Items.count - 1 do
        if Selected[i] then
          List.Add(Items.Objects[i]);
    end;
    MainForm.GrindModel.SelectList := List;
  finally
    List.free;
  end;
end;

procedure TSelectDialog.BitBtn1Click(Sender: TObject);
begin
  Hide;
end;

procedure TSelectDialog.EditBtnClick(Sender: TObject);
begin
  if ListBox1.SelCount = 1 then
  begin
    FormHide(Sender);
    ComponentDlg.SetComponent(MainForm.GrindModel.Selected);
    if ComponentDlg.ShowModal = mrOK then
      with MainForm do
        if GrindModel.Selected <> nil then
        begin
          ComponentDlg.UpdateComponent;
          ExpressLongEdit.Text := GrindModel.Selected.Expression;
          SymbolEdit.Text := GrindModel.Selected.symbol;
          TextEdit.Text := GrindModel.Selected.Text;
          FormShow(Sender);
        end;
  end;
end;

procedure TSelectDialog.ListBox1Click(Sender: TObject);
begin
  EditBtn.Enabled := ListBox1.SelCount = 1;
end;

procedure TSelectDialog.DeleteBtnClick(Sender: TObject);
var
  i: Integer;
begin
  FormHide(Sender);
  with ListBox1 do
  begin
    for i := Items.count - 1 downto 0 do
      if Selected[i] then
        MainForm.GrindModel.DeleteComp
          (TGRindComponent(ListBox1.Items.Objects[i]));
  end;
  FormShow(Sender);
end;

procedure TSelectDialog.FormActivate(Sender: TObject);
begin
  // Position:=poMainFormCenter;
end;

procedure TSelectDialog.ListBox1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  i: Integer;
begin
  i := ListBox1.ItemAtPos(point(X, Y), True);
  if i >= 0 then
    if ListBox1.Items.Objects[i] <> nil then
      ListBox1.hint := TGRindComponent(ListBox1.Items.Objects[i]).HintText
    else
      ListBox1.hint := '';
end;

procedure TSelectDialog.CleanUp;
var
  i: Integer;
begin
  for i := 0 to ListBox1.Items.count - 1 do
    if (MainForm.GrindModel.Components.IndexOf(ListBox1.Items.Objects[i]) < 0)
    then
      ListBox1.Items.Objects[i] := nil;
end;
//

procedure TSelectDialog.BitBtn2Click(Sender: TObject);
begin
  MainForm.HelpTopic('selectdlg');
end;

end.
