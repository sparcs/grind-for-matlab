object DataFileDialog: TDataFileDialog
  Left = 104
  Top = 85
  Caption = 'Import data'
  ClientHeight = 497
  ClientWidth = 746
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 51
    Height = 13
    Caption = 'File name: '
  end
  object Label2: TLabel
    Left = 16
    Top = 104
    Width = 113
    Height = 13
    Caption = 'Select column to import:'
    WordWrap = True
  end
  object Label3: TLabel
    Left = 16
    Top = 280
    Width = 49
    Height = 13
    Caption = 'Raw data:'
  end
  object Label4: TLabel
    Left = 336
    Top = 104
    Width = 129
    Height = 13
    Caption = 'Selected column --> symbol'
  end
  object FilenameEdit: TEdit
    Left = 104
    Top = 16
    Width = 249
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 0
  end
  object Button1: TButton
    Left = 360
    Top = 16
    Width = 33
    Height = 25
    Caption = '...'
    TabOrder = 1
    OnClick = Button1Click
  end
  object PreviewMemo: TMemo
    Left = 16
    Top = 295
    Width = 697
    Height = 137
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 2
    WantTabs = True
    WordWrap = False
    OnChange = PreviewMemoChange
  end
  object BitBtn1: TBitBtn
    Left = 416
    Top = 447
    Width = 97
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 3
  end
  object BitBtn2: TBitBtn
    Left = 304
    Top = 447
    Width = 97
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 4
  end
  object BitBtn3: TBitBtn
    Left = 192
    Top = 447
    Width = 97
    Height = 25
    Kind = bkHelp
    NumGlyphs = 2
    TabOrder = 5
  end
  object ColumnsList: TListBox
    Left = 16
    Top = 120
    Width = 201
    Height = 145
    ItemHeight = 13
    TabOrder = 6
    OnClick = ColumnsListClick
  end
  object SelectButton: TButton
    Left = 240
    Top = 144
    Width = 75
    Height = 25
    Caption = '------>'
    Enabled = False
    TabOrder = 7
    OnClick = SelectButtonClick
  end
  object SelectedList: TListBox
    Left = 336
    Top = 120
    Width = 377
    Height = 145
    ItemHeight = 13
    TabOrder = 8
    OnClick = SelectedListClick
  end
  object DeselectButton: TButton
    Left = 240
    Top = 184
    Width = 75
    Height = 25
    Caption = '<-----'
    Enabled = False
    TabOrder = 9
    OnClick = DeselectButtonClick
  end
  object HeaderCheck: TCheckBox
    Left = 432
    Top = 64
    Width = 145
    Height = 17
    Caption = 'Header included'
    TabOrder = 10
  end
  object DelimGroup: TRadioGroup
    Left = 408
    Top = 0
    Width = 313
    Height = 49
    Caption = 'Column delimiter'
    Columns = 4
    ItemIndex = 1
    Items.Strings = (
      'TAB'
      'Comma'
      'Semicolon'
      'Space')
    TabOrder = 11
    OnClick = DelimGroupClick
  end
  object OpenDialog: TOpenDialog
    Filter = 'Text files|*.txt;*.dat;*.csv|All files|*.*'
    Left = 608
    Top = 80
  end
end
