object DataFrame: TDataFrame
  Left = 0
  Top = 0
  Width = 754
  Height = 412
  TabOrder = 0
  object Label2: TLabel
    Left = 8
    Top = 16
    Width = 78
    Height = 13
    Caption = 'File with all data:'
  end
  object Label4: TLabel
    Left = 8
    Top = 48
    Width = 25
    Height = 13
    Caption = 'Filter:'
  end
  object Label3: TLabel
    Left = 8
    Top = 80
    Width = 169
    Height = 13
    Caption = 'Data (use only symbols as headers!)'
  end
  object DatafileEdit: TEdit
    Left = 96
    Top = 8
    Width = 329
    Height = 21
    TabOrder = 0
    OnChange = DatafileEditChange
  end
  object DataFilterCombo: TComboBox
    Left = 96
    Top = 40
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = DataFilterComboChange
  end
  object DataGrid: TStringGrid
    Left = 0
    Top = 104
    Width = 754
    Height = 308
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 2
    FixedCols = 0
    RowCount = 100
    FixedRows = 0
    Options = [goFixedVertLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 2
    OnDrawCell = DataGridDrawCell
    OnKeyDown = DataGridKeyDown
    OnMouseMove = DataGridMouseMove
    OnSelectCell = DataGridSelectCell
    object DataEdit: TEdit
      Left = 80
      Top = 16
      Width = 121
      Height = 21
      BorderStyle = bsNone
      TabOrder = 0
      Visible = False
      OnExit = DataEditExit
    end
  end
  object BitBtn5: TBitBtn
    Left = 256
    Top = 40
    Width = 97
    Height = 25
    Caption = 'Paste'
    TabOrder = 3
    OnClick = PasteClick
  end
  object LoadBtn: TBitBtn
    Left = 360
    Top = 40
    Width = 97
    Height = 25
    Caption = 'Load from file'
    TabOrder = 4
    OnClick = LoadBtnClick
  end
  object TotalDataGrid: TStringGrid
    Left = 380
    Top = 120
    Width = 313
    Height = 297
    ColCount = 2
    FixedCols = 0
    FixedRows = 0
    TabOrder = 5
    Visible = False
  end
  object DataFileButton: TButton
    Left = 432
    Top = 8
    Width = 33
    Height = 25
    Caption = '...'
    TabOrder = 6
    OnClick = DataFileButtonClick
  end
  object DataPopupMenu: TPopupMenu
    Left = 292
    Top = 112
    object Copy1: TMenuItem
      Caption = 'Copy'
      ShortCut = 16451
      OnClick = Copy1Click
    end
    object Paste: TMenuItem
      Caption = 'Paste'
      ShortCut = 16470
      OnClick = PasteClick
    end
    object Deleteselection1: TMenuItem
      Caption = 'Clear cells'
      ShortCut = 46
      OnClick = Deleteselection1Click
    end
    object SelectAll: TMenuItem
      Caption = 'Select All'
      ShortCut = 16449
      OnClick = SelectAllClick
    end
    object Insertrows1: TMenuItem
      Caption = 'Insert rows...'
      OnClick = Insertrows1Click
    end
  end
  object DataOpenDialog: TOpenDialog
    Filter = 'Tab delimited text file (*.txt)|*.dat;*.txt|Any file (*.*)|*.*'
    Title = 'Open tab delimited text file'
    Left = 356
    Top = 272
  end
end
