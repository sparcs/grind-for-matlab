object ErrorDlg: TErrorDlg
  Left = 314
  Top = 182
  Caption = 'Error in scheme'
  ClientHeight = 234
  ClientWidth = 641
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ErrorLabel: TLabel
    Left = 24
    Top = 48
    Width = 63
    Height = 16
    Caption = 'ErrorLabel'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object ExpressionLabel: TLabel
    Left = 24
    Top = 144
    Width = 51
    Height = 13
    Caption = 'Expression'
  end
  object BitBtn1: TBitBtn
    Left = 525
    Top = 112
    Width = 97
    Height = 25
    Hint = 'Ignore this error and continue'
    Caption = 'Ignore'
    Kind = bkRetry
    NumGlyphs = 2
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object ChangeBtn: TBitBtn
    Left = 525
    Top = 192
    Width = 97
    Height = 25
    Hint = 'Change and continue'
    Caption = 'Change'
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
    OnClick = ChangeBtnClick
  end
  object BitBtn4: TBitBtn
    Left = 525
    Top = 152
    Width = 97
    Height = 25
    Hint = 'Go to scheme'
    Caption = 'To Scheme'
    Kind = bkClose
    NumGlyphs = 2
    TabOrder = 2
    OnClick = BitBtn4Click
  end
  object DeleteBtn: TBitBtn
    Left = 525
    Top = 72
    Width = 97
    Height = 25
    Hint = 'Delete this component and continue'
    Caption = 'Delete'
    Kind = bkNo
    NumGlyphs = 2
    TabOrder = 3
    OnClick = DeleteBtnClick
  end
  object BitBtn2: TBitBtn
    Left = 525
    Top = 32
    Width = 97
    Height = 25
    Hint = 'Help'
    Kind = bkHelp
    NumGlyphs = 2
    TabOrder = 4
    OnClick = BitBtn2Click
  end
  object SymbolEdit: TEdit
    Left = 24
    Top = 163
    Width = 121
    Height = 21
    TabOrder = 5
    OnChange = ExpressionEditChange
  end
  object ExpressionMemo: TMemo
    Left = 24
    Top = 163
    Width = 457
    Height = 21
    TabOrder = 6
    OnChange = ExpressionEditChange
  end
  object MatrixBtn: TButton
    Left = 480
    Top = 161
    Width = 23
    Height = 25
    Caption = '..'
    TabOrder = 7
    OnClick = MatrixBtnClick
  end
end
