unit ErrorDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, GrindComps, MemoLongstrings;

type
  TOKaction = (Nothing, OpenDialog, RunDirect);

  TErrorDlg = class(TForm)
    ErrorLabel: TLabel;
    BitBtn1: TBitBtn;
    ChangeBtn: TBitBtn;
    ExpressionLabel: TLabel;
    BitBtn4: TBitBtn;
    DeleteBtn: TBitBtn;
    BitBtn2: TBitBtn;
    SymbolEdit: TEdit;
    ExpressionMemo: TMemo;
    MatrixBtn: TButton;
    procedure ChangeBtnClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DeleteBtnClick(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure ExpressionEditChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MatrixBtnClick(Sender: TObject);
  private
    { Private declarations }
    FOKAction: TOKaction;
    FCurrError: TGrindError;
    FExpressionLongMemo: TMemoLongstrings;
    procedure ShowError;
  public
    procedure Execute(OKAction: TOKaction);
    { Public declarations }
  end;

var
  ErrorDlg: TErrorDlg;

implementation

uses visgrind, ModelDialog, SymbolUsedDialog, MatrixDlg;
{$R *.DFM}

procedure TErrorDlg.ChangeBtnClick(Sender: TObject);
begin
  MainForm.GrindModel.Selected := FCurrError.errComp;
  MainForm.SymbolEdit.Text := SymbolEdit.Text;
  MainForm.ExpressLongEdit.Text := FExpressionLongMemo.Text;
  MainForm.GrindModel.Selected := nil;
  FCurrError := MainForm.GrindModel.NextError(-1);
  ShowError;
end;

procedure TErrorDlg.Execute(OKAction: TOKaction);
begin
  MainForm.GrindModel.Selected := nil;
  SymbolErrorDlg.IgnoreAll := False;
  FOKAction := OKAction;
  FCurrError := MainForm.GrindModel.FirstError;
  // show;
  ShowError;
end;

procedure TErrorDlg.ShowError;
{ TGRindError = record
  Founderr: boolean;
  errString: string;
  errKind: TErrKind;
  errComp: TGrindComponent;
  end;
}
begin
  if FCurrError.FoundErr then
  begin
    show;
    ExpressionLabel.Caption := format('%s of %s:',
      [FCurrError.errComp.ExpressionType, FCurrError.errComp.Symbol]);
    ErrorLabel.Caption := FCurrError.errString;
    ErrorLabel.Width := ExpressionMemo.Width;
    FExpressionLongMemo.Text := FCurrError.errComp.Expression;
    SymbolEdit.Text := FCurrError.errComp.Symbol;
    SymbolEdit.Hide;
    MatrixBtn.Hide;
    ChangeBtn.Enabled := False;
    if FCurrError.errKind = ekExpression then
    begin
      ExpressionMemo.show;
      ExpressionLabel.show;
      ExpressionLabel.Caption := 'Expression';
      MatrixBtn.Visible:= FCurrError.errComp.TheModel.IsMatrix;
      DeleteBtn.Hide;
    end
    else if FCurrError.errKind = ekSymbol then
    begin
      SymbolEdit.show;
      ExpressionMemo.Hide;
      ExpressionLabel.show;
      ExpressionLabel.Caption := 'Symbol';
      DeleteBtn.Hide;
    end
    else if FCurrError.errKind = ekDelete then
    begin
      DeleteBtn.show;
      ExpressionMemo.Hide;
      ExpressionLabel.Hide;
    end
    else
    begin
      DeleteBtn.Hide;
      ExpressionMemo.Hide;
      ExpressionLabel.Hide;
    end;
  end
  else
  begin
    if FOKAction <> Nothing then
    begin
      ModelDlg.TheModel := MainForm.GrindModel;
      ModelDlg.show;
      if FOKAction = RunDirect then
        ModelDlg.RunBtn.Click;
    end;
    Hide;
    if FOKAction = Nothing then
      ShowMessage('The diagram is consistent, no errors found');
  end;
end;

procedure TErrorDlg.BitBtn1Click(Sender: TObject);
begin
  MainForm.GrindModel.Selected := FCurrError.errComp;
  FCurrError := MainForm.GrindModel.NextError;
  ShowError;
end;

procedure TErrorDlg.DeleteBtnClick(Sender: TObject);
begin
  MainForm.GrindModel.DeleteSelection;
  FCurrError := MainForm.GrindModel.FirstError;
  ShowError;
end;

procedure TErrorDlg.BitBtn4Click(Sender: TObject);
begin
  Hide;
end;

procedure TErrorDlg.ExpressionEditChange(Sender: TObject);
begin
  ChangeBtn.Enabled := TRue;
end;

procedure TErrorDlg.FormCreate(Sender: TObject);
begin
  FExpressionLongMemo := TMemoLongstrings.Create(ExpressionMemo);
end;

procedure TErrorDlg.FormDestroy(Sender: TObject);
begin
  FExpressionLongMemo.Free;
end;

procedure TErrorDlg.MatrixBtnClick(Sender: TObject);
begin
  if FCurrError.errComp <> nil then
  begin
    MatrixDialog.ComponentLabel.Caption := FCurrError.errComp.TypeStr + ' name:  ' +
      FCurrError.errComp.Symbol;
    MatrixDialog.ExpressionLongMemo := nil;
    MatrixDialog.ColUpDown.position := FCurrError.errComp.NCols;
    MatrixDialog.RowUpDown.position := FCurrError.errComp.nRows;
    MatrixDialog.ExpressionLongMemo := FExpressionLongMemo;
    if MatrixDialog.ShowModal = mrOK then
    begin
      // MatrixDialog.UpdateComponent;
      FExpressionLongMemo.BeginUpdate;
      try
        FExpressionLongMemo.Memo := ExpressionMemo;
        // TheComp.Expression;
      finally
        FExpressionLongMemo.EndUpdate;
      end;
    end;
  end;
end;

procedure TErrorDlg.BitBtn2Click(Sender: TObject);
begin
  MainForm.HelpTopic('errordlg');
end;

end.
