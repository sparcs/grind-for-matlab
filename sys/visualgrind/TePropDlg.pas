unit TePropDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Chart, ExtCtrls, TeEngine, Buttons;

type
  TTePropDialog = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    SeriesCombo: TComboBox;
    SeriesColorButton: TButton;
    SeriesColorShape: TShape;
    AxisLogCheck: TCheckBox;
    AxesRadio: TRadioGroup;
    AxisMinEdit: TEdit;
    AxisMaxEdit: TEdit;
    Label3: TLabel;
    TitleFontButton: TButton;
    AxisTitleEdit: TEdit;
    TitlesMemo: TMemo;
    FontDialog: TFontDialog;
    AxisIncrEdit: TEdit;
    SeriesTitle: TEdit;
    Label5: TLabel;
    ColorDialog: TColorDialog;
    LegAlignmentRadio: TRadioGroup;
    LegVisibleCheck: TCheckBox;
    LegendFontButton: TButton;
    TitlesFontButton: TButton;
    Label6: TLabel;
    LabelsFontButton: TButton;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    AutomaticCheck: TCheckBox;
    PointStyleCombo: TComboBox;
    StyleLabel: TLabel;
    SizeEdit: TEdit;
    SizeLabel: TLabel;
    SizeUpDown: TUpDown;
    BorderColorBtn: TButton;
    BorderColorShape: TShape;
    procedure TitleFontButtonClick(Sender: TObject);
    procedure AxesRadioClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SeriesComboChange(Sender: TObject);
    procedure SeriesColorButtonClick(Sender: TObject);
    procedure SeriesTitleChange(Sender: TObject);
    procedure LegendFontButtonClick(Sender: TObject);
    procedure TitlesFontButtonClick(Sender: TObject);
    procedure LabelsFontButtonClick(Sender: TObject);
    procedure AxisMinEditChange(Sender: TObject);
    procedure BorderColorBtnClick(Sender: TObject);
  private
    FChart: TChart;
    FCurrentAxis: TChartAxis;
    FCurrentSeries: TChartSeries;
    procedure SetChart(AChart: TChart);
    procedure SetCurrentAxis(const Value: TChartAxis);
    procedure SetCurrentSeries(const Value: TChartSeries);
    procedure SaveCurrentAxis;
    procedure SaveCurrentSeries;
    { Private declarations }
  public
    { Public declarations }
    property Chart: TChart read FChart write SetChart;
    property CurrentAxis: TChartAxis read FCurrentAxis write SetCurrentAxis;
    property CurrentSeries: TChartSeries read FCurrentSeries
      write SetCurrentSeries;
  end;

var
  TePropDialog: TTePropDialog;

implementation

uses Series, VCLTee.TeCanvas, System.UITypes;
{$R *.DFM}

procedure TTePropDialog.SetChart(AChart: TChart);
var
  i: integer;
begin
  FChart := AChart;
  PageControl1.ActivePage := TabSheet1;
  if FChart <> nil then
    with FChart do
    begin
      CurrentAxis := LeftAxis;
      SeriesCombo.Clear;
      for i := 0 to SeriesCount - 1 do
        if Series[i].Active then
          SeriesCombo.Items.AddObject('Series ' + IntToStr(i) + ': ' +
            Series[i].Title, Series[i]);
      if SeriesCombo.Items.Count > 0 then
        CurrentSeries := TChartSeries(SeriesCombo.Items.Objects[0]);
      LegAlignmentRadio.ItemIndex := integer(Legend.Alignment);
      LegVisibleCheck.Checked := Legend.Visible;
      TitlesMemo.Text := Title.Text.Text;
    end;
end;

procedure TTePropDialog.SetCurrentAxis(const Value: TChartAxis);
begin
  FCurrentAxis := Value;
  if FCurrentAxis <> nil then
    with FCurrentAxis do
    begin
      AxisTitleEdit.Text := Title.Caption;
      AxesRadio.OnClick := nil;
      if CurrentAxis = Chart.LeftAxis then
        AxesRadio.ItemIndex := 0
      else if CurrentAxis = Chart.RightAxis then
        AxesRadio.ItemIndex := 1
      else if CurrentAxis = Chart.TopAxis then
        AxesRadio.ItemIndex := 2
      else if CurrentAxis = Chart.BottomAxis then
        AxesRadio.ItemIndex := 3;
      AxesRadio.OnClick := AxesRadioClick;
      AxisMinEdit.Text := FloatToStr(Minimum);
      AxisMaxEdit.Text := FloatToStr(Maximum);
      AxisIncrEdit.Text := FloatToStr(Increment);
      AxisLogCheck.Checked := Logarithmic;
      AutomaticCheck.Checked := Automatic;
    end;
end;

procedure TTePropDialog.SetCurrentSeries(const Value: TChartSeries);
var
  i: integer;
begin
  FCurrentSeries := Value;
  if FCurrentSeries <> nil then
  begin
    SeriesCombo.OnChange := nil;
    with Chart do
      for i := 0 to SeriesCount - 1 do
        if Series[i] = FCurrentSeries then
          SeriesCombo.ItemIndex := i;
    SeriesCombo.OnChange := SeriesComboChange;
    with FCurrentSeries do
    begin
      if FCurrentSeries is TLineSeries then
        with TCustomSeries(FCurrentSeries) do
        begin
          BorderColorShape.Brush.Color := TCustomSeries(CurrentSeries)
            .Pointer.Pen.Color;
          PointStyleCombo.Visible := True;
          PointStyleCombo.ItemIndex := integer(Pointer.Style);
          SizeUpDown.Position := Pointer.HorizSize;
        end
      else
        PointStyleCombo.Visible := False;
      BorderColorShape.Visible := PointStyleCombo.Visible;
      BorderColorBtn.Visible := PointStyleCombo.Visible;
      StyleLabel.Visible := PointStyleCombo.Visible;
      SizeUpDown.Visible := PointStyleCombo.Visible;
      SizeEdit.Visible := PointStyleCombo.Visible;
      SizeLabel.Visible := PointStyleCombo.Visible;
      SeriesColorShape.Brush.Color := SeriesColor;
      SeriesTitle.OnChange := nil;
      SeriesTitle.Text := Title;
      SeriesTitle.OnChange := SeriesTitleChange;
    end;
  end;
end;

procedure TTePropDialog.TitleFontButtonClick(Sender: TObject);
begin
  FontDialog.Font := CurrentAxis.Title.Font;
  if FontDialog.Execute then
    CurrentAxis.Title.Font.Assign(FontDialog.Font);
end;

procedure TTePropDialog.SaveCurrentAxis;
var
  min, max: double;
begin
  with CurrentAxis do
  begin
    Title.Caption := AxisTitleEdit.Text;
    min := StrToFloat(AxisMinEdit.Text);
    max := StrToFloat(AxisMaxEdit.Text);
    Logarithmic := AxisLogCheck.Checked;
    Increment := StrToFloat(AxisIncrEdit.Text);
    if abs(max - Maximum) + abs(min - Minimum) > 0.0001 then
    begin
      Automatic := False;
      Minimum := min;
      Maximum := max;
    end;
    Automatic := AutomaticCheck.Checked;
  end;
end;

procedure TTePropDialog.AxesRadioClick(Sender: TObject);
begin
  SaveCurrentAxis;
  with Chart do
    case AxesRadio.ItemIndex of
      0:
        CurrentAxis := LeftAxis;
      1:
        CurrentAxis := RightAxis;
      2:
        CurrentAxis := TopAxis;
      3:
        CurrentAxis := BottomAxis;
    end;
end;

procedure TTePropDialog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if ModalResult = mrOK then
  begin
    SaveCurrentAxis;
    SaveCurrentSeries;
    with Chart do
    begin
      Legend.Alignment := TLegendAlignment(LegAlignmentRadio.ItemIndex);
      Legend.Visible := LegVisibleCheck.Checked;
    end;
    Chart.Title.Text.Text := TitlesMemo.Text;
  end;
end;

procedure TTePropDialog.SaveCurrentSeries;
begin
  CurrentSeries.Title := SeriesTitle.Text;
  if PointStyleCombo.Visible then
    with TCustomSeries(CurrentSeries) do
    begin
      Pointer.Style := TSeriesPointerStyle(PointStyleCombo.ItemIndex);
      Pointer.HorizSize := SizeUpDown.Position;
      Pointer.VertSize := SizeUpDown.Position;
    end;
end;

procedure TTePropDialog.SeriesComboChange(Sender: TObject);
begin
  SaveCurrentSeries;
  with SeriesCombo do
    CurrentSeries := TChartSeries(SeriesCombo.Items.Objects[ItemIndex]);
end;

procedure TTePropDialog.SeriesColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color := CurrentSeries.SeriesColor;
  if ColorDialog.Execute then
  begin
    CurrentSeries.SeriesColor := ColorDialog.Color;
    SeriesColorShape.Brush.Color := ColorDialog.Color;
  end;
end;

procedure TTePropDialog.SeriesTitleChange(Sender: TObject);
var
  i: integer;
begin
  inherited;
  with SeriesCombo do
  begin
    i := ItemIndex;
    Items[i] := 'Series ' + IntToStr(i) + ': ' + SeriesTitle.Text;
    Text := Items[i];
    ItemIndex := i;
  end;
end;

procedure TTePropDialog.LegendFontButtonClick(Sender: TObject);
begin
  FontDialog.Font := Chart.Legend.Font;
  if FontDialog.Execute then
    Chart.Legend.Font.Assign(FontDialog.Font);
end;

procedure TTePropDialog.TitlesFontButtonClick(Sender: TObject);
begin
  FontDialog.Font := Chart.Title.Font;
  if FontDialog.Execute then
    Chart.Title.Font.Assign(FontDialog.Font);
end;

procedure TTePropDialog.LabelsFontButtonClick(Sender: TObject);
begin
  FontDialog.Font := CurrentAxis.LabelsFont;
  if FontDialog.Execute then
    CurrentAxis.LabelsFont.Assign(FontDialog.Font);
end;

procedure TTePropDialog.AxisMinEditChange(Sender: TObject);
begin
  AutomaticCheck.Checked := False;
end;

procedure TTePropDialog.BorderColorBtnClick(Sender: TObject);
begin
  ColorDialog.Color := TCustomSeries(CurrentSeries).Pointer.Pen.Color;
  if ColorDialog.Execute then
  begin
    TCustomSeries(CurrentSeries).Pointer.Pen.Color := ColorDialog.Color;
    BorderColorShape.Brush.Color := ColorDialog.Color;
  end;
end;

end.
