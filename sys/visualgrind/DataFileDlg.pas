unit DataFileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TDataFileDialog = class(TForm)
    Label1: TLabel;
    FilenameEdit: TEdit;
    Button1: TButton;
    PreviewMemo: TMemo;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    ColumnsList: TListBox;
    Label2: TLabel;
    Label3: TLabel;
    SelectButton: TButton;
    SelectedList: TListBox;
    DeselectButton: TButton;
    Label4: TLabel;
    HeaderCheck: TCheckBox;
    OpenDialog: TOpenDialog;
    DelimGroup: TRadioGroup;
    procedure Button1Click(Sender: TObject);
    procedure DelimGroupClick(Sender: TObject);
    procedure ColumnsListClick(Sender: TObject);
    procedure SelectButtonClick(Sender: TObject);
    procedure SelectedListClick(Sender: TObject);
    procedure DeselectButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure PreviewMemoChange(Sender: TObject);
  private
    { Private declarations }
    FirstLine: string;
    delimiter: char;
  public
    function Execute: integer;
    procedure Paste;
    { Public declarations }
  end;

var
  DataFileDialog: TDataFileDialog;

implementation

uses SymbolDlg, CompDialog, GrindComps, prgTools, clipbrd;

{$R *.DFM}

function readdelim(s: string; index: integer; delimiter: char): string;
var
  i, p: integer;
begin
  result := '';
  for i := 0 to index do
  begin
    p := pos(delimiter, s);
    if p > 0 then
    begin
      result := trim(copy(s, 1, p - 1));
      s := copy(s, p + 1, length(s));
    end
    else
    begin
      result := trim(s);
      s := '';
    end;
  end;
end;

procedure TDataFileDialog.Button1Click(Sender: TObject);
var
  s1, s: string;
  datfile: textfile;
begin
  if OpenDialog.Execute then
  begin
    FilenameEdit.Text := OpenDialog.Filename;
    if FileExists(FilenameEdit.Text) then
    begin
      screen.Cursor := crHourGlass;
      PreviewMemo.selstart := 0;
      PreviewMemo.sellength := 0;
      PreviewMemo.Lines.Clear;
      assignfile(datfile, FilenameEdit.Text);
      reset(datfile);
      s := '';
      try
        while not eof(datfile) do
        begin
          readln(datfile, s1);
          s := s + s1 + #10;
        end;
      finally
        closefile(datfile);
      end;
      PreviewMemo.Lines.Text := s;
      screen.Cursor := crDefault;
      PreviewMemo.selstart := 0;
      PreviewMemo.sellength := 0;
    end;
  end;
end;

procedure TDataFileDialog.DelimGroupClick(Sender: TObject);
begin
  case DelimGroup.ItemIndex of
    0:
      delimiter := #9;
    1:
      delimiter := ',';
    2:
      delimiter := ';';
    3:
      delimiter := ' ';
  end;
end;

procedure TDataFileDialog.ColumnsListClick(Sender: TObject);
begin
  SelectButton.Enabled := True;
  DeselectButton.Enabled := False;
end;

procedure TDataFileDialog.SelectButtonClick(Sender: TObject);
var
  col: string;
begin
  col := ColumnsList.Items[ColumnsList.ItemIndex];
  SymbolDialog.SetColumn(col, SelectedList);
  if SymbolDialog.ShowModal = idOK then
    with SymbolDialog.SymbolCombo do
      if Items[ItemIndex] = 't' then
        SelectedList.Items.InsertObject(0, col + ' --> ' + Items[ItemIndex],
          Items.Objects[ItemIndex])
      else
        SelectedList.Items.addobject(col + ' --> ' + Items[ItemIndex],
          Items.Objects[ItemIndex]);
end;

procedure TDataFileDialog.SelectedListClick(Sender: TObject);
begin
  SelectButton.Enabled := False;
  DeselectButton.Enabled := True;
end;

procedure TDataFileDialog.DeselectButtonClick(Sender: TObject);
begin
  SelectedList.Items.delete(SelectedList.ItemIndex);
end;

procedure TDataFileDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  i, oldIndex: integer;

  procedure ReadColumn(s: string; comp: TGrindComponent);
  var
    c, tOld, cold, tcol, err, r, r1: integer;
    symb, tstr, cstr: string;
  begin
    with ComponentDlg do
      if comp <> nil then
      begin
        symb := comp.Symbol;
        comp.HasData := True;
        val(copy(s, 1, pos(':', s) - 1), c, err);
        if err <> 0 then
          exit
        else
          c := c - 1;
        val(copy(SelectedList.Items[0], 1, pos(':', SelectedList.Items[0]) - 1),
          tcol, err);
        if err <> 0 then
          exit
        else
          tcol := tcol - 1;
        tOld := 0;
        while (tOld < DataGrid.ColCount) and (DataGrid.Cells[tOld, 0] <> 't') do
          inc(tOld);
        if tOld >= DataGrid.ColCount then
        begin
          if DataGrid.ColCount > 1 then
            DataGrid.ColCount := DataGrid.ColCount + 1;
          tOld := DataGrid.ColCount - 1;
          DataGrid.Cells[tOld, 0] := 't';
        end;
        cold := 0;
        while (cold < DataGrid.ColCount) and
          (DataGrid.Cells[cold, 0] <> symb) do
          inc(cold);
        if cold >= DataGrid.ColCount then
        begin
          DataGrid.ColCount := DataGrid.ColCount + 1;
          cold := DataGrid.ColCount - 1;
        end
        else
          DataGrid.Cols[cold].Clear;
        DataGrid.Cells[cold, 0] := symb;
        for r := 0 to PreviewMemo.Lines.Count - 1 do
        begin
          tstr := readdelim(PreviewMemo.Lines[r], tcol, delimiter);
          cstr := readdelim(PreviewMemo.Lines[r], c, delimiter);
          if isnumber(tstr) and isnumber(cstr) then
          begin
            r1 := 0;
            while (r1 < DataGrid.RowCount) and
              ((DataGrid.Cells[tOld, r1] <> tstr)) do
              inc(r1);
            if r1 >= DataGrid.RowCount then
            begin
              DataGrid.RowCount := DataGrid.RowCount + 1;
              r1 := DataGrid.RowCount - 1;
              DataGrid.Rows[r1].Clear;
              DataGrid.Cells[tOld, r1] := tstr;
            end;
            DataGrid.Cells[cold, r1] := cstr;
          end;
        end;
        NumSortGrid(DataGrid, tOld, 1);
      end;
  end;

begin
  if ModalResult = mrOK then
  begin
    if (SelectedList.Items.Count = 0) then
    begin
      ShowMessage('No columns selected, cannot read data');
      CanClose := False;
    end
    else if pos(' --> t', SelectedList.Items[0]) = 0 then
    begin
      ShowMessage('No column with time selected, cannot read data');
      CanClose := False;
    end
    else
    begin
      oldIndex := ComponentDlg.DataFilterCombo.ItemIndex;
      ComponentDlg.DataFilterCombo.ItemIndex := 0;
      ComponentDlg.DataFilterCombo.OnChange(nil);
      for i := 1 to SelectedList.Items.Count - 1 do
        ReadColumn(SelectedList.Items[i],
          TGrindComponent(SelectedList.Items.Objects[i]));
      ComponentDlg.DataTabShow(nil);
      ComponentDlg.DataFilterCombo.ItemIndex := oldIndex;
      ComponentDlg.DataFilterCombo.OnChange(nil);
    end;
  end;
end;

function TDataFileDialog.Execute: integer;
begin
  Button1.Click;
  result := ShowModal;
end;

procedure TDataFileDialog.PreviewMemoChange(Sender: TObject);
var
  s1, col: string;
  i, j: integer;

  function ndelims(s: string): integer;
  var
    i: integer;
  begin
    result := 0;
    for i := 1 to length(s) do
      if s[i] = delimiter then
        result := result + 1;
    if (length(s) > 0) then
      result := result + 1;
  end;

begin
  if PreviewMemo.Lines.Count > 0 then
    FirstLine := PreviewMemo.Lines[0]
  else
    FirstLine := '';
  if pos(#9, FirstLine) > 0 then
    DelimGroup.ItemIndex := 0
  else if pos(',', FirstLine) > 0 then
    DelimGroup.ItemIndex := 1
  else if pos(';', FirstLine) > 0 then
    DelimGroup.ItemIndex := 2
  else if pos(' ', FirstLine) > 0 then
    DelimGroup.ItemIndex := 3
  else
    DelimGroup.ItemIndex := 0;
  DelimGroupClick(nil);
  HeaderCheck.checked := not isnumber(s1);
  ColumnsList.Items.Clear;
  SelectedList.Items.Clear;
  SymbolDialog.SetColumn('t', SelectedList);
  for i := 0 to ndelims(FirstLine) - 1 do
  begin
    s1 := readdelim(FirstLine, i, delimiter);
    if HeaderCheck.checked then
    begin
      if s1 = '' then
        col := format('%d: Column %d', [i + 1, i + 1])
      else
        col := format('%d: %s', [i + 1, s1]);
      ColumnsList.Items.add(col);
      if s1 = 't' then
        SelectedList.Items.InsertObject(0, col + ' --> ' + s1, nil)
      else
      begin
        j := SymbolDialog.SymbolID(s1);
        if j >= 0 then
          with SymbolDialog.SymbolCombo do
            SelectedList.Items.addobject(col + ' --> ' + s1, Items.Objects[j]);
      end;
    end
    else
      ColumnsList.Items.add(format('%d: Column %d', [i + 1, i + 1]));
    // inc(i);
  end;
end;

procedure TDataFileDialog.Paste;
begin
  FilenameEdit.Text := '[Clipboard]';
  PreviewMemo.Lines.Text := Clipboard.AsText;
end;

end.
