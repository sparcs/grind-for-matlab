object MatrixDialog: TMatrixDialog
  Left = 0
  Top = 0
  Caption = 'Edit a vector or matrix'
  ClientHeight = 535
  ClientWidth = 680
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnHide = FormHide
  OnShow = FormShow
  DesignSize = (
    680
    535)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 21
    Top = 53
    Width = 76
    Height = 13
    Caption = 'Number of rows'
  end
  object Label2: TLabel
    Left = 21
    Top = 80
    Width = 71
    Height = 13
    Caption = 'Number of cols'
  end
  object ComponentLabel: TLabel
    Left = 21
    Top = 26
    Width = 88
    Height = 13
    Caption = 'Component name:'
  end
  object RowEdit: TEdit
    Left = 120
    Top = 50
    Width = 49
    Height = 21
    TabOrder = 0
    Text = '1'
    OnChange = RowEditChange
  end
  object ColEdit: TEdit
    Left = 120
    Top = 77
    Width = 49
    Height = 21
    TabOrder = 1
    Text = '1'
    OnChange = RowEditChange
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 120
    Width = 682
    Height = 337
    ActivePage = TabSheet2
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Values in Table'
      PopupMenu = PopupMenu1
      OnEnter = TabSheet1Enter
      object Error2Label: TLabel
        Left = 3
        Top = 13
        Width = 425
        Height = 13
        Caption = 
          'Cannot update table due to a syntax error or unsupported functio' +
          'n in MATLAB command'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object TableGrid: TStringGrid
        Left = 0
        Top = 32
        Width = 674
        Height = 277
        Align = alBottom
        Anchors = [akLeft, akTop, akRight, akBottom]
        DefaultColWidth = 80
        FixedCols = 0
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        PopupMenu = PopupMenu1
        TabOrder = 0
        OnClick = TableGridClick
        OnSetEditText = TableGridSetEditText
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'MATLAB command'
      ImageIndex = 1
      PopupMenu = PopupMenu1
      DesignSize = (
        674
        309)
      object ErrorLabel: TLabel
        Left = 3
        Top = 21
        Width = 20
        Height = 13
        Caption = 'tete'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 293
        Top = 45
        Width = 237
        Height = 13
        Caption = 'Supported functions for updating Values in Table:'
      end
      object ExpressionMemo1: TMemo
        Left = 3
        Top = 64
        Width = 668
        Height = 193
        Anchors = [akLeft, akTop, akRight, akBottom]
        PopupMenu = PopupMenu1
        ScrollBars = ssBoth
        TabOrder = 0
        OnChange = ExpressionMemo1Change
      end
      object FunctionsCombo: TComboBox
        Left = 536
        Top = 37
        Width = 135
        Height = 21
        Style = csDropDownList
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
        OnChange = FunctionsComboChange
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 216
    Top = 480
    Width = 105
    Height = 27
    Anchors = [akLeft, akBottom]
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 3
  end
  object BitBtn2: TBitBtn
    Left = 352
    Top = 480
    Width = 105
    Height = 27
    Anchors = [akLeft, akBottom]
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 4
  end
  object RowUpDown: TUpDown
    Left = 169
    Top = 50
    Width = 16
    Height = 21
    Associate = RowEdit
    Min = 1
    Max = 1000000
    Position = 1
    TabOrder = 5
  end
  object ColUpDown: TUpDown
    Left = 169
    Top = 77
    Width = 16
    Height = 21
    Associate = ColEdit
    Min = 1
    Max = 1000000
    Position = 1
    TabOrder = 6
  end
  object PopupMenu1: TPopupMenu
    Left = 352
    Top = 24
    object UndoItem: TMenuItem
      Caption = '&Undo'
      ShortCut = 16474
      OnClick = UndoBtnClick
    end
    object RedoItem: TMenuItem
      Caption = '&Redo'
      OnClick = RedoBtnClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CopyItem: TMenuItem
      Caption = '&Copy'
      ShortCut = 16451
      OnClick = CopyItemClick
    end
    object PasteItem: TMenuItem
      Caption = '&Paste'
      ShortCut = 16470
      OnClick = PasteItemClick
    end
    object SelectAllItem: TMenuItem
      Caption = 'Select &All'
      ShortCut = 16449
      OnClick = SelectAllItemClick
    end
  end
end
