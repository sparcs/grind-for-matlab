unit prgTools;

interface

uses forms,grids, windows, sysutils, graphics, dialogs;
procedure SaveBakFile(FileName: string);
procedure NumSortGrid(aGrid: TStringGrid; aCol: integer; startRow: integer = 0);
function GridToStr(aGrid: TStringGrid; ASelection: TRect;
  Delimiter: char): string;
procedure StrToGrid(s: string; aGrid: TStringGrid; Delimiter: char;
  SelCol: integer = 0; SelRow: integer = 0);
procedure CopyGridToClipboard(aGrid: TStringGrid; UseSelection: Boolean);
procedure PasteClipboardToGrid(aGrid: TStringGrid; UseSelection: Boolean);
function FileToGRid(FileName: string; var DataGrid: TStringGrid): Boolean;
procedure GridToFile(var DataGrid: TStringGrid; FileName: string);
function isNumber(s: string): Boolean;
function HSLToRGB(HSL: integer): TColor;
function RGBToHSL(RGB: TColor): integer;

type
  THSL = record
    H, s, L, Dummibyte: Byte;
  end;

implementation

uses  controls, clipbrd, classes;



procedure SaveBakFile(FileName: string);
var
  ext, bakext, bakfile: string;
begin
  if FileExists(FileName) then
  begin
    ext := ExtractFileExt(FileName);
    bakext := '.~';
    if length(ext) > 1 then
      bakext := bakext + copy(ext, 2, length(ext));
    bakfile := ChangeFileExt(FileName, bakext);
    if FileExists(bakfile) then
      DeleteFile(bakfile);
    renamefile(FileName, bakfile);
  end;
end;

procedure NumSortGrid(aGrid: TStringGrid; aCol: integer; startRow: integer = 0);
var
  doubleArray: array of double;
  index: array of integer;
  r, c, n, err: integer;
  d: double;
  HGrid: TStringGrid;
  { QUICKSORT sorts elements in the array A with indices between }
  { Lo and Hi (both inclusive). Note that the QUICKSORT proce- }
  { dure provides only an "interface" to the program. The actual }
  { processing takes place in the SORT procedure, which executes }
  { itself recursively. }

  procedure QSort(L, r: integer);
  var
    I, J: integer;
    X: double;
    H: integer;
  begin
    I := L;
    J := r;
    X := doubleArray[index[(L + r) div 2]];
    repeat
      while (I < length(index)) and (doubleArray[index[I]] < X) do
        Inc(I);
      while (J > 0) and (doubleArray[index[J]] > X) do
        Dec(J);
      if I <= J then
      begin
        H := index[I];
        index[I] := index[J];
        index[J] := H;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then
      QSort(L, J);
    if I < r then
      QSort(I, r);
  end;

begin
  with aGrid do
  begin
    setlength(doubleArray, RowCount - startRow);
    setlength(Index, RowCount - startRow);
    n := 0;
    for r := startRow to RowCount - 1 do
    begin
      val(Cells[aCol, r], d, err);
      if err = 0 then
      begin
        err := -99;
        c := 0;
        while (c < ColCount) and (err = -99) do
        begin
          if (c <> aCol) and isNumber(Cells[c, r]) then
            err := 0;
          c := c + 1;
        end;
      end;
      if err = 0 then
        doubleArray[r - startRow] := d
      else
      begin
        doubleArray[r - startRow] := 1E100;
        n := n + 1;
      end;
      Index[r - startRow] := r - startRow;
    end;
    if RowCount - startRow - 1 > 0 then
      QSort(0, RowCount - startRow - 1);
    HGrid := TStringGrid.Create(nil);
    try
      HGrid.RowCount := RowCount;
      HGrid.ColCount := ColCount;
      for r := 0 to RowCount - 1 do
        for c := 0 to ColCount - 1 do
          HGrid.Cells[c, r] := aGrid.Cells[c, r];
      for r := startRow to RowCount - 1 - n do
        aGrid.Rows[r] := HGrid.Rows[index[r - startRow] + startRow];
      for r := aGrid.RowCount - n to aGrid.RowCount - 1 do
        for c := 0 to aGrid.ColCount - 1 do
          aGrid.Cells[c, r] := ''; // Else these values may come back
      aGrid.RowCount := aGrid.RowCount - n;
    finally
      HGrid.Free;
    end;
  end;
end;
{$HINTS OFF}

function isNumber(s: string): Boolean;
var
  d: double;
  err: integer;
begin
  val(s, d, err);
  Result := (err = 0) and (length(s) > 0) and (upcase(s[1]) <> 'E');
end;
{$HINTS ON}

procedure GridToFile(var DataGrid: TStringGrid; FileName: string);
var
  s: string;
  datfile: textfile;
begin
  with DataGrid do
    s := GridToStr(DataGrid, Rect(0, 0, ColCount - 1, RowCount - 1), #9);
  assignFile(datfile, FileName);
  rewrite(datfile);
  try
    write(datfile, s);
  finally
    closefile(datfile);
  end;
end;

function FileToGRid(FileName: string; var DataGrid: TStringGrid): Boolean;
var
  s1, s: string;
  r: integer;
  datfile: textfile;
begin
  Result := False;
  if FileExists(FileName) then
  begin
    s := '';
    assignFile(datfile, FileName);
    reset(datfile);
    try
      r := 0;
      while not eof(datfile) do
      begin
        readln(datfile, s1);
        s := s + s1 + #10#13;
        r := r + 1;
      end;
    finally
      closefile(datfile);
    end;
    Result := length(s) > 0;
    DataGrid.RowCount := r;
    if Pos(#9, s) > 0 then
      StrToGrid(s, DataGrid, #9)
    else
      StrToGrid(s, DataGrid, ',');
  end;
end;

function GridToStr(aGrid: TStringGrid; ASelection: TRect;
  Delimiter: char): string;
var
  Line: string;
  aRow, aCol: integer;
  // i:integer;
begin
  setlength(Result, 10000);
  Result := '';
  with aGrid do
  begin
    for aRow := ASelection.Top to ASelection.Bottom do
    begin
      Line := '';
      for aCol := ASelection.Left to ASelection.Right do
      begin
        if aCol > ASelection.Left then
          Line := Line + Delimiter;
        Line := Line + Cells[aCol, aRow];
      end;
      Line := Line + #13#10;
      Result := Result + Line;
    end;
  end;
  { i:=length(result);
    while Result[i] in [#13,#10] do
    i:=i-1;
    Result := copy(Result, 1, i);
  }
end;

procedure StrToGrid(s: string; aGrid: TStringGrid; Delimiter: char;
  SelCol: integer = 0; SelRow: integer = 0);
var
  Line: string;
  len, i1, i2: integer;
  aRow, aCol, maxCol: integer;
  OldRowCount, OldColCount: integer;
begin
  len := length(s);
  OldRowCount := aGrid.RowCount;
  OldColCount := aGrid.ColCount;
  // aGrid.RowCount := 20;
  aRow := SelRow - 1;
  maxCol := 0;
  i1 := 1;
  with aGrid do
  begin
    while i1 < len do
    begin
      Line := '';
      aCol := SelCol;
      while (i1 < len) and CharInSet(s[i1], [#10, #13]) do
        Inc(i1);
      Inc(aRow);
      repeat
        i2 := i1;
        while (i2 < len) and (s[i2] <> Delimiter) and
          not CharInSet(s[i2], [#0, #13, #10]) do
          Inc(i2);
        if Delimiter = ' ' then
          while (i2 + 1 < len) and (s[i2 + 1] = ' ') do
            Inc(i2);
        if aCol >= ColCount then
          ColCount := ColCount + 5;
        if (i2 > i1) then
          Line := copy(s, i1, i2 - i1)
        else
          Line := '';
        Cells[aCol, aRow] := Line;
        i1 := i2 + 1;
        Inc(aCol);
      until (i2 = len) or CharInSet(s[i2], [#0, #10, #13]);
      if aCol > maxCol then
        maxCol := aCol;
      if aRow > RowCount then
        RowCount := RowCount * 2;
    end;
    if maxCol > OldColCount then
      ColCount := maxCol;
    if aRow > OldRowCount then
      RowCount := aRow + 1;
  end;
end;

procedure CopyGridToClipboard(aGrid: TStringGrid; UseSelection: Boolean);
var
  Sel: TRect;
begin
  Screen.Cursor := crHourGlass;
  with aGrid do
  begin
    if UseSelection then
      Sel := TRect(Selection)
    else
    begin
      Sel.Top := 0;
      Sel.Bottom := RowCount - 1;
      Sel.Left := 0;
      Sel.Right := ColCount - 1;
    end;
    Clipboard.AsText := GridToStr(aGrid, Sel, #9);
  end;
  Screen.Cursor := crDefault;
end;

procedure PasteClipboardToGrid(aGrid: TStringGrid; UseSelection: Boolean);
begin
  Screen.Cursor := crHourGlass;
  if UseSelection then
    StrToGrid(Clipboard.AsText, aGrid, #9, aGrid.Col, aGrid.Row)
  else
    StrToGrid(Clipboard.AsText, aGrid, #9);
  Screen.Cursor := crDefault;
end;

const
  HSLRange: integer = 255;
  {
    function HSl(h, s, l: integer): Integer;
    function HSl(h, s, l: integer): Integer;
    begin
    THSL(Result).h := h;
    THSL(Result).s := s;
    THSL(Result).l := l;
    end;
  }

function _HSLtoRGB(H, s, L: double): TColor;
var
  M1, M2: double;

  function HueToColourValue(Hue: double): Byte;
  var
    V: double;
  begin
    if Hue < 0 then
      Hue := Hue + 1
    else if Hue > 1 then
      Hue := Hue - 1;

    if 6 * Hue < 1 then
      V := M1 + (M2 - M1) * Hue * 6
    else if 2 * Hue < 1 then
      V := M2
    else if 3 * Hue < 2 then
      V := M1 + (M2 - M1) * (2 / 3 - Hue) * 6
    else
      V := M1;
    Result := round(255 * V)
  end;

var
  r, G, B: Byte;
begin
  if s = 0 then
  begin
    r := round(255 * L);
    G := r;
    B := r
  end
  else
  begin
    if L <= 0.5 then
      M2 := L * (1 + s)
    else
      M2 := L + s - L * s;
    M1 := 2 * L - M2;
    r := HueToColourValue(H + 1 / 3);
    G := HueToColourValue(H);
    B := HueToColourValue(H - 1 / 3)
  end;
  Result := RGB(r, G, B)
end;

function HSLToRGB(HSL: integer): TColor;
begin
  Result := _HSLtoRGB(THSL(HSL).H / (HSLRange - 1), THSL(HSL).s / HSLRange,
    THSL(HSL).L / HSLRange)
end;

// Convert RGB value (0-255 range) into HSL value (0-1 values)

procedure _RGBtoHSL(RGB: TColor; var H, s, L: double);

  function Max(a, B: double): double;
  begin
    if a > B then
      Result := a
    else
      Result := B
  end;

  function Min(a, B: double): double;
  begin
    if a < B then
      Result := a
    else
      Result := B
  end;

var
  r, G, B, d, Cmax, Cmin: double;
begin
  r := GetRValue(RGB) / 255;
  G := GetGValue(RGB) / 255;
  B := GetBValue(RGB) / 255;
  Cmax := Max(r, Max(G, B));
  Cmin := Min(r, Min(G, B));
  // calculate luminosity
  L := (Cmax + Cmin) / 2;
  if Cmax = Cmin then // it's grey
  begin
    H := 0; // it's actually undefined
    s := 0
  end
  else
  begin
    d := Cmax - Cmin;

    // calculate Saturation
    if L < 0.5 then
      s := d / (Cmax + Cmin)
    else
      s := d / (2 - Cmax - Cmin);

    // calculate Hue
    if r = Cmax then
      H := (G - B) / d
    else if G = Cmax then
      H := 2 + (B - r) / d
    else
      H := 4 + (r - G) / d;

    H := H / 6;
    if H < 0 then
      H := H + 1
  end
end;

function RGBToHSL(RGB: TColor): integer;
var
  Hd, Sd, Ld: double;
begin
  _RGBtoHSL(RGB, Hd, Sd, Ld);
  THSL(Result).H := round(Hd * (HSLRange - 1));
  THSL(Result).s := round(Sd * HSLRange);
  THSL(Result).L := round(Ld * HSLRange);
end;

end.
