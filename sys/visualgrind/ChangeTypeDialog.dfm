object ChangeTypeDlg: TChangeTypeDlg
  Left = 309
  Top = 274
  Caption = 'Change Type'
  ClientHeight = 213
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 146
    Height = 13
    Caption = 'Change type of component to: '
  end
  object TypesList: TListBox
    Left = 40
    Top = 40
    Width = 161
    Height = 121
    ItemHeight = 13
    Items.Strings = (
      'State variable'
      'Auxiliary variable'
      'Parameter'
      'External variable'
      'Connector'
      'Flow'
      'Train'
      'Cloud'
      'Permanent variable'
      'User-defined function'
      'Model equations')
    TabOrder = 0
  end
  object OKBtn: TBitBtn
    Left = 40
    Top = 184
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object CancelBtn: TBitBtn
    Left = 144
    Top = 184
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
end
