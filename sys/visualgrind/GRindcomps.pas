unit Grindcomps;
{$WEAKLINKRTTI ON}

interface

uses
  Windows, SysUtils, Classes, graphics, extctrls, stdctrls, ParseExpr;

const
  emptyRect: TRect = (left: - 1; top: - 1; right: - 1; bottom: - 1);
  nstyles = 4;
  missingInteger = -9999;

type
  TComptype = (ctStatevar, ctAuxilvar, ctParameter, ctExternvar, ctConnector,
    ctFlow, ctTrain, ctCloud, ctPermanentVar, ctUserFunction, ctModelEquations, ctTextOnly);

  TConnectKind = (cFrom, cTo, cAll);

  TModelKind = (mkFlow, mkTrain, mkUnknown);

  TPointArray3 = array [0 .. 3] of TPoint;

  TDragKind = (dUnknown, dFrom, dTo, dHelpFrom, dHelpTo, dValve);

  TShapeKind = (skNone, skRectangle, skRoundRect, skEllipse, skBar, skHbar,
    skCloud, skValve, skTrain, skConnector);
  TOutsideRange = (tsNoCycle, tsCycle, tsFloor, tsCycleFloor);

  TCompStyle = record
    width: integer;
    height: integer;
    pencolor: TColor;
    pencolor2: TColor;
    penwidth: integer;
    penstyle: TPenStyle;
    brushcolor: TColor;
    brushstyle: TBrushstyle;
    Shape: TShapeKind;
    ShowSymbol: Boolean;
    ShowText: Boolean;
    SwapTextSymbol: Boolean;
//    TypeStr: string;
//    DefaultText: string;
  end;

  TCompData = record
     TypeStr: string;
     DefaultText: string;
  end;

  TMyStringList = class(TStringList)
  private
    // case sensitive variant of TStringList
  public
    procedure Sort; override;
    function Find(const S: string; var Index: integer): Boolean; override;
  end;

  PCompStyle = ^TCompStyle;
  TComponentStyles = array [ctStatevar .. ctUserFunction] of TCompStyle;

  TComponentDataArray = array [ctStatevar .. ctTextOnly] of TCompData;

  TStyleSchemes = array [0 .. nstyles - 1] of TComponentStyles;

  TZoomCanvas = class
  private
    FCanvas: TCanvas;
    FZoomfactor: double;
    FOrigin: TPoint;
    procedure SetCanvas(const Value: TCanvas);
    procedure SetZoomfactor(const Value: double);
    procedure UpdateCanvas;
  public
    Pen: TPen;
    Brush: TBrush;
    Font: TFont;
    constructor Create;
    destructor Destroy; override;
    function Scrn2X(SX: integer): integer;
    function Scrn2Y(SY: integer): integer;
    function X2Scrn(X: integer): integer;
    function Y2Scrn(Y: integer): integer;
    procedure MoveTo(X, Y: integer);
    procedure LineTo(X, Y: integer);
    procedure TextOut(X, Y: integer; const Text: string);
    procedure Circle(X, Y, width: integer);
    procedure Ellipse(R: TRect);
    procedure Rectangle(R: TRect);
    procedure DrawFocusRect(R: TRect);
    function TextExtent(Text: string): TSize;
    procedure RoundRect(x1, y1, x2, y2, x3, y3: integer);
    procedure PolyBezier(Points: array of TPoint);
    procedure Polygon(Points: array of TPoint);
    property Origin: TPoint read FOrigin write FOrigin;
    property Canvas: TCanvas read FCanvas write SetCanvas;
    property Zoomfactor: double read FZoomfactor write SetZoomfactor;
  end;

  TUndoAction = (uaChanged, uaDeleted, uaCreated);

  TUndoRec = record
    Undonr: integer;
    Action: TUndoAction;
    ID: integer;
    X: integer;
    Y: integer;
    FromX: integer;
    FromY: integer;
    NCols: integer;
    NRows: integer;
    FromHelp: TPoint;
    ToHelp: TPoint;
    From: string;
    aTo: string;
    TheType: TComptype;
    Text: string;
    Expression: string;
    Description: string;
    Symbol: string;
    TheUnit: string;
    Visible: Boolean;
    HasData: Boolean;
    OutsideRange: TOutsideRange;
  end;

const
  maxundo = 1000;

type
  TUndoList = array [0 .. maxundo] of TUndoRec;

  TGrindComponent = class;
  TErrKind = (ekOther, ekSymbol, ekExpression, ekDelete, ekData);

  TGRindError = record
    Founderr: Boolean;
    errString: string;
    errKind: TErrKind;
    errComp: TGrindComponent;
  end;

  TGrindModel = class // container of all elements
  private
    FComponents: TList; // All components are owned by TGrindModel
    FTempList: TList; // A temporary list
    FSelectList: TList;
    FPaintBox: TPaintBox;
    FDragging: Boolean;
    FModelKind: TModelKind;
    FSelectBox: TRect;
    FActiveUndo: Boolean;
    FDraggingSelectbox: Boolean;
    FZoomCanvas: TZoomCanvas;
    FStyleSchemes: TStyleSchemes;
    FStyles: TComponentStyles;
    FUndoList: TUndoList;
    FCurrUndo: integer;
    FFirstUndo: integer;
    FLastUndo: integer;
    FUndoNr: integer;
    FParsVisible: Boolean;
    FDataFileChanged: Boolean;
    FCurrErr: integer;
    FIsMatrix: Boolean;
    FParser: TMATLABParser;
    FIsStochastic: Boolean;
    FCompData: TComponentDataArray;
    procedure SelectComp(AComp: TGrindComponent);
    procedure CheckModelKind;
    function GetSelected: TGrindComponent;
    procedure SetDraggingSelectBox(const Value: Boolean);
    procedure SetStyles(const Value: TComponentStyles);
    function GetStyleSchemes(Items: integer): TComponentStyles;
    procedure SetStyleSchemes(Items: integer; const Value: TComponentStyles);
    procedure SetActiveUndo(const Value: Boolean);
    procedure SetParsVisible(const Value: Boolean);
    procedure CreateCompdata;
    function GetCompdata(Item: TCompType): TCompData;
    procedure SetIsMatrix(const Value: Boolean);
  public
    procedure SaveUndo(Comp: TGrindComponent; Action: TUndoAction;
      IncUndo: integer);
    procedure Undo(Redo: Boolean);
    function NextID: integer;
    procedure ReorderComponents;
    function CanUndo: Boolean;
    function CanRedo: Boolean;
    function CanConnect(Connector, Comp: TGrindComponent;
      aFrom: Boolean): Boolean;
    procedure SetDefaultStyles(var aStyles: TComponentStyles; No: integer);
    function removet(eq: string; doadd: Boolean): string;
    procedure Draw(Canvas: TCanvas);
    procedure DragSelectBox(SX, SY: integer);
    procedure DeleteIsolatedComponents;
    procedure UpdateConnections;
    procedure MultiSelect(AList: TList);
    function FirstError: TGRindError;
    function NextError(shift: integer = 0): TGRindError;
    // (ShowError: boolean): TGrindComponent;
    procedure Connect(Connector, Comp: TGrindComponent; aFrom: Boolean);
    procedure ReadScheme(const List: TStrings);
    function IndexOf(ASymbol: string): integer;
    procedure ExchangeComps(Comp1, Comp2: TGrindComponent);
    function IsUnique(ASymbol: string; AComp: TGrindComponent = nil): Boolean;
    function IsSelected(AComp: TGrindComponent): Boolean;
    function CompsOfType(aType: TComptype): TList;
    function Connectors(AComp: TGrindComponent; kind: TConnectKind): TList;
    function ChangeType(Comp: TGrindComponent; newtype: TComptype):TGrindComponent;
    function ReplaceComp(AComp: TGrindComponent; aType: TComptype)
      : TGrindComponent;
    procedure DeleteComp(AComp: TGrindComponent; MultiSelect: Boolean = False);
    procedure DeleteSelection;
    procedure Clear;
    function GetXY(SX, SY: integer; neglectcomp: TGrindComponent = nil)
      : TGrindComponent;
    function GetCanvasRect: TRect;
    function GetatRect(SLeft, STop, SRight, SBottom: integer;
      neglectcomp: TGrindComponent = nil): TGrindComponent;
    procedure RenameSymbol(old, new: string);
    function AddComponent(SX, SY: integer; aType: TComptype): TGrindComponent;
    constructor Create(aPaintBox: TPaintBox); virtual;
    destructor Destroy; override;
    function ClearParser: TMATLABParser;
    property Components: TList read FComponents;
    property ZoomCanvas: TZoomCanvas read FZoomCanvas write FZoomCanvas;
    property Selected: TGrindComponent read GetSelected write SelectComp;
    property Dragging: Boolean read FDragging write FDragging;
    property PaintBox: TPaintBox read FPaintBox;
    property Parser: TMATLABParser read FParser;
    property ModelKind: TModelKind read FModelKind;
    property CompData[Item:TCompType]:TCompData  read GetCompdata;
    property IsStochastic: Boolean read FIsStochastic write FIsStochastic;
    property SelectList: TList read FSelectList write MultiSelect;
    property StyleSchemes[Items: integer]: TComponentStyles read GetStyleSchemes
      write SetStyleSchemes;
    property ParsVisible: Boolean read FParsVisible write SetParsVisible;
    property ActiveUndo: Boolean read FActiveUndo write SetActiveUndo;
    property Styles: TComponentStyles read FStyles write SetStyles;
    property IsMatrix: Boolean read FIsMatrix write SetIsMatrix;
    property DraggingSelectBox: Boolean read FDraggingSelectbox
      write SetDraggingSelectBox;
    property DataFileChanged: Boolean read FDataFileChanged
      write FDataFileChanged;
  end;

  TGrindComponent = class // Basic abstract type for all elements
  private
    FTheType: TComptype;
    FID: integer;
    FX: integer;
    FY: integer;
    FFromX: integer;
    FFromY: integer;
    FFrom: TGrindComponent;
    FTo: TGrindComponent;
    FTheModel: TGrindModel;
    FExpression: string;
    FExpression2: string;
    FDescription: string;
    FSymbol: string;
    FUnit: string;
    FSyntaxError: string;
    FDragKind: TDragKind;
    FDragPoint: TPoint;
    FSymbolRect: TRect;
    FTextRect: TRect;
    FStyle: PCompStyle;
    FVisible: Boolean;
    FHasData: Boolean;
    FNCols: integer;
    FNRows: integer;
    function GetRect: TRect;
    procedure SetSymbol(Value: string);
    function MakeUniqueName(Short: string): string;
    function GetTypeStr: string;
    procedure DrawSelected(Canvas: TZoomCanvas);
    function getHeight: integer;
    function getWidth: integer;
    procedure SetText(const Value: string); virtual;
    function GetIsconnector: Boolean;
    procedure SetExpression(const Value: string); virtual;
    function GetExpressionFmt: string;
    function GetDisplExpression: string;
  protected
    FText: string;
    function Vertical: Boolean; virtual;
    function right: Boolean; virtual;
    function Down: Boolean; virtual;
    function ExtractComment(S: string): string;
    function GetExpressionType: string; virtual;
    procedure SaveUndoRec(var UndoRec: TUndoRec); virtual;
    procedure SetUndoRec(UndoRec: TUndoRec); virtual;
  public
    constructor Create(Owner: TGrindModel; aX, aY: integer); virtual;
    procedure Assign(source: TGrindComponent); virtual;
    procedure Draw(Canvas: TZoomCanvas); virtual;
    procedure CheckConnect;
    procedure ReverseConnection; virtual;
    function ConnectPoint(fromP: TPoint): TPoint;
    function HasPoint(aX, aY: integer): Boolean; virtual;
    function HasError(var aError: TGRindError): Boolean; virtual;
    function HintText: string; virtual;
    function HasText(aX, aY: integer; doSelect: Boolean): TEdit;
    procedure ReadList(AList: TStrings); virtual;
    procedure DragComponent(SX, SY: integer); virtual;
    procedure Select(Canvas: TZoomCanvas); virtual;
    function fromtoStatevar: Boolean;
    function ExtractParameters: integer; // returns number of added components
    procedure SaveToString(const List: TStrings); virtual;
    property X: integer read FX write FX;
    property Y: integer read FY write FY;
    property width: integer read getWidth;
    property height: integer read getHeight;
    property Rect: TRect read GetRect;
    property TheModel: TGrindModel read FTheModel;
    property Symbol: string read FSymbol write SetSymbol;
    property Text: string read FText write SetText;
    property TheUnit: string read FUnit write FUnit;
    property Visible: Boolean read FVisible write FVisible;
    property Description: string read FDescription write FDescription;
    property DisplExpression: string read GetDisplExpression;
    property SyntaxError: string read FSyntaxError write FSyntaxError;
    property ExpressionFmt: string read GetExpressionFmt;
    property Expression: string read FExpression write SetExpression;
    property Expression2: string read FExpression2 write FExpression2;
    property TheType: TComptype read FTheType;
    property TypeStr: string read GetTypeStr;
    property DragPoint: TPoint read FDragPoint write FDragPoint;
    property From: TGrindComponent read FFrom write FFrom;
    property aTo: TGrindComponent read FTo write FTo;
    property FromX: integer read FFromX write FFromX;
    property FromY: integer read FFromY write FFromY;
    property HasData: Boolean read FHasData write FHasData;
    property IsConnector: Boolean read GetIsconnector;
    property ExpressionType: string read GetExpressionType;
    property NCols: integer read FNCols write FNCols;
    property NRows: integer read FNRows write FNRows;
    property ID: integer read FID write FID;
  end;

  TCloud = class(TGrindComponent)
  public
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
    function HintText: string; override;
    procedure Select(Canvas: TZoomCanvas); override;
    procedure DragComponent(SX, SY: integer); override;
  end;

  TTextOnly = class(TGrindComponent)
  private
    FModStyle: TCompStyle;
    procedure SetText(const Value: string); override;
  public
    function HintText: string; override;
    function HasError(var aError: TGRindError): Boolean; override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
    property Text: string read FText write SetText;
  end;

  TModelEquations = class(TGrindComponent)
    // Total model in one component
  private
    FModStyle: TCompStyle;
  protected
    function GetExpressionType: string; override;
  public
    function HintText: string; override;
    function HasError(var aError: TGRindError): Boolean; override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
  end;

  TStatevar = class(TGrindComponent)
  private
  protected
    function GetExpressionType: string; override;
  public
    function HintText: string; override;
    function EquationText(flowsymbols: Boolean = False): string;
    function HasError(var aError: TGRindError): Boolean; override;
    procedure DragComponent(SX, SY: integer); override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
  end;

  TAuxvar = class(TGrindComponent)
  public
    function HintText: string; override;
    function HasError(var aError: TGRindError): Boolean; override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
  end;

  TPermanentvar = class(TAuxvar)
  public
     constructor Create(Owner: TGrindModel; aX, aY: integer); override;
  end;

  TUserFunction = class(TGrindComponent)
  protected
    function GetExpressionType: string; override;
  public
    function HintText: string; override;
    function HasError(var aError: TGRindError): Boolean; override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
  end;

  TParameter = class(TGrindComponent)
  protected
    function GetExpressionType: string; override;
  public
    function HasError(var aError: TGRindError): Boolean; override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
  end;

  TExternvar = class(TGrindComponent)
  private
    FOutsideRange: TOutsideRange;
    function getOutsideOption: string;
  protected
    function GetExpressionType: string; override;
    procedure SaveUndoRec(var UndoRec: TUndoRec); override;
    procedure SetUndoRec(UndoRec: TUndoRec); override;
  public
    function HintText: string; override;
    function HasError(var aError: TGRindError): Boolean; override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
    procedure Assign(source: TGrindComponent); override;
    property OutsideOption: string read getOutsideOption;
    property OutsideRange: TOutsideRange read FOutsideRange write FOutsideRange;
  end;

  TFlow = class(TGrindComponent)
  private
    FValveYOff: integer;
    FValveXOff: integer; // relative to X
    FVertValve: Boolean;
    FValveRelXpos: double;
    FValveRelYpos: double;
    function GetValveX: integer;
    function GetValveY: integer;
    procedure SetValveX(const Value: integer);
    procedure SetValveY(const Value: integer);
  protected
    function right: Boolean; override;
    function Down: Boolean; override;
    function Vertical: Boolean; override;
    procedure plotValve(Canvas: TZoomCanvas; h3: TPoint;
      aVertical: Boolean); virtual;
  public
    function HintText: string; override;
    procedure ReadList(AList: TStrings); override;
    procedure SaveToString(const List: TStrings); override;
    function HasError(var aError: TGRindError): Boolean; override;
    function HasPoint(aX, aY: integer): Boolean; override;
    procedure Draw(Canvas: TZoomCanvas); override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
    procedure Assign(source: TGrindComponent); override;
    property ValveX: integer read GetValveX write SetValveX;
    property ValveY: integer read GetValveY write SetValveY;
    property ValveRelXpos: double read FValveRelXpos write FValveRelXpos;
    property ValveRelYpos: double read FValveRelYpos write FValveRelYpos;
  end;

  TTrain = class(TFlow)
  protected
    procedure plotValve(Canvas: TZoomCanvas; h3: TPoint;
      aVertical: Boolean); override;
  public
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
  end;

  TConnector = class(TGrindComponent)
  private
    FFromHelpYOff: integer;
    FFromHelpXOff: integer; // relative to FromX
    FHelpYOff: integer;
    FHelpXOff: integer; // relative to X
    function GetFromHelpX: integer;
    function GetFromHelpY: integer;
    function GetHelpX: integer;
    function GetHelpY: integer;
    procedure SetFromHelpX(const Value: integer);
    procedure SetFromHelpY(const Value: integer);
    procedure SetHelpX(const Value: integer);
    procedure SetHelpY(const Value: integer);
    function GetDefaultCurve: Boolean;
    procedure SetDefaultCurve(const Value: Boolean);
  protected
    procedure SaveUndoRec(var UndoRec: TUndoRec); override;
    procedure SetUndoRec(UndoRec: TUndoRec); override;
  public
    procedure ReverseConnection; override;
    function HintText: string; override;
    function HasError(var aError: TGRindError): Boolean; override;
    procedure Select(Canvas: TZoomCanvas); override;
    function HasPoint(aX, aY: integer): Boolean; override;
    procedure ReadList(AList: TStrings); override;
    procedure SaveToString(const List: TStrings); override;
    procedure DragComponent(SX, SY: integer); override;
    procedure Draw(Canvas: TZoomCanvas); override;
    constructor Create(Owner: TGrindModel; aX, aY: integer); override;
    procedure Assign(source: TGrindComponent); override;
    property FromHelpX: integer read GetFromHelpX write SetFromHelpX;
    property FromHelpY: integer read GetFromHelpY write SetFromHelpY;
    property HelpX: integer read GetHelpX write SetHelpX;
    property HelpY: integer read GetHelpY write SetHelpY;
    property DefaultCurve: Boolean read GetDefaultCurve write SetDefaultCurve;
  end;

function CompTextCompare(Item1, Item2: Pointer): integer;
function InvCompTextCompare(Item1, Item2: Pointer): integer;

implementation

uses visgrind, dialogs, ParseClass, prgTools, Forms, math,
  CompDialog, ModelDialog, SelectDlg, SymbolUsedDialog, MatrixDlg, system.types, system.UITypes;

const
  // clErrorBk = $00E0E0FF;

  clErrorFont = $00000B0;
  clErrConnect = $00B0B0B0;
  err_isolated_component = 'Error 1.1 in %s: isolated component';
  err_flow_isolated = 'Error 1.2 in flow %s: component isolated';
  err_connect1 = 'Error 1.3 in connector: %s points to nothing';
  err_connect2 = 'Error 1.4 in connector %s: start not connected';
  err_connect3 = 'Error 1.5 in connector %s: isolated component';
  err_symbol_unknown = 'Error 2.1 in expression of %s: "%s" unknown';
  err_Parser_error = 'Syntax error 2.2 in expression of %s: %s';
  err_par_wrong_size =
    'Error 2.5 Dimensions of %s (%d,%d) do not match any state veriable';
  err_function_unknown =
    'Error 2.3 in expression of %s: function "%s()" unknown';
  err_User_Function = 'Syntax error 2.4 in user defined function %s: %s';
  err_t_notation_not_allowed =
    'Error 2.5 in expression %s, remove (t) from state variable %s';
  err_no_initial_value = 'Error 3.1 in state var %s: no initial condition';
  err_aux_no_equation = 'Error 3.2 in auxiliary var %s: no equation entered';
  err_par_no_value = 'Error 3.3 in parameter %s: no value entered';
  err_ext_no_default = 'Error 3.4 in external variable %s:' +
    ' no default value entered';
  err_flow_no_eq = 'Error 3.5 in flow %s: no equation entered';
  err_isolated_statevar =
    'Error 3.6 in state variable %s: no connected discrete/continuous flow';
  war_expr_not_used =
    'Warning 4.1 in expression of %s: symbol "%s" is defined but not used';
  war_double_negative = 'Warning 5.1 in flow %s: possibly double negative ' +
    'expression in flow from %s:'#10#10'%s becomes: -(%s)'#10#10 +
    'The arrow FROM the state variable implies already that it is a negative term!';
  war_negative_expr =
    'Warning 5.2 in flow %s to %s: negative expression: %s'#10#10 +
    'For the logic of the scheme it might be better to use an' +
    ' arrow FROM the state variable';

  { TGrindModel }

function CompCompare(Item1, Item2: Pointer): integer;
begin
  Result := TGrindComponent(Item1).ID - TGrindComponent(Item2).ID;
end;

function CompTextCompare(Item1, Item2: Pointer): integer;
begin
  Result := CompareText(TGrindComponent(Item1).Symbol,
    TGrindComponent(Item2).Symbol);
end;

function InvCompTextCompare(Item1, Item2: Pointer): integer;
begin
  Result := CompareText(TGrindComponent(Item2).Symbol,
    TGrindComponent(Item1).Symbol);
end;

function TGrindModel.ReplaceComp(AComp: TGrindComponent; aType: TComptype)
  : TGrindComponent;
var
  conn, conn1: TList;
  i: integer;
begin
  FActiveUndo := False;
  conn := TList.Create;
  try
    conn1 := Connectors(AComp, cAll);
    for i := 0 to conn1.count - 1 do
      conn.Add(conn1.Items[i]);
    Result := AddComponent(AComp.X, AComp.Y, aType);
    Result.From := AComp.From;
    Result.aTo := AComp.aTo;
    for i := 0 to conn.count - 1 do
    begin
      if TGrindComponent(conn.Items[i]).From = AComp then
        TGrindComponent(conn.Items[i]).From := Result;
      if TGrindComponent(conn.Items[i]).aTo = AComp then
        TGrindComponent(conn.Items[i]).aTo := Result;
    end;
    // DeleteComp(AComp);
  finally
    conn.free;
    FActiveUndo := True;
  end;
end;

function TGrindModel.AddComponent(SX, SY: integer; aType: TComptype)
  : TGrindComponent;
var
  newcomp: TGrindComponent;
  aX, aY: integer;
begin
  { TComptype = (ctStatevar, ctAuxvar, ctPar, ctConnector, ctStream, ctGhost,
    ctCloud); }
  aX := ZoomCanvas.Scrn2X(SX);
  aY := ZoomCanvas.Scrn2Y(SY);
  case aType of
    ctCloud:
      newcomp := TCloud.Create(self, aX, aY);
    ctStatevar:
      newcomp := TStatevar.Create(self, aX, aY);
    ctParameter:
      newcomp := TParameter.Create(self, aX, aY);
    ctExternvar:
      newcomp := TExternvar.Create(self, aX, aY);
    ctAuxilvar:
      newcomp := TAuxvar.Create(self, aX, aY);
    ctPermanentvar:
      newcomp := TPermanentvar.Create(self, aX, aY);
    ctConnector:
      newcomp := TConnector.Create(self, aX, aY);
    ctFlow:
      newcomp := TFlow.Create(self, aX, aY);
    ctTrain:
      newcomp := TTrain.Create(self, aX, aY);
    ctUserFunction:
      newcomp := TUserFunction.Create(self, aX, aY);
    ctModelEquations:
      newcomp := TModelEquations.Create(self, aX, aY);
    ctTextOnly:
      newcomp := TTextOnly.Create(self, aX, aY);
  else
    newcomp := nil;
  end;
  Result := newcomp;
  FComponents.Add(newcomp);
  SaveUndo(newcomp, uaCreated, 1);
  CheckModelKind;
  // Selected := newComp;
  MainForm.UpdatePaintBox;
end;

constructor TGrindModel.Create(aPaintBox: TPaintBox);
var
  i: integer;
begin
  FPaintBox := aPaintBox;
  FActiveUndo := True;
  FCurrUndo := 0;
  FParsVisible := True;
  FIsMatrix := False;
  FIsStochastic := False;
  FFirstUndo := 0;
  FLastUndo := 0;
  FUndoNr := 0;
  CreateCompData;
  FParser := TMATLABParser.Create;
  FParser.Optimize := False;
  FComponents := TList.Create;
  FTempList := TList.Create;
  FSelectList := TList.Create;
  FPaintBox := aPaintBox;
  FModelKind := mkUnknown;
  FZoomCanvas := TZoomCanvas.Create;
  FZoomCanvas.Zoomfactor := 1;
  for i := 0 to nstyles - 1 do
  begin
    SetDefaultStyles(FStyles, i);
    FStyleSchemes[i] := FStyles;
  end;
  FStyles := FStyleSchemes[0];
end;

destructor TGrindModel.Destroy;
var
  i: integer;
begin
  inherited;
  with FComponents do
    for i := 0 to count - 1 do
      TObject(Items[i]).free;
  FComponents.free;
  FParser.free;
  FTempList.free;
  FSelectList.free;
  FZoomCanvas.free;
end;

procedure TGrindModel.Draw(Canvas: TCanvas);
var
  i: integer;
begin
  ZoomCanvas.Canvas := Canvas;
  for i := 0 to FComponents.count - 1 do
    if FComponents.Items[i] <> Selected then
      TGrindComponent(FComponents.Items[i]).Draw(ZoomCanvas);
  if Selected <> nil then
    Selected.Draw(ZoomCanvas);
  if DraggingSelectBox then
  begin
    ZoomCanvas.Brush.style := bsClear;
    ZoomCanvas.Pen.style := psDot;
    ZoomCanvas.Pen.width := 0;
    ZoomCanvas.Pen.Color := clBlack;
    ZoomCanvas.Rectangle(FSelectBox);
    ZoomCanvas.Brush.style := bsSolid;
    ZoomCanvas.Pen.style := psSolid;
  end;
end;

procedure TGrindModel.SelectComp(AComp: TGrindComponent);
begin
  if (Selected <> nil) and (Selected <> AComp) then
  begin
    Selected.Text := MainForm.TextEdit.Text;
    Selected.Symbol := MainForm.SymbolEdit.Text;
    Selected.Expression := MainForm.ExpressLongEdit.Text;
  end;
  if (SelectList.count > 1) or (AComp <> Selected) then
  begin
    FSelectList.count := 0;
    if AComp <> nil then
    begin
      SaveUndo(AComp, uaChanged, 1);
      FSelectList.Add(AComp);
    end;
    if AComp = nil then
    begin
      FDragging := False;
      MainForm.TextEdit.Visible := False;
      MainForm.SymbolEdit.Visible := False;
      MainForm.ExpressEdit.Visible := False;
      MainForm.StatusBar1.SimpleText := '';
      MainForm.UpdatePaintBox; // PaintBox.Repaint;
    end
    else
      AComp.Select(ZoomCanvas);
  end;
end;

function TGrindModel.GetatRect(SLeft, STop, SRight, SBottom: integer;
  neglectcomp: TGrindComponent = nil): TGrindComponent;
begin
  Result := GetXY(SLeft, STop, neglectcomp);
  if Result = nil then
    Result := GetXY(SRight, STop, neglectcomp);
  if Result = nil then
    Result := GetXY(SLeft, SBottom, neglectcomp);
  if Result = nil then
    Result := GetXY(SRight, SBottom, neglectcomp);
end;

function TGrindModel.GetXY(SX, SY: integer; neglectcomp: TGrindComponent = nil)
  : TGrindComponent;
var
  i: integer;
  mind, d: integer;
begin
  Result := nil;
  mind := 999999;
  for i := 0 to FComponents.count - 1 do
    if not(TGrindComponent(FComponents.Items[i]) = neglectcomp) then
    begin
      if TGrindComponent(FComponents.Items[i]).HasPoint(ZoomCanvas.Scrn2X(SX),
        ZoomCanvas.Scrn2Y(SY)) then
      begin
        d := round(sqrt(sqr(TGrindComponent(FComponents.Items[i]).X -
          ZoomCanvas.Scrn2X(SX)) + sqr(TGrindComponent(FComponents.Items[i]).Y -
          ZoomCanvas.Scrn2Y(SY))));
        if d < mind then
        begin
          mind := d;
          Result := TGrindComponent(FComponents.Items[i]);
        end;
      end;
    end;
end;

procedure TGrindModel.DeleteComp(AComp: TGrindComponent;
  MultiSelect: Boolean = False);
var
  i: integer;
begin
  if AComp <> nil then
  begin
    if MultiSelect then
      SaveUndo(AComp, uaDeleted, 0)
    else
      SaveUndo(AComp, uaDeleted, 1);
    i := FComponents.IndexOf(AComp);
    if AComp.HasData and (ComponentDlg.HasData(AComp.Symbol) >= 0) then
      if (dialogs.MessageDlg('Do you want to delete the data of "' +
        AComp.Symbol + '" also?', mtConfirmation, [mbYes, mbNo], 0, mbYes)
        = idYes) then
        ComponentDlg.deletedata(AComp.Symbol);
    FComponents.Delete(i);
    if AComp = Selected then
      Selected := nil;
    i := FSelectList.IndexOf(AComp);
    if i >= 0 then
      FSelectList.Delete(i);
    i := FTempList.IndexOf(AComp);
    if i >= 0 then
      FTempList.Delete(i);

    // Remove connections
    for i := 0 to FComponents.count - 1 do
      with TGrindComponent(FComponents.Items[i]) do
      begin
        if aTo = AComp then
        begin
          aTo := nil;
          FSyntaxError := '';
          if From <> nil then
            From.FSyntaxError := '';
        end;
        if TGrindComponent(FComponents.Items[i]).From = AComp then
        begin
          TGrindComponent(FComponents.Items[i]).From := nil;
          FSyntaxError := '';
          if aTo <> nil then
            aTo.FSyntaxError := '';
        end;
      end;
    // If Connector is deleted then reset the error of ATo and From
    if AComp.aTo <> nil then
      AComp.aTo.FSyntaxError := '';
    if AComp.From <> nil then
      AComp.From.FSyntaxError := '';
    AComp.free;
    CheckModelKind;
  end;
end;

function TGrindModel.CompsOfType(aType: TComptype): TList;
var
  i: integer;
begin
  Result := FTempList;
  FTempList.count := 0;
  for i := 0 to FComponents.count - 1 do
  begin
    if TGrindComponent(FComponents.Items[i]).TheType = aType then
      FTempList.Add(FComponents.Items[i]);
  end;
end;

function TGrindModel.IndexOf(ASymbol: string): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to Components.count - 1 do
  begin
    if TGrindComponent(Components.Items[i]).Symbol = ASymbol then
      Result := i;
  end;
end;

function TGrindModel.IsUnique(ASymbol: string;
  AComp: TGrindComponent = nil): Boolean;
var
  i: integer;
  AList: TStringList;
  S: string;
begin
  Result := True;
  for i := 0 to Components.count - 1 do
  begin
    if (Components.Items[i] <> AComp) and
      (TGrindComponent(Components.Items[i]).Symbol = ASymbol) then
      Result := False;
  end;
  AList := TStringList.Create;
  try
    Parser.GetFunctionNames(AList);
    for i := 0 to AList.count - 1 do
    begin
      S := AList.Strings[i];
      if pos('(', S) > 0 then
        S := copy(S, 1, pos('(', S) - 1);
      if (S = ASymbol) then
        Result := False;
    end;
  finally
    AList.free;
  end;
end;

function TGrindModel.Connectors(AComp: TGrindComponent;
  kind: TConnectKind): TList;
var
  i: integer;
begin
  Result := FTempList;
  FTempList.count := 0;
  if kind in [cFrom, cAll] then
    for i := 0 to Components.count - 1 do
      if TGrindComponent(Components.Items[i]).From = AComp then
        FTempList.Add(Components.Items[i]);
  if kind in [cTo, cAll] then
    for i := 0 to Components.count - 1 do
      if TGrindComponent(Components.Items[i]).aTo = AComp then
        FTempList.Add(Components.Items[i]);
end;

function TGrindModel.CanConnect(Connector, Comp: TGrindComponent;
  aFrom: Boolean): Boolean;
var
  List: TList;
begin
  Result := (Connector <> Comp);
  Result := Result and (not(Comp.TheType in [ctConnector])) and
    (Connector.TheType in [ctConnector, ctFlow, ctTrain]);
  Result := Result and not((Connector.TheType = ctConnector) and (not aFrom) and
    (Comp.TheType in [ctStatevar]));
  Result := Result and not((Connector.TheType = ctConnector) and
    (Comp.TheType in [ctCloud]));
  Result := Result and not((Connector.TheType in [ctFlow, ctTrain]) and
    (Comp.TheType in [ctParameter, ctExternvar, ctAuxilvar, ctPermanentvar, ctUserFunction]));
  Result := Result and not((not aFrom) and (Comp.TheType in [ctParameter,
    ctExternvar, ctUserFunction]));
  Result := Result and not((aFrom) and (Comp.TheType in [ { ctTrain, ctFlow, }
    ctModelEquations]));
  Result := Result and not((Comp.TheType in [ctFlow, ctTrain]) and
    (Connector.TheType in [ctFlow, ctTrain]));
  if Comp.TheType = ctCloud then
  begin
    // can only connect once
    List := Connectors(Comp, cAll);
    CanConnect := Result and (List.count = 0) or
      ((List.count = 1) and (List.IndexOf(Connector) >= 0));
  end;
  { if Result and ((not aFrom) and (connector.From<>nil)) then
    begin
    // can only connect once
    list := connectors(comp, cTo);
    CanConnect := Result and ((list.count = 0) or ((list.count >= 1)
    and (list.IndexOf(connector.From) >= 0)));
    end; }
end;

procedure TGrindModel.Connect(Connector, Comp: TGrindComponent; aFrom: Boolean);
var
  P: TPoint;
begin
  if CanConnect(Connector, Comp, aFrom) then
  begin
    if aFrom then
    begin
      Connector.From := Comp;
      P := Comp.ConnectPoint(point(Connector.X, Connector.Y));
      Connector.FromX := P.X;
      Connector.FromY := P.Y;
    end
    else
    begin
      if Connector.aTo <> Comp then
        Comp.FSyntaxError := '';
      Connector.aTo := Comp;
      P := Comp.ConnectPoint(point(Connector.FromX, Connector.FromY));
      Connector.X := P.X;
      Connector.Y := P.Y;
    end;
  end;
end;

constructor TGrindComponent.Create(Owner: TGrindModel; aX, aY: integer);
begin
  // FDefaultText := '';
  FTheModel := Owner;
  FSymbolRect := emptyRect;
  FTextRect := emptyRect;
  FText := '';
  FExpression2:='';
  FNCols := 1;
  FNRows := 1;
  X := aX;
  Y := aY;
  FromX := missingInteger;
  FromY := missingInteger;
  FHasData := False;
  FVisible := True;
  FID := TheModel.NextID;
end;

function TGrindComponent.ExtractComment(S: string): string;
var
  lst: TStringList;
  i: integer;
begin
  lst := TStringList.Create;
  try
    if S <> '' then
    begin
      lst.Text := S;
      for i := lst.count - 1 downto 0 do
      begin
        if (lst[i] <> '') and (lst[i][1] = '%') then
          lst.Delete(i);
      end;
      S := lst.Text;
      if (length(S) > 2) and (S[length(S)] = #$A) and (S[length(S) - 1] = #$D)
      then
        S := copy(S, 1, length(S) - 2);
      if (length(S) > 0) and (S[length(S)] = ';') then
        S := copy(S, 1, length(S) - 1);
    end;
  finally
    lst.free;
    Result := S;
  end;
end;

function TGrindModel.NextError(shift: integer = 0): TGRindError;
begin
  with Result do
  begin
    Founderr := False;
    errString := '';
    errKind := ekOther;
    errComp := nil;
  end;
  FCurrErr := FCurrErr + shift;
  if (shift < 0) and (FCurrErr < 0) then
    FCurrErr := 0;
  if (shift > 0) and (FCurrErr > Components.count - 1) then
    FCurrErr := FComponents.count - 1;
  while (not Result.Founderr) and (FCurrErr < Components.count) do
    with TGrindComponent(Components.Items[FCurrErr]) do
    begin
      FSyntaxError := '';
      if HasError(Result) then
      begin
        Selected := Result.errComp;
        Result.Founderr := True;
      end;
      FCurrErr := FCurrErr + 1;
    end;
end;

procedure TGrindModel.ReadScheme(const List: TStrings);
var
  i: integer;
  S, TypeStr, fil, oldpath: string;
  f, f1: integer;
  thex, they, err: integer;
  Comp: TGrindComponent;
  DataFileChanged: Boolean;
begin
  DataFileChanged := False;
  Comp := nil;
  IsMatrix := False;
  for i := 0 to List.count - 1 do
  begin
    S := List.Strings[i];
    if copy(S, 1, 5) = '%com=' then
      ComponentDlg.ModelMemo.Lines.Add(copy(S, 6, length(S)))
    else if copy(S, 1, 5) = '%dat=' then
    begin
      DataFileChanged := True;
      fil := copy(S, 6, length(S));
      if fil = '-intern' then
      begin
        ComponentDlg.ExternDatafile := '';
        ComponentDlg.ReadScheme(List);
      end
      else
      begin
        GetDir(0, oldpath);
        try
          if ModelDlg.ThePath <> '' then
            chdir(ModelDlg.ThePath);
          fil := ExpandFileName(fil);
        finally
          chdir(oldpath);
        end;
        ComponentDlg.ExternDatafile := fil;
      end;
      // ComponentDlg.DataFilePath := ExtractFilePath(fil);
    end
    else if copy(S, 1, 6) = '%vec=1' then
      IsMatrix := True;
  end;
  for i := 0 to List.count - 1 do
  begin
    S := List.Strings[i];
    f := pos('[', S);
    if (S <> '') and (f > 0) and (S<>'%[data]') and  (copy(S, 1, 5) <> '%com=') then
    begin
      f1 := pos(';', S);
      TypeStr := copy(S, f + 1, f1 - 1 - f);
      S := copy(S, f1 + 1, length(S));
      f1 := pos(';', S);
      val(copy(S, 1, f1 - 1), thex, err);
      S := copy(S, f1 + 1, length(S));
      f1 := pos(']', S);
      val(copy(S, 1, f1 - 1), they, err);
      thex := ZoomCanvas.X2Scrn(thex);
      they := ZoomCanvas.Y2Scrn(they);
      if TypeStr='' then
         TypeStr:='Parameter';  //This can lead to a parameter P01 to be displayed in the upper left corner
      if TypeStr = 'State variable' then
        Comp := AddComponent(thex, they, ctStatevar)
      else if (TypeStr = 'Function') or (TypeStr = 'Auxiliary variable') then
        Comp := AddComponent(thex, they, ctAuxilvar)
      else if (TypeStr = 'Permanent variable') then
        Comp := AddComponent(thex, they, ctPermanentvar)
      else if TypeStr = 'Parameter' then
        Comp := AddComponent(thex, they, ctParameter)
      else if TypeStr = 'External variable' then
        Comp := AddComponent(thex, they, ctExternvar)
      else if TypeStr = 'User function' then
        Comp := AddComponent(thex, they, ctUserFunction)
      else if TypeStr = 'Connector' then
        Comp := AddComponent(thex, they, ctConnector)
      else if TypeStr = 'Flow' then
        Comp := AddComponent(thex, they, ctFlow)
      else if TypeStr = 'Train' then
        Comp := AddComponent(thex, they, ctTrain)
      else if TypeStr = 'Cloud' then
        Comp := AddComponent(thex, they, ctCloud)
      else if TypeStr = 'Model equations' then
        Comp := AddComponent(thex, they, ctModelEquations)
      else if TypeStr = 'Text Only' then
        Comp := AddComponent(thex, they, ctTextOnly);
    end;
    if copy(S, 2, 3) = 'sym' then
      Comp.Symbol := copy(S, 6, length(S))
  end;
  for i := 0 to Components.count - 1 do
    TGrindComponent(Components.Items[i]).ReadList(List);
  MainForm.ZoomComboChange(nil);
  UpdateConnections;
  if DataFileChanged then
    with ComponentDlg do
      OpenDatafile(ExternDatafile);
end;

procedure TGrindModel.CheckModelKind;
var
  i: integer;
begin
  MainForm.TrainBtn.Enabled := True;
  MainForm.FlowBtn.Enabled := True;
  MainForm.Continuousflow1.Enabled := True;
  MainForm.DiscreteFlow1.Enabled := True;
  FModelKind := mkUnknown;
  for i := 0 to Components.count - 1 do
    if TGrindComponent(Components.Items[i]).TheType = ctFlow then
    begin
      FModelKind := mkFlow;
      MainForm.TrainBtn.Enabled := False;
      MainForm.DiscreteFlow1.Enabled := False;
      break;
    end
    else if TGrindComponent(Components.Items[i]).TheType = ctTrain then
    begin
      FModelKind := mkTrain;
      MainForm.FlowBtn.Enabled := False;
      MainForm.Continuousflow1.Enabled := False;
      break;
    end;
end;

procedure TGrindModel.Clear;
var
  i: integer;
begin
  FModelKind := mkUnknown;
  ActiveUndo := False;
  FCurrUndo := 0;
  FFirstUndo := 0;
  FLastUndo := 0;
  IsMatrix := False;
  MainForm.HideParameters1.Caption := 'Hide parameters';
  with ModelDlg do
  begin
    inifileEdit.Text := '';
    DefaultsBtnClick(nil);
    ModelMemo.Clear;
    ParamLongMemo.Clear;
    SchemeLongMemo.Clear;
  end;
  with ComponentDlg do
  begin
    ModelMemo.Clear;
    ClearDataFile;
  end;
  Selected := nil;
  with FComponents do
    for i := 0 to count - 1 do
      TObject(Items[i]).free;
  FComponents.count := 0;
  DataFileChanged := False;
  ZoomCanvas.Zoomfactor := 1;
  CheckModelKind;
  ActiveUndo := True;
  ClearParser;
end;

function TGrindModel.removet(eq: string; doadd: Boolean): string;
var
  P: integer;
  List: TList;
  S: string;
  i, j, ls: integer;

  function isoper(ch: char): Boolean;
  begin
    Result := not CharInSet(ch, ['A' .. 'Z', 'a' .. 'z', '0' .. '9', '_']);
  end;

begin
  List := CompsOfType(ctStatevar);
  for i := 0 to List.count - 1 do
  begin
    S := TGrindComponent(List.Items[i]).Symbol + '(t)';
    P := pos(S, eq);
    while P > 0 do
    begin
      eq := copy(eq, 1, P - 1) + TGrindComponent(List.Items[i]).Symbol +
        copy(eq, P + length(S), length(eq));
      P := pos(S, eq);
    end;
  end;
  if doadd then
  begin
    for i := 0 to List.count - 1 do
    begin
      S := TGrindComponent(List.Items[i]).Symbol;
      ls := length(S);
      for j := length(eq) - ls + 1 downto 1 do
        if (copy(eq, j, ls) = S) and ((j = 1) or isoper(eq[j - 1])) and
          ((j + ls > length(eq)) or isoper(eq[j + ls])) then
          eq := copy(eq, 1, j - 1) + S + '(t)' + copy(eq, j + ls, length(eq));
    end;
  end;
  Result := eq;
end;

function TGrindModel.GetCanvasRect: TRect;
var
  i: integer;
begin
  Result := Rect(99999, 99999, 0, 0);
  for i := 0 to Components.count - 1 do
    with TGrindComponent(Components.Items[i]) do
    begin
      if X + width + 10 > Result.right then
        Result.right := X + width + 10;
      if Y + height + 10 > Result.bottom then
        Result.bottom := Y + height + 10;
      if X - 10 < Result.left then
        Result.left := X - 10;
      if Y - 10 < Result.top then
        Result.top := Y - 10;
    end;
end;

function TGrindModel.GetCompdata(Item: TCompType): TCompData;
begin
  Result:=FCompData[Item];
end;

function TGrindModel.GetSelected: TGrindComponent;
begin
  if FSelectList.count <> 1 then
    Result := nil
  else
    Result := TGrindComponent(FSelectList.Items[0]);
end;

procedure TGrindModel.MultiSelect(AList: TList);
var
  i: integer;
  AComp: TGrindComponent;
begin
  Selected := nil;
  if AList.count = 1 then
    Selected := TGrindComponent(AList.Items[0])
  else
    for i := 0 to AList.count - 1 do
    begin
      AComp := TGrindComponent(AList.Items[i]);
      if i = 0 then
        SaveUndo(AComp, uaChanged, 1)
      else
        SaveUndo(AComp, uaChanged, 0);
      FSelectList.Add(AComp);
      AComp.Select(ZoomCanvas);
    end;
  MainForm.UpdatePaintBox;
end;

function TGrindModel.IsSelected(AComp: TGrindComponent): Boolean;
begin
  Result := FSelectList.IndexOf(AComp) <> -1;
end;

procedure TGrindModel.DragSelectBox(SX, SY: integer);
begin
  Selected := nil;
  if not DraggingSelectBox then
  begin
    DraggingSelectBox := True;
    FSelectBox.left := ZoomCanvas.Scrn2X(SX);
    FSelectBox.top := ZoomCanvas.Scrn2Y(SY);
    FSelectBox.right := ZoomCanvas.Scrn2X(SX);
    FSelectBox.bottom := ZoomCanvas.Scrn2Y(SY);
  end
  else
  begin
    FSelectBox.right := ZoomCanvas.Scrn2X(SX);
    FSelectBox.bottom := ZoomCanvas.Scrn2Y(SY);
  end;
  MainForm.UpdatePaintBox;
end;

procedure TGrindModel.SetDraggingSelectBox(const Value: Boolean);
var
  Comp: TGrindComponent;
  i: integer;
begin
  if FDraggingSelectbox and not Value then
  begin
    Selected := nil;
    FTempList.count := 0;
    if FSelectBox.left > FSelectBox.right then
    begin
      i := FSelectBox.left;
      FSelectBox.left := FSelectBox.right;
      FSelectBox.right := i;
    end;
    if FSelectBox.top < FSelectBox.bottom then
    begin
      i := FSelectBox.top;
      FSelectBox.top := FSelectBox.bottom;
      FSelectBox.bottom := i;
    end;
    for i := 0 to Components.count - 1 do
    begin
      Comp := TGrindComponent(Components.Items[i]);
      if (Comp.X + Comp.width > FSelectBox.left) and (Comp.X < FSelectBox.right)
        and (Comp.Y < FSelectBox.top) and
        (Comp.Y + Comp.height > FSelectBox.bottom) then
        FTempList.Add(Comp);
    end;
    FSelectBox.right := 0;
    FSelectBox.bottom := 0;
    FDraggingSelectbox := Value;
    SelectList := FTempList;
    if SelectList.count = 0 then
      MainForm.UpdatePaintBox;
  end
  else
    FDraggingSelectbox := Value;
end;

procedure TGrindModel.SetIsMatrix(const Value: Boolean);
var
  Comp: TGrindComponent;
  i: integer;
begin
  FIsMatrix := Value;
  if not FIsMatrix then
    for i := 0 to Components.count - 1 do
    begin
      Comp := TGrindComponent(Components.Items[i]);
      Comp.NCols := 1;
      Comp.NRows := 1;
    end;
end;

procedure TGrindModel.SetDefaultStyles(var aStyles: TComponentStyles;
  No: integer);
var
  i: integer;
begin
  with aStyles[ctStatevar] do
  begin
    width := 50;
    height := 45;
    pencolor := clBlue;
    penwidth := 3;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skRectangle;
    ShowSymbol := True;
    ShowText := True;
    SwapTextSymbol := False;
 //   DefaultText := 'State var';
 //   TypeStr := 'State variable';
  end;
  with aStyles[ctCloud] do
  begin
    width := 19;
    height := 14;
    pencolor := clBlack;
    penstyle := psSolid;
    penwidth := 1;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skCloud;
    ShowSymbol := False;
    ShowText := False;
    SwapTextSymbol := False;
 //   DefaultText := '';
 ///   TypeStr := 'Cloud';
  end;
  with aStyles[ctAuxilvar] do
  begin
    width := 50;
    height := 45;
    pencolor := clGreen;
    penwidth := 3;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skRoundRect;
    ShowSymbol := True;
    ShowText := True;
    SwapTextSymbol := False;
  //  DefaultText := 'Auxiliary var';
  //  TypeStr := 'Auxiliary variable';
  end;
  aStyles[ctPermanentvar]:=aStyles[ctAuxilvar];


  with aStyles[ctUserFunction] do
  begin
    width := 45;
    height := 25;
    pencolor := clBlack;
    penwidth := 1;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skRectangle;
    ShowSymbol := True;
    ShowText := False;
    SwapTextSymbol := False;
 //   DefaultText := 'User function';
 //   TypeStr := 'External variable';
  end;
  with aStyles[ctParameter] do
  begin
    width := 30;
    height := 30;
    pencolor := clRed;
    penwidth := 2;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skEllipse;
    ShowSymbol := True;
    ShowText := False;
    SwapTextSymbol := False;
 //   DefaultText := 'Parameter';
 //   TypeStr := 'Parameter';
  end;
  with aStyles[ctExternvar] do
  begin
    width := 30;
    height := 30;
    pencolor := clBlue;
    penwidth := 2;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skEllipse;
    ShowSymbol := True;
    ShowText := False;
    SwapTextSymbol := False;
 //   DefaultText := 'Externvar';
 //   TypeStr := 'External variable';
  end;

  with aStyles[ctConnector] do
  begin
    width := 5;
    height := 5;
    pencolor := clRed;
    penwidth := 0;
    penstyle := psDot;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skConnector;
    ShowSymbol := False;
    ShowText := False;
    SwapTextSymbol := False;
 //   DefaultText := '';
  //  TypeStr := 'Connector';
  end;
  with aStyles[ctFlow] do
  begin
    width := 30;
    height := 35;
    pencolor := clBlack;
    pencolor2 := clBlack;
    penwidth := 2;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skValve;
    ShowSymbol := False;
    ShowText := True;
    SwapTextSymbol := False;
  //  DefaultText := 'Flow';
  //  TypeStr := 'Flow';
  end;
  with aStyles[ctTrain] do
  begin
    width := 30;
    height := 50;
    pencolor := clBlack;
    pencolor2 := clRed;
    penwidth := 2;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skTrain;
    ShowSymbol := False;
    ShowText := True;
    SwapTextSymbol := False;
   // DefaultText := 'Discrete flow';
   // TypeStr := 'Train';
  end;
  if No = 1 then
  begin
    with aStyles[ctParameter] do
    begin
      width := 40;
      height := 15;
      pencolor := clRed;
      penwidth := 2;
      penstyle := psSolid;
      brushcolor := clWhite;
      brushstyle := bsSolid;
      Shape := skHbar;
      ShowSymbol := True;
      ShowText := False;
      SwapTextSymbol := False;
    end;
    with aStyles[ctAuxilvar] do
    begin
      Shape := skEllipse;
      width := 40;
      height := 40;
    end;
    with aStyles[ctPermanentvar] do
    begin
      Shape := skEllipse;
      width := 40;
      height := 40;
    end;

    for i := integer(ctStatevar) to integer(ctCloud) do
      with aStyles[TComptype(i)] do
      begin
        pencolor := clBlack;
        penwidth := 2;
        pencolor2 := clBlack;
      end;
    aStyles[ctConnector].penwidth := 0;
    aStyles[ctCloud].penwidth := 0;
    aStyles[ctStatevar].ShowText := False;
    aStyles[ctAuxilvar].ShowText := False;
    aStyles[ctPermanentvar].ShowText := False;
  end
  else if No = 2 then
    for i := integer(ctStatevar) to integer(ctCloud) do
      with aStyles[TComptype(i)] do
        ShowText := False;
end;

procedure TGrindModel.CreateCompdata;
begin
  with FCompdata[ctStatevar] do
  begin
    DefaultText := 'State var';
    TypeStr := 'State variable';
  end;
  with FCompdata[ctCloud] do
  begin
    DefaultText := '';
    TypeStr := 'Cloud';
  end;
  with FCompdata[ctAuxilvar] do
  begin
    DefaultText := 'Auxiliary var';
    TypeStr := 'Auxiliary variable';
  end;
  with FCompdata[ctPermanentvar] do
  begin
    DefaultText := 'Permanent var';
    TypeStr := 'Permanent variable';
  end;

  with FCompdata[ctUserFunction] do
  begin
   DefaultText := 'User function';
    TypeStr := 'User function';
  end;
  with FCompdata[ctParameter] do
  begin
    DefaultText := 'Parameter';
    TypeStr := 'Parameter';
  end;
  with FCompdata[ctExternvar] do
  begin
    DefaultText := 'Externvar';
    TypeStr := 'External variable';
  end;

  with FCompdata[ctConnector] do
  begin
    DefaultText := '';
    TypeStr := 'Connector';
  end;
  with FCompdata[ctFlow] do
  begin
    DefaultText := 'Flow';
    TypeStr := 'Flow';
  end;
  with FCompdata[ctTrain] do
  begin
    DefaultText := 'Discrete flow';
    TypeStr := 'Train';
  end;
    with FCompdata[ctTextOnly] do
  begin
    TypeStr := 'Text Only';
    DefaultText := 'Text';
  end;  with FCompdata[ctModelEquations] do
  begin
    TypeStr := 'Model equations';
    DefaultText := 'Model equations';
  end;
end;
procedure TGrindModel.UpdateConnections;
var
  i, j: integer;
begin
  for i := 0 to Components.count - 1 do
    with (TGrindComponent(Components.Items[i])) do
      if IsConnector then
        CheckConnect;
  for i := 0 to Components.count - 1 do
    with (TGrindComponent(Components.Items[i])) do
    begin
      if From <> nil then
        Connect(TGrindComponent(Components.Items[i]), From, True);
      if aTo <> nil then
        Connect(TGrindComponent(Components.Items[i]), aTo, False);
    end;
  // Make double Flow/Train visible
  for i := 0 to Components.count - 2 do
    with (TGrindComponent(Components.Items[i])) do
      if (TheType in [ctFlow, ctTrain]) and (From <> nil) and (aTo <> nil) then
      begin
        for j := i + 1 to Components.count - 1 do
          if ((TGrindComponent(Components.Items[j]).aTo = aTo) and
            (TGrindComponent(Components.Items[j]).From = From)) or
            ((TGrindComponent(Components.Items[j]).aTo = From) and
            (TGrindComponent(Components.Items[j]).From = aTo)) then
            if abs(FromY - Y) < abs(FromX - X) then
            begin
              (TFlow(Components.Items[j])).Y :=
                (TFlow(Components.Items[j])).Y + 22;
              (TFlow(Components.Items[j])).FromY := (TFlow(Components.Items[j]))
                .FromY + 22;
              Y := Y - 21;
              FromY := FromY - 21;
            end
            else
            begin
              (TFlow(Components.Items[j])).X :=
                (TFlow(Components.Items[j])).X + 22;
              (TFlow(Components.Items[j])).FromX := (TFlow(Components.Items[j]))
                .FromX + 22;
              (TFlow(Components.Items[i])).X :=
                (TFlow(Components.Items[i])).X - 21;
              (TFlow(Components.Items[i])).FromX := (TFlow(Components.Items[i]))
                .FromX - 21;
            end

      end;
end;

procedure TGrindModel.SetStyles(const Value: TComponentStyles);
begin
  FStyles := Value;
  UpdateConnections;
  MainForm.UpdatePaintBox;
end;

function TGrindModel.GetStyleSchemes(Items: integer): TComponentStyles;
begin
  Result := FStyleSchemes[Items];
end;

procedure TGrindModel.SetStyleSchemes(Items: integer;
  const Value: TComponentStyles);
begin
  FStyleSchemes[Items] := Value;
end;

function TGrindModel.CanRedo: Boolean;
begin
  Result := FCurrUndo <> FLastUndo;
end;

function TGrindModel.CanUndo: Boolean;
begin
  Result := FCurrUndo <> FFirstUndo
end;
{
  procedure TGrindModel.Redo;
  var
  comp: TGrindComponent;
  UndoRec: TUndoRec;
  i: integer;
  begin
  if CanRedo then
  begin
  FCurrUndo := FCurrUndo + 1;
  if FCurrUndo > maxUndo then
  FCurrUndo := 0;
  end;
  if FUndoList[FCurrUndo].Action = uaChanged then
  begin
  i := IndexOf(FUndoList[FCurrUndo].Symbol);
  if i >= 0 then
  begin
  Comp := Components.Items[i];
  UndoRec := FUndoList[FCurrUndo];
  Selected := Comp;
  //SaveUndo(comp, uaChanged, 1);
  Comp.SetUndoRec(UndoRec);
  UpdateConnections;
  MainForm.UpdatePaintBox;
  end;
  end;
  end; }

procedure TGrindModel.SaveUndo(Comp: TGrindComponent; Action: TUndoAction;
  IncUndo: integer);
// IncUndo can be -1 0 or 1 (resp Undo,MultiSave,First save)
var
  UndoRec: TUndoRec;
  canredo1: Boolean;
begin
  if (Comp <> nil) and ActiveUndo then
  begin
    if IncUndo >= 0 then
    begin
      canredo1 := CanRedo;
      FCurrUndo := FCurrUndo + 1;
      if FCurrUndo >= maxundo then
        FCurrUndo := 0;
      if FFirstUndo = FCurrUndo then
      begin
        FFirstUndo := FFirstUndo + 1;
        if FFirstUndo >= maxundo then
          FFirstUndo := 0;
      end;
      if not canredo1 then
        FLastUndo := FCurrUndo;
      FUndoNr := FUndoNr + IncUndo;
    end;
    UndoRec.Undonr := FUndoNr;
    UndoRec.Action := Action;
    Comp.SaveUndoRec(UndoRec);
    FUndoList[FCurrUndo] := UndoRec;
    if (IncUndo < 0) and CanUndo then
    begin
      FCurrUndo := FCurrUndo - 1;
      if FCurrUndo < 0 then
        FCurrUndo := maxundo;
    end;
  end;
end;

procedure TGrindModel.Undo(Redo: Boolean);
var
  Comp: TGrindComponent;
  UndoRec: TUndoRec;
  n, i: integer;
  // s:string;
begin
  if Redo then
    FCurrUndo := FCurrUndo + 1;
  UndoRec := FUndoList[FCurrUndo];
  n := UndoRec.Undonr;
  { s := format('Before: First=%d, Curr=%d, Last=%d, Curr.Symbol=%s,Curr.Nr=%d' +
    #10, [FFirstUndo, FCurrUndo, FLastUndo, UndoRec.Symbol,
    integer(UndoRec.UndoNr)]);
  }
  Comp := nil;
  i := IndexOf(UndoRec.Symbol);
  if i >= 0 then
    Comp := Components.Items[i];
  FCurrUndo := FCurrUndo - 1;
  if FCurrUndo < 0 then
    FCurrUndo := maxundo;
  Selected := nil;
  if not Redo then
    case UndoRec.Action of
      uaChanged:
        Selected := Comp;
      uaCreated:
        begin
          DeleteComp(Comp);
          Comp := nil;
        end;
      uaDeleted:
        begin
          Comp := AddComponent(UndoRec.X, UndoRec.Y, UndoRec.TheType);
          Comp.SetUndoRec(UndoRec);
          Comp.CheckConnect;
        end;
    end
  else
    case UndoRec.Action of
      uaChanged:
        Selected := Comp;
      uaDeleted:
        begin
          DeleteComp(Comp);
          Comp := nil;
        end;
      uaCreated:
        Comp := AddComponent(UndoRec.X, UndoRec.Y, UndoRec.TheType);
    end;
  if not Redo then
    FCurrUndo := FCurrUndo - 1;
  if FCurrUndo < 0 then
    FCurrUndo := maxundo;
  if Comp <> nil then
  begin
    Comp.SetUndoRec(UndoRec);
    MainForm.TextEdit.Text := Comp.Text;
    MainForm.TextEdit.width :=
      max(70, MainForm.ThePaintBox.Canvas.Textwidth(Comp.Text));
    MainForm.SymbolEdit.Text := Comp.Symbol;
    MainForm.ExpressLongEdit.Text := Comp.Expression;
    if not(Comp.TheType in [ctTextOnly, ctCloud, ctConnector]) then
      MainForm.StatusBar1.SimpleText := Format('%s = ', [Comp.Symbol]);
  end;
  UndoRec := FUndoList[FCurrUndo];
  if UndoRec.Undonr = n then
    Undo(Redo);
  { ShowMessage(format('%sAfter: First=%d, Curr=%d, Last=%d, Curr.Symbol=%s,Curr.nr=%d',
    [s, FFirstUndo, FCurrUndo, FLastUndo, UndoRec.Symbol,
    integer(UndoRec.UndoNr)]));
  }
  UpdateConnections;
  MainForm.UpdatePaintBox;
  UpdateConnections;
  // MainForm.UpdatePaintBox;

end;

procedure TGrindModel.SetActiveUndo(const Value: Boolean);
begin
  FActiveUndo := Value;
  if not Value then
  begin
    FCurrUndo := FFirstUndo;
    FLastUndo := FFirstUndo;
  end;
end;

procedure TGrindModel.DeleteSelection;
var
  AComp: TGrindComponent;
  i: integer;
begin
  AComp := Selected;
  if AComp <> nil then
  begin
    if (AComp.From <> nil) and (AComp.From.TheType = ctCloud) then
      DeleteComp(AComp.From);
    if (AComp.aTo <> nil) and (AComp.aTo.TheType = ctCloud) then
      DeleteComp(AComp.aTo);
    DeleteComp(AComp);
  end;
  if SelectList.count > 1 then
    if (MessageDlg('OK to delete selection?', mtConfirmation, [mbYes, mbNo], 0)
      = idYes) then
    begin
      for i := SelectList.count - 1 downto 0 do
        DeleteComp(SelectList.Items[i], True);
    end;
  MainForm.UpdatePaintBox;
end;

procedure TGrindModel.DeleteIsolatedComponents;
var
  conn: TList;
  Comp: TGrindComponent;
  i: integer;
begin
  for i := Components.count - 1 downto 0 do
  begin
    Comp := TGrindComponent(Components.Items[i]);
    conn := Connectors(Comp, cAll);
    if (conn.count = 0) and ((Comp.From = nil) and (Comp.aTo = nil)) then
      DeleteComp(Comp);
  end;
end;

procedure TGrindModel.SetParsVisible(const Value: Boolean);
var
  i: integer;
begin
  FParsVisible := Value;
  with Components do
    for i := 0 to count - 1 do
      with TGrindComponent(Items[i]) do
        if (TheType = ctParameter) or
          ((From <> nil) and (From.TheType = ctParameter)) then
          Visible := Value;

end;

function TGrindModel.FirstError: TGRindError;
begin
  FCurrErr := 0;
  IsStochastic := False;
  Result := NextError;
end;

function TGrindModel.NextID: integer;
begin
  if Components.count > 0 then
    Result := TGrindComponent(Components.Items[Components.count - 1]).ID + 1
  else
    Result := 0;
end;

procedure TGrindModel.RenameSymbol(old, new: string);
var
  i, P: integer;
  S: string;
  function poswhole(substr, S: string; startat: integer): integer;
  var
    len: integer;
  begin
    len := length(substr);
    for Result := startat to length(S) - len + 1 do
      if (copy(S, Result, len) = substr) and
        ((Result = 1) or not CharInSet(S[Result - 1], ['a' .. 'z', 'A' .. 'Z',
        '0' .. '9', '_'])) and ((Result > length(S) - len) or
        not CharInSet(S[Result + len], ['a' .. 'z', 'A' .. 'Z', '0' .. '9',
        '_'])) then
        exit;
    Result := 0;
  end;

begin
  if old <> new then
  begin
    for i := 0 to Components.count - 1 do
      with TGrindComponent(Components.Items[i]) do
      begin
        S := Expression;
        if S <> '' then
        begin
          P := 1;
          repeat
            P := poswhole(old, S, P);
            if P > 0 then
            begin
              S := copy(S, 1, P - 1) + new + copy(S, P + length(old),
                length(S));
              P := P + length(new);
            end;
          until P = 0;
          Expression := S;
        end;
      end;
    ComponentDlg.RenameSymbolData(old, new);
  end;
end;

procedure TGrindModel.ReorderComponents;
var
  i: integer;
begin
  Components.Sort(CompCompare);
  for i := 0 to Components.count - 1 do
    TGrindComponent(Components.Items[i]).ID := i;
end;

function TGrindModel.ClearParser: TMATLABParser;
begin
  FParser.free;
  FParser := TMATLABParser.Create;
  FParser.Optimize := False;
  Result := FParser;
end;

function TGrindModel.ChangeType(Comp: TGrindComponent; newtype: TComptype):TGrindComponent;
begin
  if newtype <> Comp.TheType then
  begin
    Selected := nil;
    result := ReplaceComp(Comp, newtype);
    result.Assign(Comp);
    DeleteComp(Comp);
    SaveUndo(result, uaCreated, 0);
  end
  else
  result:=nil;
end;

procedure TGrindModel.ExchangeComps(Comp1, Comp2: TGrindComponent);
var
  h, i1, i2: integer;
begin
  i1 := FComponents.IndexOf(Comp1);
  i2 := FComponents.IndexOf(Comp2);
  h := Comp1.ID;
  Comp1.ID := Comp2.ID;
  Comp2.ID := h;
  FComponents.Items[i1] := Comp2;
  FComponents.Items[i2] := Comp1;
end;

{ TGrindComponent }

procedure TGrindComponent.ReadList(AList: TStrings);
var
  i, i1, i2: integer;
  S, s1: string;
  n, thex, they, err: integer;
begin
  i := 0;
  S := '';
  while (i < AList.count - 1) and (AList.Strings[i] <> '%sym=' + Symbol) do
    i := i + 1;
  while (i < AList.count - 1) and (pos('%[', S) <> 1) do
  begin
    S := AList.Strings[i];
    s1 := copy(S, 2, 3);
    if s1 = 'fro' then
      with TheModel do
      begin
        i1 := IndexOf(Symbol);
        i2 := IndexOf(copy(S, 6, length(S)));
        if (i1 >= 0) and (i2 >= 0) then
          Connect(TGrindComponent(Components.Items[i1]),
            TGrindComponent(Components.Items[i2]), True);
      end
    else if s1 = 'to ' then
      with TheModel do
      begin
        i1 := IndexOf(Symbol);
        i2 := IndexOf(copy(S, 6, length(S)));
        if (i1 >= 0) and (i2 >= 0) then
          Connect(TGrindComponent(Components.Items[i1]),
            TGrindComponent(Components.Items[i2]), False);
      end
    else if s1 = 'exp' then
      Expression := copy(S, 6, length(S))
    else if s1 = 'ex2' then
      Expression2 := copy(S, 6, length(S))  //for permanent variables
     else if s1 = 'row' then
    begin
      val(copy(S, 6, length(S)), n, err);
      if TheModel.IsMatrix  then
          NRows := n
       else
          NRows := 1;
    end
    else if s1 = 'col' then
    begin
      val(copy(S, 6, length(S)), n, err);
      if TheModel.IsMatrix  then
         NCols := n
      else
         NCols  :=1;
    end
    else if s1 = 'tex' then
      Text := copy(S, 6, length(S))
    else if s1 = 'hid' then
      Visible := False
    else if s1 = 'hdt' then
      HasData := True
    else if s1 = 'uni' then
      TheUnit := copy(S, 6, length(S))
    else if s1 = 'des' then
      Description := copy(S, 6, length(S))
    else if (s1 = 'frx') then
    begin
      val(copy(S, 6, length(S)), thex, err);
      FromX := TheModel.ZoomCanvas.X2Scrn(thex);
    end
    else if (s1 = 'fry') then
    begin
      val(copy(S, 6, length(S)), they, err);
      FromY := TheModel.ZoomCanvas.Y2Scrn(they);
    end;
    i := i + 1;
  end;
end;

procedure TGrindComponent.CheckConnect;
const
  range = 18;
var
  Comp: TGrindComponent;
begin
  if (From <> nil) then
    TheModel.Connect(self, From, True)
  else
  begin
    with TheModel.ZoomCanvas do
      Comp := TheModel.GetatRect(X2Scrn(FromX - range), Y2Scrn(FromY - range),
        X2Scrn(FromX + range), Y2Scrn(FromY + range), self);
    if Comp <> nil then
      TheModel.Connect(self, Comp, True);
  end;
  if aTo <> nil then
    TheModel.Connect(self, aTo, False)
  else
  begin
    with TheModel.ZoomCanvas do
      Comp := TheModel.GetatRect(X2Scrn(X - range), Y2Scrn(Y - range),
        X2Scrn(X + range), X2Scrn(Y + range), self);
    if (Comp <> nil) and (Comp <> From) then
      TheModel.Connect(self, Comp, False);
  end;
end;

function TGrindComponent.ConnectPoint(fromP: TPoint): TPoint;
var
  dx, dy: integer;
  a, b, R, phi: double;
  x1, y1, x2, Height1, Width1: integer;
  P1: TPoint;
begin
  if TheType in [ctFlow, ctTrain] then
  begin // not very nice;
    P1.X := TFlow(self).ValveX;
    P1.Y := TFlow(self).ValveY;
  end
  else
  begin
    P1.X := X + width div 2;
    P1.Y := Y + height div 2;
  end;
  Result := P1;
  // account for extra space needed to draw vector/matrix var
  if ((NCols > 1) or (NRows > 1)) and (TheModel.IsMatrix) then
  begin
    P1.X := P1.X - 3;
    P1.Y := P1.Y - 5;
    Width1 := width + 5;
    Height1 := height + 4;
  end
  else
  begin
    Width1 := width;
    Height1 := height;
    if TheType in [ctFlow] then
      if not Vertical then
      begin
        Height1 := Height1 + 8;
        Width1 := Width1 - 10;
        P1.Y := P1.Y + 10;
      end
      else
      begin
        Height1 := Height1 - 14;
        Width1 := Width1 + 4;
        P1.X := P1.X + 6;
      end;
    if (TheType in [ctTrain]) and Vertical then
    begin
      Height1 := Height1 - 4;
      Width1 := Width1 + 10;
      P1.X := P1.X + 18;
      P1.Y := P1.Y + 5;
    end;
  end;
  dy := fromP.Y - P1.Y;
  dx := fromP.X - P1.X;
  if dy = 0 then
    dy := 1;
  if dx = 0 then
    dx := 1;
  if FStyle.Shape in [skEllipse] then
  begin
    phi := arctan2(dy, dx);
    R := Height1 * Width1 / 4 / sqrt(sqr(Height1 / 2 * cos(phi)) +
      sqr(Width1 / 2 * sin(phi))) + 2;
    Result := point(P1.X + round(R * cos(phi)), P1.Y + round(R * sin(phi)));
  end
  else if FStyle.Shape in [skRoundRect] then
  begin
    phi := arctan2(dy, dx);
    R := 1.05 * Height1 * Width1 / 4 /
      sqrt(sqr(Height1 / 2 * cos(phi)) + sqr(Width1 / 2 * sin(phi))) + 2;
    Result := point(max(P1.X - Width1 div 2 - 2, min(P1.X + Width1 div 2 + 2,
      P1.X + round(R * cos(phi)))), max(P1.Y - Height1 div 2 - 2,
      min(P1.Y + Height1 div 2 + 2, P1.Y + round(R * sin(phi)))));
  end
  else
  begin
    a := dy / dx;
    b := P1.Y - a * P1.X;
    if dy < 0 then
      y1 := P1.Y - (Height1 div 2) - 2
    else
      y1 := P1.Y + (Height1 div 2) + 2;
    x2 := round((y1 - b) / a);
    if dx < 0 then
    begin
      x1 := P1.X - (Width1 div 2) - 2;
      if x2 > x1 then
        Result := point(x2, y1)
      else
        Result := point(x1, round(a * x1 + b));
    end
    else
    begin
      x1 := P1.X + (Width1 div 2) + 2;
      if x2 < x1 then
        Result := point(x2, y1)
      else
        Result := point(x1, round(a * x1 + b));
    end;
  end;
end;

function TGrindComponent.Down: Boolean;
begin
  Result := False;
end;

procedure TGrindComponent.DragComponent(SX, SY: integer);
var
  Cons: TList;
  Comp, ccomp: TGrindComponent;
  i: integer;
  P: TPoint;
  aX, aY: integer;
begin
  ccomp := TheModel.GetXY(SX, SY, self);
  aX := TheModel.ZoomCanvas.Scrn2X(SX);
  aY := TheModel.ZoomCanvas.Scrn2Y(SY);
  if FDragKind = dUnknown then
    FDragKind := dTo;
  if (FDragPoint.X = 0) and (FDragPoint.Y = 0) then
    FDragPoint := point(aX - X, aY - Y);
  if (FDragKind = dTo) and (aTo = nil) then
  begin
    X := aX - FDragPoint.X;
    Y := aY - FDragPoint.Y;
  end;
  if IsConnector and (ccomp <> nil) and TheModel.CanConnect(self, ccomp, False)
  then
  begin
    MainForm.StatusBar1.SimpleText := Format('Connect to %s?', [ccomp.Symbol]);
    MainForm.ExpressEdit.Visible := False;
    P := ccomp.ConnectPoint(point(X, Y));
    X := P.X;
    Y := P.Y;
  end
  else if pos('Connect to ', MainForm.StatusBar1.SimpleText) > 0 then
  begin
    if not(TheType in [ctTextOnly, ctCloud, ctConnector]) then
      MainForm.ExpressEdit.Visible := True;
    MainForm.StatusBar1.SimpleText := '';
  end;
  Cons := TheModel.Connectors(self, cFrom);
  for i := 0 to Cons.count - 1 do
  begin
    Comp := TGrindComponent(Cons.Items[i]);
    TheModel.Connect(Comp, self, True);
    Comp.CheckConnect;
    Cons := TheModel.Connectors(self, cFrom);
  end;
  Cons := TheModel.Connectors(self, cTo);
  for i := 0 to Cons.count - 1 do
  begin
    Comp := TGrindComponent(Cons.Items[i]);
    TheModel.Connect(Comp, self, False);
    Comp.CheckConnect;
    Cons := TheModel.Connectors(self, cTo);
  end;
  // R := Rect;
end;

function TGrindComponent.GetRect: TRect;
begin
  Result.left := X;
  Result.top := Y;
  Result.right := X + width;
  Result.bottom := Y + height;
end;

function TGrindComponent.HasPoint(aX, aY: integer): Boolean;
const
  diff = 5;
var
  R: TRect;
begin
  R := Rect;
  Result := ((aX > R.left - diff) and (aX < R.right + diff) and
    (aY > R.top - diff) and (aY < R.bottom + diff));
end;

function TGrindComponent.MakeUniqueName(Short: string): string;
var
  i: integer;
begin
  i := 1;
  repeat
    Result := Format('%s%0.2d', [Short, i]);
    i := i + 1;
  until TheModel.IsUnique(Result, nil);
end;

procedure TGrindComponent.Select(Canvas: TZoomCanvas);
var
  w: TSize;
begin
  FDragKind := dUnknown;
  FDragPoint := point(0, 0);
  w := Canvas.TextExtent(Symbol);
  MainForm.TextEdit.Text := Text;
  MainForm.SymbolEdit.Text := Symbol;
  MainForm.SymbolEdit.Color := FStyle.brushcolor;
  MainForm.ExpressLongEdit.Text := Expression;
  if not(TheType in [ctTextOnly, ctCloud, ctConnector]) then
  begin
    MainForm.ExpressEdit.Visible := True;
    MainForm.StatusBar1.SimpleText := Format('%s = ', [Symbol]);
  end;
  // p := point(X, Y);
  // p.y := p.y - Mainform.scrollbox1.VertScrollbar.position;
  // p.x := p.x - Mainform.scrollbox1.HorzScrollbar.position;
  // MainForm.TextEdit.Left := p.X;
  // MainForm.TextEdit.Top := p.Y + Height - w.cy;
  // MainForm.TextEdit.Visible := TheModel.GoEdit;
  // MainForm.SymbolEdit.Left := p.X + ((Width - w.cx) div 2);
  // MainForm.SymbolEdit.Width := w.cx + 7;
  // MainForm.SymbolEdit.Top := p.Y + (Height - 2 * w.cy) div 2;
  // MainForm.SymbolEdit.Visible := TheModel.GoEdit;
  { if MainForm.SymbolEdit.Visible then
    begin
    MainForm.SymbolEdit.setfocus;
    MainForm.SymbolEdit.selectall;
    end; }
  // MainForm.UpdatePaintBox;
end;

procedure TGrindComponent.SetSymbol(Value: string);
var
  List: TList;
  OldSymbol: string;
  i: integer;

  (* function CheckSymbol(Value: string): string;
    var
    i: integer;
    // v: double;
    begin
    Result := Value;
    if Value = 't' then
    Result := InputBox('The symbol "t" is reserved for time',
    'Enter symbol', Value);
    if Value = '' then
    Result := InputBox('Symbol may not be empty', 'Enter symbol', Value);
    // val(Value, v, err);
    if isNumber(Value) then
    Result := InputBox('Symbol may not be a number', 'Change symbol', Value);
    if (length(Result) > 0) and CharInSet(Result[1], ['0' .. '9']) then
    Result[1] := '_';
    for i := 1 to length(Result) do
    if not CharInSet(Result[i], ['a' .. 'z', 'A' .. 'Z', '0' .. '9', '_'])
    then
    Result[i] := '_';
    end; *)

begin
  if Symbol <> Value then
  begin
    OldSymbol := Symbol;
    if SymbolErrorDlg <> nil then
      Value := SymbolErrorDlg.Run(Value, self);
    TheModel.RenameSymbol(FSymbol, Value);
    FSymbol := Value;
    MainForm.SymbolEdit.Text := Value;
    List := TheModel.Connectors(self, cFrom);
    for i := 0 to List.count - 1 do
      with TGrindComponent(List.Items[i]) do
      begin
        FSyntaxError := '';
        if aTo <> nil then
          aTo.FSyntaxError := '';
      end;
  end;
end;

function TGrindComponent.HasError(var aError: TGRindError): Boolean;
var
  conn: TList;
  AList: TList;
  i, j, c, R: integer;
  S: string;
  found: Boolean;
  fcomp: TGrindComponent;
begin
  Result := False;
  aError.errString := '';
  aError.errComp := self;
  aError.errKind := ekOther;
  if (SymbolErrorDlg <> nil) and SymbolErrorDlg.HasError(Symbol, self) then
  begin
    aError.errString := SymbolErrorDlg.ErrorLabel.Caption;
    if not SymbolErrorDlg.CriticalError then
      aError.errKind := ekSymbol;
    Result := True;
    exit;
  end;
  conn := TheModel.Connectors(self, cAll);
  if (conn.count = 0) and ((From = nil) and (aTo = nil)) then
  begin
    Result := True;
    aError.errString := Format(err_isolated_component, [Symbol]);
    aError.errKind := ekDelete;
    exit;
  end;
  if (Expression <> '') then
    if FSyntaxError <> '' then
    begin
      aError.errString := FSyntaxError;
      aError.errKind := ekExpression;
      Result := FSyntaxError <> 'No Error';
    end
    else
    begin
      try
        if pos('[', Expression) = 1 then
        begin
          MatrixDialog.ParseMATLAB(Expression, True);
          if MatrixDialog.LastError <> '' then
          begin
            Result := True;
            aError.errKind := ekExpression;
            aError.errString := Format(err_Parser_error,
              [Symbol, MatrixDialog.LastError]);
          end;
        end;
        AList := TheModel.CompsOfType(ctStatevar);
        for i := 0 to AList.count - 1 do
        begin
          S := Format('%s(t)', [TGrindComponent(AList.Items[i]).Symbol]);
          if pos(S, Expression) > 0 then
          begin
            aError.errKind := ekExpression;
            aError.errString := Format(err_t_notation_not_allowed,
              [Expression, S]);
            Result := True;
            exit;
          end;
        end;
        AList := TList.Create;
        try
          aError.errKind := ekExpression;
          if pos('\n', Expression) = 0 then
          begin
            TheModel.ClearParser.AddExpression
              (TheModel.removet(Expression, False));
            TheModel.Parser.GetGeneratedVars(AList, gVariable);
          end;
          if not(TheType in [ctStatevar, ctParameter]) then
          begin
            S := '';
            conn := TheModel.Connectors(self, cTo);
            for i := 0 to AList.count - 1 do
            begin
              found := (TExprWord(AList.Items[i]).name = Symbol);
              if not found then
                for j := 0 to conn.count - 1 do
                begin
                  fcomp := TGrindComponent(conn.Items[j]).From;
                  if (fcomp <> nil) and
                    (fcomp.TheType in [ctStatevar, ctAuxilvar, ctPermanentvar, ctUserFunction,
                    ctParameter, ctExternvar, ctFlow]) and
                    (fcomp.Symbol = TExprWord(AList.Items[i]).name) then
                    found := True;
                end;
              if not found then
                S := TExprWord(AList.Items[i]).name;
            end;
            if S <> '' then
            begin
              aError.errString := Format(err_symbol_unknown, [Symbol, S]);
              Result := True;
            end;
            TheModel.Parser.GetGeneratedVars(AList, gFunction);
            S := '';
            conn := TheModel.Connectors(self, cTo);
            for i := 0 to AList.count - 1 do
            begin
              found := (TExprWord(AList.Items[i]).name = Symbol);
              if not found then
                for j := 0 to conn.count - 1 do
                begin
                  fcomp := TGrindComponent(conn.Items[j]).From;

                  if (fcomp <> nil) and ((fcomp.TheType in [ctUserFunction]) or
                    (TheModel.IsMatrix and (fcomp.TheType in [ctStatevar,
                    ctParameter]))) and
                  { statevar(i,j) can be interpreted as function }
                    (fcomp.Symbol = TExprWord(AList.Items[i]).name) then
                    found := True;
                end;
              if not found then
                S := TExprWord(AList.Items[i]).name;
            end;
            if S <> '' then
            begin
              aError.errString := Format(err_function_unknown, [Symbol, S]);
              Result := True;
            end;
          end;
          TheModel.Parser.GetGeneratedVars(AList, gAll);
          S := '';
          if not(TheType in [ctStatevar]) then
          begin
            S := '';
            for j := 0 to conn.count - 1 do
            begin
              found := False;
              fcomp := TGrindComponent(conn.Items[j]).From;
              if fcomp <> nil then
                for i := 0 to AList.count - 1 do
                begin
                  if (fcomp <> nil) and
                    (fcomp.TheType in [ctStatevar, ctAuxilvar, ctPermanentvar, ctParameter,
                    ctExternvar, ctUserFunction, ctFlow]) and
                    (fcomp.Symbol = TExprWord(AList.Items[i]).name) then
                    found := True;
                end;
              if not found and (TGrindComponent(conn.Items[j]).From <> nil) then
                S := TGrindComponent(conn.Items[j]).From.Symbol;
            end;
            if S <> '' then
            begin
              aError.errString := Format(war_expr_not_used, [Symbol, S]);
              Result := True;
            end;
          end;
        finally
          AList.free;
        end
      except
        on E: Exception do
        begin
          Result := True;
          aError.errString := Format(err_Parser_error, [Symbol, E.Message]);
        end;
      end;
      if Result then
        FSyntaxError := aError.errString
      else
      begin
        aError.errKind := ekOther;
        FSyntaxError := 'No Error';
      end;
    end;
  if not Result and HasData then
  begin
    { if (ComponentDlg.ExternDatafile = '')and (ComponentDlg.DataCombo.ItemIndex<>0) then
      begin
      aError.errString :=
      Format('No data available for %s - no data file name entered' + #10 +
      'Double click on this component and check the data tab', [Symbol]);
      aError.errKind := ekData;
      Result := True;
      end
      else }
    with ComponentDlg, TotalDataGrid do
    begin
      c := 0;
      while (c < ColCount) and (Cells[c, 0] <> Symbol) do
        inc(c);
      if c >= ColCount then
      begin
        aError.errKind := ekData;
        aError.errString :=
          Format('No data available for %s - no column header with this name' +
          #10 + 'Double click on this component and check the data tab',
          [Symbol]);
        Result := True;
      end
      else
      begin
        found := False;
        R := 0;
        while (R < RowCount) and not found do
        begin
          found := isNumber(Cells[c, R]);
          R := R + 1;
        end;
        if not found then
        begin
          aError.errKind := ekData;
          aError.errString :=
            Format('No data available for %s - the column "%s" has no data' +
            #10 + 'Double click on this component and check the data tab',
            [Symbol, Symbol]);
          Result := True;
        end
      end;
    end;
  end;
end;

procedure TGrindComponent.DrawSelected(Canvas: TZoomCanvas);
var
  R: TRect;
begin
  if TheModel.IsSelected(self) then
  begin
    R := Rect;
    if ((NRows > 1) or (NCols > 1)) and (TheModel.IsMatrix) then
    begin
      R.top := R.top - 5;
      R.left := R.left - 4;
    end;
    R.top := R.top - 4;
    R.left := R.left - 4;
    R.right := R.right + 4;
    R.bottom := R.bottom + 8;
    Canvas.DrawFocusRect(R);
  end;
end;

function TGrindComponent.GetTypeStr: string;
begin
  Result := TheModel.CompData[FTheType].TypeStr;
end;

procedure TGrindComponent.SaveToString(const List: TStrings);
begin
  List.Add(Format('%%[%s;%d;%d]', [TypeStr, X, Y]));
  List.Add('%sym=' + Symbol);
  if Expression <> '' then
    List.Add('%exp=' + Expression);
  if Expression2 <> '' then
    List.Add('%ex2=' + Expression);
  if Text <> '' then
    List.Add('%tex=' + Text);
  if not Visible then
    List.Add('%hid=t');
  if HasData and not(TheType = ctExternvar) then
    List.Add('%hdt=t');
  if TheUnit <> '' then
    List.Add('%uni=' + TheUnit);
  if Description <> '' then
    List.Add('%des=' + Description);
  if From <> nil then
    List.Add('%fro=' + From.Symbol);
  if aTo <> nil then
    List.Add('%to =' + aTo.Symbol);
  if NCols <> 1 then
    List.Add(Format('%%col=%d', [NCols]));
  if NRows <> 1 then
    List.Add(Format('%%row=%d', [NRows]));
  if (FromX <> missingInteger) { and (From = nil) } then
    List.Add(Format('%%frx=%d', [FromX]));
  if (FromY <> missingInteger) { and (aTo = nil) } then
    List.Add(Format('%%fry=%d', [FromY]));
end;

function TGrindComponent.HintText: string;
var
  err: TGRindError;

begin
  Result := Symbol;
  if Expression <> '' then
    Result := Result + ' = ' + ExtractComment(DisplExpression) + ' ' + TheUnit;
  if TheModel.IsMatrix and (NCols + NRows > 2) then
    Result := Format('%s'#10'size: %dx%d', [Result, NRows, NCols]);
  if Description <> '' then
    Result := Result + #10 + Description;
  if (From <> nil) then
  begin
    Result := Result + #10 + From.Symbol + ' --> ';
    if (aTo <> nil) then
      Result := Result + aTo.Symbol
    else
      Result := Result + '[Nothing]';
  end
  else if aTo <> nil then
    Result := Result + #10 + '[Nothing] --> ' + aTo.Symbol;
  if HasError(err) then
  begin
    Result := Result + #10 + err.errString;
    // Application.HintColor := clErrorBk;
    Screen.HintFont.Color := clErrorFont;
  end
  else
    Screen.HintFont.Color := clBlack;

  // Result:=StringReplace(Result,'|','�',[rfReplaceAll]);
end;

procedure TGrindComponent.Draw(Canvas: TZoomCanvas);
const
  clsize = 14;
var
  w: TSize;
  h: integer;
  scal: double;
  txt: string;

  procedure DrawShape(hx, hy: integer);
  begin
    case FStyle.Shape of
      skRectangle:
        Canvas.Rectangle(Classes.Rect(X + hx, Y + hy, X + hx + FStyle.width,
          Y + hy + FStyle.height - h));
      skRoundRect:
        Canvas.RoundRect(X + hx, Y + hy, X + hx + FStyle.width,
          Y + hy + FStyle.height - h, FStyle.width - 10,
          FStyle.height - 10 - h);
      skEllipse:
        Canvas.Ellipse(Classes.Rect(X + hx, Y + hy, X + hx + FStyle.width,
          Y + hy + FStyle.height - h));
      skBar:
        begin
          Canvas.MoveTo(X + hx - 6 + (FStyle.width div 2), Y + hy);
          Canvas.LineTo(X + hx - 6 + (FStyle.width div 2),
            Y + hy + FStyle.height - h);
          Canvas.Circle(X + hx - 6 - 2 + (FStyle.width div 2),
            Y + hy + (FStyle.height - h) div 2, 4);
          FSymbolRect.left := X - 6 + (FStyle.width div 2) + 3;
        end;
      skHbar:
        begin
          FSymbolRect.top := Y + FStyle.height div 10;
          Canvas.MoveTo(X + hx, FSymbolRect.top - 1 + hy);
          Canvas.LineTo(X + hx + FStyle.width, FSymbolRect.top - 1 + hy);
          Canvas.Circle(X + hx + FStyle.width div 2 - 2,
            FSymbolRect.top - 1 - 2 + hy, 4);
        end;
      skCloud:
        begin
          if FStyle.width / (FStyle.height - h) > 20 / 14 then
            scal := (FStyle.height - h) / 14
          else
            scal := FStyle.width / 20;
          Canvas.Circle(X + hx - round(2 * scal), Y + hy - h + round(5 * scal),
            round(clsize * scal));
          Canvas.Circle(X + hx + round((-2 + 10) * scal),
            Y + hy - h + round(5 * scal), round(clsize * scal));
          Canvas.Circle(X + hx + round((-2 + 5) * scal), Y + hy - h,
            round(clsize * scal));
        end;
    end;
  end;

begin
  if Visible then
  begin
    if FStyle.SwapTextSymbol then
      txt := FSymbol
    else
      txt := FText;
    Canvas.Pen.Color := FStyle.pencolor;
    Canvas.Pen.width := FStyle.penwidth;
    Canvas.Pen.style := FStyle.penstyle;
    Canvas.Brush.Color := FStyle.brushcolor;
    Canvas.Brush.style := FStyle.brushstyle;
    w := Canvas.TextExtent(txt);
    if (FStyle.ShowText) and not(FStyle.SwapTextSymbol and not FStyle.ShowSymbol)
    then
    begin
      h := w.cy;
      FTextRect.left := X + ((FStyle.width - w.cx) div 2);
      FTextRect.top := Y + FStyle.height - w.cy;
      FTextRect.right := FTextRect.left + w.cx;
      FTextRect.bottom := FTextRect.top + w.cy;
      Canvas.Brush.style := bsClear;
      Canvas.TextOut(FTextRect.left, FTextRect.top, txt);
      Canvas.Brush.Color := FStyle.brushcolor;
      Canvas.Brush.style := FStyle.brushstyle;
    end
    else
    begin
      FTextRect := emptyRect;
      h := 0;
    end;
    if ((NRows > 1) or (NCols > 1)) and (TheModel.IsMatrix) then
    begin
      DrawShape(-4, -5);
    end;
    DrawShape(0, 0);
    if FStyle.ShowSymbol or (FStyle.SwapTextSymbol and FStyle.ShowText) then
    begin
      if FStyle.SwapTextSymbol then
        txt := FText
      else
        txt := FSymbol;
      w := Canvas.TextExtent(txt);
      if FStyle.Shape <> skBar then
        FSymbolRect.left := X + ((width - w.cx) div 2);
      if FStyle.Shape <> skHbar then
        FSymbolRect.top := Y + (height - w.cy - h) div 2;
      FSymbolRect.right := FSymbolRect.left + w.cx;
      FSymbolRect.bottom := FSymbolRect.top + w.cy;
      Canvas.Brush.style := bsClear;
      Canvas.TextOut(FSymbolRect.left, FSymbolRect.top, txt);
    end
    else
      FSymbolRect := emptyRect;
    Canvas.Pen.Color := clBlack;
    Canvas.Pen.width := 1;
    Canvas.Brush.Color := clWhite;
    Canvas.Brush.style := bsSolid;
    Canvas.Pen.style := psSolid;
  end;
  DrawSelected(Canvas);
end;

function TGrindComponent.HasText(aX, aY: integer; doSelect: Boolean): TEdit;
begin
  Result := nil;
  if not Comparemem(@FSymbolRect, @emptyRect, sizeof(TRect)) then
    if (aX + 3 > FSymbolRect.left) and (aX - 3 < FSymbolRect.right) and
      (aY < FSymbolRect.bottom) and (aY > FSymbolRect.top) then
    begin
      Result := MainForm.SymbolEdit;
      if doSelect then
      begin
        Result.Text := Symbol;
        Result.width := round(TheModel.ZoomCanvas.Zoomfactor *
          (FSymbolRect.right - FSymbolRect.left + 15));
        Result.left := -MainForm.scrollbox1.HorzScrollbar.position +
          TheModel.ZoomCanvas.X2Scrn(FSymbolRect.left);
        Result.top := -MainForm.scrollbox1.VertScrollbar.position +
          TheModel.ZoomCanvas.Y2Scrn(FSymbolRect.top);
        Result.Show;
        Result.setfocus;
        Result.selectall;
        Result.Font.size := round(TheModel.ZoomCanvas.Font.size *
          TheModel.ZoomCanvas.Zoomfactor);
        if MainForm.TextEdit.Visible then
          Text := MainForm.TextEdit.Text;
        MainForm.TextEdit.hide;
      end;
    end;
  if not Comparemem(@FTextRect, @emptyRect, sizeof(TRect)) then
    if (aX > FTextRect.left) and (aX < FTextRect.right) and
      (aY < FTextRect.bottom) and (aY > FTextRect.top) then
    begin
      Result := MainForm.TextEdit;
      if doSelect then
      begin
        MainForm.TextEdit.Text := Text;
        MainForm.TextEdit.width :=
          round(TheModel.ZoomCanvas.Zoomfactor *
          (FTextRect.right - FTextRect.left + 15));
        MainForm.TextEdit.left := -MainForm.scrollbox1.HorzScrollbar.position +
          TheModel.ZoomCanvas.X2Scrn(FTextRect.left);
        MainForm.TextEdit.top := -MainForm.scrollbox1.VertScrollbar.position +
          TheModel.ZoomCanvas.Y2Scrn(FTextRect.top);
        MainForm.TextEdit.Show;
        MainForm.TextEdit.setfocus;
        MainForm.TextEdit.selectall;
        MainForm.TextEdit.Font.size :=
          round(TheModel.ZoomCanvas.Font.size * TheModel.ZoomCanvas.Zoomfactor);
        // round(MainForm.PaintBox1.Font.Size *  TheModel.ZoomCanvas.ZoomFactor);
        MainForm.TextEdit.height := round(21 * TheModel.ZoomCanvas.Zoomfactor);
        if MainForm.SymbolEdit.Visible then
          Symbol := MainForm.SymbolEdit.Text;
        MainForm.SymbolEdit.hide;
      end;
    end;
end;

function TGrindComponent.getHeight: integer;
begin
  Result := FStyle.height;
end;

function TGrindComponent.getWidth: integer;
begin
  Result := FStyle.width;
end;

function TGrindComponent.GetIsconnector: Boolean;
begin
  Result := TheType in [ctFlow, ctTrain, ctConnector];
end;

procedure TGrindComponent.SaveUndoRec(var UndoRec: TUndoRec);
begin
  UndoRec.X := X;
  UndoRec.Y := Y;
  UndoRec.FromX := FromX;
  UndoRec.FromY := FromY;
  if From = nil then
    UndoRec.From := ''
  else
    UndoRec.From := From.Symbol;
  if aTo = nil then
    UndoRec.aTo := ''
  else
    UndoRec.aTo := aTo.Symbol;
  UndoRec.TheType := TheType;
  UndoRec.Text := Text;
  UndoRec.Expression := Expression;
  UndoRec.Description := Description;
  UndoRec.Symbol := Symbol;
  UndoRec.NCols := NCols;
  UndoRec.NRows := NRows;
  UndoRec.TheUnit := TheUnit;
  UndoRec.Visible := Visible;
  UndoRec.HasData := HasData;
  UndoRec.OutsideRange := tsNoCycle;
  UndoRec.ToHelp := point(0, 0);
  UndoRec.FromHelp := point(0, 0);
end;

procedure TGrindComponent.SetUndoRec(UndoRec: TUndoRec);
var
  i: integer;
begin
  X := UndoRec.X;
  Y := UndoRec.Y;
  FromX := UndoRec.FromX;
  FromY := UndoRec.FromY;
  NRows := UndoRec.NRows;
  NCols := UndoRec.NCols;
  From := nil;
  aTo := nil;
  if UndoRec.From <> '' then
  begin
    i := TheModel.IndexOf(UndoRec.From);
    if i >= 0 then
      From := TheModel.Components.Items[i];
  end;
  if UndoRec.aTo <> '' then
  begin
    i := TheModel.IndexOf(UndoRec.aTo);
    if i >= 0 then
      aTo := TheModel.Components.Items[i];
  end;
  Text := UndoRec.Text;
  if TheModel.IsUnique(UndoRec.Symbol, self) then
    Symbol := UndoRec.Symbol;
  Expression := UndoRec.Expression;
  Description := UndoRec.Description;
  HasData := UndoRec.HasData;
  TheUnit := UndoRec.TheUnit;
  Visible := UndoRec.Visible;
end;

procedure TGrindComponent.SetExpression(const Value: string);
var
  List: TList;
  i: integer;
begin
  if FExpression <> Value then
  begin
    FExpression := trim(StringReplace(Value, '...'#13#10, '', [rfReplaceAll]));
    FSyntaxError := '';
    List := TheModel.Connectors(self, cFrom);
    for i := 0 to List.count - 1 do
      with TGrindComponent(List.Items[i]) do
      begin
        FSyntaxError := '';
        if aTo <> nil then
          aTo.FSyntaxError := '';
      end;
  end;
end;

function TGrindComponent.GetExpressionType: string;
begin
  Result := 'Expression';
end;

procedure TGrindComponent.ReverseConnection;
var
  h: integer;
  HC: TGrindComponent;
begin
  h := X;
  X := FromX;
  FromX := h;
  h := Y;
  Y := FromY;
  FromY := h;
  HC := FTo;
  FTo := FFrom;
  FFrom := HC;
end;

function TGrindComponent.right: Boolean;
begin
  Result := False;
end;

function TGrindComponent.GetExpressionFmt: string;
begin
  Result := StringReplace(Expression, '\n', #13#10, [rfReplaceAll]);
end;

procedure TGrindComponent.Assign(source: TGrindComponent);
begin
  FID := source.FID;
  FX := source.FX;
  FY := source.FY;
  FFromX := source.FFromX;
  FFromY := source.FFromY;
  FFrom := source.FFrom;
  FTo := source.FTo;
  FTheModel := source.FTheModel;
  if not(CompareStr(source.FText, TheModel.CompData[source.FTheType].DefaultText) = 0) then
    FText := source.FText;
  // don't change the FText if it is the default text
  FExpression := source.FExpression;
  FExpression2 := source.FExpression2;
  FDescription := source.FDescription;
  FSymbol := source.FSymbol;
  FUnit := source.FUnit;
  FSyntaxError := source.FSyntaxError;
  FDragKind := source.FDragKind;
  FDragPoint := source.FDragPoint;
  FSymbolRect := source.FSymbolRect;
  FTextRect := source.FTextRect;
  FVisible := source.FVisible;
  FHasData := source.FHasData;
  FNCols := source.FNCols;
  FNRows := source.FNRows;
  if not(TheType in [ctStatevar, ctExternvar]) then
    FHasData := False;
  if IsConnector and (FFromX = missingInteger) then
  begin
    FFromX := FX;
    FFromY := FY;
  end;

end;

function TGrindComponent.GetDisplExpression: string;
begin
  Result := ExpressionFmt;
  if pos(',', Result) <> 0 then
    Result := StringReplace(Result, ';', ';'#10, [rfReplaceAll]);
  if length(Result) > 200 then
    Result := copy(Result, 1, 100) + #10'(...)'#10 +
      copy(Result, length(Result) - 100 + 1, 200) + ' (partly shown)';
end;

function TGrindComponent.Vertical: Boolean;
begin
  Result := False;
end;

function TGrindComponent.fromtoStatevar: Boolean;
begin
  Result := (FFrom <> nil) and (FTo <> nil) and (FFrom.TheType = ctStatevar) and
    (FTo.TheType = ctStatevar);
end;

procedure TGrindComponent.SetText(const Value: string);
begin
  FText := Value;
end;

{ TCloud }

const
  clsize = 14;

constructor TCloud.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctCloud;
  FStyle := @TheModel.FStyles[FTheType];
  FSymbol := MakeUniqueName('W');
end;

procedure TCloud.DragComponent(SX, SY: integer);
var
  conn: TList;
  i: integer;
begin
  inherited;
  conn := TheModel.CompsOfType(ctConnector);
  for i := 0 to conn.count - 1 do
    TGrindComponent(conn.Items[i]).CheckConnect;
end;

function TCloud.HintText: string;
begin
  Result := 'Cloud';
  Screen.HintFont.Color := clBlack;
end;

procedure TCloud.Select(Canvas: TZoomCanvas);
begin
  inherited;
  MainForm.TextEdit.Visible := False;
  MainForm.SymbolEdit.Visible := False;
  MainForm.ExpressEdit.Visible := False;
  MainForm.StatusBar1.SimpleText := '';
end;

{ TStatevar }

constructor TStatevar.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctStatevar;
  FStyle := @TheModel.FStyles[FTheType];
  Expression := '0.01';
  FSymbol := MakeUniqueName('X');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

procedure TStatevar.DragComponent(SX, SY: integer);
var
  conn: TList;
  i: integer;
begin
  inherited;
  conn := TheModel.CompsOfType(ctConnector);
  for i := 0 to conn.count - 1 do
    TGrindComponent(conn.Items[i]).CheckConnect;
end;

function TStatevar.HasError(var aError: TGRindError): Boolean;
var
  List: TList;
  found: Boolean;
  i: integer;
begin
  Result := inherited HasError(aError);
  if not Result then
  begin
    if Expression = '' then
    begin
      Result := True;
      aError.errKind := ekExpression;
      aError.errString := Format(err_no_initial_value, [Symbol]);
    end
  end;
  if not Result then
  begin
    List := TheModel.Connectors(self, cAll);
    found := False;
    for i := 0 to List.count - 1 do
      if (TGrindComponent(List.Items[i]).TheType in [ctFlow, ctTrain]) or
        ((TGrindComponent(List.Items[i]).TheType in [ctConnector]) and
        ((TGrindComponent(List.Items[i]).aTo <> nil) and
        (TGrindComponent(List.Items[i]).aTo.TheType = ctModelEquations))) then
      begin
        found := True;
        break;
      end;
    if not found then
    begin
      Result := True;
      aError.errKind := ekDelete;
      aError.errString := Format(err_isolated_statevar, [Symbol]);
    end;
  end;
end;

function TStatevar.EquationText(flowsymbols: Boolean = False): string;
var
  k, j: integer;
  first: Boolean;
  S, siz: string;
  function hasfromcomps(Comp: TGrindComponent): Boolean;
  var
    i: integer;
    fcomp: TGrindComponent;
  begin
    Result := False;
    for i := 0 to TheModel.Components.count - 1 do
      if TGrindComponent(TheModel.Components.Items[i]).From = Comp then
      begin
        fcomp := TGrindComponent(TheModel.Components.Items[i]).From.aTo;
        if (fcomp <> nil) and
          (fcomp.TheType in [ctStatevar, ctAuxilvar, ctPermanentvar, ctParameter, ctExternvar,
          ctFlow]) then
        begin
          Result := True;
          exit;
        end;
      end;
  end;

  function getexpr(Comp: TObject): string;
  begin
    with TGrindComponent(Comp) do
      if flowsymbols or fromtoStatevar or hasfromcomps(TFlow(Comp)) then
        Result := Symbol
      else
      begin
        Result := Expression;
        if pos(';', Result) > 0 then
          Result := copy(Result, 1, pos(';', Result) - 1);
      end;
  end;

begin
  k := 0;
  first := True;
  Result := '';
  for j := 0 to TheModel.Components.count - 1 do
    if ((TGrindComponent(TheModel.Components.Items[j]).From = self) or
      (TGrindComponent(TheModel.Components.Items[j]).aTo = self)) and
      (TGrindComponent(TheModel.Components.Items[j]).TheType
      in [ctFlow, ctTrain]) then
      k := k + 1;
  for j := 0 to TheModel.Components.count - 1 do
    if (TGrindComponent(TheModel.Components.Items[j]).aTo = self) and
      (TGrindComponent(TheModel.Components.Items[j]).TheType
      in [ctFlow, ctTrain]) then
      if k > 1 then
      begin
        if first then
          Result := Format('%s %s',
            [Result, getexpr(TheModel.Components.Items[j])])
        else
          Result := Format('%s +%s',
            [Result, getexpr(TheModel.Components.Items[j])]);
        first := False;
      end
      else
        Result := Format('%s %s',
          [Result, getexpr(TheModel.Components.Items[j])]);
  for j := 0 to TheModel.Components.count - 1 do
    if (TGrindComponent(TheModel.Components.Items[j]).From = self) and
      (TGrindComponent(TheModel.Components.Items[j]).TheType in [ctTrain,
      ctFlow]) then
    begin
      S := getexpr(TheModel.Components.Items[j]);
      if S <> '' then
      begin
        if (pos('-', S) = 0) and (pos('+', S) = 0) then
          Result := Format('%s -%s', [Result, S])
        else
          Result := Format('%s -(%s)', [Result, S]);
      end;
    end;
  Result := trim(Result);
  if (Result <> '') then
  begin
    if TheModel.IsMatrix then
    begin
      if NCols = 1 then
        siz := Format('(1:%d)', [NRows])
      else
        siz := Format('(1:%d,1:%d)', [NRows, NCols]);
    end
    else
      siz := '';
    if (TheModel.ModelKind = mkFlow) then
      Result := Format('%s%s'' = %s', [Symbol, siz, Result])
    else
      Result := Format('%s%s(t+1) = %s',
        [Symbol, siz, TheModel.removet(Result, True)]);
  end;
end;

function TStatevar.HintText: string;
var
  S: string;
  err: TGRindError;
begin
  Result := 'State variable: ' + Symbol + #10;
  if TheModel.IsMatrix and (NCols + NRows > 2) then
    Result := Format('%ssize: %dx%d'#10, [Result, NRows, NCols]);
  if Description <> '' then
    Result := Result + Description + #10;
  if Expression <> '' then
    Result := Result + 'Initial ' + Symbol + ' = ' +
      ExtractComment(DisplExpression) + ' ' + TheUnit;
  S := EquationText;
  if S <> '' then
    Result := Result + #10 + S;

  if HasError(err) then
  begin
    Result := Result + #10 + err.errString;
    // Application.HintColor := clErrorBk;
    Screen.HintFont.Color := clErrorFont;
  end
  else
    Screen.HintFont.Color := clBlack;

  // Result:=StringReplace(Result,'|','�',[rfReplaceAll]);
end;

function TStatevar.GetExpressionType: string;
begin
  Result := 'Initial condition';
end;

{ TConnector }

constructor TConnector.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FromX := X;
  FromY := Y;
  FTheType := ctConnector;
  FStyle := @TheModel.FStyles[FTheType];
  X := FromX + width;
  Y := FromY + height;
  FFromHelpXOff := missingInteger;
  FFromHelpYOff := missingInteger;
  FHelpXOff := missingInteger;
  FHelpYOff := missingInteger;
  FText := TheModel.CompData[FTheType].DefaultText;
  FSymbol := MakeUniqueName('C');
end;

procedure TConnector.DragComponent(SX, SY: integer);
var
  aX, aY: integer;
begin
  aX := TheModel.ZoomCanvas.Scrn2X(SX);
  aY := TheModel.ZoomCanvas.Scrn2Y(SY);

  if MainForm.ConnectorBtn.Down then
    FDragKind := dTo
  else
  begin
    if abs(aX - X) + abs(aY - Y) < 15 then
      FDragKind := dTo
    else if abs(aX - FromX) + abs(aY - FromY) < 7 then
      FDragKind := dFrom
    else if abs(aX - FromHelpX) + abs(aY - FromHelpY) < 7 then
      FDragKind := dHelpFrom
    else if abs(aX - HelpX) + abs(aY - HelpY) < 7 then
      FDragKind := dHelpTo
    else
      FDragKind := dTo;
  end;
  case FDragKind of
    dFrom:
      begin
        FromX := aX;
        FromY := aY;
      end;
    dHelpFrom:
      begin
        FromHelpX := aX;
        FromHelpY := aY;
      end;
    dHelpTo:
      begin
        HelpX := aX;
        HelpY := aY;
      end;
  end;
  inherited DragComponent(SX, SY);
end;

procedure TConnector.Draw(Canvas: TZoomCanvas);
var
  PointArray: TPointArray3;
  { HSL: THsl;
    Col: TColor;
  }

  procedure PlotCircle(X, Y: integer);
  begin
    Canvas.Ellipse(Classes.Rect(X - 2, Y - 2, X + 3, Y + 3));
  end;

begin
  if Visible then
  begin
    Canvas.Pen.Color := FStyle.pencolor;
    Canvas.Pen.width := FStyle.penwidth;
    Canvas.Pen.style := FStyle.penstyle;
    if ((From = nil) or (aTo = nil)) and
      not(TheModel.Dragging and (TheModel.IsSelected(self))) then
    begin
      if Canvas.Pen.Color = clErrConnect then
        Canvas.Pen.Color := clMaroon;
      Canvas.Pen.Color := clErrConnect;
    end;
    Canvas.Brush.Color := FStyle.brushcolor;
    Canvas.Brush.style := FStyle.brushstyle;
    PointArray[0].X := FromX;
    PointArray[0].Y := FromY;
    PointArray[1].X := FromHelpX;
    PointArray[1].Y := FromHelpY;
    PointArray[2].X := HelpX;
    PointArray[2].Y := HelpY;
    PointArray[3].X := X;
    PointArray[3].Y := Y;
    Canvas.PolyBezier(PointArray);
    Canvas.Pen.style := psSolid;
    PlotCircle(FromX, FromY);
    if Comparemem(@PointArray[2], @PointArray[3], sizeof(TPoint)) and
      (From <> nil) and (aTo <> nil) then
      PlotArrowHead(Canvas, point(PointArray[3].X + From.X - aTo.X,
        PointArray[3].Y + From.Y - aTo.Y), PointArray[3])
    else
      PlotArrowHead(Canvas, PointArray[2], PointArray[3]);
    if TheModel.IsSelected(self) then
    begin
      // Canvas.Pen.Width := 1;
      PlotCircle(PointArray[1].X, PointArray[1].Y);
      PlotCircle(PointArray[2].X, PointArray[2].Y);
      Canvas.MoveTo(FromX, FromY);
      Canvas.LineTo(PointArray[1].X, PointArray[1].Y);
      Canvas.MoveTo(X, Y);
      Canvas.LineTo(PointArray[2].X, PointArray[2].Y);
    end;
    Canvas.Pen.Color := clBlack;
    Canvas.Brush.Color := clWhite;
    Canvas.Pen.width := 1;
  end;
end;

function TConnector.HasPoint(aX, aY: integer): Boolean;

  function RoundPoint(x1, y1, aX, aY: integer): Boolean;
  begin
    Result := (aX > x1 - 5) and (aX < x1 + 5) and (aY > y1 - 5) and
      (aY < y1 + 5);
  end;

  function RoundLine(x1, y1, x2, y2, aX, aY: integer): Boolean;
  var
    rc: double;
    b: double;
    y3: double;
  begin
    Result := (aX > min(x1, x2)) and (aX < max(x1, x2));
    if Result then
    begin
      rc := (y2 - y1) / (x2 - x1);
      b := y1 - rc * x1;
      y3 := rc * aX + b;
      Result := abs(y3 - aY) < 3;
    end;
  end;

begin

  Result := RoundLine(FromX, FromY, FromHelpX, FromHelpY, aX, aY) or
    RoundLine(FromHelpX, FromHelpY, HelpX, HelpY, aX, aY) or
    RoundLine(HelpX, HelpY, X, Y, aX, aY) or RoundPoint(HelpX, HelpY, aX, aY) or
    RoundPoint(FromHelpX, FromHelpY, aX, aY);
end;

function TConnector.GetFromHelpX: integer;
var
  arc: double;
begin
  arc := arctan2(Y - FromY, X - FromX);
  arc := arc - 0.2 * pi;
  if FFromHelpXOff = missingInteger then
    Result := FromX + round(0.3 * sqrt(sqr(Y - FromY) + sqr(X - FromX))
      * cos(arc))
  else
    Result := FFromHelpXOff + FromX;
end;

function TConnector.GetFromHelpY: integer;
var
  arc: double;
begin
  arc := arctan2(Y - FromY, X - FromX);
  arc := arc - 0.2 * pi;
  if FFromHelpYOff = missingInteger then
    Result := FromY + round(0.3 * sqrt(sqr(Y - FromY) + sqr(X - FromX))
      * sin(arc))
  else
    Result := FFromHelpYOff + FromY;
end;

function TConnector.GetHelpX: integer;
var
  arc: double;
begin
  arc := arctan2(Y - FromY, X - FromX);
  arc := arc + 0.2 * pi;
  if FHelpXOff = missingInteger then
    Result := X - round(0.3 * sqrt(sqr(Y - FromY) + sqr(X - FromX)) * cos(arc))
  else
    Result := FHelpXOff + X;
end;

function TConnector.GetHelpY: integer;
var
  arc: double;
begin
  arc := arctan2(Y - FromY, X - FromX);
  arc := arc + 0.2 * pi;
  if FHelpYOff = missingInteger then
    Result := Y - round(0.3 * sqrt(sqr(Y - FromY) + sqr(X - FromX)) * sin(arc))
  else
    Result := FHelpYOff + Y;
end;

procedure TConnector.Select(Canvas: TZoomCanvas);
begin
  inherited;
  MainForm.StatusBar1.SimpleText := '';
end;

procedure TConnector.SetFromHelpX(const Value: integer);
begin
  if Value <> missingInteger then
    FFromHelpXOff := Value - FromX
  else
    FFromHelpXOff := missingInteger;
end;

procedure TConnector.SetFromHelpY(const Value: integer);
begin
  if Value <> missingInteger then
    FFromHelpYOff := Value - FromY
  else
    FFromHelpYOff := missingInteger;
end;

procedure TConnector.SetHelpX(const Value: integer);
begin
  if Value <> missingInteger then
    FHelpXOff := Value - X
  else
    FHelpXOff := missingInteger;
end;

procedure TConnector.SetHelpY(const Value: integer);
begin
  if Value <> missingInteger then
    FHelpYOff := Value - Y
  else
    FHelpYOff := missingInteger;
end;

procedure TConnector.SaveToString(const List: TStrings);
begin
  inherited SaveToString(List);
  if FFromHelpXOff <> missingInteger then
  begin
    List.Add(Format('%%FHX=%d', [FFromHelpXOff]));
    List.Add(Format('%%FHY=%d', [FFromHelpYOff]));
  end;
  if FHelpXOff <> missingInteger then
  begin
    List.Add(Format('%%THX=%d', [FHelpXOff]));
    List.Add(Format('%%THY=%d', [FHelpYOff]));
  end;
end;

procedure TConnector.ReadList(AList: TStrings);
var
  i, j: integer;
  S, s1: string;
  err: integer;
begin
  inherited ReadList(AList);
  i := 0;
  S := '';
  while (i < AList.count - 1) and (AList.Strings[i] <> '%sym=' + Symbol) do
    i := i + 1;
  while (i < AList.count - 1) and (pos('[', S) = 0) do
  begin
    S := AList.Strings[i];
    s1 := copy(S, 2, 3);
    if s1 = 'FHX' then
    begin
      val(copy(S, 6, length(S)), j, err);
      FFromHelpXOff := j;
    end
    else if s1 = 'FHY' then
    begin
      val(copy(S, 6, length(S)), j, err);
      FFromHelpYOff := j;
    end
    else if s1 = 'THX' then
    begin
      val(copy(S, 6, length(S)), j, err);
      FHelpXOff := j;
    end
    else if s1 = 'THY' then
    begin
      val(copy(S, 6, length(S)), j, err);
      FHelpYOff := j;
    end;
    i := i + 1;
  end;
end;

function TConnector.GetDefaultCurve: Boolean;
begin
  Result := (FHelpXOff = missingInteger) and (FFromHelpXOff = missingInteger)
end;

procedure TConnector.SetDefaultCurve(const Value: Boolean);
begin
  if Value then
  begin
    HelpX := missingInteger;
    HelpY := missingInteger;
    FromHelpX := missingInteger;
    FromHelpY := missingInteger;
  end;
end;

function TConnector.HintText: string;
var
  err: TGRindError;
begin
  Result := 'Flow of information:'#10;
  if From = nil then
    Result := Result + '[nothing] -->'
  else
    Result := Result + From.Symbol + ' --> ';
  if (aTo <> nil) then
    Result := Result + aTo.Symbol
  else
    Result := Result + '[Nothing]';
  if HasError(err) then
  begin
    Result := Result + #10 + err.errString;
    // Application.HintColor := clErrorBk;
    Screen.HintFont.Color := clErrorFont;

  end
  else
    Screen.HintFont.Color := clBlack;

end;

procedure TConnector.SaveUndoRec(var UndoRec: TUndoRec);
begin
  inherited;
  UndoRec.FromHelp := point(FFromHelpXOff, FFromHelpYOff);
  UndoRec.ToHelp := point(FHelpXOff, FHelpYOff);
end;

procedure TConnector.SetUndoRec(UndoRec: TUndoRec);
begin
  inherited;
  FFromHelpXOff := UndoRec.FromHelp.X;
  FFromHelpYOff := UndoRec.FromHelp.Y;
  FHelpXOff := UndoRec.ToHelp.X;
  FHelpYOff := UndoRec.ToHelp.Y;
end;

function TConnector.HasError(var aError: TGRindError): Boolean;
var
  i, ndel: integer;
begin
  Result := inherited HasError(aError);
  if not Result and (From = nil) or (aTo = nil) then
  begin
    Result := True;
    aError.errKind := ekDelete;
    if (aTo = nil) and (From <> nil) then
      aError.errString := Format(err_connect1, [Symbol])
    else if aTo <> nil then
      aError.errString := Format(err_connect2, [Symbol])
    else
      aError.errString := Format(err_connect3, [Symbol]);
  end;
  ndel := 0;
  if not Result and (From <> nil) and (aTo <> nil) then
    with TheModel do
      for i := Components.count - 1 downto 0 do
        if Components[i] <> self then
          with TGrindComponent(Components[i]) do
            if (TheType = self.TheType) and (From = self.From) and
              (aTo = self.aTo) then
            begin
              DeleteComp(TGrindComponent(Components[i]));
              ndel := ndel + 1;
            end;
  if ndel > 0 then
    SelectDialog.Cleanup;
end;

procedure TConnector.ReverseConnection;
begin
  inherited;
  DefaultCurve := True;
end;

{ TFunction }

constructor TAuxvar.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  // FWidth := 40;
  // FHeight := 40;
  FTheType := ctAuxilvar;
  FStyle := @TheModel.FStyles[FTheType];
  FSymbol := MakeUniqueName('V');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

function TAuxvar.HasError(var aError: TGRindError): Boolean;
begin
  Result := inherited HasError(aError);
  if (not Result) and (Expression = '') then
  begin
    Result := True;
    aError.errKind := ekExpression;
    aError.errString := Format(err_aux_no_equation, [Symbol]);
  end;
end;

function TAuxvar.HintText: string;
begin
  Result := inherited HintText;
  Result := 'Auxiliary variable:' + #10 + Result;
end;

procedure TConnector.Assign(source: TGrindComponent);
begin
  inherited;
  if source is TConnector then
  begin
    FFromHelpYOff := TConnector(source).FFromHelpYOff;
    FFromHelpXOff := TConnector(source).FFromHelpXOff;
    FHelpYOff := TConnector(source).FHelpYOff;
    FHelpXOff := TConnector(source).FHelpXOff;
  end;
end;

{ TParameter }

constructor TParameter.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctParameter;
  FStyle := @TheModel.FStyles[FTheType];
  FSymbol := MakeUniqueName('P');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

function TParameter.GetExpressionType: string;
begin
  Result := 'Value';
end;

function TParameter.HasError(var aError: TGRindError): Boolean;
var
  AList: TList;
  i: integer;
  AnyMatch: Boolean;
begin
  Result := inherited HasError(aError);
  if (not Result) and (Expression = '') then
  begin
    Result := True;
    aError.errKind := ekExpression;
    aError.errString := Format(err_par_no_value, [Symbol]);
  end;
  if (not Result) and (TheModel.IsMatrix) and (NCols > 1) or (NRows > 1) then
  begin

    if not Result then
    begin
      AList := TheModel.CompsOfType(ctStatevar);
      if NCols > 1 then
      begin
        AnyMatch := False;
        with AList do
          for i := 0 to AList.count - 1 do
            if (TGrindComponent(Items[i]).NCols = NCols) or
              (TGrindComponent(Items[i]).NRows = NCols) then
              AnyMatch := True;
        Result := not AnyMatch;
      end;
      if not Result and (NRows > 1) then
      begin
        AnyMatch := False;
        with AList do
          for i := 0 to AList.count - 1 do
            if (TGrindComponent(Items[i]).NCols = NRows) or
              (TGrindComponent(Items[i]).NRows = NRows) then
              AnyMatch := True;
        Result := not AnyMatch;
      end;
      aError.errKind := ekExpression;
      aError.errString := Format(err_par_wrong_size, [Symbol, NRows, NCols]);
    end;
  end;
end;

{ TExternvar }

constructor TExternvar.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  // FDefaultText := 'Externvar';
  FTheType := ctExternvar;
  FStyle := @TheModel.FStyles[FTheType];
  FText := TheModel.CompData[FTheType].DefaultText;
  FSymbol := MakeUniqueName('E');
  FOutsideRange := tsNoCycle;
  FHasData := True;
end;

function TExternvar.HintText: string;
begin
  Result := inherited HintText;
  Result := 'External variable:' + #10 + Result;
end;

function TExternvar.HasError(var aError: TGRindError): Boolean;
begin
  Result := inherited HasError(aError);
  if ((not Result) or (aError.errKind = ekData)) and (Expression = '') then
  begin
    Result := True;
    aError.errKind := ekExpression;
    aError.errString := Format(err_ext_no_default, [Symbol]);
  end;
end;

function TExternvar.getOutsideOption: string;
begin
  case OutsideRange of
    tsNoCycle:
      Result := '-n';
    tsCycle:
      Result := '-c';
    tsFloor:
      Result := '-f';
    tsCycleFloor:
      Result := '-c -f';
  end;
end;

procedure TExternvar.SaveUndoRec(var UndoRec: TUndoRec);
begin
  inherited;
  UndoRec.OutsideRange := FOutsideRange;
end;

procedure TExternvar.SetUndoRec(UndoRec: TUndoRec);
begin
  inherited;
  FOutsideRange := UndoRec.OutsideRange;
end;

function TExternvar.GetExpressionType: string;
begin
  Result := 'Default value';
end;

procedure TExternvar.Assign(source: TGrindComponent);
begin
  inherited;
  if source is TExternvar then
    FOutsideRange := TExternvar(source).FOutsideRange;
  FHasData := True;
end;

{ TFlow }

const
  cc = 10;

constructor TFlow.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  // FWidth := 30;
  // FHeight := 30;
  FromX := X;
  FromY := Y;
  ValveX := X;
  ValveY := Y;
  ValveRelXpos := 0.5;
  ValveRelYpos := 0.5;
  FTheType := ctFlow;
  FStyle := @TheModel.FStyles[FTheType];
  FSymbol := MakeUniqueName('F');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

procedure TFlow.plotValve(Canvas: TZoomCanvas; h3: TPoint; aVertical: Boolean);
var
  w: TSize;
  P: array [0 .. 6] of TPoint;
begin
  Canvas.Pen.Color := FStyle.pencolor2;
  Canvas.Pen.width := FStyle.penwidth;
  Canvas.Pen.style := FStyle.penstyle;
  Canvas.Brush.Color := FStyle.brushcolor;
  Canvas.Brush.style := FStyle.brushstyle;
  if ((From = nil) or (From.TheType = ctCloud)) and
    ((aTo = nil) or (aTo.TheType = ctCloud)) and
    not(TheModel.Dragging and (TheModel.IsSelected(self))) then
  begin
    if Canvas.Pen.Color = clErrConnect then
      Canvas.Pen.Color := clMaroon;
    Canvas.Pen.Color := clErrConnect;
  end;
  if not aVertical then
  begin
    P[0] := point(h3.X - cc, h3.Y - cc);
    P[1] := point(h3.X + cc, h3.Y + cc);
    P[2] := point(h3.X + cc, h3.Y + 2 * cc);
    P[3] := point(h3.X - cc, h3.Y + 2 * cc);
    P[4] := point(h3.X - cc, h3.Y + cc);
    P[5] := point(h3.X + cc, h3.Y - cc);
    P[6] := point(h3.X - cc, h3.Y - cc);
    Canvas.Polygon(P);
    ValveX := h3.X;
    ValveY := h3.Y;
    FVertValve := False;
    begin
      Canvas.Brush.style := bsClear;
      w := Canvas.TextExtent(FText);
      FTextRect.left := h3.X - w.cx div 2;
      FTextRect.top := h3.Y + 2 * cc;
      FTextRect.right := FTextRect.left + w.cx;
      FTextRect.bottom := FTextRect.top + w.cy;
      Canvas.TextOut(FTextRect.left, FTextRect.top, Text);
      Canvas.Brush.style := bsSolid;
    end;
    if TheModel.IsSelected(self) then
    begin
      Canvas.DrawFocusRect(Classes.Rect(ValveX - round(width * 0.5),
        ValveY - round(height * 0.5), ValveX + round(width * 0.5),
        ValveY + round(height * 0.75)));
    end;
  end
  else
  begin
    // Canvas.Pen.Width := 2;
    P[0] := point(h3.X - cc, h3.Y - cc);
    P[1] := point(h3.X + cc, h3.Y + cc);
    P[2] := point(h3.X + 2 * cc, h3.Y + cc);
    P[3] := point(h3.X + 2 * cc, h3.Y - cc);
    P[4] := point(h3.X + cc, h3.Y - cc);
    P[5] := point(h3.X - cc, h3.Y + cc);
    P[6] := point(h3.X - cc, h3.Y - cc);
    Canvas.Polygon(P);
    ValveX := h3.X;
    ValveY := h3.Y;
    FVertValve := True;
    { if (TheModel.Selected = self) and TheModel.GoEdit then
      begin
      MainForm.TextEdit.Left := ValveX + 2 * cc -
      Mainform.scrollbox1.HorzScrollbar.position;
      MainForm.TextEdit.top := ValveY -
      Mainform.scrollbox1.VertScrollbar.position;
      MainForm.TextEdit.Visible := True;
      end
      else
    }
    begin
      Canvas.Brush.style := bsClear;
      w := Canvas.TextExtent(FText);
      FTextRect.left := h3.X + 2 * cc;
      FTextRect.top := h3.Y;
      FTextRect.right := FTextRect.left + w.cx;
      FTextRect.bottom := FTextRect.top + w.cy;
      Canvas.TextOut(FTextRect.left, FTextRect.top, Text);
      Canvas.Brush.style := bsSolid;
    end;
    if TheModel.IsSelected(self) then
    begin
      Canvas.DrawFocusRect(Classes.Rect(ValveX - round(width * 0.45),
        ValveY - round(height * 0.5), ValveX + round(width * 0.85),
        ValveY + round(height * 0.5)));
    end;

  end;
end;

function TFlow.Down: Boolean;
begin
  if not Vertical then
    Result := False
  else
    Result := FromY < Y;
end;

procedure TFlow.Draw(Canvas: TZoomCanvas);
var
  h1, h2: TPoint;

begin
  if Visible then
  begin
    Canvas.Pen.Color := FStyle.pencolor;
    Canvas.Pen.width := FStyle.penwidth;
    Canvas.Pen.style := FStyle.penstyle;
    Canvas.Brush.Color := FStyle.brushcolor;
    Canvas.Brush.style := FStyle.brushstyle;
    if ((From = nil) or (From.TheType = ctCloud)) and
      ((aTo = nil) or (aTo.TheType = ctCloud)) and
      not(TheModel.Dragging and (TheModel.IsSelected(self))) then
    begin
      if Canvas.Pen.Color = clErrConnect then
        Canvas.Pen.Color := clMaroon;
      Canvas.Pen.Color := clErrConnect;
    end;
    // PlotCircle(FromX, FromY);
    Canvas.MoveTo(FromX, FromY);
    h2 := point(X, Y);
    if abs(FromX - X) < abs(FromY - Y) then
    begin
      if abs(FromX - X) < 20 then
      begin
        h1 := point(FromX, FromY);
        Canvas.LineTo(FromX, Y);
        h2.X := FromX;
        plotValve(Canvas,
          point(h1.X, h1.Y + round((h2.Y - h1.Y) * ValveRelYpos)), True);
      end
      else
      begin
        h1 := point(X, FromY + round((Y - FromY) * ValveRelYpos));
        Canvas.LineTo(FromX, h1.Y);
        Canvas.LineTo(h1.X, h1.Y);
        Canvas.LineTo(X, Y);
        plotValve(Canvas, point(FromX + round((X - FromX) * ValveRelXpos),
          h1.Y), False);
      end;
    end
    else
    begin
      if abs(FromY - Y) < 20 then
      begin
        h1 := point(FromX, FromY);
        Canvas.LineTo(X, FromY);
        h2.Y := FromY;
        plotValve(Canvas, point(h1.X + round((h2.X - h1.X) * ValveRelXpos),
          h1.Y), False);
      end
      else
      begin
        h1 := point(FromX + round((X - FromX) * ValveRelXpos), Y);
        Canvas.LineTo(h1.X, FromY);
        Canvas.LineTo(h1.X, h1.Y);
        Canvas.LineTo(X, Y);
        plotValve(Canvas,
          point(h1.X, FromY + round((Y - FromY) * ValveRelYpos)), True);
      end;
    end;
    Canvas.Pen.Color := FStyle.pencolor;
    if ((From = nil) or (From.TheType = ctCloud)) and
      ((aTo = nil) or (aTo.TheType = ctCloud)) and
      not(TheModel.Dragging and (TheModel.IsSelected(self))) then
    begin
      if Canvas.Pen.Color = clErrConnect then
        Canvas.Pen.Color := clMaroon;
      Canvas.Pen.Color := clErrConnect;
    end;
    PlotArrowHead(Canvas, h1, h2);
  end;
  // Canvas.Pen.Color := clBlack;
  // Canvas.Pen.Width := 1;
end;

function TFlow.HasError(var aError: TGRindError): Boolean;
begin
  Result := inherited HasError(aError);
  if Result then
    exit
  else if ((From = nil) or (From.TheType = ctCloud)) and
    ((aTo = nil) or (aTo.TheType = ctCloud)) then
  begin
    Result := True;
    aError.errKind := ekDelete;
    aError.errString := Format(err_flow_isolated, [Symbol]);
  end
  else if (Expression = '') then
  begin
    Result := True;
    aError.errKind := ekExpression;
    aError.errString := Format(err_flow_no_eq, [Symbol]);
  end
  else if (From <> nil) and (From.TheType = ctStatevar) and
    ((Expression <> '') and (Expression[1] = '-')) then
  begin
    Result := True;
    aError.errKind := ekExpression;
    aError.errString := Format(war_double_negative,
      [Symbol, From.Symbol, Expression, Expression]);
  end
  else if (aTo <> nil) and (aTo.TheType = ctStatevar) and
    ((Expression <> '') and (Expression[1] = '-')) then
  begin
    Result := True;
    aError.errKind := ekExpression;
    aError.errString := Format(war_negative_expr,
      [Symbol, aTo.Symbol, Expression]);
  end;

end;

function TFlow.HasPoint(aX, aY: integer): Boolean;

  function RoundPoint(x1, y1, err: integer): Boolean;
  begin
    Result := (aX > x1 - err) and (aX < x1 + err) and (aY > y1 - err) and
      (aY < y1 + err);
  end;

begin
  Result := (HasText(aX, aY, False) <> nil);
  Result := (Result or (RoundPoint(ValveX, GetValveY, 20) or RoundPoint(X, Y,
    5) or RoundPoint(FromX, FromY, 5)));
end;

procedure TFlow.SaveToString(const List: TStrings);
begin
  inherited SaveToString(List);
  List.Add(Format('%%Vax=%d', [ValveX]));
  List.Add(Format('%%Vay=%d', [ValveY]));
  if FVertValve then
    List.Add('$VeV=True')
  else
    List.Add('$VeV=False');
end;

procedure TFlow.ReadList(AList: TStrings);
var
  i: integer;
  S, s1: string;
  thex, they, err: integer;
begin
  inherited ReadList(AList);
  i := 0;
  S := '';
  while (i < AList.count - 1) and (AList.Strings[i] <> '%sym=' + Symbol) do
    i := i + 1;
  while (i < AList.count - 1) and (pos('[', S) = 0) do
  begin
    S := AList.Strings[i];
    s1 := copy(S, 2, 3);
    if s1 = 'Vax' then
    begin
      val(copy(S, 6, length(S)), thex, err);
      ValveX := TheModel.ZoomCanvas.X2Scrn(thex);
    end
    else if s1 = 'Vay' then
    begin
      val(copy(S, 6, length(S)), they, err);
      ValveY := TheModel.ZoomCanvas.Y2Scrn(they);
    end
    else if s1 = 'VaV' then
    begin
      if copy(S, 6, length(S)) = 'True' then
        FVertValve := True
      else
        FVertValve := False;
    end;
    i := i + 1;
  end;
end;

function TFlow.GetValveX: integer;
begin
  Result := FValveXOff + X;
end;

function TFlow.GetValveY: integer;
begin
  Result := FValveYOff + Y;
end;

procedure TFlow.SetValveX(const Value: integer);
begin
  FValveXOff := Value - X;
end;

procedure TFlow.SetValveY(const Value: integer);
begin
  FValveYOff := Value - Y;
end;

function TFlow.HintText: string;
var
  err: TGRindError;
begin
  Result := Symbol;
  if TheUnit <> '' then
    Result := Result + ', ' + TheUnit;
  if TheType = ctTrain then
    Result := Result + #10 + 'Next flow '
  else
    Result := Result + #10 + 'Flow ';
  if (From <> nil) and (From.TheType = ctStatevar) then
    Result := Result + 'from ' + From.Symbol + ' ';
  if (aTo <> nil) and (aTo.TheType = ctStatevar) then
    Result := Result + 'to ' + aTo.Symbol + ' ';
  if Expression <> '' then
    Result := Result + ' = ' + Expression;
  if HasError(err) then
  begin
    Result := Result + #10 + err.errString;
    // Application.HintColor := clErrorBk;
    Screen.HintFont.Color := clErrorFont;
  end
  else
    Screen.HintFont.Color := clBlack;

  // Result:=StringReplace(Result,'|','�',[rfReplaceAll]);
end;

procedure TFlow.Assign(source: TGrindComponent);
begin
  inherited;
  if source is TFlow then
  begin
    FValveYOff := TFlow(source).FValveYOff;
    FValveXOff := TFlow(source).FValveXOff;
    FVertValve := TFlow(source).FVertValve;
  end;
end;

function TFlow.Vertical: Boolean;
begin
  if abs(FromX - X) < abs(FromY - Y) then
    Result := abs(FromX - X) < 20
  else
    Result := not(abs(FromY - Y) < 20);
end;

function TFlow.right: Boolean;
begin
  if Vertical then
    Result := False
  else
    Result := FromX < X;
end;

{ TTrain }

constructor TTrain.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctTrain;
  FStyle := @TheModel.FStyles[FTheType];
  FSymbol := MakeUniqueName('T');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

procedure TTrain.plotValve(Canvas: TZoomCanvas; h3: TPoint; aVertical: Boolean);
const
  c1 = 15;
  c2 = 2;
  c3 = 9;
var
  P: array [0 .. 10] of TPoint;
  w: TSize;
begin
  Canvas.Pen.Color := FStyle.pencolor2;
  Canvas.Pen.width := FStyle.penwidth;
  Canvas.Pen.style := FStyle.penstyle;
  Canvas.Brush.Color := FStyle.brushcolor;
  Canvas.Brush.style := FStyle.brushstyle;
  if ((From = nil) or (From.TheType = ctCloud)) and
    ((aTo = nil) or (aTo.TheType = ctCloud)) and
    not(TheModel.Dragging and (TheModel.IsSelected(self))) then
  begin
    if Canvas.Pen.Color = clErrConnect then
      Canvas.Pen.Color := clMaroon;
    Canvas.Pen.Color := clErrConnect;
  end;
  if not aVertical then
  begin
    if not right then
    begin
      h3.Y := h3.Y - 6;
      P[0] := point(h3.X + c1, h3.Y);
      P[1] := point(h3.X + c1, h3.Y - 2 * c1);
      P[2] := point(h3.X, h3.Y - 2 * c1);
      P[3] := point(h3.X, h3.Y - c1);
      P[4] := point(h3.X - c3, h3.Y - c1);
      P[5] := point(h3.X - c3, h3.Y - 2 * c1 + 2 * c2);
      P[6] := point(h3.X - c1 + 1, h3.Y - 2 * c1 + 2 * c2);
      P[7] := point(h3.X - c1 + 1, h3.Y - c1);
      P[8] := point(h3.X - c1, h3.Y - c1);
      P[9] := point(h3.X - c1, h3.Y);
      P[10] := point(h3.X + c1, h3.Y);
      Canvas.Polygon(P);
      Canvas.Ellipse(Classes.Rect(h3.X - c1 + 2 + 10, h3.Y - 2, h3.X - c1 + 2,
        h3.Y + 10 - 2));
      Canvas.Ellipse(Classes.Rect(h3.X + c1 - 2 - 10, h3.Y - 2, h3.X + c1 - 2,
        h3.Y + 10 - 2));
      Canvas.Brush.Color := clWhite;
      Canvas.Rectangle(Classes.Rect(h3.X + c1 - 3, h3.Y - c1, h3.X + 3,
        h3.Y - 2 * c1 + 3));
      h3.Y := h3.Y - 6;
      ValveX := h3.X;
      ValveY := h3.Y;
    end
    else
    begin
      h3.Y := h3.Y - 6;
      P[0] := point(h3.X - c1, h3.Y);
      P[1] := point(h3.X - c1, h3.Y - 2 * c1);
      P[2] := point(h3.X, h3.Y - 2 * c1);
      P[3] := point(h3.X, h3.Y - c1);
      P[4] := point(h3.X + c3, h3.Y - c1);
      P[5] := point(h3.X + c3, h3.Y - 2 * c1 + 2 * c2);
      P[6] := point(h3.X + c1 - 1, h3.Y - 2 * c1 + 2 * c2);
      P[7] := point(h3.X + c1 - 1, h3.Y - c1);
      P[8] := point(h3.X + c1, h3.Y - c1);
      P[9] := point(h3.X + c1, h3.Y);
      P[10] := point(h3.X - c1, h3.Y);
      Canvas.Polygon(P);
      Canvas.Ellipse(Classes.Rect(h3.X - c1 + 2 + 10, h3.Y - 2, h3.X - c1 + 2,
        h3.Y + 10 - 2));
      Canvas.Ellipse(Classes.Rect(h3.X + c1 - 2 - 10, h3.Y - 2, h3.X + c1 - 2,
        h3.Y + 10 - 2));
      Canvas.Brush.Color := clWhite;
      Canvas.Rectangle(Classes.Rect(h3.X - c1 + 3, h3.Y - c1, h3.X - 3,
        h3.Y - 2 * c1 + 3));
      h3.Y := h3.Y - 6;
      ValveX := h3.X;
      ValveY := h3.Y;
    end;
    FVertValve := False;
    w := Canvas.TextExtent(FText);
    FTextRect.left := h3.X - w.cx div 2;
    FTextRect.top := h3.Y + c1;
    FTextRect.right := FTextRect.left + w.cx;
    FTextRect.bottom := FTextRect.top + w.cy;
    Canvas.TextOut(FTextRect.left, FTextRect.top, Text);
    if TheModel.IsSelected(self) then
      Canvas.DrawFocusRect(Classes.Rect(ValveX - round(width * 0.75),
        ValveY - round(height * 0.6), ValveX + round(width * 0.75),
        ValveY + round(height * 0.6)));
  end
  else
  begin
    if not Down then
    begin
      h3.X := h3.X + 6;
      P[0] := point(h3.X, h3.Y + c1);
      P[1] := point(h3.X + 2 * c1, h3.Y + c1);
      P[2] := point(h3.X + 2 * c1, h3.Y);
      P[3] := point(h3.X + c1, h3.Y);
      P[4] := point(h3.X + c1, h3.Y - c3);
      P[5] := point(h3.X + 2 * c1 - 2 * c2, h3.Y - c3);
      P[6] := point(h3.X + 2 * c1 - 2 * c2, h3.Y - c1 + 1);
      P[7] := point(h3.X + c1, h3.Y - c1 + 1);
      P[8] := point(h3.X + c1, h3.Y - c1);
      P[9] := point(h3.X, h3.Y - c1);
      P[10] := point(h3.X, h3.Y + c1);
      Canvas.Polygon(P);
      Canvas.Ellipse(Classes.Rect(h3.X + 2, h3.Y - c1 + 2 + 10, h3.X - 10 + 2,
        h3.Y - c1 + 2));
      Canvas.Ellipse(Classes.Rect(h3.X + 2, h3.Y + c1 - 2 - 10, h3.X - 10 + 2,
        h3.Y + c1 - 2));
      Canvas.Brush.Color := clWhite;
      Canvas.Rectangle(Classes.Rect(h3.X + c1, h3.Y + c1 - 3, h3.X + 2 * c1 - 3,
        h3.Y + 3));
      h3.X := h3.X - 6;
    end
    else
    begin
      h3.X := h3.X + 6;
      P[0] := point(h3.X, h3.Y - c1);
      P[1] := point(h3.X + 2 * c1, h3.Y - c1);
      P[2] := point(h3.X + 2 * c1, h3.Y);
      P[3] := point(h3.X + c1, h3.Y);
      P[4] := point(h3.X + c1, h3.Y + c3);
      P[5] := point(h3.X + 2 * c1 - 2 * c2, h3.Y + c3);
      P[6] := point(h3.X + 2 * c1 - 2 * c2, h3.Y + c1 - 1);
      P[7] := point(h3.X + c1, h3.Y + c1 - 1);
      P[8] := point(h3.X + c1, h3.Y + c1);
      P[9] := point(h3.X, h3.Y + c1);
      P[10] := point(h3.X, h3.Y - c1);
      Canvas.Polygon(P);
      Canvas.Ellipse(Classes.Rect(h3.X + 2, h3.Y + c1 - 2 - 10, h3.X - 10 + 2,
        h3.Y + c1 - 2));
      Canvas.Ellipse(Classes.Rect(h3.X + 2, h3.Y - c1 + 2 + 10, h3.X - 10 + 2,
        h3.Y - c1 + 2));
      Canvas.Brush.Color := clWhite;
      Canvas.Rectangle(Classes.Rect(h3.X + c1, h3.Y - c1 + 3, h3.X + 2 * c1 - 3,
        h3.Y - 3));
      h3.X := h3.X - 6;
    end;
    ValveX := h3.X;
    ValveY := h3.Y;
    FVertValve := True;
    w := Canvas.TextExtent(FText);
    FTextRect.left := h3.X + 2;
    FTextRect.top := h3.Y + c1 + 2;
    FTextRect.right := FTextRect.left + w.cx;
    FTextRect.bottom := FTextRect.top + w.cy;
    Canvas.TextOut(FTextRect.left, FTextRect.top, Text);
    if TheModel.IsSelected(self) then
      Canvas.DrawFocusRect(Classes.Rect(ValveX - round(height * 0.1),
        ValveY - round(width * 0.75), ValveX + round(height * 0.8),
        ValveY + round(width * 0.6)));
  end;
  Canvas.Pen.Color := clBlack;
end;

{ TZoomCanvas }

procedure TZoomCanvas.Ellipse(R: TRect);
begin
  UpdateCanvas;
  Canvas.Ellipse(Rect(X2Scrn(R.left), Y2Scrn(R.top), X2Scrn(R.right),
    Y2Scrn(R.bottom)));
end;

procedure TZoomCanvas.SetCanvas(const Value: TCanvas);
begin
  FCanvas := Value;
  Pen.Assign(FCanvas.Pen);
  Pen.width := 1;
  Font.Assign(FCanvas.Font);
  Font.size := MainForm.ThePaintBox.Font.size;
  Brush := FCanvas.Brush;
end;

function TZoomCanvas.X2Scrn(X: integer): integer;
begin
  Result := round(Zoomfactor * X) - Origin.X;
end;

function TZoomCanvas.Y2Scrn(Y: integer): integer;
begin
  Result := round(Zoomfactor * Y) - Origin.Y;
end;

function TZoomCanvas.Scrn2X(SX: integer): integer;
begin
  Result := round(SX / Zoomfactor) + Origin.X;
end;

function TZoomCanvas.Scrn2Y(SY: integer): integer;
begin
  Result := round(SY / Zoomfactor) + Origin.Y;
end;

procedure TZoomCanvas.PolyBezier(Points: array of TPoint);
var
  i: integer;
begin
  UpdateCanvas;
  for i := 0 to length(Points) - 1 do
    Points[i] := point(X2Scrn(Points[i].X), Y2Scrn(Points[i].Y));
  Canvas.PolyBezier(Points);
end;

procedure TZoomCanvas.Rectangle(R: TRect);
begin
  UpdateCanvas;
  Canvas.Rectangle(Rect(X2Scrn(R.left), Y2Scrn(R.top), X2Scrn(R.right),
    Y2Scrn(R.bottom)));
end;

procedure TZoomCanvas.RoundRect(x1, y1, x2, y2, x3, y3: integer);
begin
  UpdateCanvas;
  Canvas.RoundRect(X2Scrn(x1), Y2Scrn(y1), X2Scrn(x2), Y2Scrn(y2), X2Scrn(x3),
    Y2Scrn(y3));
end;

procedure TZoomCanvas.TextOut(X, Y: integer; const Text: string);
begin
  UpdateCanvas;
  Canvas.TextOut(X2Scrn(X), Y2Scrn(Y), Text);
end;

function TZoomCanvas.TextExtent(Text: string): TSize;
begin
  UpdateCanvas;
  Result := Canvas.TextExtent(Text);
  Result.cx := Scrn2X(Result.cx) - Origin.X;
  Result.cy := Scrn2Y(Result.cy) - Origin.Y;
end;

procedure TZoomCanvas.DrawFocusRect(R: TRect);
begin
  Canvas.DrawFocusRect(Rect(X2Scrn(R.left), Y2Scrn(R.top), X2Scrn(R.right),
    Y2Scrn(R.bottom)));
end;

procedure TZoomCanvas.Polygon(Points: array of TPoint);
var
  i: integer;
begin
  UpdateCanvas;
  for i := 0 to length(Points) - 1 do
    Points[i] := point(X2Scrn(Points[i].X), Y2Scrn(Points[i].Y));
  Canvas.Polygon(Points);
end;

procedure TZoomCanvas.LineTo(X, Y: integer);
begin
  UpdateCanvas;
  Canvas.LineTo(X2Scrn(X), Y2Scrn(Y));
end;

procedure TZoomCanvas.MoveTo(X, Y: integer);
begin
  Canvas.MoveTo(X2Scrn(X), Y2Scrn(Y));
end;

procedure TZoomCanvas.SetZoomfactor(const Value: double);
begin
  FZoomfactor := Value;
  if FZoomfactor < 0.1 then
    FZoomfactor := 0.1;
  // MainForm.SymbolEdit.Font.Size := round(zoomfactor * 10);
  // MainForm.TextEdit.Font.Size := round(zoomfactor * 10);
end;

constructor TZoomCanvas.Create;
begin
  Pen := TPen.Create;
  Font := TFont.Create;
  FOrigin := point(0, 0);
end;

destructor TZoomCanvas.Destroy;
begin
  inherited;
  Pen.free;
  Font.free;
end;

procedure TZoomCanvas.UpdateCanvas;
begin
  Canvas.Pen.Assign(Pen);
  Canvas.Pen.width := round(Zoomfactor * Pen.width);
  Canvas.Font.Assign(Font);
  Canvas.Font.size := round(Zoomfactor * Font.size);
end;

procedure TZoomCanvas.Circle(X, Y, width: integer);
begin
  UpdateCanvas;
  Canvas.Ellipse(X2Scrn(X), Y2Scrn(Y), X2Scrn(X + width), Y2Scrn(Y + width));
end;

{ TModelEquations }

function TGrindComponent.ExtractParameters: integer;
type
  charset = set of char;
const
  npar = 18;
  // noffs = 130;
var
  List, NamesList, VarsList, ExtList: TMyStringList;
  Comp1: TGrindComponent;
  conn, fcomp: TGrindComponent;
  infunction, found: Boolean;
  ParList, Conns: TList;
  auxvars: string;
  S, s1, s2, nam: string;
  i, i1, i2, j, n, nadded, n1, n2, d, P, P1, p2: integer;

  function poswithout(const substr, S: string; const chars: charset): integer;
  // Pos but without adjacent chars in charset ('=' but not '==','~=' etc)
  // monly the first pos is evaluated
  begin
    Result := pos(substr, S);
    if Result > 0 then
    begin
      if (Result > 1) and CharInSet(S[Result - 1], chars) then
        Result := 0
      else if (Result < length(S)) and CharInSet(S[Result + 1], chars) then
        Result := 0;
    end;
  end;

  function AddComponent1(n, offY: integer; t: TComptype; symb: string)
    : TGrindComponent;
  var
    i, k, XX, aX, aY: integer;
    s2: string;
    AList: TList;
    connected: Boolean;
  begin
    i := TheModel.IndexOf(symb);
    if (i >= 0) and
      ((TGrindComponent(TheModel.Components[i]).TheType = ctUserFunction) and
      (t in [ctStatevar, ctAuxilvar, ctPermanentvar])) then
    begin
      TheModel.ChangeType(TGrindComponent(TheModel.Components[i]), t);
      i := TheModel.IndexOf(symb);
    end;
    if (i < 0) then
    begin
      repeat
        if (FromX <> missingInteger) and (FromX < X) then
          XX := FromX
        else
          XX := X;
        aX := XX + (n mod npar) * (TheModel.Styles[t].width + 10);
        aY := Y + offY + 50 * (n div npar);
        // aX := X+Width div 2+round( cos(2/npar*pi*(n mod npar))*OffY*(1+n div npar));
        // aY := Y-Height div 2+round(sin(2/npar*pi*(n mod npar))*OffY*(1+n div npar));
        n := n + 1;
      until TheModel.GetXY(aX, aY) = nil;
      Result := TheModel.AddComponent(aX, aY, t);
      Result.Symbol := symb;
      k := ModelDlg.IndexOfPar(symb, ModelDlg.Strings);
      if k >= 0 then
      begin
        s2 := ModelDlg.Strings[k];
        P := poswithout('=', s2, ['=', '~', '<', '>']);
        if (P > 0) then
          s2 := copy(s2, P + 1, length(s2));
        if pos('%', s2) > 0 then
          s2 := copy(s2, 1, pos('%', s2) - 1);
        if s2[length(s2)] = ';' then
          s2 := copy(s2, 1, length(s2) - 1);
        Result.Expression := s2;
      end;
      conn := TheModel.AddComponent(Result.X, Result.Y, ctConnector);
      TheModel.Connect(conn, Result, True);
      TheModel.Connect(conn, self, False);
      nadded := nadded + 2;
    end
    else
    begin
      Result := TheModel.Components[i];
      if Result <> self then
      begin
        AList := TheModel.Connectors(Result, cFrom);
        connected := False;
        for i := 0 to AList.count - 1 do
          if (TGrindComponent(AList.Items[i]).aTo = self) or
            (TGrindComponent(AList.Items[i]).From = self) then
            connected := True;
        if not connected then
        begin
          conn := TheModel.AddComponent(Result.X, Result.Y, ctConnector);
          TheModel.Connect(conn, Result, True);
          TheModel.Connect(conn, self, False);
          nadded := nadded + 1;
        end;
      end;
      // Result := nil;
    end;
  end;

begin
  auxvars := ',';
  nadded := 0;
  ModelDlg.GetMemoStrings(ModelDlg.ParamLongMemo);
  List := TMyStringList.Create;
  ParList := TList.Create;
  NamesList := TMyStringList.Create;
  NamesList.sorted := True;
  NamesList.Duplicates := dupIgnore;
  VarsList := TMyStringList.Create;
  VarsList.sorted := True;
  VarsList.Duplicates := dupIgnore;
  ExtList := TMyStringList.Create;
  ExtList.sorted := True;
  ExtList.Duplicates := dupIgnore;
  TheModel.ClearParser.AddExpression('');
  try
    S := StringReplace(Expression, '\n', #10, [rfReplaceAll]);
    S := StringReplace(S, '...'#10, '', [rfReplaceAll]);
    if pos('(t+1)', S) > 0 then
      S := StringReplace(S, '(t)', '', [rfReplaceAll])
    else if pos('(t)', S) > 0 then
      S := StringReplace(S, '(t-1)', '', [rfReplaceAll]);
    List.Text := S;
    infunction := False;
    for i := 0 to List.count - 1 do
    begin
      S := List.Strings[i];
      // s := stringReplace(s, ';', '', [rfReplaceAll]);
      if S[1] <> '%' then
      begin
        P := poswithout('=', S, ['=', '~', '<', '>']);
        // TheModel.Parser.ClearExpressions;
        if pos('function ', S) > 0 then
          infunction := True
        else if (P > 0) then
        begin
          n := 1;
          while not CharInSet(S[n], ['=', '(', '''']) do
            inc(n);
          if CharInSet(S[n], ['(', '''']) then
          begin
            VarsList.Add(trim(copy(S, 1, P)));
            auxvars := auxvars + trim(copy(S, 1, n - 1)) + ',';
          end
          else
            auxvars := auxvars + trim(copy(S, 1, n - 1)) + ',';
          S := copy(S, pos('=', S) + 1, length(S));
        end;
        if pos('defextern ', S) > 0 then
        begin
          s1 := trim(copy(S, 10, length(S)));
          s1 := StringReplace(s1, '''', '', [rfReplaceAll]);
          ExtList.Add(s1);
          auxvars := auxvars + trim(copy(s1, 1, pos(' ', s1) - 1)) + ',';
        end
        else if pos('definepars ', S) > 0 then
        begin
          s1 := trim(copy(S, 11, length(S)));
          if s1[length(s1)] = ';' then
            s1 := copy(s1, 1, length(s1) - 1);
          P := pos(',', s1);
          if P = 0 then
            P := pos(' ', s1);
          while P > 0 do
          begin
            NamesList.Add(copy(s1, 1, P - 1));
            s1 := copy(s1, P + 1, length(s1));
            P := pos(',', s1);
            if P = 0 then
              P := pos(' ', s1);
          end;
          NamesList.Add(s1);
        end
        else if pos('return', S) > 0 then
          infunction := False
        else if not infunction then
        begin
          try
            TheModel.Parser.AddExpressionNoCheck(S);
          except
          end;
          TheModel.Parser.GetGeneratedVars(ParList, gFunction);
          for j := 0 to ParList.count - 1 do
          begin
            NamesList.Add(TExprWord(ParList.Items[j]).name);
            Comp1 := AddComponent1(j, 100, ctUserFunction,
              TExprWord(ParList.Items[j]).name);
            i1 := 0;
            while i1 < List.count do
            begin
              if (pos('%', List.Strings[i1]) <> 1) and
                (pos('function ', List.Strings[i1]) > 0) then
              begin
                s2 := List.Strings[i1];
                P1 := pos('=', s2);
                p2 := pos('(', s2);
                if (P1 > 0) and (p2 > 0) then
                  nam := trim(copy(s2, P1 + 1, p2 - P1 - 1));
                if nam = Comp1.Symbol then
                begin
                  i2 := 1;
                  while (i1 + i2 < List.count) and
                    (pos('return', List.Strings[i1 + i2]) = 0) do
                  begin
                    s2 := s2 + '\n' + List.Strings[i1 + i2];
                    inc(i2);
                  end;
                  s2 := s2 + '\n' + List.Strings[i1 + i2];
                  i1 := i1 + i2;
                  Comp1.Expression := s2;
                end;
              end;
              i1 := i1 + 1;
            end;
          end;
        end;
      end;
    end;
    TheModel.Parser.GetGeneratedVars(ParList, gVariable);
    for j := 0 to ParList.count - 1 do
      NamesList.Add(TExprWord(ParList.Items[j]).name);
    for j := 0 to VarsList.count - 1 do
    begin
      S := VarsList.Strings[j];
      n := 1;
      while (n < length(S)) and not CharInSet(S[n], ['=', '(', '''']) do
        inc(n);
      nam := copy(S, 1, n - 1);
      Comp1 := AddComponent1(j, -80, ctStatevar, nam);
      NamesList.Add(nam);
      n := pos(':', S);
      if (n > 0) and (Comp1 <> nil) then
      begin
        TheModel.IsMatrix := True;
        n1 := n + 1;
        while CharInSet(S[n1], ['0' .. '9']) do
          inc(n1);
        val(copy(S, n + 1, n1 - n - 1), d, n2);
        if (n2 = 0) then
          Comp1.NRows := d;
        S := copy(S, n1, length(S));
        n := pos(':', S);
        if n > 0 then
        begin
          n1 := n + 1;
          while CharInSet(S[n1], ['0' .. '9']) do
            inc(n1);
          val(copy(S, n + 1, n1 - n - 1), d, n2);
          if n2 = 0 then
            Comp1.NCols := d;
        end;
      end
      else
        TheModel.IsMatrix := False;
    end;
    n := 0;
    for j := 0 to NamesList.count - 1 do
      if (pos(',' + NamesList.Strings[j] + ',', auxvars) = 0) then
      begin
        Comp1 := AddComponent1(n, 100, ctParameter, NamesList.Strings[j]);
        if Comp1 <> nil then
          n := n + 1;
      end;
    for j := 0 to ExtList.count - 1 do
    begin
      S := ExtList.Strings[j];
      Comp1 := AddComponent1(n, 100, ctExternvar,
        trim(copy(S, 1, pos(' ', S) - 1)));
      if Comp1 <> nil then
      begin
        n1 := pos('-', S);
        if n1 > 0 then
        begin
          if (pos('-f', S) > 0) and (pos('-c', S) > 0) then
            TExternvar(Comp1).OutsideRange := tsCycleFloor
          else if S[n1 + 1] = 'f' then
            TExternvar(Comp1).OutsideRange := tsFloor
          else if S[n1 + 1] = 'c' then
            TExternvar(Comp1).OutsideRange := tsCycle
          else
            TExternvar(Comp1).OutsideRange := tsNoCycle;
        end;
        S := copy(S, pos(' ', S) + 1, length(S));
        if pos(' ', S) > 0 then
          Comp1.Expression := copy(S, 1, pos(' ', S) - 1)
        else if pos(';', S) > 0 then
          Comp1.Expression := copy(S, 1, pos(';', S) - 1)
        else
          Comp1.Expression := S;
      end;
      n := n + 1;
    end;
    n := ModelDlg.IndexOfPar('loaddata', ModelDlg.Strings);
    if n >= 0 then
    begin
      S := trim(copy(ModelDlg.Strings[n], 9, length(ModelDlg.Strings[n])));
      S := StringReplace(S, '''', '', [rfReplaceAll]);
      if (S <> '') and (S[length(S)] = ';') then
        S := copy(S, 1, length(S) - 1);
      if (S <> '') and (S[1] <> '-') then
        ComponentDlg.ExternDatafile := ExpandFileName(S)
      else
        ComponentDlg.ExternDatafile := '';;
    end;
    TheModel.Selected := nil;
    TheModel.UpdateConnections;
    Conns := TheModel.Connectors(self, cTo);
    if not(TheType in [ctStatevar]) then
    begin
      for j := Conns.count - 1 downto 0 do
      begin
        found := False;
        fcomp := TGrindComponent(Conns.Items[j]).From;
        if fcomp <> nil then
          for i := 0 to NamesList.count - 1 do
          begin
            if (fcomp <> nil) and
              (fcomp.TheType in [ctStatevar, ctAuxilvar, ctPermanentvar, ctParameter, ctExternvar,
              ctUserFunction, ctFlow]) and (fcomp.Symbol = NamesList[i]) then
              found := True;
          end;
        if not found and (fcomp <> nil) then
        begin
          TheModel.DeleteComp(TGrindComponent(Conns.Items[j]));
          Conns := TheModel.Connectors(self, cTo);
        end;
      end;
    end;
    infunction := False;
    S := '';
    for i := List.count - 1 downto 0 do
    begin
      if pos('return', List.Strings[i]) = 1 then
        infunction := True;
      if not infunction then
      begin
        if S = '' then
          S := List.Strings[i]
        else
          S := S + '\n' + List.Strings[i];
      end;
      if pos('function ', List.Strings[i]) = 1 then
        infunction := False;
    end;
    Expression := S;
  finally
    Result := nadded;
    List.free;
    NamesList.free;
    VarsList.free;
    ExtList.free;
    ParList.free;
  end;
end;

constructor TModelEquations.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctModelEquations;
  FModStyle := TheModel.FStyles[ctStatevar];
  with FModStyle do
  begin
    width := 150;
    height := 50;
    pencolor := clGreen;
    penwidth := 3;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skRectangle;
    ShowSymbol := False;
    ShowText := True;
    SwapTextSymbol := True;
  end;
  FStyle := @FModStyle;
  FSymbol := MakeUniqueName('M');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

function TModelEquations.GetExpressionType: string;
begin
  Result := 'Whole model equations';
end;

function TModelEquations.HasError(var aError: TGRindError): Boolean;
begin
  Result := False;
end;

function TModelEquations.HintText: string;
begin
  Result := 'Model equations:'#10 + ExtractComment(ExpressionFmt);
  // Result := stringreplace(Result, '||', ' or ', [rfReplaceAll]);
  // Result := stringreplace(Result, '|', ' or ', [rfReplaceAll]);
  // in Hint text everything after | is removed (placed in the status bar)
end;

{ TUserFunction }

constructor TUserFunction.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctUserFunction;
  FStyle := @TheModel.FStyles[FTheType];
  FSymbol := MakeUniqueName('U');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

function TUserFunction.GetExpressionType: string;
begin
  Result := 'Define MATLAB function';
end;

function TUserFunction.HasError(var aError: TGRindError): Boolean;
begin
  Result := inherited HasError(aError);
  if Expression <> '' then
  begin
    if (pos('function ', Expression) = 0) then
    begin
      Result := True;
      aError.errString := Format(err_User_Function,
        [Symbol, '"function" keyword not found']);
      aError.errKind := ekExpression;
      exit;
    end;
    if (pos('return', Expression) = 0) then
    begin
      Result := True;
      aError.errString := Format(err_User_Function,
        [Symbol, '"return" keyword not found']);
      aError.errKind := ekExpression;
      exit;
    end;
  end;
end;

function TUserFunction.HintText: string;
begin
  inherited HintText;
  if Expression <> '' then
    Result := 'User-defined MATLAB function:' + #10 + ExpressionFmt
  else
    Result := 'External MATLAB function';
end;

{ TMyStringList }

function TMyStringList.Find(const S: string; var Index: integer): Boolean;
var
  L, h, i, c: integer;
begin
  Result := False;
  L := 0;
  h := count - 1;
  while L <= h do
  begin
    i := (L + h) shr 1;
    c := CompareStr(Strings[i], S);
    if c < 0 then
      L := i + 1
    else
    begin
      h := i - 1;
      if c = 0 then
      begin
        Result := True;
        if Duplicates <> dupAccept then
          L := i;
      end;
    end;
  end;
  Index := L;
end;

function StringListCompareStr(List: TStringList;
  Index1, Index2: integer): integer;
begin
  Result := CompareStr(List.Strings[Index1], List.Strings[Index2]);
end;

procedure TMyStringList.Sort;
begin
  CustomSort(StringListCompareStr);
end;

{ TText }

constructor TTextOnly.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctTextOnly;
  FModStyle := TheModel.FStyles[ctStatevar];
  with FModStyle do
  begin
    width := 150;
    height := 17;
    pencolor := clGreen;
    penwidth := 3;
    penstyle := psSolid;
    brushcolor := clWhite;
    brushstyle := bsSolid;
    Shape := skNone;
    ShowSymbol := False;
    ShowText := True;
    SwapTextSymbol := False;
  end;
  FStyle := @FModStyle;
  FSymbol := MakeUniqueName('Q');
  FText := TheModel.CompData[FTheType].DefaultText;
end;

function TTextOnly.HasError(var aError: TGRindError): Boolean;
begin
  Result := False;
end;

function TTextOnly.HintText: string;
begin
  Result := 'Free text';
  Screen.HintFont.Color := clBlack;
end;

procedure TTextOnly.SetText(const Value: string);
begin
  FText := Value;
  FModStyle.width := max(70, MainForm.ThePaintBox.Canvas.Textwidth(Text));
end;

{ TPermanentvar }

constructor TPermanentvar.Create(Owner: TGrindModel; aX, aY: integer);
begin
  inherited;
  FTheType := ctPermanentVar;
  FStyle := @TheModel.FStyles[FTheType];
  FSymbol := MakeUniqueName('M');
  FExpression2:='0';
end;

end.
