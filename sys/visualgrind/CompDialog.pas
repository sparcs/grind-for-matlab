unit CompDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, GrindComps, Grids, ParseExpr, Menus, CompFrm,
  ExtCtrls, MemoLongStrings,
  NiceChart;

type
  TComponentDlg = class(TForm)
    PageControl1: TPageControl;
    CompTab: TTabSheet;
    SymbolLabel1: TLabel;
    TypeLabel: TLabel;
    FromCombo: TComboBox;
    ToCombo: TComboBox;
    Connects: TLabel;
    withLabel: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    ExprLabel: TLabel;
    TabSheet3: TTabSheet;
    ModelMemo: TMemo;
    Label1: TLabel;
    TabSheet4: TTabSheet;
    ParamGrid: TStringGrid;
    DataPopupMenu: TPopupMenu;
    Copy1: TMenuItem;
    DefConnCheck: TCheckBox;
    UnitEdit: TEdit;
    UnitLabel: TLabel;
    SymbolEdit: TEdit;
    eqLabel: TLabel;
    DataCombo: TComboBox;
    DataLabel: TLabel;
    DataFileButton: TButton;
    DataTab: TTabSheet;
    DataGrid: TStringGrid;
    LoadBtn: TBitBtn;
    DataAvailCheck: TCheckBox;
    ParamEdit: TEdit;
    SelectAll: TMenuItem;
    Paste: TMenuItem;
    DataEdit: TEdit;
    BitBtn5: TBitBtn;
    Label2: TLabel;
    Label4: TLabel;
    DataFilterCombo: TComboBox;
    TotalDataGrid: TStringGrid;
    Insertrows1: TMenuItem;
    Deleteselection1: TMenuItem;
    Label3: TLabel;
    EquationsTab: TTabSheet;
    EqsGrid: TStringGrid;
    EqsEdit: TEdit;
    ifLabel: TLabel;
    IfEdit: TEdit;
    ThenLabel: TLabel;
    ThenEdit: TEdit;
    ElseEdit: TEdit;
    ElseLabel: TLabel;
    IfThenElseCheck: TCheckBox;
    RowEdit: TEdit;
    ColEdit: TEdit;
    ColUpDown: TUpDown;
    RowUpDown: TUpDown;
    RowLabel: TLabel;
    Label6: TLabel;
    ColLabel: TLabel;
    VectorCheck: TCheckBox;
    ExpandButton: TButton;
    Btns: TCompFrame;
    DataOpenDialog: TOpenDialog;
    PrintDialog1: TPrintDialog;
    MoveUp: TMenuItem;
    MoveDown: TMenuItem;
    EquationEdit: TMemo;
    VectorCheck2: TCheckBox;
    ExternDefinedCheck: TCheckBox;
    DataPageControl: TPageControl;
    DataTabelTab: TTabSheet;
    DataGraphTab: TTabSheet;
    InsertRows: TMenuItem;
    InsertColumns: TMenuItem;
    Delete1: TMenuItem;
    DeleteRow: TMenuItem;
    DeleteColumn: TMenuItem;
    GraphPopup: TPopupMenu;
    CopyClipboard: TMenuItem;
    DescrEdit: TEdit;
    DescrLabel: TLabel;
    DataFileCombo: TComboBox;
    InitLabel: TLabel;
    InitEdit: TEdit;
    PermanentCheck: TCheckBox;
    UnitButton: TButton;
    EditUnitButton: TButton;
    DataChart: TNiceChart;
    XYLabel: TLabel;
    SelectedLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DataFileButtonClick(Sender: TObject);
    procedure DataAvailCheckClick(Sender: TObject);
    procedure ParamGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ParamGridMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure ParamEditExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SelectAllClick(Sender: TObject);
    procedure PasteClick(Sender: TObject);
    procedure ParamGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure DataGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure DataEditExit(Sender: TObject);
    procedure DataGridMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure LoadBtnClick(Sender: TObject);
    procedure DataTabShow(Sender: TObject);
    procedure DataFilterComboChange(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Deleteselection1Click(Sender: TObject);
    procedure EquationsTabShow(Sender: TObject);
    procedure EqsEditExit(Sender: TObject);
    procedure IfThenElseCheckClick(Sender: TObject);
    procedure CompTabShow(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure IfEditEnter(Sender: TObject);
    procedure VectorCheckClick(Sender: TObject);
    procedure VectorizeButtonClick(Sender: TObject);
    procedure ExpandButtonClick(Sender: TObject);
    procedure DataPopupMenuPopup(Sender: TObject);
    procedure MoveUpClick(Sender: TObject);
    procedure MoveDownClick(Sender: TObject);
    procedure VectorCheck2Click(Sender: TObject);
    procedure ExternDefinedCheckClick(Sender: TObject);
    procedure DataPageControlChange(Sender: TObject);
    procedure DataChartMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure DataTabMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure DataChartMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DataEditClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure InsertRowsClick(Sender: TObject);
    procedure InsertColumnsClick(Sender: TObject);
    procedure DeleteColumnClick(Sender: TObject);
    procedure DeleteRowClick(Sender: TObject);
    procedure Properties1Click(Sender: TObject);
    procedure CopyClipboardClick(Sender: TObject);
    procedure RowEditChange(Sender: TObject);
    procedure PermanentCheckClick(Sender: TObject);
    procedure UnitButtonClick(Sender: TObject);
    procedure BtnsButton1Click(Sender: TObject);
    procedure EditUnitButtonClick(Sender: TObject);
  private
    { Private declarations }
    FDataFilePath: string;
    CurrCol, CurrRow: Integer;
    CurrentSymbol: string;
    UndoTotalDataGrid: string;
    UndoDataGrid: string;
    prevDataFileChanged: Boolean;
    CurrentEdit: TEdit;
    // CurrentIfEdit: TEdit;
    CurrentGrid: TStringGrid;
    EquationLongEdit: TMemoLongStrings;
    minCol, minRow: Integer;
    procedure UpdateParamGrid;
    procedure UpdateEqsGrid;
    procedure MergeDataGrid;
    procedure MoveBy(i: Integer);
    procedure SetExpressionLines(n: Integer);
    procedure SetDataFilePath(const Value: string);
    function GetExternDataFile: string;
    procedure SetExternDataFile(const Value: string);
  public
    TheComp: TGrindComponent;
    procedure SaveDatafile;
    procedure RenameSymbolData(Old, New: string);
    procedure OpenDataFile(aFile: string);
    procedure ReadScheme(const AList: TStrings);
    procedure AddToScheme(const AList: TStrings);
    procedure ClearDatafile;
    procedure UpdateComponent;
    function HasAnyData: Boolean;
    function HasData(Symb: string): Integer;
    procedure deletedata(Symb: string);
    procedure SetComponent(Comp: TGrindComponent);
    property ExternDatafile: string read GetExternDataFile
      write SetExternDataFile;
    property DataFilePath: string read FDataFilePath write SetDataFilePath;
    { Public declarations }
  end;

var
  ComponentDlg: TComponentDlg;

implementation

uses clipbrd, ModelDialog, visgrind, prgTools, MultiLineDlg, DataFileDlg,
  MatrixDlg, UnitDlg, System.UITypes;
{$R *.DFM}

function posstacked(achar: char; s: string): Integer;
// (accounts for nested functions)
var
  stacknr: Integer;
begin
  result := 1;
  stacknr := 0;
  while (result < length(s)) and not((stacknr = 0) and (s[result] = achar)) do
  begin
    if s[result] = '(' then
      inc(stacknr);
    if (s[result] = ')') and (stacknr > 0) then
      dec(stacknr);
    inc(result);
  end;
end;

function hasIIf(s: string): Boolean;
begin
  s := trim(s);
  result := (copy(s, 1, 4) = 'iif(') and
    (posstacked(')', copy(s, 5, length(s))) = length(s) - 4)
end;

{ TComponentDlg }

procedure TComponentDlg.SetComponent(Comp: TGrindComponent);
var
  i: Integer;
  fcomp: TGrindComponent;
  chan: TNotifyEvent;
begin
  IfThenElseCheck.checked := False;
  TheComp := Comp;
  CurrRow := -1;
  CurrCol := -1;
  if TheComp <> nil then
  begin
    DataTab.TabVisible := True;
    CompTab.TabVisible := True;
    EquationLongEdit.Text := TheComp.ExpressionFmt;
    i := EquationEdit.Lines.Count;
    PermanentCheck.Visible := (TheComp.TheType in [ctPermanentVar, ctAuxilvar]);
    InitLabel.Visible := False;
    InitEdit.Visible := False;
    if PermanentCheck.Visible then
    begin
      chan := PermanentCheck.OnClick;
      PermanentCheck.OnClick := nil;
      PermanentCheck.checked := TheComp.TheType = ctPermanentVar;
      PermanentCheck.OnClick := chan;
      InitLabel.Visible := TheComp.TheType = ctPermanentVar;
      InitEdit.Visible := TheComp.TheType = ctPermanentVar;
      InitEdit.Text := TheComp.Expression2;
    end;
    if (i < 4) and (TheComp.TheType in [ctUserFunction, ctModelEquations]) then
      i := 4;
    SetExpressionLines(i);
    UnitEdit.Text := TheComp.TheUnit;
    DescrEdit.Text := TheComp.Description;
    SymbolEdit.Text := TheComp.Symbol;
    TypeLabel.Caption := TheComp.TypeStr;
    FromCombo.Enabled := True;
    ToCombo.Enabled := True;
    FromCombo.Items.Clear;
    ToCombo.Items.Clear;
    for i := 0 to TheComp.TheModel.Components.Count - 1 do
    begin
      fcomp := TGrindComponent(TheComp.TheModel.Components[i]);
      if TheComp.TheModel.CanConnect(TheComp, fcomp, True) or
        (fcomp = TheComp.from) then
        FromCombo.Items.Add(fcomp.Symbol);
      if TheComp.TheModel.CanConnect(TheComp, fcomp, False) or
        (fcomp = TheComp.ato) then
        ToCombo.Items.Add(fcomp.Symbol);
    end;
    FromCombo.Items.Add('[None]');
    ToCombo.Items.Add('[None]');
    if TheComp.from = nil then
      FromCombo.ItemIndex := FromCombo.Items.IndexOf('[None]')
    else
      FromCombo.ItemIndex := FromCombo.Items.IndexOf(TheComp.from.Symbol);
    if TheComp.ato = nil then
      ToCombo.ItemIndex := ToCombo.Items.IndexOf('[None]')
    else
      ToCombo.ItemIndex := ToCombo.Items.IndexOf(TheComp.ato.Symbol);
    if TheComp.TheType in [ctStatevar, ctPermanentVar, ctAuxilvar, ctParameter,
      ctExternvar, ctUserFunction, ctCloud, ctModelEquations] then
    begin
      FromCombo.Enabled := False;
      ToCombo.Enabled := False;
    end;
    // ExtractButton.Visible:=TheComp.TheType=ctModelEquations;
    DataLabel.Visible := TheComp.TheType in [ctExternvar];
    DataCombo.Visible := DataLabel.Visible;
    if DataLabel.Visible then
      DataCombo.ItemIndex := Integer(TExternvar(TheComp).OutsideRange);
    ExprLabel.Visible := not(TheComp.TheType in [ctConnector, ctCloud]);
    EquationEdit.Visible := ExprLabel.Visible;
    ExpandButton.Visible := (ExprLabel.Visible and TheComp.TheModel.IsMatrix and
      not(TheComp.TheType in [ctFlow, ctTrain])) or
      (TheComp.TheType = ctModelEquations);
    IfThenElseCheck.Visible := TheComp.TheType in [ctPermanentVar, ctAuxilvar,
      ctFlow, ctTrain];
    if hasIIf(EquationEdit.Lines.Text) then
    begin
      IfThenElseCheck.checked := True;
      IfThenElseCheckClick(nil);
    end;
    UnitEdit.Visible := ExprLabel.Visible and
      not(TheComp.TheType in [ctUserFunction, ctModelEquations]);
    DescrEdit.Visible := TheComp.TheType in [ctParameter, ctStatevar,
      ctExternvar];
    DescrLabel.Visible := DescrEdit.Visible;
    eqLabel.Visible := UnitEdit.Visible;
    UnitLabel.Visible := UnitEdit.Visible;
    Btns.SetComponent(TheComp);
    VectorCheck.Visible := TheComp.TheType in [ctStatevar, ctParameter,
      ctExternvar];
    VectorCheck2.checked := TheComp.TheModel.IsMatrix;
    ColEdit.Visible := TheComp.TheModel.IsMatrix and
      (TheComp.TheType in [ctStatevar, ctParameter]);
    RowEdit.Visible := ColEdit.Visible;
    ColLabel.Visible := ColEdit.Visible;
    RowLabel.Visible := ColEdit.Visible;
    ColUpDown.Visible := ColEdit.Visible;
    RowUpDown.Visible := ColEdit.Visible;
    ColUpDown.position := TheComp.NCols;
    RowUpDown.position := TheComp.NRows;
    ToCombo.Visible := not(TheComp.TheType in [ctParameter, ctExternvar,
      ctPermanentVar, ctAuxilvar, ctUserFunction, ctStatevar, ctCloud,
      ctModelEquations]);
    FromCombo.Visible := ToCombo.Visible;
    Connects.Visible := ToCombo.Visible;
    withLabel.Visible := ToCombo.Visible;
    ExprLabel.Caption := Format('%s:', [TheComp.ExpressionType]);
    EquationEdit.Width := 490;
    // UnitEdit.Left := 632;
    if TheComp.TheType in [ctStatevar, ctParameter, ctExternvar] then
      EquationEdit.Width := 130;
    UnitEdit.Left := EquationEdit.Left + EquationEdit.Width + 30;
    if DescrEdit.Visible then
    begin
      EquationEdit.Left := SymbolEdit.Left + SymbolEdit.Width + 20;
      DescrEdit.Left := EquationEdit.Left + EquationEdit.Width + 20;
      DescrLabel.Left := DescrEdit.Left;
      eqLabel.Left := SymbolEdit.Left + SymbolEdit.Width + 5;
      // EquationEdit.left := DescrEdit.left + DescrEdit.Width + 20;
      UnitEdit.Left := DescrEdit.Left + 10 + DescrEdit.Width;
      DataCombo.Left := DescrEdit.Left;
      DataLabel.Left := DataCombo.Left;
    end
    else
    begin
      eqLabel.Left := SymbolEdit.Left + SymbolEdit.Width + 7;
      EquationEdit.Left := SymbolEdit.Left + SymbolEdit.Width + 20;
      UnitEdit.Left := EquationEdit.Left + 10 + EquationEdit.Width;
    end;
    ExprLabel.top := SymbolLabel1.top;
    EquationEdit.top := SymbolEdit.top;
    eqLabel.top := SymbolEdit.top;
    DescrEdit.top := EquationEdit.top;
    DescrLabel.top := ExprLabel.top;
    UnitLabel.top := SymbolLabel1.top;
    UnitEdit.top := SymbolEdit.top;
    UnitButton.Left := UnitEdit.Left + UnitEdit.Width + 1;
    UnitButton.top := UnitEdit.top;

    IfEdit.top := SymbolEdit.top;
    ThenEdit.top := SymbolEdit.top + IfEdit.Height + 2;
    ElseEdit.top := ThenEdit.top + IfEdit.Height + 2;
    ifLabel.top := IfEdit.top;
    ThenLabel.top := ThenEdit.top;
    ElseLabel.top := ElseEdit.top;

    ExprLabel.Left := EquationEdit.Left;
    DataAvailCheck.Left := DescrEdit.Left;
    ExpandButton.Left := EquationEdit.Left + EquationEdit.Width + 1;
    ExpandButton.top := EquationEdit.top;
    UnitLabel.Left := UnitEdit.Left;
    DefConnCheck.Visible := (TheComp.TheType = ctConnector) and
      not(TConnector(TheComp).DefaultCurve);
    DefConnCheck.checked := False;
    DataAvailCheck.Visible := TheComp.TheType in [ctStatevar, ctExternvar];
    DataAvailCheck.checked := TheComp.HasData;
    DataAvailCheck.Enabled := TheComp.TheType <> ctExternvar;
    DataTab.TabVisible := DataAvailCheck.checked;
    DataGrid.Cells[0, 0] := 't';
    DataGrid.Cells[1, 0] := TheComp.Symbol;
    if TheComp.TheType = ctUserFunction then
    begin
      ExternDefinedCheck.Visible := True;
      ExternDefinedCheck.checked := (TheComp.Expression = '');
      EquationEdit.Visible := not ExternDefinedCheck.checked;
    end
    else
      ExternDefinedCheck.Visible := False;
  end
  else
  begin
    DataTab.TabVisible := False;
    CompTab.TabVisible := False;
  end;
end;

procedure TComponentDlg.UpdateComponent;
var
  i, p1, p2, nrow, ncol: Integer;
  s: string;
begin
  if TheComp <> nil then
  begin
    if IfThenElseCheck.checked then
    begin
      IfThenElseCheck.checked := False;
      IfThenElseCheck.checked := True;
    end;
    TheComp.TheModel.Selected := TheComp;
    if (TheComp.TheType = ctUserFunction) and (EquationEdit.Lines.Count > 0)
    then
    begin
      if pos('function', EquationEdit.Lines[0]) > 0 then
      begin
        p1 := pos('=', EquationEdit.Lines[0]);
        p2 := pos('(', EquationEdit.Lines[0]);
        if (p1 >= 0) and (p2 > p1) then
          SymbolEdit.Text := trim(copy(EquationEdit.Lines[0], p1 + 1,
            p2 - p1 - 1));
      end;
    end;

    s := '';
    for i := 0 to EquationLongEdit.Count - 1 do
      if (EquationLongEdit.Strings[i] <> '') then
      begin
        if i > 0 then
          s := s + '\n';
        s := s + EquationLongEdit.Strings[i];
      end;
    TheComp.Expression := s;
    TheComp.Expression2 := InitEdit.Text;
    TheComp.TheUnit := UnitEdit.Text;
    TheComp.Description := DescrEdit.Text;
    TheComp.Symbol := SymbolEdit.Text;
    if DataLabel.Visible then
      TExternvar(TheComp).OutsideRange := TOutsideRange(DataCombo.ItemIndex);
    if FromCombo.ItemIndex = FromCombo.Items.IndexOf('[None]') then
      TheComp.from := nil
    else
      TheComp.from := TheComp.TheModel.Components.Items
        [TheComp.TheModel.IndexOf(FromCombo.Items[FromCombo.ItemIndex])];
    if ToCombo.ItemIndex = ToCombo.Items.IndexOf('[None]') then
      TheComp.ato := nil
    else
      TheComp.ato := TheComp.TheModel.Components.Items
        [TheComp.TheModel.IndexOf(ToCombo.Items[ToCombo.ItemIndex])];
    if DefConnCheck.checked then
      TConnector(TheComp).DefaultCurve := True;
    TheComp.HasData := DataAvailCheck.checked;
    TheComp.TheModel.IsMatrix := VectorCheck.checked;
    if TheComp.TheModel.IsMatrix then
    begin
      MatrixDialog.GetColRow(TheComp.Expression, ncol, nrow);
      if ncol > 0 then
        TheComp.NCols := ncol
      else
        TheComp.NCols := ColUpDown.position;
      if nrow > 0 then
        TheComp.NRows := nrow
      else
        TheComp.NRows := RowUpDown.position;
    end;
  end;
end;

procedure TComponentDlg.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  EquationLongEdit := TMemoLongStrings.Create(EquationEdit);
  Btns.CurrentEdit := EquationEdit;
  CurrentSymbol := '-1';
  MoveUp.ShortCut := ShortCut(Vk_Up, [ssCtrl]);
  MoveDown.ShortCut := ShortCut(Vk_Down, [ssCtrl]);
end;

procedure TComponentDlg.FormShow(Sender: TObject);
begin
  Application.HintColor := clInfoBk;
  minCol := -1;
  minRow := -1;
  SelectedLabel.Caption := '';
  DataPageControl.ActivePageIndex := 0;
  if FDataFilePath = '' then
    FDataFilePath := ModelDlg.ThePath;
  prevDataFileChanged := MainForm.GRindModel.DataFileChanged;
  if TheComp <> nil then
  begin
    PageControl1.ActivePageIndex := 0;
    Caption := 'Edit component';
  end
  else
  begin
    PageControl1.ActivePageIndex := 2;
    Caption := 'Parameters/initial values';
  end;
  with TotalDataGrid do
    UndoTotalDataGrid := GridToStr(TotalDataGrid,
      Rect(0, 0, ColCount - 1, RowCount - 1), #9);
  CurrentSymbol := '-1';
end;

procedure TComponentDlg.UpdateParamGrid;
var
  n: Integer;
  Comp: TGrindComponent;
  list: TList;
  i, icomp: Integer;
  myRect: TGridRect;
begin
  with ParamGrid do
  begin
    RowCount := 2;
    Cells[0, 0] := 'Symbol';
    Cells[1, 0] := 'Display text';
    Cells[2, 0] := 'Description';
    Cells[3, 0] := 'Value';
    Cells[4, 0] := 'Unit';
    list := MainForm.GRindModel.CompsOfType(ctParameter);
    n := list.Count;
    list := MainForm.GRindModel.CompsOfType(ctExternvar);
    n := n + list.Count;
    list := MainForm.GRindModel.CompsOfType(ctStatevar);
    list.Sort(CompTextCompare);
    n := n + list.Count;
    RowCount := n + 1;
    icomp := -1;
    for i := 0 to list.Count - 1 do
    begin
      Comp := TGrindComponent(list.Items[i]);
      Objects[0, i + 1] := Comp;
      Cells[0, i + 1] := Comp.Symbol;
      Cells[1, i + 1] := Comp.Text;
      Cells[2, i + 1] := Comp.Description;
      Cells[3, i + 1] := Comp.Expression;
      Cells[4, i + 1] := Comp.TheUnit;
      if (Comp = TheComp) then
        icomp := i + 1;
    end;
    n := list.Count + 1;
    list := MainForm.GRindModel.CompsOfType(ctParameter);
    list.Sort(CompTextCompare);
    for i := 0 to list.Count - 1 do
    begin
      Comp := TGrindComponent(list.Items[i]);
      Objects[0, i + n] := Comp;
      Cells[0, i + n] := Comp.Symbol;
      Cells[1, i + n] := Comp.Text;
      Cells[2, i + n] := Comp.Description;
      Cells[3, i + n] := Comp.Expression;
      Cells[4, i + n] := Comp.TheUnit;
      if (Comp = TheComp) then
        icomp := i + n;
    end;
    n := n + list.Count;
    list := MainForm.GRindModel.CompsOfType(ctExternvar);
    list.Sort(CompTextCompare);
    for i := 0 to list.Count - 1 do
    begin
      Comp := TGrindComponent(list.Items[i]);
      Objects[0, i + n] := Comp;
      Cells[0, i + n] := Comp.Symbol;
      Cells[1, i + n] := Comp.Text;
      Cells[2, i + n] := Comp.Description;
      Cells[3, i + n] := Comp.Expression;
      Cells[4, i + n] := Comp.TheUnit;
      if (Comp = TheComp) then
        icomp := i + n;
    end;
    if icomp > 0 then
    begin
      myRect.Left := 0;
      myRect.top := icomp;
      myRect.Right := 4;
      myRect.Bottom := icomp;
      Selection := myRect;
    end;
  end;
end;

procedure TComponentDlg.UpdateEqsGrid;
var
  n: Integer;
  Comp: TGrindComponent;
  list: TList;
  icomp: Integer;
  myRect: TGridRect;

  function addlist(list: TList; k: Integer): Integer;
  var
    i: Integer;
  begin
    icomp := -1;
    for i := 0 to list.Count - 1 do
      with EqsGrid do
      begin
        Comp := TGrindComponent(list.Items[i]);
        Objects[0, i + k] := Comp;
        if Comp.TheType = ctStatevar then
          Cells[0, i + k] := TStatevar(Comp).EquationText(True)
        else
          Cells[0, i + k] := Format('%s = %s', [Comp.Symbol, Comp.Expression]);
        Cells[1, i + k] := Comp.Text;
        Cells[2, i + k] := Comp.TheUnit;
        if (Comp = TheComp) then
          icomp := i + k;
      end;
    result := k + list.Count;
  end;

begin
  with EqsGrid do
  begin
    RowCount := 2;
    Cells[0, 0] := 'Equation';
    Cells[1, 0] := 'Display text';
    Cells[2, 0] := 'Unit';
    list := MainForm.GRindModel.CompsOfType(ctTrain);
    n := list.Count;
    list := MainForm.GRindModel.CompsOfType(ctAuxilvar);
    n := n + list.Count;
    list := MainForm.GRindModel.CompsOfType(ctPermanentVar);
    n := n + list.Count;
    list := MainForm.GRindModel.CompsOfType(ctFlow);
    n := n + list.Count;
    list := MainForm.GRindModel.CompsOfType(ctStatevar);
    n := n + list.Count;
    RowCount := n + 1;
    n := addlist(list, 1);
    list := MainForm.GRindModel.CompsOfType(ctTrain);
    n := addlist(list, n);
    list := MainForm.GRindModel.CompsOfType(ctFlow);
    n := addlist(list, n);
    list := MainForm.GRindModel.CompsOfType(ctAuxilvar);
    addlist(list, n);
    list := MainForm.GRindModel.CompsOfType(ctPermanentVar);
    addlist(list, n);
    if icomp > 0 then
    begin
      myRect.Left := 0;
      myRect.top := icomp;
      myRect.Right := 4;
      myRect.Bottom := icomp;
      Selection := myRect;
    end;
  end;
end;

procedure TComponentDlg.Copy1Click(Sender: TObject);
begin
  if (CurrentGrid <> nil) and (PageControl1.ActivePage <> DataTab) then
    CopyGridToClipboard(CurrentGrid, True);
end;

procedure TComponentDlg.TabSheet3Show(Sender: TObject);
begin
  CurrentGrid := ParamGrid;
  CurrentEdit := ParamEdit;
  UpdateComponent;
  UpdateParamGrid;
end;

procedure TComponentDlg.FormActivate(Sender: TObject);
begin
  position := poMainFormCenter;
end;

procedure TComponentDlg.DataFileButtonClick(Sender: TObject);
begin
  if DataFileCombo.ItemIndex <> 0 then
    DataOpenDialog.filename := DataFileCombo.Text;
  DataOpenDialog.InitialDir := FDataFilePath;
  if DataOpenDialog.Execute then
  begin
    FDataFilePath := ExtractFileDir(DataOpenDialog.filename);
    if pos('.', DataOpenDialog.filename) = 0 then
      DataOpenDialog.filename := DataOpenDialog.filename + '.txt';
    ExternDatafile := ExtractFileName(DataOpenDialog.filename);
  end;
end;

procedure TComponentDlg.DataAvailCheckClick(Sender: TObject);
begin
  DataTab.TabVisible := DataAvailCheck.checked;
end;

procedure TComponentDlg.ParamGridSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
var
  R: TRect;
begin
  if ACol <> 4 then
    EditUnitButton.Visible := False
  else
  begin
    EditUnitButton.Visible := True;
    R := CurrentGrid.CellRect(ACol, ARow);
    EditUnitButton.top := R.top;
    EditUnitButton.Left := R.Right - EditUnitButton.Width;
  end;

  if (ARow = 0) or ((CurrentGrid = EqsGrid) and (ACol = 0)) then
  begin
    CurrentEdit.Visible := False;
  end
  else
  begin
    CurrentEdit.Text := CurrentGrid.Cells[ACol, ARow];
    R := CurrentGrid.CellRect(ACol, ARow);
    CurrentEdit.top := R.top;
    CurrentEdit.Left := R.Left;
    CurrentEdit.Height := R.Bottom - R.top;
    if ACol = 4 then
      CurrentEdit.Width := R.Right - R.Left - EditUnitButton.Width
    else
      CurrentEdit.Width := R.Right - R.Left;
    CurrentEdit.Visible := True;
    // currentEdit.setfocus;
    CurrentEdit.SelectAll;
    CurrCol := ACol;
    CurrRow := ARow;
  end;
end;

procedure TComponentDlg.ParamGridMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  with Sender as TStringGrid, Selection do
    if (top <> Bottom) or (Left <> Right) then
      CurrentEdit.Visible := False;
end;

procedure TComponentDlg.ParamEditExit(Sender: TObject);
var
  CurrComp: TGrindComponent;
begin
  if ParamEdit.Modified then
  begin
    CurrComp := TGrindComponent(ParamGrid.Objects[0, CurrRow]);
    CurrComp.TheModel.Selected := CurrComp;
    case CurrCol of
      0:
        begin
          CurrComp.Symbol := ParamEdit.Text;
          ParamEdit.Text := CurrComp.Symbol;
          MainForm.SymbolEdit.Text := ParamEdit.Text;
        end; // do not change the symbol here
      1:
        MainForm.TextEdit.Text := ParamEdit.Text;
      2:
        CurrComp.Description := ParamEdit.Text;
      3:
        MainForm.ExpressLongEdit.Text := ParamEdit.Text;
      4:
        CurrComp.TheUnit := ParamEdit.Text;
    end;
    if (CurrComp = TheComp) then
      case CurrCol of
        0:
          SymbolEdit.Text := ParamEdit.Text;
        2:
          DescrEdit.Text := ParamEdit.Text;
        3:
          EquationLongEdit.Text := ParamEdit.Text;
        4:
          UnitEdit.Text := ParamEdit.Text;
      end;
    ParamGrid.Cells[CurrCol, CurrRow] := ParamEdit.Text;
  end;
end;

procedure TComponentDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) then
    case Key of
      Word('C'):
        Copy1Click(Sender);
      Word('A'):
        SelectAllClick(Sender);
      Word('V'):
        PasteClick(Sender);
    end;
  if CurrentEdit <> nil then
  begin
    if (Key in [Vk_Down, Vk_Up, VK_LEFT, VK_RIGHT]) and (CurrentEdit <> nil)
    then
      CurrentEdit.onexit(CurrentEdit);
    with CurrentGrid do
      case Key of
        { VK_DELETE:
          Deleteselection1Click(Sender);
        } Vk_Down:
          if row < RowCount - 1 then
            row := row + 1;
        Vk_Up:
          if row > 0 then
            row := row - 1;
        VK_LEFT:
          if (col > 0) and (not CurrentEdit.Visible or
            (CurrentEdit.sellength = length(CurrentEdit.Text)) or
            (CurrentEdit.selstart = 0)) then
            col := col - 1;
        VK_RIGHT:
          if (col < ColCount - 1) and
            (not CurrentEdit.Visible or (CurrentEdit.sellength = length(Text))
            or (CurrentEdit.selstart = length(CurrentEdit.Text))) then
            col := col + 1;
      end;
  end;
end;

procedure TComponentDlg.SelectAllClick(Sender: TObject);
var
  R: TGridRect;
begin
  R.Left := 0;
  R.Right := CurrentGrid.ColCount - 1;
  R.top := 0;
  R.Bottom := CurrentGrid.RowCount - 1;
  CurrentGrid.Selection := R;
end;

procedure TComponentDlg.PasteClick(Sender: TObject);
begin
  if (CurrentGrid <> nil) then
  begin
    if (PageControl1.ActivePage = DataTab) then
    begin
      DataFileDialog.Paste;
      DataFileDialog.ShowModal;
    end
    else if (PageControl1.ActivePage = EquationsTab) or
      (PageControl1.ActivePage = TabSheet4) and not(CurrentEdit.Visible) then
      PasteClipboardToGrid(CurrentGrid, True)
    else if CurrentEdit.Visible then
      CurrentEdit.Text := Clipboard.Astext;
  end;
end;

procedure TComponentDlg.PermanentCheckClick(Sender: TObject);
var
  newcomp: TGrindComponent;
begin
  if TheComp.TheType in [ctPermanentVar, ctAuxilvar] then
  // This is to be sure that a wrong type is never changed
  begin
    UpdateComponent;
    with MainForm do
    begin
      ExpressLongEdit.Text := TheComp.Expression;
      SymbolEdit.Text := TheComp.Symbol;
      TextEdit.Text := TheComp.Text;
    end;
    // with TheComp  do
    if PermanentCheck.checked then
      newcomp := TheComp.TheModel.ChangeType(TheComp, ctPermanentVar)
    else
      newcomp := TheComp.TheModel.ChangeType(TheComp, ctAuxilvar);
    SetComponent(newcomp);
  end;
end;

procedure TComponentDlg.ParamGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if ARow = 0 then
    with Sender as TStringGrid do
    begin
      Canvas.Brush.Color := FixedColor;
      Canvas.FillRect(Rect);
      Canvas.Pen.Color := clBlack;
      Canvas.Rectangle(Classes.Rect(Rect.Left - 1, Rect.top - 1, Rect.Right + 1,
        Rect.Bottom + 1));
      Canvas.MoveTo(Rect.Left + 1, Rect.Bottom - 1);
      Canvas.Pen.Color := clDkGray;
      Canvas.LineTo(Rect.Right - 1, Rect.Bottom - 1);
      Canvas.LineTo(Rect.Right - 1, Rect.top + 1);
      Canvas.Pen.Color := clWhite;
      Canvas.MoveTo(Rect.Right, Rect.top);
      Canvas.LineTo(Rect.Left, Rect.top);
      Canvas.LineTo(Rect.Left, Rect.Bottom);
      Canvas.TextOut(Rect.Left + 3, Rect.top + 2, Cells[ACol, ARow]);
    end;
end;

procedure TComponentDlg.DataGridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
  R: TRect;
begin
  if ARow = 0 then
    DataEdit.Color := DataGrid.FixedColor
  else
    DataEdit.Color := clWhite;
  DataEdit.Text := DataGrid.Cells[ACol, ARow];
  R := DataGrid.CellRect(ACol, ARow);
  DataEdit.top := R.top;
  DataEdit.Left := R.Left;
  DataEdit.Height := R.Bottom - R.top;
  DataEdit.Width := R.Right - R.Left;
  DataEdit.Visible := True;
  DataEdit.setFocus;
  DataEdit.SelectAll;
  CurrCol := ACol;
  CurrRow := ARow;
end;

procedure TComponentDlg.DataEditExit(Sender: TObject);
var
  s: string;
begin
  if (CurrCol >= 0) then
  begin
    s := DataEdit.Text;
    if CurrRow > 0 then
    begin
      if (not isNumber(s)) and (comparetext(s, 'nan') <> 0) then
      begin
        s := InputBox('Data Edit',
          Format('"%s" is not a valid number, please edit', [s]), s);
        if not isNumber(s) then
          s := 'NaN';
      end;
    end;
    DataGrid.Cells[CurrCol, CurrRow] := s;
  end;
end;

procedure TComponentDlg.DataGridMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  with DataGrid.Selection do
    if (top <> Bottom) or (Left <> Right) then
      DataEdit.Visible := False;
end;

procedure TComponentDlg.MergeDataGrid;
var
  tcol, c, R, r1, c1, n, tOld, cOld, err: Integer;
  s, tim: string;
begin
  if DataEdit.Visible then
  begin
    DataEdit.onexit(nil);
    DataEdit.Visible := False;
  end;
  if CurrentSymbol = '' then
  begin
    for c := 0 to TotalDataGrid.ColCount - 1 do
      for R := 0 to TotalDataGrid.RowCount - 1 do
        TotalDataGrid.Cells[c, R] := '';
    TotalDataGrid.ColCount := DataGrid.ColCount;
    TotalDataGrid.RowCount := DataGrid.RowCount;
    for c := 0 to DataGrid.ColCount - 1 do
      for R := 0 to DataGrid.RowCount - 1 do
        TotalDataGrid.Cells[c, R] := DataGrid.Cells[c, R]
  end
  else if (MainForm.GRindModel.IndexOf(CurrentSymbol) >= 0) then
  begin
    tcol := 0;
    while (tcol < DataGrid.ColCount) and (DataGrid.Cells[tcol, 0] <> 't') do
      inc(tcol);
    if tcol >= DataGrid.ColCount then
    begin
      s := InputBox('Data',
        'Cannot find time in the table, which column is time?', '1');
      val(s, tcol, err);
      if err < 0 then
        exit;
    end;
    c := 0;
    while (c < DataGrid.ColCount) and (DataGrid.Cells[c, 0] <> CurrentSymbol) do
      inc(c);
    if c >= DataGrid.ColCount then
    begin
      s := InputBox('Data',
        Format('Cannot find "%s" in the table, which column is "%s" ?',
        [CurrentSymbol, CurrentSymbol]), ' 1');
      val(s, c, err);
      if err < 0 then
        exit;
    end;
    tOld := 0;
    while (tOld < TotalDataGrid.ColCount) and
      (TotalDataGrid.Cells[tOld, 0] <> 't') do
      inc(tOld);
    if tOld >= TotalDataGrid.ColCount then
    begin
      if TotalDataGrid.ColCount > 1 then
        TotalDataGrid.ColCount := TotalDataGrid.ColCount + 1;
      tOld := TotalDataGrid.ColCount - 1;
      TotalDataGrid.Cells[tOld, 0] := 't';
    end;
    cOld := 0;
    while (cOld < TotalDataGrid.ColCount) and
      (TotalDataGrid.Cells[cOld, 0] <> CurrentSymbol) do
      inc(cOld);
    if cOld >= TotalDataGrid.ColCount then
    begin
      TotalDataGrid.ColCount := TotalDataGrid.ColCount + 1;
      cOld := TotalDataGrid.ColCount - 1;
    end;
    TotalDataGrid.Cols[cOld].Clear;
    TotalDataGrid.Cells[cOld, 0] := CurrentSymbol;
    for R := 0 to DataGrid.RowCount - 1 do
      if isNumber(DataGrid.Cells[tcol, R]) and isNumber(DataGrid.Cells[c, R])
      then
      begin
        tim := DataGrid.Cells[tcol, R];
        r1 := 0;
        while (r1 < TotalDataGrid.RowCount) and
          ((TotalDataGrid.Cells[tOld, r1] <> tim)) do
          inc(r1);
        if r1 >= TotalDataGrid.RowCount then
        begin
          TotalDataGrid.RowCount := TotalDataGrid.RowCount + 1;
          r1 := TotalDataGrid.RowCount - 1;
          for c1 := 0 to TotalDataGrid.ColCount - 1 do
            TotalDataGrid.Cells[c1, r1] := '';
          TotalDataGrid.Cells[tOld, r1] := tim;
        end;
        TotalDataGrid.Cells[cOld, r1] := DataGrid.Cells[c, R];
      end;
    NumSortGrid(TotalDataGrid, tOld, 1);
    n := TotalDataGrid.ColCount;
    with TotalDataGrid do
    begin
      for c := ColCount - 1 downto 0 do
      begin
        R := 1;
        while (R < RowCount) and (Cells[c, R] = '') do
          inc(R);
        if R >= RowCount then
        begin
          dec(n);
          for r1 := 0 to RowCount - 1 do
            for c1 := c + 1 to ColCount - 1 do
              Cells[c1 - 1, r1] := Cells[c1, r1];
        end;
      end;
      for r1 := 0 to RowCount - 1 do
        for c1 := n + 1 to ColCount - 1 do
          Cells[c1, r1] := ''; // Else these values may come back
      ColCount := n;
      if ColCount < 2 then
        ColCount := 2;
      if RowCount < 10 then
        RowCount := 10;
    end;
  end;
  // Make the first row to be time
  tOld := 0;
  while (tOld < TotalDataGrid.ColCount) and
    (TotalDataGrid.Cells[tOld, 0] <> 't') do
    inc(tOld);
  if tOld > 0 then
    with TotalDataGrid do
    begin
      for r1 := 0 to RowCount - 1 do
      begin
        s := Cells[0, r1];
        Cells[0, r1] := Cells[tOld, r1];
        Cells[tOld, r1] := s;
      end;
    end;
end;

procedure TComponentDlg.LoadBtnClick(Sender: TObject);
begin
  DataFileDialog.OpenDialog.InitialDir := FDataFilePath;
  DataFileDialog.Execute;
end;

procedure TComponentDlg.DataTabShow(Sender: TObject);
var
  i: Integer;
  Comp: TGrindComponent;
begin
  CurrentGrid := DataGrid;
  CurrentEdit := DataEdit;
  DataFilterCombo.Items.Clear;
  DataFilterCombo.Items.Add('[No filter]');
  with MainForm.GRindModel do
    for i := 0 to Components.Count - 1 do
    begin
      Comp := TGrindComponent(Components.Items[i]);
      if (Comp = TheComp) or Comp.HasData then
        DataFilterCombo.Items.AddObject(Comp.Symbol + ' only', Comp);
    end;
  DataFilterCombo.ItemIndex := DataFilterCombo.Items.IndexOfObject(TheComp);
  DataFilterCombo.OnChange(nil);
  MainForm.GRindModel.DataFileChanged := True;
end;

procedure TComponentDlg.DataFilterComboChange(Sender: TObject);
var
  c, R, r1, tcol, prevPage: Integer;
  s: string;
begin
  if DataEdit.Visible then
  begin
    DataEdit.onexit(nil);
    DataEdit.Visible := False;
  end;
  prevPage := DataPageControl.ActivePageIndex;
  DataPageControl.ActivePageIndex := 0;
  DataEdit.hide;
  minRow := -1;
  SelectedLabel.Caption := '';
  MergeDataGrid;
  if DataFilterCombo.ItemIndex = 0 then
  begin
    CurrentSymbol := '';
    DataGrid.ColCount := TotalDataGrid.ColCount;
    DataGrid.RowCount := TotalDataGrid.RowCount;
    for c := 0 to TotalDataGrid.ColCount - 1 do
      for R := 0 to TotalDataGrid.RowCount - 1 do
        DataGrid.Cells[c, R] := TotalDataGrid.Cells[c, R]
  end
  else
    with DataFilterCombo, DataGrid do
    begin
      s := TGrindComponent(Items.Objects[ItemIndex]).Symbol;
      CurrentSymbol := s;
      tcol := 0;
      while (tcol < TotalDataGrid.ColCount) and
        (TotalDataGrid.Cells[tcol, 0] <> 't') do
        inc(tcol);
      if tcol > TotalDataGrid.ColCount then
      begin
        TotalDataGrid.ColCount := 1;
        TotalDataGrid.Cells[0, 0] := 't';
        tcol := 0;
      end;
      r1 := 1;
      DataGrid.ColCount := 2;
      DataGrid.RowCount := TotalDataGrid.RowCount;
      Cells[0, 0] := 't';
      Cells[1, 0] := s;
      c := 0;
      while (c < TotalDataGrid.ColCount) and (TotalDataGrid.Cells[c, 0] <> s) do
        inc(c);
      if c < TotalDataGrid.ColCount then
      begin
        for R := 1 to TotalDataGrid.RowCount - 1 do
          if isNumber(TotalDataGrid.Cells[c, R]) and
            isNumber(TotalDataGrid.Cells[tcol, R]) then
          begin
            DataGrid.Cells[0, r1] := TotalDataGrid.Cells[tcol, R];
            DataGrid.Cells[1, r1] := TotalDataGrid.Cells[c, R];
            r1 := r1 + 1;
          end;
        DataGrid.RowCount := r1;
      end
      else
      begin
        DataGrid.ColCount := 2;
        for R := 0 to DataGrid.ColCount - 1 do
          DataGrid.Cols[R].Clear;
        DataGrid.RowCount := 100;
        Cells[0, 0] := 't';
        Cells[1, 0] := s;
      end;
      with DataGrid do
        UndoDataGrid := GridToStr(DataGrid, Rect(0, 0, ColCount - 1,
          RowCount - 1), #9);
    end;
  DataPageControl.ActivePageIndex := prevPage;
  DataPageControl.OnChange(nil);
end;

procedure TComponentDlg.FormHide(Sender: TObject);
var
  c: Integer;
begin
  if (CurrentEdit <> nil) and CurrentEdit.Visible then
    CurrentEdit.onexit(nil);
  if DataTab.TabVisible then
  begin
    if (ModalResult = mrCancel) then
    begin
      MainForm.GRindModel.DataFileChanged := prevDataFileChanged;
      with DataGrid do
        for c := 0 to ColCount - 1 do
          Cols[c].Clear;
      with TotalDataGrid do
        for c := 0 to ColCount - 1 do
          Cols[c].Clear;
      TotalDataGrid.RowCount := 1;
      CurrentSymbol := '-1';
      StrtoGrid(UndoTotalDataGrid, TotalDataGrid, #9);
    end
    else
      MergeDataGrid;
  end;
end;

procedure TComponentDlg.ClearDatafile;
var
  i, j: Integer;
begin
  ExternDatafile := '';
  with TotalDataGrid do
    for i := 0 to ColCount - 1 do
      for j := 0 to RowCount - 1 do
        Cells[i, j] := '';
  TotalDataGrid.RowCount := 0;
  TotalDataGrid.ColCount := 0;
  with DataGrid do
    for i := 0 to ColCount - 1 do
      for j := 0 to RowCount - 1 do
        Cells[i, j] := '';
  DataGrid.RowCount := 100;
  CurrentSymbol := '-1';
  UndoTotalDataGrid := '';
  UndoDataGrid := '';
end;

procedure TComponentDlg.SaveDatafile;
begin
  if ExternDatafile <> '' then
  begin
    SaveBakFile(ExternDatafile);
    GridToFile(TotalDataGrid, ExternDatafile);
  end;
end;

procedure TComponentDlg.ReadScheme(const AList: TStrings);
var
  i, R: Integer;
  s: string;
begin
  i := 0;
  while (i < AList.Count) and (AList.Strings[i] <> '%[data]') do
    i := i + 1;
  if (i < AList.Count) and (AList.Strings[i] = '%[data]') then
  begin
    i := i + 1;
    s := '';
    R := 0;
    while i < AList.Count do
    begin
      s := s + AList.Strings[i] + #10#13;
      R := R + 1;
      i := i + 1;
    end;
    TotalDataGrid.RowCount := R;
    if pos(#9, s) > 0 then
      StrtoGrid(s, TotalDataGrid, #9)
    else
      StrtoGrid(s, TotalDataGrid, ' ');
  end;
end;

procedure TComponentDlg.RenameSymbolData(Old, New: string);
var
  i: Integer;
begin
  with TotalDataGrid do
    for i := 1 to ColCount do
      if Cells[i, 0] = Old then
        Cells[i, 0] := New;
end;

procedure TComponentDlg.RowEditChange(Sender: TObject);
var
  Table: TDynArray;
  s, s1, aval, aval1: string;
  i, j, ColCount, RowCount: Integer;
  allTheSame, iseye: Boolean;
begin
  if EquationLongEdit.Text <> '' then
  begin
    s := MatrixDialog.SetColRow(EquationLongEdit.Text, RowUpDown.position,
      ColUpDown.position, Table);
    if (s = '') and (length(Table) > 0) then
    begin
      allTheSame := True;
      iseye := True;
      aval1 := FloatToStr(Table[0, 0]);
      ColCount := length(Table[0]);
      RowCount := length(Table);
      for i := 0 to RowCount - 1 do
      begin
        s1 := '';
        for j := 0 to ColCount - 1 do
        begin
          aval := FloatToStr(Table[i, j]);
          if ((i = j) and (CompareStr(aval, '1') <> 0)) or
            ((i <> j) and (CompareStr(aval, '0') <> 0)) then
            iseye := False;
          if (i + j > 0) and (aval <> aval1) then
            allTheSame := False;

          s1 := s1 + FloatToStr(Table[i, j]) + ',';
        end;
        if s1[length(s1)] = ',' then
          s1 := copy(s1, 1, length(s1) - 1);
        s := s + s1 + ';';
      end;
      if s[length(s)] = ';' then
        s := copy(s, 1, length(s) - 1);
      if (RowCount = 1) and (ColCount = 1) then
        s := aval1
      else if allTheSame then
        if CompareStr(aval1, '0') = 0 then
          s := Format('zeros(%d,%d)', [RowCount, ColCount])
        else if CompareStr(aval1, '1') = 0 then
          s := Format('ones(%d,%d)', [RowCount, ColCount])
        else
          s := Format('%s+zeros(%d,%d)', [aval1, RowCount, ColCount])
      else if iseye then
        s := Format('eye(%d,%d)', [RowCount, ColCount])
      else
        s := '[' + s + ']';
    end;
    EquationLongEdit.Text := s;
  end;

  {
    begin
    MatrixDialog.GrindComponent := TheComp;
    if comparetext(MatrixDialog.ExpressionLongMemo.Text,
    EquationLongEdit.Text) <> 0 then
    begin
    MatrixDialog.ExpressionLongMemo.Text := EquationLongEdit.Text;
    MatrixDialog.ColUpDown.position := ColUpDown.position;
    MatrixDialog.RowUpDown.position := RowUpDown.position;
    MatrixDialog.RowEdit.OnChange(nil);
    EquationLongEdit.Text := MatrixDialog.ExpressionLongMemo.Text;
    end;
    end; }
end;

procedure TComponentDlg.Deleteselection1Click(Sender: TObject);
var
  R, c: Integer;
begin
  with CurrentGrid, Selection do
    if (Right - Left > 0) or (Bottom - top > 0) then
    begin
      with CurrentEdit do
      begin
        onexit(Sender);
        hide;
      end;
      for c := Selection.Left to Selection.Right do
        for R := Selection.top to Selection.Bottom do
          Cells[c, R] := '';
    end;
end;

procedure TComponentDlg.EquationsTabShow(Sender: TObject);
begin
  CurrentGrid := EqsGrid;
  CurrentEdit := EqsEdit;
  Btns.CurrentEdit := EqsEdit;
  UpdateComponent;
  UpdateEqsGrid;
end;

procedure TComponentDlg.EditUnitButtonClick(Sender: TObject);
begin
  UnitDialog.UnitEdit.Text := trim(CurrentEdit.Text);
  UnitDialog.ShowModal;
  if UnitDialog.ModalResult = mrOK then
  begin
    CurrentEdit.Text := UnitDialog.UnitEdit.Text;
    CurrentEdit.Modified := True;
    CurrentEdit.onexit(CurrentEdit);
  end;
end;

procedure TComponentDlg.EqsEditExit(Sender: TObject);
var
  CurrComp: TGrindComponent;
begin
  if EqsEdit.Modified then
  begin
    CurrComp := TGrindComponent(EqsGrid.Objects[0, CurrRow]);
    CurrComp.TheModel.Selected := CurrComp;
    case CurrCol of
      1:
        MainForm.TextEdit.Text := EqsEdit.Text;
      2:
        CurrComp.TheUnit := EqsEdit.Text;
    end;
    if (CurrComp = TheComp) then
      case CurrCol of
        2:
          UnitEdit.Text := EqsEdit.Text;
      end;
    EqsGrid.Cells[CurrCol, CurrRow] := EqsEdit.Text;
  end;
end;

procedure TComponentDlg.IfThenElseCheckClick(Sender: TObject);
var
  f: Integer;
  s: string;
begin
  ifLabel.Visible := IfThenElseCheck.checked;
  ThenLabel.Visible := IfThenElseCheck.checked;
  ElseLabel.Visible := IfThenElseCheck.checked;
  IfEdit.Visible := IfThenElseCheck.checked;
  ThenEdit.Visible := IfThenElseCheck.checked;
  ElseEdit.Visible := IfThenElseCheck.checked;
  EquationEdit.Visible := not IfThenElseCheck.checked;
  ExpandButton.Visible := (not IfThenElseCheck.checked) and
    TheComp.TheModel.IsMatrix and not(TheComp.TheType in [ctFlow, ctTrain]);
  if IfThenElseCheck.checked then
  begin
    if hasIIf(EquationEdit.Lines.Text) then
    begin
      IfEdit.Text := '';
      ThenEdit.Text := '';
      ElseEdit.Text := '';
      s := trim(EquationEdit.Lines.Text);
      s := copy(s, 5, length(s));
      s := copy(s, 1, length(s) - 1);
      f := posstacked(',', s);
      if f > 1 then
      begin
        IfEdit.Text := trim(copy(s, 1, f - 1));
        s := copy(s, f + 1, length(s));
        f := posstacked(',', s);
        if f > 1 then
        begin
          ThenEdit.Text := trim(copy(s, 1, f - 1));
          ElseEdit.Text := trim(copy(s, f + 1, length(s)));
        end
        else
          ThenEdit.Text := s;
      end
      else
        IfEdit.Text := s;
    end
    else
      ThenEdit.Text := trim(EquationEdit.Lines.Text);
  end
  else
  begin
    if IfEdit.Text <> '' then
      EquationEdit.Lines.Text := Format('iif(%s, %s, %s)' + #10,
        [IfEdit.Text, ThenEdit.Text, ElseEdit.Text])
    else
      EquationEdit.Lines.Text := ThenEdit.Text;
    IfEdit.Text := '';
    ThenEdit.Text := '';
    ElseEdit.Text := '';
  end
end;

procedure TComponentDlg.CompTabShow(Sender: TObject);
begin
  CurrentGrid := nil;
  CurrentEdit := nil;
end;

procedure TComponentDlg.AddToScheme(const AList: TStrings);
var
  s: string;
  AList2: TStringList;
  procedure Cleanup(const AList: TStrings);
  var
    i: Integer;
  begin
    for i := AList.Count - 1 downto 0 do
      if trim(AList.Strings[i]) = '' then
        AList.Delete(i);
  end;

begin
  AList2 := TStringList.Create;
  try
    AList2.Capacity := TotalDataGrid.RowCount;
    with TotalDataGrid do
      s := GridToStr(TotalDataGrid, Rect(0, 0, ColCount - 1, RowCount - 1), #9);
    AList2.Text := '%[data]'#10#13 + s;
    Cleanup(AList2);
    if AList2.Count > 1 then
    begin
      with AList do
        while trim(Strings[Count - 1]) = '' do
          Delete(Count - 1);
      AList.AddStrings(AList2);
    end;
  finally
    AList2.Free;
  end;
end;

procedure TComponentDlg.BitBtn3Click(Sender: TObject);
begin
  MainForm.HelpTopic('componentdlg');
end;

procedure TComponentDlg.BtnsButton1Click(Sender: TObject);
begin
  Btns.Button1Click(Sender);
end;

procedure TComponentDlg.UnitButtonClick(Sender: TObject);
begin
  UnitDialog.UnitEdit.Text := trim(UnitEdit.Text);
  UnitDialog.ShowModal;
  if UnitDialog.ModalResult = mrOK then
    UnitEdit.Text := UnitDialog.UnitEdit.Text;
end;

procedure TComponentDlg.IfEditEnter(Sender: TObject);
begin
  Btns.CurrentEdit := TEdit(Sender);
end;

procedure TComponentDlg.VectorCheckClick(Sender: TObject);
begin
  ExpandButton.Visible := (not IfThenElseCheck.checked) and
    VectorCheck.checked and not(TheComp.TheType in [ctFlow, ctTrain]);
  ColEdit.Visible := VectorCheck.checked;
  RowEdit.Visible := ColEdit.Visible;
  ColLabel.Visible := ColEdit.Visible;
  RowLabel.Visible := ColEdit.Visible;
  ColUpDown.Visible := ColEdit.Visible;
  RowUpDown.Visible := ColEdit.Visible;
end;

procedure TComponentDlg.VectorizeButtonClick(Sender: TObject);
// "vectorize" equation, i e add dots
var
  s: string;
  i: Integer;
begin
  s := EquationEdit.Lines.Text;
  for i := length(s) downto 1 do
    if CharInSet(s[i], ['*', '/', '^']) and ((i = 1) or (s[i - 1] <> '.')) then
    begin
      if i = 1 then
        s := '.' + s
      else
        s := copy(s, 1, i - 1) + '.' + copy(s, i, length(s));
    end;
  EquationEdit.Lines.Text := s;
end;

procedure TComponentDlg.ExpandButtonClick(Sender: TObject);
var
  oldtext: string;
  oldrow, oldcol: Integer;
begin
  if TheComp.TheType in [ctStatevar, ctParameter] then
  begin
    MatrixDialog.ComponentLabel.Caption := TheComp.TypeStr + ' name:  ' +
      TheComp.Symbol;
    MatrixDialog.ExpressionLongMemo := nil;
    MatrixDialog.ColEdit.OnChange := nil;
    MatrixDialog.RowEdit.OnChange := nil;
    MatrixDialog.ColUpDown.position := ColUpDown.position;
    MatrixDialog.RowUpDown.position := RowUpDown.position;
    MatrixDialog.ColEdit.OnChange := MatrixDialog.RowEditChange;
    MatrixDialog.RowEdit.OnChange := MatrixDialog.RowEditChange;
    MatrixDialog.ExpressionLongMemo := EquationLongEdit;
    oldtext := EquationLongEdit.Text;
    oldcol := ColUpDown.position;
    oldrow := RowUpDown.position;
    MatrixDialog.ShowModal;
    EquationLongEdit.BeginUpdate;
    try
      ColEdit.OnChange := nil;
      RowEdit.OnChange := nil;
      if MatrixDialog.ModalResult = mrOK then
      begin
        ColUpDown.position := MatrixDialog.ColUpDown.position;
        RowUpDown.position := MatrixDialog.RowUpDown.position;
        EquationLongEdit.Memo := EquationEdit;
      end
      else
      begin
        ColUpDown.position := oldcol;
        RowUpDown.position := oldrow;
        EquationLongEdit.Memo.Text := oldtext;
      end;
      ColEdit.OnChange := RowEditChange;
      RowEdit.OnChange := RowEditChange;
    finally
      EquationLongEdit.EndUpdate;
    end;
  end
  else
  begin
    MultiLineDialog.Btns.SetComponent(TheComp);
    MultiLineDialog.Expression := EquationLongEdit.Text;
    if MultiLineDialog.ShowModal = mrOK then
    begin
      EquationLongEdit.Text := MultiLineDialog.Expression;
      SetExpressionLines(EquationLongEdit.Count);
    end;
  end
end;

procedure TComponentDlg.DataPopupMenuPopup(Sender: TObject);
begin
  MoveUp.Enabled := (PageControl1.ActivePage = EquationsTab) or
    (PageControl1.ActivePage = TabSheet4);
  MoveDown.Enabled := MoveUp.Enabled;
  Insertrows1.Enabled := not MoveUp.Enabled;
  Delete1.Enabled := Insertrows1.Enabled;
  DeleteColumn.Enabled := (DataFilterCombo.ItemIndex = 0);
  InsertColumns.Enabled := DeleteColumn.Enabled;
  DeleteRow.Enabled := DataGrid.row <> 0;
  if DeleteColumn.Enabled and (DataGrid.Cells[0, DataGrid.col] = 't') then
    DeleteColumn.Enabled := False;
end;

procedure TComponentDlg.MoveUpClick(Sender: TObject);
begin
  MoveBy(-1);
end;

procedure TComponentDlg.MoveDownClick(Sender: TObject);
begin
  MoveBy(1)
end;

procedure TComponentDlg.MoveBy(i: Integer);
var
  R, j: Integer;
  s: string;
  Comp1, Comp2: TGrindComponent;
begin
  with CurrentGrid do
  begin
    R := row;
    if (R + i > 0) and (R + i < RowCount) then
    begin
      Comp1 := TGrindComponent(Objects[0, R + i]);
      Comp2 := TGrindComponent(Objects[0, R]);
      Objects[0, R + i] := Comp2;
      Objects[0, R] := Comp1;
      Comp1.ID := Comp2.ID - i;
      Comp1.TheModel.ReorderComponents;
      for j := 0 to ColCount - 1 do
      begin
        s := Cells[j, R];
        Cells[j, R] := Cells[j, R + i];
        Cells[j, R + i] := s;
      end;
    end;
    row := R + i;
  end;
end;

procedure TComponentDlg.SetExpressionLines(n: Integer);
begin
  if n < 1 then
    n := 1;
  if n > 4 then
    n := 4;
  with EquationEdit do
  begin
    Height := SymbolEdit.Height * n;
    top := SymbolEdit.top;
    WantReturns := n > 1;
    if n > 1 then
      ScrollBars := ssVertical
    else
      ScrollBars := ssNone;
  end;
end;

procedure TComponentDlg.SetExternDataFile(const Value: string);
var
  path: string;
begin
  if Value = '' then
    DataFileCombo.ItemIndex := 0
  else
  begin
    DataFileCombo.Items[1] := ExtractFileName(Value);
    path := ExtractFilePath(Value);
    if path <> '' then
      DataFilePath := path;
    DataFileCombo.ItemIndex := 1;
  end;
end;

procedure TComponentDlg.VectorCheck2Click(Sender: TObject);
begin
  VectorCheck.checked := VectorCheck2.checked;
end;

procedure TComponentDlg.ExternDefinedCheckClick(Sender: TObject);
begin
  EquationEdit.Visible := not ExternDefinedCheck.checked;
  ExprLabel.Visible := EquationEdit.Visible;
end;

procedure TComponentDlg.SetDataFilePath(const Value: string);
begin
  FDataFilePath := Value;
  if (Value <> '') and (FDataFilePath[length(FDataFilePath)] = '\') then
    FDataFilePath := copy(FDataFilePath, 1, length(FDataFilePath) - 1);
end;

procedure TComponentDlg.DataPageControlChange(Sender: TObject);
var
  Ser: TNiceSeries;
  X, Y: double;
  err, i, j, n: Integer;
begin
  XYLabel.Caption := '';
  if DataPageControl.ActivePageIndex = 1 then
  begin
    SelectedLabel.Caption := '';
    DataChart.BeginUpdate;
    n := DataChart.SeriesCount;
    for i := n + 1 to DataGrid.ColCount - 1 do
      DataChart.AddSeries(skLine);
    n := DataChart.SeriesCount;

    if DataGrid.ColCount - 1 < n then
    begin
      n := DataGrid.ColCount - 1;
      for j := n to DataChart.SeriesCount - 1 do
        DataChart.Series[j].Active := False;
    end;
    for j := 0 to n - 1 do
    begin
      Ser := DataChart.Series[j];
      Ser.Clear;
      Ser.Active := True;
      Ser.Caption := 'Observed ' + DataGrid.Cells[j + 1, 0];
      with DataGrid do
        for i := 1 to RowCount - 1 do
        begin
          val(Cells[0, i], X, err);
          if err = 0 then
          begin
            val(Cells[j + 1, i], Y, err);
            if err = 0 then
              Ser.AddXY(X, Y);
          end;
        end;
    end;
    DataChart.EndUpdate;
  end
  else
  begin
    if (minRow > 0) and (minCol > 0) then
    begin
      DataGrid.col := minCol;
      DataGrid.row := minRow;
    end;
  end
end;

procedure TComponentDlg.DataChartMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  ax, ay: double;
begin
  if DataChart.ClientToChart(X, Y, ax, ay) then
    XYLabel.Caption := FormatFloat('0.##', ax) + ', ' + FormatFloat('0.##', ay)
  else
    XYLabel.Caption := '';
end;

procedure TComponentDlg.DataTabMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  XYLabel.Caption := '';
end;

procedure TComponentDlg.DataChartMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i, j, mini, minj, k: Integer;
  Sr: TNiceSeries;
  mindist, dist: double;
  aval, minval: TXYinfo;
begin
  minj := -1;
  mini := -1;
  with DataChart do
  begin
    mindist := 9999;
    k := -1;
    for i := 0 to SeriesCount - 1 do
      if Series[i].Active then
      begin
        k := k + 1;
        Sr := Series[i];
        for j := 0 to Sr.ValueCount - 1 do
        begin
          aval := Sr.Value[j]^;
          dist := sqrt(sqr(X - aval.Px) + sqr(Y - aval.Py));
          if dist < mindist then
          begin
            mini := k;
            minj := j + 1;
            minval := aval;
            mindist := dist;
          end;
        end;
      end;
  end;
  if mindist < 15 then
  begin
    SelectedLabel.Left := minval.Px + 3;
    SelectedLabel.top := minval.Py + 3;
    minRow := minj; // used if DataGrid gets focus
    minCol := mini + 1;
    SelectedLabel.Caption := Format('t=%s,  %s=%s',
      [DataGrid.Cells[0, minj], DataGrid.Cells[mini + 1, 0],
      DataGrid.Cells[mini + 1, minj]]);
  end
  else
  begin
    minRow := -1;
    minCol := -1;
    SelectedLabel.Caption := '';
  end;
end;

procedure TComponentDlg.DataEditClick(Sender: TObject);
begin
  minRow := -1;
end;

procedure TComponentDlg.FormResize(Sender: TObject);
begin
  SelectedLabel.Caption := '';
end;

procedure TComponentDlg.InsertRowsClick(Sender: TObject);
var
  s: string;
  R, c: Integer;
  n, err: Integer;
begin
  s := InputBox('Insert rows', 'How many rows?', '10');
  val(s, n, err);
  if err = 0 then
    with CurrentGrid do
    begin
      RowCount := RowCount + n;
      if (row < RowCount - n - 1) then
        for R := RowCount - n - 1 downto row do
          for c := 0 to ColCount - 1 do
            Cells[c, R + n] := Cells[c, R];
      for R := row + 1 to row + n do
        for c := 0 to ColCount - 1 do
          Cells[c, R] := '';
    end;
end;

procedure TComponentDlg.InsertColumnsClick(Sender: TObject);
var
  s: string;
  R, c: Integer;
  n, err: Integer;
begin
  s := InputBox('Insert columns', 'How many columns?', '1');
  val(s, n, err);
  if err = 0 then
    with CurrentGrid do
    begin
      ColCount := ColCount + n;
      if (col < ColCount - n - 1) then
        for c := ColCount - n - 1 downto col do
          for R := 0 to RowCount - 1 do
            Cells[c + n, R] := Cells[c, R];
      for c := col + 1 to col + n do
        for R := 0 to RowCount - 1 do
          Cells[c, R] := '';
    end;
end;

procedure TComponentDlg.DeleteColumnClick(Sender: TObject);
var
  R, c: Integer;
begin
  CurrentEdit.onexit(Sender);
  CurrentEdit.Visible := False;
  if MessageDlg('Are you sure to delete the current column?', mtConfirmation,
    [mbYes, mbNo], 0) = idYes then
    with CurrentGrid do
    begin
      for c := col + 1 to ColCount - 1 do
        for R := 0 to RowCount - 1 do
          Cells[c - 1, R] := Cells[c, R];
      for R := 0 to RowCount - 1 do
        Cells[ColCount - 1, R] := '';
      ColCount := ColCount - 1;
    end;
end;

procedure TComponentDlg.deletedata(Symb: string);
var
  c, R: Integer;
begin
  with TotalDataGrid do
  begin
    col := HasData(Symb);
    if col >= 0 then
    begin
      for R := 0 to RowCount - 1 do
        for c := col + 1 to ColCount - 1 do
          Cells[c - 1, R] := Cells[c, R];
      for R := 0 to RowCount - 1 do
        Cells[ColCount - 1, R] := '';
      ColCount := ColCount - 1;
    end;
  end;
end;

procedure TComponentDlg.DeleteRowClick(Sender: TObject);
var
  R, c: Integer;
begin
  CurrentEdit.onexit(Sender);
  CurrentEdit.Visible := False;
  if MessageDlg('Are you sure to delete the current Row?', mtConfirmation,
    [mbYes, mbNo], 0) = idYes then
    with CurrentGrid do
    begin
      for R := row + 1 to RowCount - 1 do
        for c := 0 to ColCount - 1 do
          Cells[c, R - 1] := Cells[c, R];
      for c := 0 to ColCount - 1 do
        Cells[c, RowCount - 1] := '';
      RowCount := RowCount - 1;
    end;
end;

procedure TComponentDlg.OpenDataFile(aFile: string);
begin
  ExternDatafile := aFile;
  FiletoGrid(aFile, TotalDataGrid);
end;

function TComponentDlg.GetExternDataFile: string;
begin
  if (DataFileCombo.ItemIndex = 0) or
    (CompareStr(DataFileCombo.Text, 'Attach to inifile (recommended)') = 0) or
    (CompareStr(DataFileCombo.Text, '[enter file name]') = 0) then
    result := ''
  else
  begin
    result := DataFilePath + '\' + ExtractFileName(DataFileCombo.Text);
  end;
end;

function TComponentDlg.HasAnyData: Boolean;
var
  i, j: Integer;
begin
  result := False;
  for i := 1 to TotalDataGrid.ColCount do
    for j := 1 to TotalDataGrid.RowCount do
      if (trim(TotalDataGrid.Cells[i, j]) <> '') and
        (comparetext(TotalDataGrid.Cells[i, j], 'nan') <> 0) then
      begin
        result := True;
        exit;
      end;
end;

function TComponentDlg.HasData(Symb: string): Integer;
var
  c: Integer;
begin
  result := -1;
  with TotalDataGrid do
    for c := 0 to ColCount - 1 do
      if Cells[c, 0] = Symb then
        result := c;
end;

procedure TComponentDlg.Properties1Click(Sender: TObject);
begin
  // TePropDialog.Chart := DataChart;
  // TePropDialog.ShowModal;
end;

procedure TComponentDlg.CopyClipboardClick(Sender: TObject);
begin
  // DataChart.CopyToClipboardMetafile(True);
  DataChart.CopyToClipboard;
end;

end.
