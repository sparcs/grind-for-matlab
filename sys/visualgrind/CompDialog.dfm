�
 TCOMPONENTDLG 0+  TPF0TComponentDlgComponentDlgLeft�Top� CaptionEdit a componentClientHeight�ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoMainFormCenterShowHint	
OnActivateFormActivateOnCreate
FormCreateOnHideFormHide	OnKeyDownFormKeyDownOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel6LeftTop`WidthHeightCaptionRows  TPageControlPageControl1Left Top WidthHeight�
ActivePageDataTabAlignalTopTabOrder  	TTabSheetCompTabCaption
ExpressionOnShowCompTabShowExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TLabelSymbolLabel1LeftTop� Width%HeightCaptionSymbol:  TLabel	TypeLabelLeftTop� Width2HeightCaption	TypeLabel  TLabelConnectsLeftTopiWidth-HeightCaptionConnects  TLabel	withLabelLeft0TopiWidthHeightCaptionwith  TLabel	ExprLabelLeftxTop� Width6HeightCaptionExpression:  TLabel	UnitLabelLeft�Top� WidthHeightCaptionUnit:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabeleqLabelLefthTop� WidthHeightCaption=  TLabel	DataLabelLeft@Top)Width]HeightCaptionOutside data range:  TLabelifLabelLeftxTop� WidthHeightCaptionIfVisible  TLabel	ThenLabelLeftxTopWidthHeightCaptionThenVisible  TLabel	ElseLabelLeftxTop8WidthHeightCaptionElseVisible  TLabelRowLabelLeftfTop� WidthiHeightCaptionSize (Rows, Columns):  TLabelColLabelLeftRTop� WidthHeightCaptionx  TLabel
DescrLabelLeft�TopRWidth8HeightCaptionDescription:  TLabel	InitLabelLeftTopHWidth8HeightCaptionInitial value:  �
TCompFrameBtnsLeftTopWidth|Height� TabOrderExplicitLeftExplicitTop �TLabelLabel7Width1ExplicitWidth1  �TLabelLabel8WidthdExplicitWidthd  �TButtonButton4LeftaExplicitLefta  �TButtonButton1LeftaTop!OnClickBtnsButton1ClickExplicitLeftaExplicitTop!  �TButtonVectorizeButtonOnClickVectorizeButtonClick   	TCheckBoxVectorCheckLeftdTop� Width� HeightCaptionVector/Matrix variable?TabOrderOnClickVectorCheckClick  	TComboBox	FromComboLeftxTopaWidth� HeightStylecsDropDownListSorted	TabOrder   	TComboBoxToComboLeftpTopaWidth� HeightStylecsDropDownListSorted	TabOrder  	TCheckBoxDefConnCheckLeftxTopAWidth� HeightHintdefault shape of curveCaptionReset to default curveTabOrderVisible  TEditUnitEditLeft�Top� WidthQHeightHintunit of the componentTabOrderTextUnitEdit  TEdit
SymbolEditLeftTop	WidthQHeightHintEdit symbolTabOrderText
SymbolEdit  	TComboBox	DataComboLeft@Top8Width� HeightHintwhat is done outside data rangeStylecsDropDownListTabOrderItems.StringsUse default valueCycle time series&Do not interpolate between data pointsCycle+do not interpolate   	TCheckBoxDataAvailCheckLeftxTopiWidth� HeightCaptionObservation data availlable?TabOrderVisibleOnClickDataAvailCheckClick  TEditIfEditLeft� Top� Width�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTabOrderVisibleOnEnterIfEditEnter  TEditThenEditLeft� TopWidth�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTabOrderVisibleOnEnterIfEditEnter  TEditElseEditLeft� Top2Width�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTabOrder	VisibleOnEnterIfEditEnter  	TCheckBoxIfThenElseCheckLeftcTop� Width� HeightCaption$Conditional expression: If Then ElseTabOrder
OnClickIfThenElseCheckClick  TEditRowEditLeftTop� Width(HeightTabOrderText1OnChangeRowEditChange  TEditColEditLeftCTop� Width(HeightTabOrderText1OnChangeRowEditChange  TUpDown	ColUpDownLeftkTop� WidthHeight	AssociateColEditMinMax�PositionTabOrder	Thousands  TUpDown	RowUpDownLeft3Top� WidthHeight	AssociateRowEditMinMax�PositionTabOrder	Thousands  TButtonExpandButtonLeft�Top� WidthHeightHintEnter multiline expressionCaption...TabOrderOnClickExpandButtonClick  TMemoEquationEditLeftyTop� WidthHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTabOrderWantReturnsWordWrapOnEnterIfEditEnter  	TCheckBoxExternDefinedCheckLeftxTopjWidth� HeightCaption"Externally defined MATLAB functionTabOrderOnClickExternDefinedCheckClick  TEdit	DescrEditLeft�TopbWidth	HeightTabOrder  TEditInitEditLeft`TopFWidth� HeightTabOrder  	TCheckBoxPermanentCheckLeftcTop� Width� HeightCaptionPermanent variable?TabOrderVisibleOnClickPermanentCheckClick  TButton
UnitButtonLeft�Top� WidthHeightHintEnter an unitCaption...TabOrderOnClickUnitButtonClick   	TTabSheetDataTabCaptionData
ImageIndexOnMouseMoveDataTabMouseMoveOnShowDataTabShow TLabelLabel2LeftTopWidth^HeightCaptionFile to store all data:  TLabelLabel4LeftTop(WidthHeightCaptionFilter:  TLabelLabel3LeftTop@Width� HeightCaption#Data (use only symbols as headers!)  TButtonDataFileButtonLeft�Top Width!HeightCaption...TabOrderOnClickDataFileButtonClick  TBitBtnLoadBtnLeft�TopWidthnHeightCaptionLoad from fileTabOrder OnClickLoadBtnClick  TBitBtnBitBtn5LeftTopWidthnHeightCaption
Paste dataTabOrderOnClick
PasteClick  	TComboBoxDataFilterComboLeftwTopWidth� HeightStylecsDropDownListTabOrderOnChangeDataFilterComboChange  TStringGridTotalDataGridLeft�Top� WidthHeightaColCount	FixedCols 	FixedRows TabOrderVisible  TPageControlDataPageControlLeft TopXWidth
Height.
ActivePageDataGraphTabAlignalBottomTabOrderOnChangeDataPageControlChange 	TTabSheetDataTabelTabCaptionTabelExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TStringGridDataGridLeft TopWidthHeightAlignalBottomColCount	FixedCols RowCountd	FixedRows OptionsgoFixedVertLine
goVertLine
goHorzLinegoRangeSelectgoColSizing 	PopupMenuDataPopupMenuTabOrder 
OnDrawCellParamGridDrawCell	OnKeyDownFormKeyDownOnMouseMoveDataGridMouseMoveOnSelectCellDataGridSelectCell TEditDataEditLeft8TopWidthyHeightBorderStylebsNoneTabOrder VisibleOnClickDataEditClickOnExitDataEditExit    	TTabSheetDataGraphTabCaptionPreview
ImageIndex
DesignSize  
TNiceChart	DataChartLeft TopWidth�Height
ShowLegend		ShowTitle	ShowXGrid		ShowYGrid	TitleChart TitleTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameArialTitleFont.Style 
AxisXTitleTime (t)
AxisYTitle AxisXOnePerValue
AxisXScale       ��?
AxisYScale       ��?
Monochrome
SoftColorsBorderStylebsSingle
BevelOuterbvNoneAnchorsakLeftakTopakRight OnMouseMoveDataChartMouseMoveOnMouseDownDataChartMouseDown	PopupMenu
GraphPopup TLabelXYLabelLeft�Top� WidthHeight  TLabelSelectedLabelLeftxTop8WidthBHeightCaptionSelectedLabel     	TComboBoxDataFileComboLeftxTopWidthRHeightTabOrderTextAttach to inifile (recommended)Items.StringsAttach to inifile (recommended)[enter file name]    	TTabSheet	TabSheet4CaptionParameters/initial values
ImageIndex	PopupMenuDataPopupMenuOnShowTabSheet3ShowExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TStringGrid	ParamGridLeft Top Width
Height�AlignalClient	FixedCols 	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelectgoDrawFocusSelectedgoColSizing 	PopupMenuDataPopupMenuTabOrder 
OnDrawCellParamGridDrawCellOnMouseMoveParamGridMouseMoveOnSelectCellParamGridSelectCell	ColWidthsCQ�DS  TEdit	ParamEditLefthTopWidthyHeightBorderStylebsNoneTabOrder VisibleOnExitParamEditExit   TButtonEditUnitButtonLeftxTopWidthHeightCaption...TabOrderVisibleOnClickEditUnitButtonClick   	TTabSheetEquationsTabCaptionAll Equations
ImageIndexOnShowEquationsTabShowExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TStringGridEqsGridLeft Top Width
Height�AlignalClientColCount	FixedCols 	FixedRows 	PopupMenuDataPopupMenuTabOrder 
OnDrawCellParamGridDrawCellOnMouseMoveParamGridMouseMoveOnSelectCellParamGridSelectCell	ColWidthsm[  TEditEqsEditLeft�TopWidthyHeightBorderStylebsNoneTabOrder VisibleOnExitEqsEditExit    	TTabSheet	TabSheet3CaptionGeneral model settings
ImageIndexOnShowCompTabShowExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TLabelLabel1LeftHTopWidthSHeightCaptionModel description  TMemo	ModelMemoLeftHTop Width!Heighty
ScrollBarsssBothTabOrder   	TCheckBoxVectorCheck2LeftHTop� Width�HeightCaption+The state variables are vectors or matricesTabOrderOnClickVectorCheck2Click    TBitBtnBitBtn1Left� Top�Width� HeightKindbkOK	NumGlyphsTabOrder  TBitBtnBitBtn2Left0Top�Width� HeightKindbkCancel	NumGlyphsTabOrder  TBitBtnBitBtn3Left�Top�Width� HeightKindbkHelp	NumGlyphsTabOrderOnClickBitBtn3Click  
TPopupMenuDataPopupMenuOnPopupDataPopupMenuPopupLeftdTopp 	TMenuItemCopy1CaptionCopyShortCutC@OnClick
Copy1Click  	TMenuItemPasteCaptionPasteShortCutV@OnClick
PasteClick  	TMenuItemDeleteselection1CaptionClear cellsShortCut.OnClickDeleteselection1Click  	TMenuItem	SelectAllCaption
Select AllShortCutA@OnClickSelectAllClick  	TMenuItemMoveUpCaptionMove UpShortCutU@OnClickMoveUpClick  	TMenuItemMoveDownCaption	Move DownShortCutD@OnClickMoveDownClick  	TMenuItemInsertrows1CaptionInsert after 	TMenuItem
InsertRowsCaptionRow...OnClickInsertRowsClick  	TMenuItemInsertColumnsCaption	Column...OnClickInsertColumnsClick   	TMenuItemDelete1CaptionDelete 	TMenuItem	DeleteRowCaptionRowOnClickDeleteRowClick  	TMenuItemDeleteColumnCaptionColumnOnClickDeleteColumnClick    TOpenDialogDataOpenDialogFilter1Tab delimited text file|*.txt;*.dat|All files|*.*TitleOpen tab delimited text fileLeft�Top   TPrintDialogPrintDialog1Left�Top   
TPopupMenu
GraphPopupLeftHTop  	TMenuItemCopyClipboardCaptionCopyOnClickCopyClipboardClick    