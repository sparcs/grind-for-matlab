unit visgrind;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Menus, ToolWin, StdCtrls, ImgList, GRINDComps,
  FileLst, MemoLongStrings, System.ImageList;

type

  TMainForm = class(TForm)
    ToolBar1: TToolBar;
    MainMenu1: TMainMenu;
    StatevarBtn: TToolButton;
    AuxvarBtn: TToolButton;
    ParBtn: TToolButton;
    File1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    FlowBtn: TToolButton;
    ConnectorBtn: TToolButton;
    ScrollBox1: TScrollBox;
    ThePaintBox: TPaintBox;
    SelectBtn: TToolButton;
    RunBtn: TToolButton;
    TextEdit: TEdit;
    SymbolEdit: TEdit;
    ToolButton6: TToolButton;
    ToolImages: TImageList;
    StatusBar1: TStatusBar;
    Trainbtn: TToolButton;
    GhostBtn: TToolButton;
    New1: TMenuItem;
    Copyasmetafile1: TMenuItem;
    N1: TMenuItem;
    ToMATLAB1: TMenuItem;
    Cancel1: TMenuItem;
    Help1: TMenuItem;
    VismodHelp1: TMenuItem;
    GRINDHelp1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Editcomponent1: TMenuItem;
    Deletecomponent1: TMenuItem;
    ZoomCombo: TComboBox;
    ToolButton1: TToolButton;
    Styleofcomponents1: TMenuItem;
    StylesCombo: TComboBox;
    Customizestyle1: TMenuItem;
    N2: TMenuItem;
    Hide1: TMenuItem;
    ExtractPars: TMenuItem;
    ExternvarBtn: TToolButton;
    Undo1: TMenuItem;
    Redo1: TMenuItem;
    Hideparameters1: TMenuItem;
    Helponthiscomponent1: TMenuItem;
    EditMenu: TMenuItem;
    Parameter1: TMenuItem;
    Statevariable1: TMenuItem;
    Auxiliaryvariable1: TMenuItem;
    Externalvariable1: TMenuItem;
    Continuousflow1: TMenuItem;
    DiscreteFlow1: TMenuItem;
    Connector1: TMenuItem;
    ImageList1: TImageList;
    Run2: TMenuItem;
    Compile1: TMenuItem;
    Compileandrun1: TMenuItem;
    N4: TMenuItem;
    Select1: TMenuItem;
    Parameternames1: TMenuItem;
    Insert1: TMenuItem;
    N5: TMenuItem;
    Searchcomponent1: TMenuItem;
    N3: TMenuItem;
    Selectall1: TMenuItem;
    View1: TMenuItem;
    Resetdefaults1: TMenuItem;
    Currentcomponent1: TMenuItem;
    EditCurrent: TMenuItem;
    Delete1: TMenuItem;
    Hide2: TMenuItem;
    Checkequation2: TMenuItem;
    N6: TMenuItem;
    Help2: TMenuItem;
    Unhide1: TMenuItem;
    Unhide2: TMenuItem;
    ReverseConnection1: TMenuItem;
    ModelEquationsItem: TMenuItem;
    N7: TMenuItem;
    ModelEquationsButton: TToolButton;
    FunctionBtn: TToolButton;
    Addfunction1: TMenuItem;
    Changetype1: TMenuItem;
    Reorderstatevariables1: TMenuItem;
    TextonlyButton: TToolButton;
    TextOnly1: TMenuItem;
    About1: TMenuItem;
    Exportscheme1: TMenuItem;
    SaveDialog1: TSaveDialog;
    ExpressEdit: TMemo;
    Allign1: TMenuItem;
    AlignLeft1: TMenuItem;
    AlignTop1: TMenuItem;
    DistributeHorizontally1: TMenuItem;
    DistributeVertically1: TMenuItem;
    AlignCenter1: TMenuItem;
    AlignRight1: TMenuItem;
    N8: TMenuItem;
    AlignMiddle1: TMenuItem;
    AlignBottom1: TMenuItem;
    N9: TMenuItem;
    procedure ThePaintBoxPaint(Sender: TObject);
    procedure ThePaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; SX, SY: Integer);
    procedure FormCreate(Sender: TObject);
    procedure ShowHint(var HintStr: String; var CanShow: Boolean;
      var HintInfo: Controls.THintInfo);
    procedure ThePaintBoxMouseMove(Sender: TObject; Shift: TShiftState;
      SX, SY: Integer);
    procedure ThePaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; SX, SY: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ToolBar1Click(Sender: TObject);
    procedure ThePaintBoxDblClick(Sender: TObject);
    procedure RunBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Open1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure CopyasMetafile1Click(Sender: TObject);
    procedure Cancel1Click(Sender: TObject);
    procedure ToMATLAB1Click(Sender: TObject);
    procedure GRINDHelp1Click(Sender: TObject);
    procedure VismodHelp1Click(Sender: TObject);
    procedure Selectcomponent1Click(Sender: TObject);
    procedure Deletecomponent1Click(Sender: TObject);
    procedure ZoomComboChange(Sender: TObject);
    procedure Styleofcomponents1Click(Sender: TObject);
    procedure StylesComboChange(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Hide1Click(Sender: TObject);
    procedure FileListClick(Sender: TObject; FileName: string;
      FileType: Integer);
    procedure Edit1Click(Sender: TObject);
    procedure Redo1Click(Sender: TObject);
    procedure Undo1Click(Sender: TObject);
    procedure Hideparameters1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Helponthiscomponent1Click(Sender: TObject);
    procedure Statevariable1Click(Sender: TObject);
    procedure Compile1Click(Sender: TObject);
    procedure Parameternames1Click(Sender: TObject);
    procedure Selectall1Click(Sender: TObject);
    procedure Resetdefaults1Click(Sender: TObject);
    procedure EditMenuClick(Sender: TObject);
    procedure Unhide1Click(Sender: TObject);
    procedure ReverseConnection1Click(Sender: TObject);
    procedure ExtractParsClick(Sender: TObject);
    procedure Changetype1Click(Sender: TObject);
    procedure File1Click(Sender: TObject);
    procedure Reorderstatevariables1Click(Sender: TObject);
    procedure TextOnly1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Exportscheme1Click(Sender: TObject);
    procedure AlignLeft1Click(Sender: TObject);
    procedure AlignTop1Click(Sender: TObject);
    procedure DistributeHorizontally1Click(Sender: TObject);
    procedure DistributeVertically1Click(Sender: TObject);
    procedure AlignCenter1Click(Sender: TObject);
    procedure AlignRight1Click(Sender: TObject);
    procedure AlignMiddle1Click(Sender: TObject);
    procedure AlignBottom1Click(Sender: TObject);
  private
    doubleclick: Boolean;
    helpfilename: string;
    function CreateSchemeMetafile: TMetafile;
    { Private declarations }
  public
    ExpressLongEdit: TMemoLongStrings;
    FileList: TFileList;
    GrindModel: TGrindModel;
    procedure ConfigFile(load: Boolean);
    procedure UpdatePaintBox(Direct: Boolean = False);
    // procedure GRINDHelp(AFile: string);
    procedure HelpTopic(atopic: string);
    { Public declarations }
  end;

procedure PlotArrowHead(Canvas: TZoomCanvas; F, T: TPoint);

var
  MainForm: TMainForm;

implementation

uses CompDialog, ModelDialog, Clipbrd, ShellAPI, SelectDlg, StylesDlg,
  ErrorDialog, FindFileDlg, System.Types, ChangeTypeDialog, AboutDlg,
  reorderdlg,
  System.UITypes;

{$R *.DFM}

procedure PlotArrowHead(Canvas: TZoomCanvas; F, T: TPoint);

const
  arrowlen = 10;
  arrowwidth = 4;
var
  dy, dx, len: double;
  A, B, C, D: TPoint;
begin
  dy := (T.Y - F.Y);
  dx := (T.X - F.X);
  len := sqrt(sqr(dy) + sqr(dx));
  if len < 1E-30 then
  begin
    len := 1;
    dy := sqrt(2);
    dx := sqrt(2);
  end;
  if len < arrowlen * 2 then
  begin
    F.Y := T.Y - round(dy * arrowlen * 2 / len);
    F.X := T.X - round(dx * arrowlen * 2 / len);
    len := sqrt(sqr(dy) + sqr(dx));
  end;
  A.X := T.X - round(dx * arrowlen / len);
  A.Y := T.Y - round(dy * arrowlen / len);
  D.X := T.X - round(dx * arrowlen / (len * 1.4));
  D.Y := T.Y - round(dy * arrowlen / (len * 1.4));
  dy := (A.Y - F.Y);
  dx := (A.X - F.X);
  len := sqrt(sqr(dy) + sqr(dx));
  if len < 1 then
    len := 1;
  C.X := A.X + round(dy * arrowwidth / len);
  C.Y := A.Y - round(dx * arrowwidth / len);
  B.X := A.X - round(dy * arrowwidth / len);
  B.Y := A.Y + round(dx * arrowwidth / len);
  Canvas.brush.color := Canvas.pen.color;
  Canvas.Polygon([C, T, B, D]);
  Canvas.brush.color := clWhite;
end;

procedure TMainForm.ThePaintBoxPaint(Sender: TObject);
begin
  GrindModel.Draw(ThePaintBox.Canvas);
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ActiveControl = ScrollBox1 then
  begin
    if Key = VK_DELETE then
    begin
      GrindModel.DeleteSelection;
    end
    else if Key = VK_RETURN then
      ThePaintBoxDblClick(Sender)
    else if (Key = Word('A')) and (Shift = [ssCtrl]) then
      Selectall1Click(Sender)
    else if (Key = Word('E')) and (Shift = [ssCtrl]) then
      ExtractParsClick(Sender);
  end;
end;

procedure TMainForm.ToolBar1Click(Sender: TObject);
begin
  MainForm.ActiveControl := ScrollBox1;
  GrindModel.Selected := nil;
  UpdatePaintBox;
  Statevariable1.Checked := StatevarBtn.Down;
  Parameter1.Checked := ParBtn.Down;
  Auxiliaryvariable1.Checked := AuxvarBtn.Down;
  Addfunction1.Checked := FunctionBtn.Down;
  Externalvariable1.Checked := ExternvarBtn.Down;
  Continuousflow1.Checked := FlowBtn.Down;
  DiscreteFlow1.Checked := Trainbtn.Down;
  Connector1.Checked := ConnectorBtn.Down;
  Select1.Checked := SelectBtn.Down;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FormatSettings.DecimalSeparator:='.';
  FormatSettings.ThousandSeparator:=',';
  GrindModel := TGrindModel.Create(ThePaintBox);
  FileList := TFileList.Create(File1, 5, FileListClick);
  StylesCombo.ItemIndex := 0;
  ThePaintBox.Canvas.Font.Assign(ThePaintBox.Font);
  ThePaintBox.Left := 0;
  ThePaintBox.Top := 0;
  doubleclick := False;
  ActiveControl := ScrollBox1; // ToolBar1;
  ScrollBox1.DoubleBuffered := True; // causes smooth updates
  ConfigFile(True);
  Application.ShowHint := True;
  Application.OnShowHint := ShowHint;
  ExpressLongEdit := TMemoLongStrings.Create(ExpressEdit);
  helpfilename := ExtractFilePath(paramstr(0));
  if helpfilename[length(helpfilename)] <> '\' then
    helpfilename := helpfilename + '\';
  helpfilename := helpfilename + 'grindhelp.chm';

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  HtmlHelp(0, nil, HH_CLOSE_ALL, 0);
  ConfigFile(False);
  GrindModel.Free;
  GrindModel := nil;
  FileList.Free;
end;

procedure TMainForm.ThePaintBoxMouseMove(Sender: TObject; Shift: TShiftState;
  SX, SY: Integer);
var
  Ccomp, Comp: TGrindComponent;
  Edit: TEdit;
  i: Integer;
  // OldX, OldY, dX, dY: integer;
begin
  // Application.HintColor := clInfoBk;
  if SelectBtn.Down then
    ThePaintBox.Cursor := crDefault
  else if StatevarBtn.Down or AuxvarBtn.Down or FunctionBtn.Down or ParBtn.Down
  then
    ThePaintBox.Cursor := crCross
  else
    ThePaintBox.Cursor := crDrag;
  if GrindModel.DraggingSelectBox then
    GrindModel.DragSelectBox(SX, SY)
  else if (GrindModel.Dragging) and (GrindModel.SelectList.Count <> 0) then
  begin
    if GrindModel.Selected <> nil then
      GrindModel.Selected.DragComponent(SX, SY)
    else
    begin
      Ccomp := GrindModel.GetXY(SX, SY);
      if Ccomp <> nil then
      begin
        for i := 0 to GrindModel.SelectList.Count - 1 do
        begin
          Comp := TGrindComponent(GrindModel.SelectList[i]);
          if Comp <> Ccomp then
            with GrindModel.ZoomCanvas do
              Comp.DragComponent(SX + round(ZoomFactor * (Ccomp.X - Comp.X)),
                SY + round(ZoomFactor * (Ccomp.Y - Comp.Y)));
        end;
        Ccomp.DragComponent(SX, SY);
      end;
    end;
    UpdatePaintBox(True);
    ThePaintBox.Hint := '';
    Application.ActivateHint(ThePaintBox.ClientToScreen(point(SX, SY)));
  end
  else
  begin
    Comp := GrindModel.GetXY(SX, SY);
    if Comp = nil then
      ThePaintBox.Hint := '' // format('(%d,%d)', [sX,sY])
    else
    begin
      if ((StatevarBtn.Down and not(Comp.TheType in [ctTextOnly, ctCloud])) or
        AuxvarBtn.Down or ParBtn.Down or FunctionBtn.Down) then
        ThePaintBox.Cursor := crHandPoint;
      if SelectBtn.Down then
      begin
        Edit := Comp.HasText(GrindModel.ZoomCanvas.Scrn2X(SX),
          GrindModel.ZoomCanvas.Scrn2Y(SY), False);
        if (GrindModel.SelectList.Count <= 1) and (Edit <> nil) then
          ThePaintBox.Cursor := crIBeam
        else
          ThePaintBox.Cursor := crHandPoint;
      end;
      ThePaintBox.Hint := stringReplace(Comp.HintText, '|', '�',
        [rfReplaceAll]);
    end;
    Application.ActivateHint(ThePaintBox.ClientToScreen(point(SX, SY)));
  end;
end;

procedure TMainForm.ThePaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; SX, SY: Integer);
var
  Comp, Comp1: TGrindComponent;
  Edit: TEdit;
begin
  if Button = mbLeft then
  begin
    if GrindModel.Selected <> nil then
      GrindModel.Selected.DragPoint := point(0, 0);
    MainForm.ActiveControl := ScrollBox1; // ToolBar1;
    Comp := GrindModel.GetatRect(SX, SY, SX + 30, SY + 30);
    if (Comp = nil) or (Comp.TheType in [ctConnector]) then
    begin
      if StatevarBtn.Down then
        GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctStatevar);
      if FunctionBtn.Down then
        GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctUserFunction);
      if AuxvarBtn.Down then
        GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctAuxilvar);
      if ParBtn.Down then
        GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctParameter);
      if ExternvarBtn.Down then
        GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctExternvar);
      if ModelEquationsButton.Down then
        GrindModel.Selected := GrindModel.AddComponent(SX, SY,
          ctModelEquations);
      if TextonlyButton.Down then
        GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctTextOnly);
    end
    else if StatevarBtn.Down and (Comp.TheType in [ctCloud]) then
    begin
      Comp1 := GrindModel.ReplaceComp(Comp, ctStatevar);
      GrindModel.DeleteComp(Comp);
      GrindModel.SaveUndo(Comp1, uaCreated, 0);
      GrindModel.Selected := Comp1;
    end
    else if (StatevarBtn.Down or AuxvarBtn.Down or FunctionBtn.Down or
      ParBtn.Down or ExternvarBtn.Down) and
      (Comp.TheType in [ctStatevar, ctParameter, ctExternvar, ctPermanentvar,
      ctAuxilvar, ctUserFunction, ctFlow, ctTrain]) then
      GrindModel.Selected := Comp;
    if SelectBtn.Down and not doubleclick then
    begin
      Comp := GrindModel.GetXY(SX, SY);
      if (ssShift in Shift) then
      begin
        if Comp <> nil then
          GrindModel.SelectList.Add(Comp);
        UpdatePaintBox;
      end
      else if (ssCtrl in Shift) and (GrindModel.Selected <> nil) and
        (Comp <> nil) and (Comp <> GrindModel.Selected) then
      begin
        Comp1 := GrindModel.AddComponent(SX, SY, ctConnector);
        if GrindModel.canconnect(Comp1, GrindModel.Selected, True) and
          GrindModel.canconnect(Comp1, Comp, False) then
        begin
          Comp1.From := GrindModel.Selected;
          Comp1.aTo := Comp;
          Comp.SyntaxError := '';
          GrindModel.Selected.SyntaxError := '';
        end
        else
          GrindModel.DeleteComp(Comp1);
      end
      else if (Comp = nil) or (GrindModel.SelectList.Count <= 1) then
      begin
        GrindModel.Selected := Comp;
        GrindModel.DraggingSelectBox := False;
        if Comp = nil then
          GrindModel.DragSelectBox(SX, SY)
        else
        begin
          GrindModel.Dragging := True;
          Edit := Comp.HasText(GrindModel.ZoomCanvas.Scrn2X(SX),
            GrindModel.ZoomCanvas.Scrn2Y(SY), True);
          if Edit = nil then
          begin
            SymbolEdit.Hide;
            TextEdit.Hide;
          end
          else
          begin
            Edit.show;
            GrindModel.Dragging := False;
          end
        end;
      end
      else
        GrindModel.Dragging := True
    end;
    if FlowBtn.Down then
    begin
      GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctFlow);
      GrindModel.Dragging := True;
    end;
    if Trainbtn.Down then
    begin
      GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctTrain);
      GrindModel.Dragging := True;
    end;
    if ConnectorBtn.Down then
    begin
      GrindModel.Selected := GrindModel.AddComponent(SX, SY, ctConnector);
      GrindModel.Dragging := True;
    end;
    doubleclick := False;
  end;
end;

procedure TMainForm.ThePaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; SX, SY: Integer);
var
  newcomp: TGrindComponent;
begin
  if Button = mbLeft then
  begin
    if GrindModel.Dragging and (GrindModel.Selected <> nil) then
    begin
      GrindModel.Selected.Checkconnect;
      if not(GrindModel.Selected.TheType in [ctConnector, ctCloud, ctTextOnly])
      then
      begin
        StatusBar1.SimpleText := format('%s =', [GrindModel.Selected.Symbol]);
        ExpressEdit.Visible := True;
      end
      else
        StatusBar1.SimpleText := '';
      UpdatePaintBox;
    end;
    GrindModel.Dragging := False;
    if (GrindModel.SelectList.Count > 1) and not(ssShift in Shift) then
      GrindModel.Selected := nil;
    GrindModel.DraggingSelectBox := False;
    with GrindModel do
      if (Selected <> nil) and (Selected.TheType in [ctTrain, ctFlow]) then
      begin
        if (Selected.From = nil) then
        begin
          newcomp := TCloud.Create(GrindModel, Selected.FromX - 30,
            Selected.FromY - 15);
          GrindModel.connect(Selected, newcomp, True);
          Components.Add(newcomp);
        end;
        if (Selected.aTo = nil) then
        begin
          newcomp := TCloud.Create(GrindModel, Selected.X - 30,
            Selected.Y - 15);
          GrindModel.connect(Selected, newcomp, False);
          Components.Add(newcomp);
        end;
        // UpdatePaintBox; //PaintBox1.Repaint;
      end;
    GrindModel.UpdateConnections;
    UpdatePaintBox;
    SelectBtn.Down := True;
  end;
end;

procedure TMainForm.ThePaintBoxDblClick(Sender: TObject);
begin
  if GrindModel.Selected <> nil then
  begin
    doubleclick := True;
    GrindModel.Dragging := False;
    ComponentDlg.SetComponent(GrindModel.Selected);
    if not(GrindModel.Selected.TheType in [ctCloud, ctTextOnly]) and
      (ComponentDlg.ShowModal = mrOK) then
    begin
      ComponentDlg.UpdateComponent;
      if GrindModel.Selected <> nil then
      begin
        ExpressLongEdit.Text := GrindModel.Selected.Expression;
        SymbolEdit.Text := GrindModel.Selected.Symbol;
        TextEdit.Text := GrindModel.Selected.Text;
      end;
    end;
    UpdatePaintBox;
  end;
end;

procedure TMainForm.RunBtnClick(Sender: TObject);
begin
  ErrorDlg.Execute(OpenDialog);
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  if ModelDlg.inifilename = '' then
    Caption := 'vismod - [New]'
  else
    Caption := format('vismod - %s', [ModelDlg.inifilename]);
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult <> mrOK then
    case MessageDlg('Do you want to save changes to the current model?',
      mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
      mrYes:
        begin
          ModelDlg.show;
          ModelDlg.Hide;
          ModelDlg.saveinifile;
        end;
      mrCancel:
        Action := caNone;
    end;
end;

procedure TMainForm.Open1Click(Sender: TObject);
begin
  // FindFileDialog.showmodal;
  ModelDlg.LoadButtonClick(nil);
  UpdatePaintBox; // PaintBox1.Repaint;
end;

procedure TMainForm.Save1Click(Sender: TObject);
begin
  ModelDlg.show;
  ModelDlg.Hide;
  ModelDlg.dotButtonClick(nil);
  ModelDlg.saveinifile;
end;

procedure TMainForm.New1Click(Sender: TObject);
begin
  if MessageDlg('Are you sure to clear the current model?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    GrindModel.Clear;
    UpdatePaintBox;
  end;
end;

procedure TMainForm.CopyasMetafile1Click(Sender: TObject);
var
  aMetafile: TMetafile;
begin
  aMetafile := CreateSchemeMetafile;
  try
    Clipboard.Assign(aMetafile);
  finally
    aMetafile.Free;
  end;
end;

function TMainForm.CreateSchemeMetafile: TMetafile;
var
  aCanvas: TMetafilecanvas;
  R1: TRect;
  oldOrigin: TPoint;
  oldzoom: double;
begin
  GrindModel.Selected := nil;
  Result := TMetafile.Create;
  with Result do
  begin
    R1 := GrindModel.getCanvasRect;
    oldOrigin := GrindModel.ZoomCanvas.Origin;
    oldzoom := GrindModel.ZoomCanvas.ZoomFactor;
    GrindModel.ZoomCanvas.ZoomFactor := 1;
    GrindModel.ZoomCanvas.Origin := point(R1.Left, R1.Top);
    Height := round(GrindModel.ZoomCanvas.ZoomFactor * (R1.bottom - R1.Top));
    Width := round(GrindModel.ZoomCanvas.ZoomFactor * (R1.right - R1.Left));
    Enhanced := True;
  end;
  aCanvas := TMetafilecanvas.CreateWithComment(Result,
    ThePaintBox.Canvas.Handle, Application.Exename, Caption);
  try
    GrindModel.Draw(aCanvas);
    aCanvas.Free;
  finally
    GrindModel.ZoomCanvas.Origin := oldOrigin;
    GrindModel.ZoomCanvas.ZoomFactor := oldzoom;
  end;
end;

procedure TMainForm.About1Click(Sender: TObject);
begin
  AboutBox.ShowModal;
end;

procedure TMainForm.AlignBottom1Click(Sender: TObject);
var
  i: Integer;
  maxy: Integer;
begin
  maxy := -9999;
  if GrindModel.SelectList.Count > 0 then
    for i := 0 to GrindModel.SelectList.Count - 1 do
      with TGrindComponent(GrindModel.SelectList.Items[i]) do
        if Y + Height > maxy then
          maxy := Y + Height;
  for i := 0 to GrindModel.SelectList.Count - 1 do
    TGrindComponent(GrindModel.SelectList.Items[i]).Y :=
      maxy - TGrindComponent(GrindModel.SelectList.Items[i]).Height;
  UpdatePaintBox;
end;

procedure TMainForm.AlignCenter1Click(Sender: TObject);
var
  i: Integer;
  sumx: Integer;
begin
  sumx := 0;
  if GrindModel.SelectList.Count > 0 then
    for i := 0 to GrindModel.SelectList.Count - 1 do
      with TGrindComponent(GrindModel.SelectList.Items[i]) do
        sumx := sumx + X + (Height div 2);
  sumx := sumx div GrindModel.SelectList.Count;
  for i := 0 to GrindModel.SelectList.Count - 1 do
    with TGrindComponent(GrindModel.SelectList.Items[i]) do
      X := sumx - (Height div 2);
  UpdatePaintBox;
end;

procedure TMainForm.AlignLeft1Click(Sender: TObject);
var
  i: Integer;
  minx: Integer;
begin
  minx := 9999;
  if GrindModel.SelectList.Count > 0 then
    for i := 0 to GrindModel.SelectList.Count - 1 do
      if TGrindComponent(GrindModel.SelectList.Items[i]).X < minx then
        minx := TGrindComponent(GrindModel.SelectList.Items[i]).X;
  for i := 0 to GrindModel.SelectList.Count - 1 do
    TGrindComponent(GrindModel.SelectList.Items[i]).X := minx;
  UpdatePaintBox;
end;

procedure TMainForm.AlignMiddle1Click(Sender: TObject);
var
  i: Integer;
  sumy: Integer;
begin
  sumy := 0;
  if GrindModel.SelectList.Count > 0 then
    for i := 0 to GrindModel.SelectList.Count - 1 do
      with TGrindComponent(GrindModel.SelectList.Items[i]) do
        sumy := sumy + Y + (Width div 2);
  sumy := sumy div GrindModel.SelectList.Count;
  for i := 0 to GrindModel.SelectList.Count - 1 do
    with TGrindComponent(GrindModel.SelectList.Items[i]) do
      Y := sumy - (Width div 2);
  UpdatePaintBox;
end;

procedure TMainForm.AlignRight1Click(Sender: TObject);
var
  i: Integer;
  maxx: Integer;
begin
  maxx := -9999;
  if GrindModel.SelectList.Count > 0 then
    for i := 0 to GrindModel.SelectList.Count - 1 do
      with TGrindComponent(GrindModel.SelectList.Items[i]) do
        if X + Width > maxx then
          maxx := X + Width;
  for i := 0 to GrindModel.SelectList.Count - 1 do
    TGrindComponent(GrindModel.SelectList.Items[i]).X :=
      maxx - TGrindComponent(GrindModel.SelectList.Items[i]).Width;
  UpdatePaintBox;
end;

procedure TMainForm.AlignTop1Click(Sender: TObject);
var
  i: Integer;
  miny: Integer;
begin
  miny := 9999;
  if GrindModel.SelectList.Count > 0 then
    for i := 0 to GrindModel.SelectList.Count - 1 do
      if TGrindComponent(GrindModel.SelectList.Items[i]).Y < miny then
        miny := TGrindComponent(GrindModel.SelectList.Items[i]).Y;
  for i := 0 to GrindModel.SelectList.Count - 1 do
    TGrindComponent(GrindModel.SelectList.Items[i]).Y := miny;
  UpdatePaintBox;
end;

procedure TMainForm.Cancel1Click(Sender: TObject);
begin
  ModalResult := mrCancel;
  Close;
end;

procedure TMainForm.ToMATLAB1Click(Sender: TObject);
begin
  ErrorDlg.Execute(RunDirect);
end;

procedure TMainForm.HelpTopic(atopic: string);
begin
  if atopic <> '' then
    HtmlHelp(0, pchar(format('%s::/%s.htm', [helpfilename, atopic])),
      HH_DISPLAY_TOPIC, 0)
  else
    HtmlHelp(0, pchar(helpfilename), HH_DISPLAY_TOPIC, 0);
end;

procedure TMainForm.GRINDHelp1Click(Sender: TObject);
begin
  HelpTopic('');
end;

procedure TMainForm.VismodHelp1Click(Sender: TObject);
begin
  HelpTopic('vismod');
end;

procedure TMainForm.Selectcomponent1Click(Sender: TObject);
begin
  SelectDialog.show;
  SelectBtn.Down := True;
end;

procedure TMainForm.ShowHint(var HintStr: String; var CanShow: Boolean;
  var HintInfo: Controls.THintInfo);
begin
  if not(HintInfo.HintControl = ThePaintBox) then
    Screen.HintFont.color := clBlack;
end;

procedure TMainForm.Deletecomponent1Click(Sender: TObject);
begin
  if GrindModel.SelectList.Count > 0 then
    GrindModel.DeleteSelection;
  UpdatePaintBox;
end;

procedure TMainForm.DistributeHorizontally1Click(Sender: TObject);
var
  i, j: Integer;
  spacer, minx, maxx: Integer;
  h: pointer;
begin
  if GrindModel.SelectList.Count > 2 then
  begin
    for i := 0 to GrindModel.SelectList.Count - 1 do
      for j := i + 1 to GrindModel.SelectList.Count - 1 do
        if TGrindComponent(GrindModel.SelectList.Items[i]).X >
          TGrindComponent(GrindModel.SelectList.Items[j]).X then
        begin
          h := GrindModel.SelectList.Items[i];
          GrindModel.SelectList.Items[i] := GrindModel.SelectList.Items[j];
          GrindModel.SelectList.Items[j] := h;
        end;
    with TGrindComponent(GrindModel.SelectList.Items[0]) do
      minx := X + Width div 2;
    with TGrindComponent(GrindModel.SelectList.Items
      [GrindModel.SelectList.Count - 1]) do
      maxx := X + Width div 2;
    spacer := (maxx - minx) div (GrindModel.SelectList.Count - 1);
    for i := 0 to GrindModel.SelectList.Count - 1 do
      with TGrindComponent(GrindModel.SelectList.Items[i]) do
        X := minx + spacer * i - Width div 2;
    UpdatePaintBox;
  end;
end;

procedure TMainForm.DistributeVertically1Click(Sender: TObject);
var
  i, j: Integer;
  spacer, miny, maxy: Integer;
  h: pointer;
begin
  if GrindModel.SelectList.Count > 2 then
  begin
    for i := 0 to GrindModel.SelectList.Count - 1 do
      for j := i + 1 to GrindModel.SelectList.Count - 1 do
        if TGrindComponent(GrindModel.SelectList.Items[i]).Y >
          TGrindComponent(GrindModel.SelectList.Items[j]).Y then
        begin
          h := GrindModel.SelectList.Items[i];
          GrindModel.SelectList.Items[i] := GrindModel.SelectList.Items[j];
          GrindModel.SelectList.Items[j] := h;
        end;
    with TGrindComponent(GrindModel.SelectList.Items[0]) do
      miny := Y + Height div 2;
    with TGrindComponent(GrindModel.SelectList.Items
      [GrindModel.SelectList.Count - 1]) do
      maxy := Y + Height div 2;
    spacer := (maxy - miny) div (GrindModel.SelectList.Count - 1);
    for i := 0 to GrindModel.SelectList.Count - 1 do
      with TGrindComponent(GrindModel.SelectList.Items[i]) do
        Y := miny + spacer * i - Height div 2;
    UpdatePaintBox;
  end;
end;

procedure TMainForm.ZoomComboChange(Sender: TObject);
var
  s: string;
  D: double;
  err: Integer;
  R, R2: TRect;
  aX, aY: Integer;

  function min(A, B: double): double;
  begin
    if A < B then
      Result := A
    else
      Result := B;
  end;

  procedure SetZoomFactor(fact: double; X, Y: Integer);
  begin
    GrindModel.ZoomCanvas.ZoomFactor := fact;
    ScrollBox1.HorzScrollBar.Position := GrindModel.ZoomCanvas.X2Scrn(X);
    ScrollBox1.VertScrollBar.Position := GrindModel.ZoomCanvas.Y2Scrn(Y);
    ScrollBox1.HorzScrollBar.Range := GrindModel.ZoomCanvas.X2Scrn(2000);
    ScrollBox1.VertScrollBar.Range := GrindModel.ZoomCanvas.Y2Scrn(2000);
    ThePaintBox.Width := ScrollBox1.HorzScrollBar.Range;
    ThePaintBox.Height := ScrollBox1.VertScrollBar.Range;
  end;

begin
  if GrindModel <> nil then
  begin
    GrindModel.Selected := nil;
    s := ZoomCombo.Text;
    if s = 'Fit' then
    begin
      R := GrindModel.getCanvasRect;
      R2 := ScrollBox1.ClientRect;
      SetZoomFactor(min((R2.right - R2.Left) / (R.right - R.Left),
        (R2.Top - R2.bottom) / (R.Top - R.bottom)), R.Left, R.Top);
    end
    else
    begin
      if pos('%', s) > 0 then
        s := copy(s, 1, pos('%', s) - 1);
      val(trim(s), D, err);
      if err = 0 then
      begin
        aX := GrindModel.ZoomCanvas.Scrn2X(ScrollBox1.HorzScrollBar.Position);
        aY := GrindModel.ZoomCanvas.Scrn2Y(ScrollBox1.VertScrollBar.Position);
        SetZoomFactor(D / 100, aX, aY);
      end;
    end;
    UpdatePaintBox;
  end;
end;

procedure TMainForm.Styleofcomponents1Click(Sender: TObject);
begin
  if GrindModel.Selected <> nil then
    StylesDialog.PageControl1.ActivePageIndex :=
      Integer(GrindModel.Selected.TheType);
  StylesDialog.ShowModal;
end;

procedure TMainForm.StylesComboChange(Sender: TObject);
begin
  GrindModel.Styles := GrindModel.StyleSchemes[StylesCombo.ItemIndex];
end;

{$HINTS OFF}

procedure TMainForm.PopupMenu1Popup(Sender: TObject);
var
  sel: Boolean;
  D: double;
  s: string;
  err, i: Integer;
begin
  sel := (GrindModel.Selected <> nil);
  Changetype1.Enabled := GrindModel.SelectList.Count > 0;
  ExtractPars.Caption := 'Auto-correct';
  ExtractPars.Enabled := True;
  if sel then
  begin
    ExtractPars.Enabled := False;
    if (GrindModel.Selected.Expression <> '') then
    begin
      s := GrindModel.Selected.Expression;
      if s[length(s)] = ';' then
        s := copy(s, 1, length(s) - 1);
      val(s, D, err);
      if (err <> 0) then
      begin
        ExtractPars.Caption := format('Auto-correct component %s',
          [GrindModel.Selected.Symbol]);
        ExtractPars.Enabled := True;
      end;
    end;
  end;
  ExtractPars.Visible := False;
  /// Praktikum/student mode
  Customizestyle1.Enabled := sel;
  Editcomponent1.Enabled := sel;
  TextOnly1.Enabled := not sel;
  Deletecomponent1.Enabled := GrindModel.SelectList.Count > 0;
  ReverseConnection1.Enabled := sel and GrindModel.Selected.IsConnector;
  Hide1.Enabled := False;
  Unhide1.Enabled := False;
  for i := 0 to GrindModel.SelectList.Count - 1 do
    if TGrindComponent(GrindModel.SelectList.Items[i]).Visible then
      Hide1.Enabled := True
    else
      Unhide1.Enabled := True;
end;

{$HINTS ON}

procedure TMainForm.ConfigFile(load: Boolean);
const
  signature = 764281713;
var
  F: textfile;
  aTC: TCompType;
  fname, s: string;
  version, i, n, idx: Integer;
  aScheme: TComponentStyles;
begin
  fname := ExtractFilePath(paramstr(0));
  if fname[length(fname)] <> '\' then
    fname := fname + '\';
  fname := fname + 'visualgrind.cfg';
  if load and FileExists(fname) then
  begin
    assignfile(F, fname);
    reset(F);
    try
      readln(F, version);
      if version = signature then
      begin
        readln(F);
        readln(F, n); // StylesCombo.Items.Count
        readln(F, idx); // StylesCombo.ItemIndex
        readln(F, s);
        if s = '[styles]' then
          for i := 0 to n - 1 do
          begin
            for aTC := ctStatevar to ctUserFunction do
              with aScheme[aTC] do
              begin
                readln(F, Width);
                readln(F, Height);
                readln(F, pencolor);
                readln(F, pencolor2);
                readln(F, penwidth);
                readln(F, n);
                penstyle := TPenStyle(n);
                readln(F, brushcolor);
                readln(F, n);
                brushstyle := TBrushStyle(n);
                readln(F, n);
                Shape := TShapeKind(n);
                readln(F, n);
                ShowSymbol := Boolean(n);
                readln(F, n);
                ShowText := Boolean(n);
                readln(F, n);
                SwapTextSymbol := Boolean(n);
              end;
            GrindModel.StyleSchemes[i] := aScheme;
          end;
        StylesCombo.ItemIndex := idx;
        StylesComboChange(nil);
        readln(F, s);
        if '[files]' = s then
          FileList.loadFileNames(F);
      end;
    finally
      CloseFile(F);
    end
  end
  else
  begin
    assignfile(F, fname);
    rewrite(F);
    try
      writeln(F, signature);
      writeln(F, 1);
      writeln(F, StylesCombo.Items.Count);
      writeln(F, StylesCombo.ItemIndex);
      writeln(F, '[styles]');
      for i := 0 to StylesCombo.Items.Count - 1 do
      begin
        aScheme := GrindModel.StyleSchemes[i];
        for aTC := ctStatevar to ctUserFunction do
          with aScheme[aTC] do
          begin
            writeln(F, Width);
            writeln(F, Height);
            writeln(F, pencolor);
            writeln(F, pencolor2);
            writeln(F, penwidth);
            writeln(F, Integer(penstyle));
            writeln(F, brushcolor);
            writeln(F, Integer(brushstyle));
            writeln(F, Integer(Shape));
            writeln(F, Integer(ShowSymbol));
            writeln(F, Integer(ShowText));
            writeln(F, Integer(SwapTextSymbol));
          end;
      end;
      writeln(F, '[files]');
      FileList.SaveFileNames(F);
    finally
      CloseFile(F);
    end;
  end;
end;

procedure TMainForm.Hide1Click(Sender: TObject);
var
  i: Integer;
begin
  if GrindModel.SelectList.Count > 0 then
  begin
    for i := 0 to GrindModel.SelectList.Count - 1 do
      TGrindComponent(GrindModel.SelectList.Items[i]).Visible := False;
  end;
  UpdatePaintBox;
end;

procedure TMainForm.FileListClick(Sender: TObject; FileName: string;
  FileType: Integer);
begin
  ModelDlg.inifileEdit.Text := FileName;
  ModelDlg.ThePath := ExtractFilePath(FileName);
  ModelDlg.Loadinifile;
end;

procedure TMainForm.Edit1Click(Sender: TObject);
begin
  Undo1.Enabled := GrindModel.CanUndo;
  Redo1.Enabled := GrindModel.CanRedo;
end;

procedure TMainForm.Redo1Click(Sender: TObject);
begin
  if GrindModel.CanRedo then
    GrindModel.Undo(True);
end;

procedure TMainForm.Undo1Click(Sender: TObject);
begin
  if GrindModel.CanUndo then
    GrindModel.Undo(False);
end;

procedure TMainForm.Hideparameters1Click(Sender: TObject);
begin
  GrindModel.ParsVisible := not GrindModel.ParsVisible;
  UpdatePaintBox;
  if GrindModel.ParsVisible then
    Hideparameters1.Caption := 'Hide parameters'
  else
    Hideparameters1.Caption := 'Show parameters';
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  if ZoomCombo.Text = 'Fit' then
    ZoomComboChange(ZoomCombo);
end;

procedure TMainForm.UpdatePaintBox(Direct: Boolean = False);
begin
  if Direct then
    ThePaintBox.Refresh
  else
    ThePaintBox.Invalidate;
end;

procedure TMainForm.Helponthiscomponent1Click(Sender: TObject);
begin
  if GrindModel.Selected <> nil then
    case GrindModel.Selected.TheType of
      ctStatevar:
        HelpTopic('concept_statevar');
      ctAuxilvar, ctPermanentvar:
        HelpTopic('concept_auxvar');
      ctUserFunction:
        HelpTopic('concept_externfunc');
      ctParameter:
        HelpTopic('concept_parameter');
      ctExternvar:
        HelpTopic('concept_externvar');
      ctConnector:
        HelpTopic('concept_connector');
      ctFlow:
        HelpTopic('concept_flow');
      ctTrain:
        HelpTopic('concept_train');
      ctCloud:
        HelpTopic('concept_cloud');
    end
  else
    HelpTopic('model_Canvas');
end;

procedure TMainForm.Statevariable1Click(Sender: TObject);
begin
  with Sender as TMenuItem do
    Checked := True;
  StatevarBtn.Down := Statevariable1.Checked;
  ParBtn.Down := Parameter1.Checked;
  FunctionBtn.Down := Addfunction1.Checked;
  AuxvarBtn.Down := Auxiliaryvariable1.Checked;
  ExternvarBtn.Down := Externalvariable1.Checked;
  FlowBtn.Down := Continuousflow1.Checked;
  Trainbtn.Down := DiscreteFlow1.Checked;
  ConnectorBtn.Down := Connector1.Checked;
  ModelEquationsButton.Down := ModelEquationsItem.Checked;
  SelectBtn.Down := Select1.Checked;
end;

procedure TMainForm.Compile1Click(Sender: TObject);
begin
  ErrorDlg.Execute(Nothing);
end;

procedure TMainForm.Parameternames1Click(Sender: TObject);
begin
  ComponentDlg.SetComponent(nil);
  ComponentDlg.ShowModal;
  UpdatePaintBox;
end;

procedure TMainForm.Selectall1Click(Sender: TObject);
begin
  GrindModel.SelectList := GrindModel.Components;
  SelectBtn.Down := True;
end;

procedure TMainForm.Resetdefaults1Click(Sender: TObject);
var
  i: Integer;
  aStyle: TComponentStyles;
begin
  if MessageDlg
    ('Are you sure to reset all customized styles? (no undo possible)',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    GrindModel.ParsVisible := True;
    Hideparameters1.Caption := '&Hide parameters';
    with GrindModel, Components do
      for i := 0 to Count - 1 do
        TGrindComponent(Items[i]).Visible := True;
    ZoomCombo.Text := '100%';
    ZoomComboChange(ZoomCombo);
    for i := 0 to nstyles - 1 do
      with GrindModel do
      begin
        aStyle := StyleSchemes[i];
        SetDefaultStyles(aStyle, i);
        StyleSchemes[i] := aStyle;
      end;
    StylesCombo.ItemIndex := 0;
    GrindModel.Styles := GrindModel.StyleSchemes[0];
  end;
end;

procedure TMainForm.EditMenuClick(Sender: TObject);
var
  i: Integer;
begin
  Currentcomponent1.Enabled := (GrindModel.SelectList.Count > 0);
  EditCurrent.Enabled := (GrindModel.Selected <> nil);
  Delete1.Enabled := Currentcomponent1.Enabled;
  Checkequation2.Enabled := Currentcomponent1.Enabled;
  Hide2.Enabled := False;
  Unhide2.Enabled := False;
  AlignLeft1.Enabled := (GrindModel.SelectList.Count > 1);
  AlignCenter1.Enabled := (GrindModel.SelectList.Count > 1);
  AlignRight1.Enabled := (GrindModel.SelectList.Count > 1);
  AlignTop1.Enabled := (GrindModel.SelectList.Count > 1);
  AlignMiddle1.Enabled := (GrindModel.SelectList.Count > 1);
  AlignBottom1.Enabled := (GrindModel.SelectList.Count > 1);
  DistributeHorizontally1.Enabled := (GrindModel.SelectList.Count > 2);
  DistributeVertically1.Enabled := (GrindModel.SelectList.Count > 2);
  for i := 0 to GrindModel.SelectList.Count - 1 do
    if TGrindComponent(GrindModel.SelectList.Items[i]).Visible then
      Hide2.Enabled := True
    else
      Unhide2.Enabled := True;
end;

procedure TMainForm.Unhide1Click(Sender: TObject);
var
  i: Integer;
begin
  if GrindModel.SelectList.Count > 0 then
  begin
    for i := 0 to GrindModel.SelectList.Count - 1 do
      TGrindComponent(GrindModel.SelectList.Items[i]).Visible := True;
  end;
  UpdatePaintBox;
end;

procedure TMainForm.ReverseConnection1Click(Sender: TObject);
begin
  if GrindModel.Selected <> nil then
    GrindModel.Selected.ReverseConnection;
  UpdatePaintBox;
end;

procedure TMainForm.Exportscheme1Click(Sender: TObject);
var
  aMetafile: TMetafile;
  aBitmap: TBitmap;
begin
  SaveDialog1.InitialDir := ModelDlg.ThePath;
  if SaveDialog1.Execute then
  begin
    if length(ExtractFileExt(SaveDialog1.FileName)) < 1 then
    begin
      if SaveDialog1.FilterIndex = 2 then
        SaveDialog1.FileName := SaveDialog1.FileName + '.wmf'
      else if SaveDialog1.FilterIndex = 3 then
        SaveDialog1.FileName := SaveDialog1.FileName + '.bmp';
    end;
    aMetafile := CreateSchemeMetafile;
    try
      if SaveDialog1.FilterIndex = 3 then
      begin
        aBitmap := TBitmap.Create;
        try
          with aBitmap do
          begin
            Height := aMetafile.Height;
            Width := aMetafile.Width;
            Canvas.Draw(0, 0, aMetafile);
            SaveToFile(SaveDialog1.FileName);
          end;
        finally
          aBitmap.Free;
        end;
      end
      else
        aMetafile.SaveToFile(SaveDialog1.FileName);
    finally
      aMetafile.Free;
    end;
  end;
end;

procedure TMainForm.ExtractParsClick(Sender: TObject);
var
  i, ndeleted, prevcount, nadded: Integer;
  Comp: TGrindComponent;
  Alist: TList;
begin
  nadded := 0;
  prevcount := GrindModel.Components.Count;
  Alist := GrindModel.CompsOfType(ctConnector);
  for i := Alist.Count - 1 downto 0 do
    with TGrindComponent(Alist[i]) do
      if (aTo = nil) or (From = nil) then
        GrindModel.DeleteComp(TGrindComponent(Alist[i]));
  if (GrindModel.Selected = nil) or (GrindModel.Selected.Expression = '') then
  begin
    for i := GrindModel.Components.Count - 1 downto 0 do
      with TGrindComponent(GrindModel.Components[i]) do
        if Expression <> '' then
          nadded := nadded + ExtractParameters;
    GrindModel.DeleteIsolatedComponents;
  end
  else
  begin
    Comp := GrindModel.Selected;
    GrindModel.Selected := nil;
    nadded := Comp.ExtractParameters;
  end;
  ndeleted := prevcount + nadded - GrindModel.Components.Count;
  if (nadded <> 0) or (ndeleted <> 0) then
    UpdatePaintBox;
  if (nadded = 0) and (ndeleted = 0) then
    ShowMessage('No problems found')
  else
    ShowMessage(format('%d component(s) removed; %d new added',
      [ndeleted, nadded]));
end;

procedure TMainForm.Changetype1Click(Sender: TObject);
var
  List, Alist, SelList: TList;
  i, j: Integer;
begin
  SelList := TList.Create;
  try
    for i := 0 to GrindModel.SelectList.Count - 1 do
      SelList.Add(GrindModel.SelectList.Items[i]);
    if SelList.Count > 0 then
      ChangeTypeDlg.TheType := TGrindComponent(SelList.Items[0]).TheType;
    with GrindModel do
      if ChangeTypeDlg.ShowModal = mrOK then
        for j := 0 to SelList.Count - 1 do
        begin
          Selected := TGrindComponent(SelList.Items[j]);
          if ((Selected.TheType = ctFlow) and (ChangeTypeDlg.TheType = ctTrain))
            or ((Selected.TheType = ctTrain) and
            (ChangeTypeDlg.TheType = ctFlow)) then
          begin
            Alist := TList.Create;
            try
              List := GrindModel.CompsOfType(Selected.TheType);
              for i := 0 to List.Count - 1 do
                Alist.Add(List.Items[i]);
              for i := 0 to Alist.Count - 1 do
                ChangeType(TGrindComponent(Alist.Items[i]),
                  ChangeTypeDlg.TheType);
            finally
              Alist.Free;
            end;
          end
          else
            ChangeType(Selected, ChangeTypeDlg.TheType);
        end
  finally
    SelList.Free;
  end;
end;

procedure TMainForm.File1Click(Sender: TObject);
begin
  Save1.Enabled := GrindModel.Components.Count > 0;
  Copyasmetafile1.Enabled := Save1.Enabled;
  Exportscheme1.Enabled := Save1.Enabled;
end;

procedure TMainForm.Reorderstatevariables1Click(Sender: TObject);
begin
  ReorderDialog.ShowModal;
end;

procedure TMainForm.TextOnly1Click(Sender: TObject);
begin
  TextonlyButton.Down := True;
  ShowMessage('Click in the scheme to place the text');
end;

end.

