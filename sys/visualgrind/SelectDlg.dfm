object SelectDialog: TSelectDialog
  Left = 355
  Top = 113
  BorderStyle = bsDialog
  Caption = 'Select'
  ClientHeight = 377
  ClientWidth = 415
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  OnActivate = FormActivate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 97
    Height = 13
    Caption = 'Select component(s)'
  end
  object ListBox1: TListBox
    Left = 16
    Top = 32
    Width = 377
    Height = 297
    ItemHeight = 13
    MultiSelect = True
    ParentShowHint = False
    ShowHint = True
    Sorted = True
    TabOrder = 0
    OnClick = ListBox1Click
    OnDblClick = EditBtnClick
    OnMouseMove = ListBox1MouseMove
  end
  object BitBtn1: TBitBtn
    Left = 224
    Top = 344
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object EditBtn: TBitBtn
    Left = 136
    Top = 344
    Width = 75
    Height = 25
    Caption = 'Edit'
    TabOrder = 2
    OnClick = EditBtnClick
  end
  object DeleteBtn: TBitBtn
    Left = 48
    Top = 344
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 3
    OnClick = DeleteBtnClick
  end
  object BitBtn2: TBitBtn
    Left = 312
    Top = 344
    Width = 75
    Height = 25
    Kind = bkHelp
    NumGlyphs = 2
    TabOrder = 4
    OnClick = BitBtn2Click
  end
end
