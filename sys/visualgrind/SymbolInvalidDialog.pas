unit SymbolInvalidDialog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, SymbolUsedDialog, Vcl.StdCtrls,
  Vcl.Buttons;

type
  TSymbolInvalidDlg = class(TSymbolUsedDlg)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SymbolInvalidDlg: TSymbolInvalidDlg;

implementation
{$R *.dfm}

{ TSymbolInvalidDlg }



end.
