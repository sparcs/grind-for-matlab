unit DataFrm;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, Menus;

type
  TDataFrame = class(TFrame)
    DatafileEdit: TEdit;
    Label2: TLabel;
    DataFilterCombo: TComboBox;
    Label4: TLabel;
    Label3: TLabel;
    DataGrid: TStringGrid;
    DataEdit: TEdit;
    BitBtn5: TBitBtn;
    LoadBtn: TBitBtn;
    TotalDataGrid: TStringGrid;
    DataFileButton: TButton;
    DataPopupMenu: TPopupMenu;
    Copy1: TMenuItem;
    Paste: TMenuItem;
    Deleteselection1: TMenuItem;
    SelectAll: TMenuItem;
    Insertrows1: TMenuItem;
    DataOpenDialog: TOpenDialog;
    procedure DataFileButtonClick(Sender: TObject);
    procedure DatafileEditChange(Sender: TObject);
    procedure DataGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure DataGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Copy1Click(Sender: TObject);
    procedure PasteClick(Sender: TObject);
    procedure Deleteselection1Click(Sender: TObject);
    procedure SelectAllClick(Sender: TObject);
    procedure Insertrows1Click(Sender: TObject);
    procedure DataGridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure DataGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure DataFilterComboChange(Sender: TObject);
    procedure LoadBtnClick(Sender: TObject);
    procedure DataEditExit(Sender: TObject);
  private
    { Private declarations }
    CurrCol, CurrRow: integer;
    CurrentSymbol: string;
//    UndoTotalDataGrid: string;
   UndoDataGrid: string;
 //   prevDataFileChanged: boolean;
     procedure MergeDataGrid;
   { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses ModelDialog, visgrind, prgTools,clipbrd,GrindComps;

{$R *.DFM}

procedure TDataFrame.DataFileButtonClick(Sender: TObject);
begin
  DataOpenDialog.filename := Datafileedit.text;
  DataOpenDialog.InitialDir := ModelDlg.ThePath;
  if DataOpenDialog.Execute then
    Datafileedit.text := DataOpenDialog.filename;
end;

procedure TDataFrame.DatafileEditChange(Sender: TObject);
var
  c, i: integer;
  List: TList;
begin
  if FileToGrid(DatafileEdit.text, TotalDataGrid) then
  begin
    DataFilterCombo.ItemIndex := 0;
  end
  else
  begin
    TotalDataGRid.RowCount := 1;
    TotalDataGrid.ColCount := 1;
  end;
  List := MainForm.GrindModel.CompsOfType(ctStatevar);
  for i := 0 to List.Count - 1 do
    TGrindComponent(List.Items[i]).HasData := False;
  for c := 0 to TotalDataGrid.ColCount - 1 do
  begin
    i := MainForm.GrindModel.IndexOf(TotalDataGrid.Cells[c, 0]);
    if i >= 0 then
      TGrindComponent(MainForm.GrindModel.Components[i]).HasData := True;
  end;
end;

procedure TDataFrame.DataGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if ARow = 0 then
    with Sender as TStringGrid do
    begin
      Canvas.Brush.Color := FixedColor;
      Canvas.FillRect(Rect);
      Canvas.Pen.Color := clBlack;
      Canvas.Rectangle(classes.rect(Rect.left - 1, Rect.top - 1, Rect.right + 1,
        Rect.bottom + 1));
      Canvas.MoveTo(Rect.Left + 1, Rect.Bottom - 1);
      Canvas.Pen.Color := clDkGray;
      Canvas.LineTo(Rect.Right - 1, Rect.Bottom - 1);
      Canvas.LineTo(Rect.Right - 1, Rect.Top + 1);
      Canvas.Pen.Color := clWhite;
      Canvas.MoveTo(Rect.right, Rect.top);
      Canvas.LineTo(Rect.Left, Rect.Top);
      Canvas.LineTo(Rect.left, Rect.Bottom);
      Canvas.TextOut(Rect.Left + 3, Rect.Top + 2, Cells[Acol, aRow]);
    end;
end;

procedure TDataFrame.DataGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  grid: TStringGrid;
  edit: TEdit;
begin
  if (shift = [ssCtrl]) then
    case key of
      word('C'): Copy1Click(sender);
      word('A'): SelectAllClick(sender);
      word('V'): PasteClick(sender);
    end;
  grid := DataGrid;
  edit := DataEdit;
  if (key in [VK_down, VK_UP, VK_LEFT, VK_RIGHT]) then
    edit.onexit(edit);
  with Grid do
    case key of
      VK_DELETE: Deleteselection1Click(Sender);
      VK_down: if row < Rowcount - 1 then
          row := row + 1;
      VK_up: if row > 0 then
          row := row - 1;
      VK_left:
        if (col > 0) and (not edit.visible or (edit.sellength =
          length(edit.text))
          or (edit.selstart = 0)) then
          col := col - 1;
      VK_right:
        if (col < ColCount - 1) and (not edit.visible or (edit.sellength =
          length(text)) or (edit.selstart = length(edit.Text))) then
          col := col + 1;
    end;
end;

procedure TDataFrame.Copy1Click(Sender: TObject);
begin
  CopyGridToClipboard(DataGrid, True);
end;

procedure TDataFrame.PasteClick(Sender: TObject);
var
  s: string;
  Grid: TStringGrid;
  Edit: TEdit;
begin
  Grid := DataGrid;
  Edit := DataEdit;
  s := Clipboard.AsText;
  if pos(#13, s) > 0 then
  begin
    Edit.OnExit(sender);
    Edit.Visible := False;
    PasteClipboardToGrid(Grid, True);
  end
  else if Edit.Visible then
  begin
    Edit.Text := s;
    Edit.Modified := True;
  end;
end;

procedure TDataFrame.Deleteselection1Click(Sender: TObject);
var
  r, c: integer;
begin
  with DataGrid, Selection do
    if (Right - Left > 0) or (bottom - top > 0) then
    begin
      with DataEdit do
      begin
        OnExit(sender);
        Hide;
      end;
      for c := Selection.Left to Selection.Right do
        for r := Selection.Top to Selection.Bottom do
          Cells[c, r] := '';
    end;
end;

procedure TDataFrame.SelectAllClick(Sender: TObject);
var
  R: TGRidRect;
  Grid: TStringGrid;
begin
  Grid := DataGrid;
  R.Left := 0;
  R.Right := Grid.ColCount - 1;
  R.Top := 0;
  R.Bottom := Grid.RowCount - 1;
  Grid.Selection := R;
end;

procedure TDataFrame.Insertrows1Click(Sender: TObject);
var
  Grid: TStringGrid;
  s: string;
  r, c: integer;
  n, err: integer;
begin
  Grid := DataGrid;
  s := InputBox('Insert rows', 'How many rows?', '10');
  val(s, n, err);
  if err = 0 then
    with Grid do
    begin
      RowCount := RowCount + n;
      if (Row < RowCount - n - 1) then
      begin
        for r := RowCount - n - 1 downto Row do
          for c := 0 to ColCount - 1 do
            Cells[c, r + n] := Cells[c, r];
        for r := Row + 1 to Row + n do
          for c := 0 to ColCount - 1 do
            Cells[c, r] := '';
      end;
    end;
end;
procedure TDataFrame.DataGridMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  with DataGrid.Selection do
    if (Top <> Bottom) or (Left <> Right) then
      DataEdit.Visible := False;
end;

procedure TDataFrame.DataGridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
  R: TRect;
begin
  if ARow = 0 then
    DataEdit.Color := DataGrid.FixedColor
  else
    dataEdit.Color := clWhite;
  DataEdit.Text := DataGrid.Cells[ACol, ARow];
  R := DataGrid.CellRect(ACol, ARow);
  DataEdit.Top := R.Top;
  DataEdit.Left := R.Left;
  DataEdit.Height := R.Bottom - R.Top;
  DataEdit.Width := R.Right - R.Left;
  DataEdit.Visible := True;
  DataEdit.setFocus;
  DataEdit.SelectAll;
  CurrCol := ACol;
  CurrRow := ARow;
end;


procedure TDataFrame.DataFilterComboChange(Sender: TObject);
var
  c, r, r1, tcol: integer;
  s: string;
begin
  if DataEdit.visible then
    DataEdit.OnExit(nil);
  DataEdit.hide;
  MergeDataGrid;
  if DataFilterCombo.ItemIndex = 0 then
  begin
    CurrentSymbol := '';
    DataGrid.ColCount := TotalDataGrid.ColCount;
    DataGrid.RowCount := TotalDataGrid.RowCount;
    for c := 0 to TotalDataGrid.ColCount - 1 do
      for r := 0 to TotalDataGrid.RowCount - 1 do
        DataGrid.Cells[c, r] := TotalDataGrid.Cells[c, r]
  end
  else
    with DataFilterCombo, DataGrid do
    begin
      s := TGrindComponent(Items.Objects[ItemIndex]).Symbol;
      currentSymbol := s;
      tcol := 0;
      while (tcol < TotalDataGrid.ColCount) and (TotalDataGrid.Cells[tcol, 0] <>
        't') do
        inc(tcol);
      if tcol > TotalDataGRid.ColCount then
      begin
        TotalDataGRid.ColCount := 1;
        TotalDataGrid.Cells[0, 0] := 't';
        tcol := 0;
      end;
      r1 := 1;
      DataGrid.ColCount := 2;
      DataGrid.RowCount := TotalDataGrid.RowCount;
      Cells[0, 0] := 't';
      Cells[1, 0] := s;
      c := 0;
      while (c < TotalDataGrid.ColCount) and (TotalDataGrid.Cells[c, 0] <> s) do
        inc(c);
      if c < TotalDataGrid.ColCount then
      begin
        for r := 1 to TotalDataGrid.RowCount - 1 do
          if isnumber(TotalDataGrid.Cells[c, r]) and
            isnumber(TotalDataGrid.Cells[tcol, r]) then
          begin
            DataGrid.Cells[0, r1] := TotalDataGrid.Cells[tcol, r];
            DataGrid.Cells[1, r1] := TotalDataGrid.Cells[c, r];
            r1 := r1 + 1;
          end;
        DataGrid.Rowcount := r1;
      end
      else
      begin
        DataGrid.ColCount := 2;
        for r := 0 to DataGrid.ColCount - 1 do
          DataGrid.Cols[r].Clear;
        DataGrid.RowCount := 100;
        Cells[0, 0] := 't';
        Cells[1, 0] := s;
      end;
      with DataGrid do
        UndoDataGrid := GridToStr(DataGrid, rect(0, 0, ColCount - 1, RowCount -
          1), #9);
    end;
end;


procedure TDataFrame.MergeDataGrid;
var
  tcol, c, r, r1, tOld, cOld, err: integer;
  s, tim: string;
begin
  if DataEdit.Visible then
  begin
    DataEdit.OnExit(nil);
    DataEdit.Visible := False;
  end;
  if currentSymbol = '' then
  begin
    TotalDataGRid.ColCount := DataGRid.ColCount;
    TotalDataGrid.RowCount := DataGrid.RowCount;
    for c := 0 to DataGrid.ColCount - 1 do
      for r := 0 to DataGrid.RowCount - 1 do
        TotalDataGrid.Cells[c, r] := DataGrid.Cells[c, r]
  end
  else {if (TheComp <> nil) and (TheComp.TheModel.IndexOf(CurrentSymbol) >= 0)
    then                                                                      }
  begin
    tcol := 0;
    while (tcol < DataGrid.ColCount) and (DataGrid.Cells[tcol, 0] <>
      't') do
      inc(tcol);
    if tcol >= DataGrid.ColCount then
    begin
      s := InputBox('Data',
        'Cannot find time in the table, which column is time?', '1');
      val(s, tcol, err);
    end;
    if err < 0 then
      exit;
    c := 0;
    while (c < DataGrid.ColCount) and (DataGrid.Cells[c, 0] <>
      currentSymbol) do
      inc(c);
    if c >= DataGrid.ColCount then
    begin
      s := InputBox('Data',
        format('Cannot find "%s" in the table, which column is "%s" ?',
        [currentSymbol, currentSymbol]), ' 1');
      val(s, c, err);
    end;
    if err < 0 then
      exit;
    tOld := 0;
    while (tOld < TotalDataGrid.ColCount) and (TotalDataGrid.Cells[tOld, 0]
      <> 't') do
      inc(tOld);
    if tOld >= TotalDataGrid.ColCount then
    begin
      if TotalDataGrid.ColCount > 1 then
        TotalDataGrid.ColCount := TotalDataGrid.ColCount + 1;
      tOld := TotalDataGrid.ColCount - 1;
      totalDataGrid.Cells[tOld, 0] := 't';
    end;
    cOld := 0;
    while (cOld < TotalDataGrid.ColCount) and (TotalDataGrid.Cells[cOld, 0]
      <> currentSymbol) do
      inc(cOld);
    if cOld >= TotalDataGrid.ColCount then
    begin
      TotalDataGrid.ColCount := TotalDataGrid.ColCount + 1;
      cOld := TotalDataGrid.ColCount - 1;
    end
    else
      TotalDataGrid.Cols[cOld].Clear;
    TotalDataGrid.Cells[cOld, 0] := CurrentSymbol;
    for r := 0 to DataGrid.RowCount - 1 do
      if isnumber(dataGrid.Cells[tcol, r]) and isnumber(datagrid.cells[c, r])
        then
      begin
        tim := dataGrid.Cells[tcol, r];
        r1 := 0;
        while (r1 < TotalDataGrid.RowCount) and ((TotalDataGrid.Cells[tOld, r1]
          <> tim)) do
          inc(r1);
        if r1 >= TotalDataGrid.RowCount then
        begin
          TotalDataGrid.RowCount := TotalDataGrid.RowCount + 1;
          r1 := TotalDataGrid.RowCount - 1;
          TotalDataGrid.Cells[tOld, r1] := tim;
        end;
        TotalDataGrid.Cells[cOld, r1] := dataGrid.Cells[c, r];
      end;
    NumSortGRid(TotalDataGrid, tOld, 1);
  end;
end;

procedure TDataFrame.LoadBtnClick(Sender: TObject);
begin
  if DataOpenDialog.Execute then
  begin
    dataEdit.visible := False;
    FileToGrid(DataOpenDialog.Filename, DataGrid);
  end
end;

procedure TDataFrame.DataEditExit(Sender: TObject);
begin
  if CurrCol >= 0 then
    DataGrid.Cells[CurrCol, CurrRow] := DataEdit.text;
end;

end.
