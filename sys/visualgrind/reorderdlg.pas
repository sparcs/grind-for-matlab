unit reorderdlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst;

type
  TReorderDialog = class(TForm)
    Label1: TLabel;
    UpBtn: TBitBtn;
    DownBtn: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    VarsList: TListBox;
    procedure FormShow(Sender: TObject);
    procedure UpBtnClick(Sender: TObject);
    procedure DownBtnClick(Sender: TObject);
    procedure VarsListClick(Sender: TObject);
  private
    { Private declarations }
    procedure Move(i: integer);
  public
    { Public declarations }
  end;

var
  ReorderDialog: TReorderDialog;

implementation

uses GrindComps, visgrind;
{$R *.DFM}

procedure TReorderDialog.FormShow(Sender: TObject);
var
  i: integer;
  AList: TList;
begin
  with MainForm.GrindModel do
  begin
    VarsList.Items.Clear;
    AList := CompsOfType(ctStatevar);
    for i := 0 to AList.Count - 1 do
      VarsList.Items.AddObject(TGrindComponent(AList.Items[i]).symbol,
        TObject(AList.Items[i]));
    UpBtn.Enabled := False;
    DownBtn.Enabled := False;
  end;
end;

procedure TReorderDialog.UpBtnClick(Sender: TObject);
begin
  Move(-1);
end;

procedure TReorderDialog.DownBtnClick(Sender: TObject);
begin
  Move(1);
end;

procedure TReorderDialog.Move(i: integer);
var
  Comp1, Comp2: TGrindComponent;
begin
  with VarsList do
  begin
    Comp1 := TGrindComponent(Items.Objects[ItemIndex]);
    Comp2 := TGrindComponent(Items.Objects[ItemIndex + i]);
    MainForm.GrindModel.ExchangeComps(Comp1, Comp2);
    FormShow(self);
    ItemIndex := Items.IndexOfObject(Comp1);
    VarsListClick(self);
  end;
end;

procedure TReorderDialog.VarsListClick(Sender: TObject);
begin
  UpBtn.Enabled := True;
  DownBtn.Enabled := True;
  if VarsList.ItemIndex = 0 then
    UpBtn.Enabled := False;
  if VarsList.ItemIndex = VarsList.Items.Count - 1 then
    DownBtn.Enabled := False;
end;

end.
