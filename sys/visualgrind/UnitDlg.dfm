object UnitDialog: TUnitDialog
  Left = 0
  Top = 0
  Caption = 'Unit Dialog'
  ClientHeight = 382
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 79
    Top = 242
    Width = 63
    Height = 13
    Caption = 'Derived units'
  end
  object Label4: TLabel
    Left = 38
    Top = 284
    Width = 19
    Height = 13
    Caption = 'Unit'
  end
  object UsedLabel: TLabel
    Left = 277
    Top = 49
    Width = 89
    Height = 13
    Caption = 'Used in the model:'
  end
  object Label6: TLabel
    Left = 71
    Top = 49
    Width = 28
    Height = 13
    Caption = 'Prefix'
  end
  object Label1: TLabel
    Left = 132
    Top = 49
    Width = 45
    Height = 13
    Caption = 'Basic unit'
  end
  object Label3: TLabel
    Left = 196
    Top = 49
    Width = 30
    Height = 13
    Caption = 'Power'
  end
  object PrevLabel: TLabel
    Left = 8
    Top = 207
    Width = 22
    Height = 13
    Caption = 'Prev'
    Visible = False
  end
  object NextLabel: TLabel
    Left = 8
    Top = 242
    Width = 22
    Height = 13
    Caption = 'next'
    Visible = False
  end
  object WarningLabel: TLabel
    Left = 71
    Top = 266
    Width = 3
    Height = 13
  end
  object Label5: TLabel
    Left = 71
    Top = 16
    Width = 62
    Height = 13
    Caption = 'Style of unit:'
  end
  object UnitEdit: TEdit
    Left = 71
    Top = 285
    Width = 170
    Height = 21
    TabOrder = 0
    OnClick = UnitEditClick
  end
  object UnitCombo: TComboBox
    Left = 148
    Top = 239
    Width = 93
    Height = 21
    Sorted = True
    TabOrder = 1
    OnChange = UnitComboChange
    Items.Strings = (
      '#'
      #176
      #176'C'
      'a'
      'A'
      'bar'
      'Bq'
      'C'
      'd'
      'F'
      'Gy'
      'H'
      'ha'
      'hr'
      'Hz'
      'J'
      'K'
      'kat'
      'l'
      'L'
      'lm'
      'lux'
      'min'
      'N'
      'Omega'
      'Pa'
      'rad'
      'S'
      'sr'
      'Sv'
      'T'
      'V'
      'W'
      'Wb'
      'yr')
  end
  object OKButton: TButton
    Left = 71
    Top = 335
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
    OnClick = OKButtonClick
  end
  object Button1: TButton
    Left = 167
    Top = 335
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object UsedList: TListBox
    Left = 281
    Top = 68
    Width = 85
    Height = 165
    Hint = 'Select here the units that are used already'
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    Sorted = True
    TabOrder = 4
    OnClick = UsedListClick
  end
  object BasicList: TListBox
    Left = 132
    Top = 68
    Width = 45
    Height = 165
    Hint = 'Basic units'
    ItemHeight = 13
    Items.Strings = (
      'm'
      'g'
      's'
      'A'
      'K'
      'mol'
      'cd'
      ' ')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnClick = BasicListClick
  end
  object UnitButton: TButton
    Left = 247
    Top = 283
    Width = 60
    Height = 25
    Caption = 'Add'
    TabOrder = 6
    OnClick = UnitButtonClick
  end
  object Button7: TButton
    Left = 261
    Top = 335
    Width = 75
    Height = 25
    Caption = 'Help'
    TabOrder = 7
    OnClick = Button7Click
  end
  object PrefixList: TListBox
    Left = 71
    Top = 68
    Width = 45
    Height = 165
    ItemHeight = 13
    Items.Strings = (
      'Y'
      'Z'
      'E'
      'P'
      'T'
      'G'
      'M'
      'k'
      'h'
      'da'
      ' '
      'd'
      'c'
      'm'
      #181
      'mu'
      'n'
      'p'
      'f'
      'a'
      'z'
      'y')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
    OnClick = PrefixListClick
  end
  object PowerList: TListBox
    Left = 196
    Top = 68
    Width = 45
    Height = 165
    Hint = 'The power of the unit (m3 is cubic meter)'
    ItemHeight = 13
    Items.Strings = (
      '6'
      '5'
      '4'
      '3'
      '2'
      ' '
      '-1'
      '-2'
      '-3'
      '-4'
      '-5'
      '-6')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    OnClick = PrefixListClick
  end
  object StyleCombo: TComboBox
    Left = 164
    Top = 8
    Width = 77
    Height = 21
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 10
    Text = 'scientific'
    OnChange = StyleComboChange
    Items.Strings = (
      'scientific'
      'division'
      'dots'
      'brackets')
  end
end
