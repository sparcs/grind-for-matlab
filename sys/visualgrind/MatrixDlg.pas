unit MatrixDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, MemoLongStrings,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CompFrm, Vcl.Grids, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.ComCtrls, Grindcomps, Vcl.Buttons, Vcl.Menus;

type
  TDynStringArray = array of string;
  TDynArray = array of array of double;

  TMatrixDialog = class(TForm)
    RowEdit: TEdit;
    ColEdit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ComponentLabel: TLabel;
    TableGrid: TStringGrid;
    ExpressionMemo1: TMemo;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    RowUpDown: TUpDown;
    ColUpDown: TUpDown;
    ErrorLabel: TLabel;
    Error2Label: TLabel;
    PopupMenu1: TPopupMenu;
    PasteItem: TMenuItem;
    CopyItem: TMenuItem;
    N1: TMenuItem;
    UndoItem: TMenuItem;
    RedoItem: TMenuItem;
    SelectAllItem: TMenuItem;
    FunctionsCombo: TComboBox;
    Label3: TLabel;
    procedure TabSheet1Enter(Sender: TObject);
    procedure TableGridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure RowEditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure RedoBtnClick(Sender: TObject);
    procedure ExpressionMemo1Change(Sender: TObject);
    procedure TableGridClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CopyItemClick(Sender: TObject);
    procedure PasteItemClick(Sender: TObject);
    procedure SelectAllItemClick(Sender: TObject);
    procedure FunctionsComboChange(Sender: TObject);
  private
    OldValue: string;
    FundoList: TStringList;
    FLastError: string;
    FUndondx: Integer;
    FExpressionLongMemo: TMemoLongStrings;
    procedure AddUndoList(s: string);
    procedure UpdateExpressionMemo;
    procedure UpdateTableGrind(aTable: TDynArray);
    procedure SetExpressionLongMemo(const Value: TMemoLongStrings);
    { Private declarations }
  public
    { Public declarations }
    function SetColRow(s: string; row, col: Integer;
      var Table: TDynArray): string;
    function ParseMATLAB(s: string; checkonly: boolean = False): TDynArray;
    procedure GetColRow(s: string; var ncol, nrow: Integer);
    property LastError: string read FLastError;
    property ExpressionLongMemo: TMemoLongStrings read FExpressionLongMemo
      write SetExpressionLongMemo;
  end;

var
  MatrixDialog: TMatrixDialog;

implementation

uses Math, StrUtils, ParseExpr, prgTools, clipbrd, visgrind, ParseClass;
{$R *.dfm}

{ TMatrixDialog }
function findfunction(s, comm: string; var brack1pos, brack2pos: Integer;
  var args: TDynStringArray): Integer;
var
  nleft, narg, iarg: Integer;
begin
  if comm[length(comm)] <> '(' then
    comm := comm + '(';
  result := pos(comm, s);
  { whole word? }
  if (result > 1) and charinset(s[result - 1], ['a' .. 'z', 'A' .. 'Z', '_'])
  then
    result := 0;
  if result > 0 then
  begin
    setlength(args, 10);
    narg := 0;
    brack1pos := result + length(comm) - 1;
    brack2pos := brack1pos + 1;
    nleft := 1;
    iarg := brack2pos;
    while (brack2pos < length(s)) and ((s[brack2pos] <> ')') or (nleft > 1)) do
    begin
      if s[brack2pos] = '(' then
        nleft := nleft + 1;
      if s[brack2pos] = ')' then
        nleft := nleft - 1;
      brack2pos := brack2pos + 1;
      if ((s[brack2pos] = ',') and (nleft = 1)) then
      begin
        args[narg] := copy(s, iarg, brack2pos - iarg);
        narg := narg + 1;
        iarg := brack2pos + 1;
      end;
    end;
    args[narg] := copy(s, iarg, brack2pos - iarg);
    setlength(args, narg + 1);
  end
  else
    setlength(args, 0);
end;

procedure TMatrixDialog.AddUndoList(s: string);
begin
  if (FUndondx = 0) or (CompareStr(FundoList.Strings[FUndondx - 1], s) <> 0)
  then
  begin
    FundoList.Insert(FUndondx, s);
    FUndondx := FUndondx + 1;
  end;
  UndoItem.Enabled := FUndondx > 0;
  RedoItem.Enabled := FUndondx < FundoList.Count;
end;

procedure TMatrixDialog.CopyItemClick(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 0 then
    CopyGridToClipboard(TableGrid, False)
  else
    Clipboard.AsText := ExpressionLongMemo.Text;
end;

procedure TMatrixDialog.ExpressionMemo1Change(Sender: TObject);
begin
  AddUndoList(ExpressionLongMemo.Text);
end;

procedure TMatrixDialog.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  FundoList := TStringList.Create;
  ExpressionLongMemo := nil;
  FunctionsCombo.Clear;
  MainForm.GrindModel.Parser.GetFunctionNames(FunctionsCombo.Items);
  with FunctionsCombo.Items do
  begin
    for i := Count - 1 downto 0 do
      if (TExprWord(Objects[i]) is TGeneratedFunction) or
        not(Assigned(TExprWord(Objects[i]).DoubleFunc)) then
        Delete(i);
    for i := Count - 1 downto 0 do
      if (Strings[i] = 't') or (Strings[i] = 'NAN') then
        Delete(i);
  end;
  // TMemoLongStrings.Create(ExpressionMemo1);
end;

procedure TMatrixDialog.FormDestroy(Sender: TObject);
begin
  FundoList.Free;
  // ExpressionLongMemo.Free;
end;

procedure TMatrixDialog.FormHide(Sender: TObject);
begin
  FExpressionLongMemo := nil;
end;

procedure TMatrixDialog.FormShow(Sender: TObject);
begin
  if FExpressionLongMemo = nil then
    raise Exception.Create('ExpressionLongMemo is not initialized');
  FundoList.Clear;
  FUndondx := 0;
  ErrorLabel.Caption := '';
  Error2Label.Visible := False;
  // ExpressionLongMemo.Clear;
  { Expr := FGrindComponent.Expression;
    while pos('\n', Expr) > 0 do
    begin
    ExpressionLongMemo.Add(copy(Expr, 1, pos('\n', Expr) - 1));
    Expr := copy(Expr, pos('\n', Expr) + 2, length(Expr));
    end;
  }
  TabSheet1Enter(self);
  if FLastError <> '' then
  begin
    TableGrid.ColCount := 1;
    TableGrid.RowCount := 1;
    TableGrid.Cells[0, 0] := 'NaN';
    PageControl1.ActivePage := TabSheet2;
  end
  else
    PageControl1.ActivePage := TabSheet1;
  FundoList.Clear;
  FUndondx := 0;
  UndoItem.Enabled := False;
  RedoItem.Enabled := False;
end;

procedure TMatrixDialog.FunctionsComboChange(Sender: TObject);
var
  s: string;
  Len: Integer;
  IsFunction: boolean;
begin
  s := FunctionsCombo.Text;
  if s = '' then
    s := ' ';
  with ExpressionMemo1 do
    if Visible then
    begin
      IsFunction := (pos('(', s) > 0) and (length(s) > 1);
      Len := Sellength;
      if IsFunction and (Len > 0) then
      begin
        Sellength := 0;
        SelText := copy(s, 1, pos('(', s));
        SelStart := SelStart + Len;
        SelText := copy(s, pos('(', s) + 1, Len);
        SelStart := SelStart - 1;
      end
      else
      begin
        SelText := s;
        if IsFunction then
          SelStart := SelStart - 1;
      end;
    end;
end;

procedure TMatrixDialog.GetColRow(s: string; var ncol, nrow: Integer);
var
  Table: TDynArray;
begin
  Table := ParseMATLAB(s);
  if isNaN(Table[0, 0]) then
  begin
    ncol := -1;
    nrow := -1;
  end
  else
  begin
    ncol := length(Table[0]);
    nrow := length(Table);
  end;
end;

function TMatrixDialog.ParseMATLAB(s: string; checkonly: boolean = False)
  : TDynArray;
var
  s1, s2, command: string;
  arand, arandn, aneye, diagon, density: double;
  p, p1, p2, pdiagon, psymmetric, ncol, nrow, i, j: Integer;
  onesncol, onesnrow: Integer;
  Parser: TMATLABParser;
  args: TDynStringArray;
  issymmetric: boolean;

  function replacecommand(s, comm, Value: string;
    var nrow, ncol: Integer): string;
  { replace a command such as zeros(3,4) with 0 and save the number of rows and columns }
  var
    args: TDynStringArray;
  begin
    p := findfunction(s, comm, p1, p2, args);
    if p > 0 then
    begin
      if length(args) = 1 then
      begin
        if args[0] = '' then
          nrow := 1
        else
          nrow := strToInt(args[0]);
        ncol := nrow;
      end
      else
      begin
        nrow := strToInt(args[0]);
        ncol := strToInt(args[1]);
      end;
      result := copy(s, 1, p - 1) + Value + copy(s, p2 + 1, length(s));
    end
    else
      result := s;
  end;
  function replacecommand2(s, comm, Value: string; var aval: string;
    var nrow, ncol: Integer): string;
  { extra parameter in first argument
    replace a command such as zeros(3,4) with 0 and save the number of rows and columns }
  var
    args: TDynStringArray;
  begin
    p := findfunction(s, comm, p1, p2, args);
    aval := '';
    if p > 0 then
    begin
      aval := args[0];
      if length(args) = 2 then
      begin
        if args[1] = '' then
          nrow := 1
        else
          nrow := strToInt(args[1]);
        ncol := nrow;
      end
      else
      begin
        nrow := strToInt(args[1]);
        ncol := strToInt(args[2]);
      end;
      result := copy(s, 1, p - 1) + Value + copy(s, p2 + 1, length(s));
    end
    else
      result := s;
  end;

  function replacecommand3(s, comm, Value: string; var aval: string;
    var nrow, ncol: Integer): string;
  { extra parameter in last argument
    replace a command such as zeros(3,4) with 0 and save the number of rows and columns }
  var
    args: TDynStringArray;
  begin
    p := findfunction(s, comm, p1, p2, args);
    aval := '';
    if p > 0 then
    begin
      if length(args) = 2 then
      begin
        if args[0] = '' then
          nrow := 1
        else
          nrow := strToInt(args[0]);
        ncol := nrow;
        aval := args[1];
      end
      else if length(args) = 3 then
      begin
        nrow := strToInt(args[0]);
        ncol := strToInt(args[1]);
        aval := args[2];
      end;
      result := copy(s, 1, p - 1) + Value + copy(s, p2 + 1, length(s));
    end
    else
      result := s;
  end;
(* function replacecommand4(s, comm, Value: string; var aval: string;
  var nrow, ncol: Integer): string;
  { extra parameter in last argument
  replace a command such as zeros(3,4) with 0 and save the number of rows and columns }
  var
  args: TDynStringArray;
  begin
  p := findfunction(s, comm, p1, p2, args);
  aval := '';
  if p > 0 then
  begin
  if length(args) = 2 then
  begin
  if args[0] = '' then
  nrow := 1
  else
  nrow := strToInt(args[0]);
  ncol := nrow;
  aval := args[2];
  end
  else if length(args) = 3 then
  begin
  nrow := strToInt(args[0]);
  ncol := strToInt(args[1]);
  aval := args[2];
  end;
  result := copy(s, 1, p - 1) + Value + copy(s, p2 + 1, length(s));
  end
  else
  result := s;
  end; *)

begin
  Parser := TMATLABParser.Create;
  FLastError := '';
  try
    Parser.optimize := False;
    Parser.DefineVariable('g_randvalxx1', @arand);
    Parser.DefineVariable('g_randvalxx2', @arandn);
    Parser.DefineVariable('g_randvalxx4', @aneye);
    try
      nrow := 1;
      ncol := 1;
      if s = '' then
      begin
        setlength(result, nrow, ncol);
        result[0, 0] := NaN;
      end
      else if (pos('[', s) > 0) and (pos(']', s) > 0) then
      begin
        { the exact matrix is given: }
        s1 := s;
        { clean up the string }
        repeat
          s1 := ReplaceStr(s1, '  ', ' ');
        until pos('  ', s1) = 0;
        s1 := ReplaceStr(s1, '...'#13#10, '');
        s1 := ReplaceStr(s1, #$D#$A, ';');
        s1 := ReplaceStr(s1, ' ;', ';');
        s1 := ReplaceStr(s1, '; ', ';');
        repeat
          s1 := ReplaceStr(s1, ';;', ';');
        until pos(';;', s1) = 0;
        s1 := ReplaceStr(s1, '[;', '[');
        s1 := ReplaceStr(s1, ';]', ']');
        s1 := ReplaceStr(s1, ' ,', ',');
        s1 := ReplaceStr(s1, ', ', ',');
        s1 := ReplaceStr(s1, ' ', ',');
        s1 := ReplaceStr(s1, '[,', '[');
        s1 := ReplaceStr(s1, ',]', ']');
        s1 := ReplaceStr(s1, ',;', ';');
        { remove [ and ] }
        s1 := copy(s1, pos('[', s1) + 1, length(s1));
        s1 := copy(s1, 1, pos(']', s1) - 1);

        s2 := s1;
        { first loop to get the number of columns and rows }
        i := 1;
        j := 1;
        repeat
          p1 := pos(',', s1);
          p2 := pos(';', s1);
          if (p1 <> 0) and ((p2 = 0) or (p2 > p1)) then
          begin
            s1 := copy(s1, p1 + 1, length(s1));
            i := i + 1;
            if i > ncol then
              ncol := i;
          end
          else if (p2 <> 0) and ((p1 = 0) or (p1 > p2)) then
          begin
            s1 := copy(s1, p2 + 1, length(s1));
            i := 1;
            j := j + 1;
            if j > nrow then
              nrow := j;
          end;
        until (p1 = 0) and (p2 = 0);
        setlength(result, nrow, ncol);
        { second loop to fill the numbers }
        s1 := s2;
        i := 0;
        j := 0;
        repeat
          p1 := pos(',', s1);
          p2 := pos(';', s1);
          if (p1 = 0) and (p2 = 0) then
          begin
            Parser.AddExpression(s1);
            if not checkonly then
              result[j, i] := Parser.EvaluateCurrent;
          end
          else if (p2 = 0) or ((p2 > p1) and (p1 > 0)) then
          begin
            Parser.AddExpression(copy(s1, 1, p1 - 1));
            if not checkonly then
              result[j, i] := Parser.EvaluateCurrent;
            // result[j, i] := Parser.Evaluate(;
            s1 := copy(s1, p1 + 1, length(s1));
            i := i + 1;
          end
          else if (p1 = 0) or (p1 > p2) then
          begin
            Parser.AddExpression(copy(s1, 1, p2 - 1));
            if not checkonly then
              result[j, i] := Parser.EvaluateCurrent;
            s1 := copy(s1, p2 + 1, length(s1));
            i := 0;
            j := j + 1;
          end;
        until (p1 = 0) and (p2 = 0);
      end
      else if checkonly then
      begin
        Parser.AddExpression(s);
        setlength(result, nrow, ncol);
        result[0, 0] := NaN;
      end
      else
      begin
        command := s;
        onesnrow := 1;
        onesncol := 1;
        density := 1;
        issymmetric := False;
        command := replacecommand(command, 'zeros(', '0', onesnrow, onesncol);
        command := replacecommand(command, 'ones(', '1', nrow, ncol);
        command := replacecommand(command, 'nan(', 'NaN', nrow, ncol);
        command := replacecommand(command, 'nestedmatrix(', 'NaN', nrow, ncol);
        command := replacecommand(command, 'true(', '1', nrow, ncol);
        command := replacecommand(command, 'false(', '0', nrow, ncol);
        { strange names are needed to avoid conflict }
        command := replacecommand(command, 'rand(', 'g_randvalxx1', nrow, ncol);
        command := replacecommand(command, 'randn(', 'g_randvalxx2',
          nrow, ncol);
        command := replacecommand(command, 'eye(', 'g_randvalxx4', nrow, ncol);
        command := replacecommand(command, 'speye(', 'g_randvalxx4',
          nrow, ncol);
        command := replacecommand(command, 'spalloc(', '0', nrow, ncol);
        s1 := '';
        command := replacecommand3(command, 'sprandsym(', 'g_randvalxx1', s1,
          nrow, ncol);
        if s1 <> '' then
        begin
          issymmetric := True;
          density := StrToFloat(s1);
        end;
        command := replacecommand3(command, 'sprand(', 'g_randvalxx1', s1,
          nrow, ncol);
        if s1 <> '' then
          density := StrToFloat(s1);
        command := replacecommand3(command, 'sprandn(', 'g_randvalxx2', s1,
          nrow, ncol);
        if s1 <> '' then
          density := StrToFloat(s1);

        command := replacecommand2(command, 'randi(',
          '(floor(g_randvalxx1*###1###)+1)', s1, nrow, ncol);
        if s1 <> '' then
          command := ReplaceStr(command, '###1###', s1);
        psymmetric := findfunction(command, 'symmetricmat(', p1, p2, args);
        if psymmetric > 0 then
          issymmetric := True;
        diagon := 1;
        pdiagon := findfunction(command, 'setdiagon(', p1, p2, args);
        if pdiagon > 0 then
        begin
          i := p2;
          while not(charinset(command[i], [',', '('])) do
            i := i - 1;
          try
            diagon := StrToFloat(copy(command, i + 1, p2 - (i + 1)));
          except
            diagon := 1;
          end;
        end;
        command := replacecommand2(command, 'repmat(', '###2###', s1,
          nrow, ncol);
        if s1 <> '' then
          command := ReplaceStr(command, '###2###', s1);
        { parse command to a single value }
        nrow := max(nrow, onesnrow);
        ncol := max(ncol, onesncol);

        setlength(result, nrow, ncol);
        for i := 0 to nrow - 1 do
          for j := 0 to ncol - 1 do
          begin
            arand := random;
            arandn := randG(0, 1);
            if (density < 1) and (density < random) then
            begin
              arand := 0;
              arandn := 0;
            end;
            if i = j then
              aneye := 1
            else
              aneye := 0;
            result[i, j] := Parser.Evaluate(command);
            if (pdiagon > 0) and (i = j) then
              result[i, j] := diagon;
          end;
        if issymmetric then
          for i := 0 to nrow - 1 do
            for j := i + 1 to ncol - 1 do
              result[i, j] := result[j, i];
      end;

    except
      { on any error reply with NaN }
      on E: Exception do
      begin
        FLastError := E.Message;
        setlength(result, 1, 1);
        result[0, 0] := NaN;
      end;
    end;
  finally
    if Parser <> nil then
      Parser.Free;
  end;
end;

procedure TMatrixDialog.PasteItemClick(Sender: TObject);
begin
  if (pos(#9, Clipboard.AsText) > 0) or (pos(#10, Clipboard.AsText) > 0) then
  begin
    PasteClipboardToGrid(TableGrid, False);
    UpdateExpressionMemo;
  end
  else
  begin
    ExpressionLongMemo.Text := Clipboard.AsText;
    TabSheet1Enter(nil)
  end;
end;

procedure TMatrixDialog.SelectAllItemClick(Sender: TObject);
var
  myRect: TGridRect;
begin
  if PageControl1.ActivePageIndex = 0 then
  begin
    myRect.Left := 0;
    myRect.Top := 0;
    myRect.Right := TableGrid.ColCount;
    myRect.Bottom := TableGrid.RowCount;
    TableGrid.Selection := myRect;
  end
  else
  begin
    ExpressionMemo1.SelStart := 0;
    ExpressionMemo1.Sellength := length(ExpressionMemo1.Text);
  end;
end;

function TMatrixDialog.SetColRow(s: string; row, col: Integer;
  var Table: TDynArray): string;
var
  oldrow, oldcol, i, j, p: Integer;
  aval: double;
  OldTable: TDynArray;
  function isvalue(s: string): boolean;
  var
    i: Integer;
  begin
    if comparetext(s, 'nan') = 0 then
      result := True
    else
    begin
      result := True;
      s := trim(s);
      for i := 1 to length(s) do
        if not charinset(s[i], ['0' .. '9', '.', 'E', 'e', '-']) then
          result := False;
    end;
  end;
  function replacecolrow(s: string; comm: string): string;
  var
    p, p1, p2: Integer;
    args: TDynStringArray;
  begin
    p := findfunction(s, comm, p1, p2, args);
    if p > 0 then
      if length(args) = 4 then
        result := copy(s, 1, p1) + format('%d,%d,%s,%s',
          [row, col, args[2], args[3]]) + copy(s, p2, length(s))
      else if length(args) = 3 then
        result := copy(s, 1, p1) + format('%d,%d,%s', [row, col, args[2]]) +
          copy(s, p2, length(s))
      else
        result := copy(s, 1, p1) + format('%d,%d', [row, col]) +
          copy(s, p2, length(s))
    else
      result := s;
  end;
  function replacecolrow2(s: string; comm: string): string;
  var
    p, p1, p2: Integer;
    args: TDynStringArray;
  begin
    p := findfunction(s, comm, p1, p2, args);
    if p > 0 then
    begin
      if length(args) = 3 then
        result := copy(s, 1, p1 - 1) + format('%s,%d,%d', [args[0], row, col]) +
          copy(s, p2, length(s))
      else
        result := copy(s, 1, p1) + format('%d,%d', [row, col]) +
          copy(s, p2, length(s));
    end
    else
      result := s;
  end;

begin
  if (pos('[', s) > 0) and (pos(']', s) > 0) or isvalue(s) then
  begin
    Table := ParseMATLAB(s);
    oldcol := length(Table[0]);
    oldrow := length(Table);
    aval := Table[0, 0];
    { are all the values the same? }
    for i := 0 to oldrow - 1 do
      for j := 0 to oldcol - 1 do
        if not isNaN(Table[i, j]) and (Table[i, j] - aval > 1E-30) then
        begin
          aval := 0;
          break;
        end;
    OldTable := Table;
    setlength(Table, row, col); // values may change here

    for i := 0 to row - 1 do
      for j := 0 to col - 1 do
        if (i < oldrow) and (j < oldcol) then
          Table[i, j] := OldTable[i, j]
        else if i >= oldrow then
          Table[i, j] := aval;
    for j := oldcol to col - 1 do
      for i := 0 to row - 1 do
        Table[i, j] := aval;
    result := '';
    // UpdateTableGrind(Table);
    // UpdateExpressionMemo;
  end
  else
  begin
    s := ReplaceStr(s, #$D#$A, '');
    s := replacecolrow(s, 'zeros(');
    s := replacecolrow(s, 'nan(');
    s := replacecolrow(s, 'true(');
    s := replacecolrow(s, 'false(');
    s := replacecolrow(s, 'rand(');
    s := replacecolrow(s, 'randn(');
    s := replacecolrow(s, 'eye(');
    s := replacecolrow(s, 'speye(');
    s := replacecolrow(s, 'sprand(');
    s := replacecolrow(s, 'nestedmatrix(');
    s := replacecolrow(s, 'sprandn(');
    s := replacecolrow2(s, 'repmat(');
    s := replacecolrow2(s, 'randi(');
    s := replacecolrow(s, 'ones(');
    if s = '+zeros(1,1)' then { replace zeros(1,1) with 0 }
    begin
      s := '';
      setlength(Table, 1, 1);
      Table[0, 0] := NaN;
    end;
    p := pos('+zeros(1,1)', s); { replace xx+zeros(1,1) with xx }
    if (p > 1) and (p = length(s) - 10) then
      s := copy(s, 1, p - 1);
    if s = 'zeros(1,1)' then { replace zeros(1,1) with 0 }
      s := '0';
    if s = 'ones(1,1)' then { replace ones(1,1) with 1 }
      s := '1';
    result := s;
  end;
end;

procedure TMatrixDialog.SetExpressionLongMemo(const Value: TMemoLongStrings);
begin
  FExpressionLongMemo := Value;
  if Value <> nil then
    FExpressionLongMemo.Memo := ExpressionMemo1;
end;

(* procedure TMatrixDialog.SetGrindComponent(const Value: TGrindComponent);
  var
  Expr: string;
  begin
  FundoList.Clear;
  FUndondx := 0;
  FGrindComponent := Value;
  ErrorLabel.Caption := '';
  Error2Label.Visible := False;
  ComponentLabel.Caption := FGrindComponent.TypeStr + ' name:  ' +
  FGrindComponent.Symbol;
  // ExpressionLongMemo.Clear;
  Expr := FGrindComponent.Expression;
  while pos('\n', Expr) > 0 do
  begin
  ExpressionLongMemo.Add(copy(Expr, 1, pos('\n', Expr) - 1));
  Expr := copy(Expr, pos('\n', Expr) + 2, length(Expr));
  end;
  ExpressionLongMemo.Text := FGrindComponent.Expression;
  ColEdit.OnChange := nil;
  RowEdit.OnChange := nil;
  RowUpDown.Position := FGrindComponent.nrows;
  ColUpDown.Position := FGrindComponent.ncols;
  ColEdit.OnChange := RowEditChange;
  RowEdit.OnChange := RowEditChange;
  TabSheet1Enter(self);
  if FLastError <> '' then
  begin
  TableGrid.ColCount := 1;
  TableGrid.RowCount := 1;
  TableGrid.Cells[0, 0] := 'NaN';
  PageControl1.ActivePage := TabSheet2;
  end
  else
  PageControl1.ActivePage := TabSheet1;

  FundoList.Clear;
  FUndondx := 0;
  UndoBtn.Enabled := False;
  RedoBtn.Enabled := False;

  end; *)

procedure TMatrixDialog.UndoBtnClick(Sender: TObject);
begin
  if FUndondx > 0 then
  begin
    FUndondx := FUndondx - 1;
    ExpressionLongMemo.Text := FundoList.Strings[FUndondx];
    UndoItem.Enabled := FUndondx > 0;
    RedoItem.Enabled := FUndondx < FundoList.Count;
  end;
end;

procedure TMatrixDialog.RedoBtnClick(Sender: TObject);
begin
  if FUndondx < FundoList.Count then
  begin
    ExpressionLongMemo.Text := FundoList.Strings[FUndondx];
    FUndondx := FUndondx + 1;
    UndoItem.Enabled := FUndondx > 0;
    RedoItem.Enabled := FUndondx < FundoList.Count;
  end;
end;

procedure TMatrixDialog.RowEditChange(Sender: TObject);
var
  Table: TDynArray;
  s: string;
begin
  if ExpressionLongMemo <> nil then
  begin
    s := SetColRow(ExpressionLongMemo.Text, RowUpDown.Position,
      ColUpDown.Position, Table);
    if s = '' then
    begin
      UpdateTableGrind(Table);
      UpdateExpressionMemo;
    end
    else
    begin
      ExpressionLongMemo.Text := s;
      TabSheet1Enter(nil);
    end;
  end;
end;

procedure TMatrixDialog.TableGridClick(Sender: TObject);
begin
  OldValue := TableGrid.Cells[TableGrid.col, TableGrid.row];
end;

procedure TMatrixDialog.TableGridSetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: string);
begin
  if CompareStr(Value, OldValue) <> 0 then
    UpdateExpressionMemo;
end;

procedure TMatrixDialog.TabSheet1Enter(Sender: TObject);
var
  Table: TDynArray;
begin
  Error2Label.Visible := False;
  Table := ParseMATLAB(ExpressionLongMemo.Text);
  ErrorLabel.Caption := FLastError;
  if (length(Table) > 0) and
    not(isNaN(Table[0, 0]) and (ExpressionLongMemo.Text <> '')) then
    UpdateTableGrind(Table)
  else
    Error2Label.Visible := True;
end;

procedure TMatrixDialog.UpdateTableGrind(aTable: TDynArray);
var
  i, j: Integer;
begin
  with TableGrid do
  begin
    if length(aTable) > 0 then
      ColCount := length(aTable[0])
    else
      ColCount := 1;
    RowCount := length(aTable);
    for i := 0 to RowCount - 1 do
      for j := 0 to ColCount - 1 do
      begin
        Cells[j, i] := FloatToStr(aTable[i, j]);
        if Cells[j, i] = 'NAN' then
          Cells[j, i] := '';
      end;
    RowEdit.OnChange := nil;
    ColEdit.OnChange := nil;
    RowUpDown.Position := RowCount;
    ColUpDown.Position := ColCount;
    RowEdit.OnChange := RowEditChange;
    ColEdit.OnChange := RowEditChange;
  end;
end;

procedure TMatrixDialog.UpdateExpressionMemo;
var
  s, s1: string;
  i, j: Integer;
  allTheSame, iseye: boolean;
begin
  with TableGrid do
  begin
    allTheSame := True;
    iseye := True;
    for i := 0 to RowCount - 1 do
      for j := 0 to ColCount - 1 do
      begin
        if ((i = j) and (CompareStr(Cells[j, i], '1') <> 0)) or
          ((i <> j) and (CompareStr(Cells[j, i], '0') <> 0)) then
          iseye := False;
        if (i + j > 0) and (Cells[j, i] <> Cells[0, 0]) then
        begin
          allTheSame := False;
          if not iseye then
            break;
        end;
      end;
    if allTheSame and (RowCount = 1) and (ColCount = 1) then
      s := Cells[0, 0]
    else if allTheSame then
      if CompareStr(Cells[0, 0], '0') = 0 then
        s := format('zeros(%d,%d)', [RowCount, ColCount])
      else if CompareStr(Cells[0, 0], '1') = 0 then
        s := format('ones(%d,%d)', [RowCount, ColCount])
      else
        s := format('%s+zeros(%d,%d)', [Cells[0, 0], RowCount, ColCount])
    else if iseye then
      s := format('eye(%d,%d)', [RowCount, ColCount])
    else
    begin
      s := '[';
      for i := 0 to RowCount - 1 do
      begin
        s1 := '';
        for j := 0 to ColCount - 1 do
          s1 := s1 + Cells[j, i] + ',';
        if s1[length(s1)] = ',' then
          s1 := copy(s1, 1, length(s1) - 1);
        s := s + s1 + ';';
      end;
      if s[length(s)] = ';' then
        s := copy(s, 1, length(s) - 1);
      s := s + ']';
    end;
    AddUndoList(ExpressionLongMemo.Text);
    ExpressionLongMemo.Text := s;
  end;
end;

end.
