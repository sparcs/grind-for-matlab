{ -------------------------------------------------------------------------------

  The contents of this file are subject to the Mozilla Public License
  Version 1.1 (the "License"); you may not use this file except in compliance
  with the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  the specific language governing rights and limitations under the License.

  The Original Code is NiceChart.pas released at May 26th, 2007.
  The Initial Developer of the Original Code is Priyatna.
  (Website: http://www.priyatna.org Email: me@priyatna.org)
  All Rights Reserved.

  Alternatively, the contents of this file may be used under the terms of the
  GNU General Public License Version 2 or later (the "GPL"), in which case
  the provisions of the GPL are applicable instead of those above.
  If you wish to allow use of your version of this file only under the terms
  of the GPL and not to allow others to use your version of this file
  under the MPL, indicate your decision by deleting the provisions above and
  replace them with the notice and other provisions required by the GPL.
  If you do not delete the provisions above, a recipient may use your version
  of this file under either the MPL or the GPL.

  ------------------------------------------------------------------------------- }

unit NiceChart;

interface

uses
  Windows, Messages, Classes, Graphics, Forms, Controls, ExtCtrls, SysUtils,
  BSplines, Math;

const
  UndefinedValueChart: Double = MaxDouble;

type
  TNiceChart = class;

  TSeriesKind = (skLine, skSmooth, skBar);
  PXYInfo = ^TXYInfo;

  TXYInfo = record
    X, Y: Double;
    Px, Py: Integer;
    Rc: TRect;
    Hint: string;
  end;
  TNiceSeries = class(TObject)
  private
    Top: Integer;
    Values: TList;
    Chart: TNiceChart;
    FCaption: string;
    Spline: TBSpline;
    FKind: TSeriesKind;
    FActive: boolean;
    procedure SetCaption(const Value: string);
    function GetMaxXValue: Double;
    function GetMinXValue: Double;
    function GetMinYValue: Double;
    function GetMaxYValue: Double;
    procedure SetKind(const Value: TSeriesKind);
    function getValue(Index: Integer): PXYInfo;
    function GetValueCount: integer;
  protected
    procedure InternalClear;
  public
    constructor Create(AChart: TNiceChart; AKind: TSeriesKind);
    destructor Destroy; override;
    function AddXY(AX, AY: Double; AHint: string = ''): Integer;
    procedure Remove(Index: Integer);
    procedure Clear;
    property ValueCount:integer read GetValueCount;
    property Value[Index: Integer]: PXYInfo read getValue;
    property Active: boolean read FActive write FActive;
    property Caption: string read FCaption write SetCaption;
    property Kind: TSeriesKind read FKind write SetKind;
  end;

  TValueTranslator = record
    MinValue: Double;
    Scale: Double;
    Base: Integer;
  end;

  TMarkerProc = procedure(ACanvas: TCanvas; X, Y, Size: Integer);

  TNiceChart = class(TCustomPanel)
  private
    Brushes: array [0 .. 15] of TBitmap;
    Temp: TStringList;
    MarkSize: Integer;
    Marker: TMarkerProc;
    BarCount: Integer;
    BarWidth: Integer;
    DestWidth, DestHeight: Integer;
    YZero: Integer;
    ChartEmpty: boolean;
    List: TList;
    XAxis: TList;
    YAxis: TList;
    FShowLegend: boolean;
    FShowTitle: boolean;
    FTitle: string;
    FTitleFont: TFont;
    FNormalFont: TFont;
    FUpdating: boolean;
    RcChart, RcLegend, RcTitle: TRect;
    FXTranslate: TValueTranslator;
    FYTranslate: TValueTranslator;
    FAxisXOnePerValue: boolean;
    FAxisYTitle: string;
    FAxisXTitle: string;
    FShowYGrid: boolean;
    FShowXGrid: boolean;
    FAxisYScale: Single;
    FAxisXScale: Single;
    FMonochrome: boolean;
    FSoftColors: boolean;
    procedure InternalClear;
    procedure InternalPaint(ACanvas: TCanvas);
    procedure Calculate(AWidth, AHeight: Integer);
    procedure DoPaint;
    procedure SetShowLegend(const Value: boolean);
    procedure SetShowTitle(const Value: boolean);
    procedure SetTitle(const Value: string);
    procedure SetTitleFont(const Value: TFont);
    procedure TitleFontChanged(Sender: TObject);
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure WMEraseBkgnd(var Msg: TWMEraseBkgnd); message WM_ERASEBKGND;
    function GetSeries(Index: Integer): TNiceSeries;
    function GetSeriesCount: Integer;
    procedure DrawLegend(ACanvas: TCanvas);
    procedure DrawTitle(ACanvas: TCanvas);
    procedure SetAxisXTitle(const Value: string);
    procedure SetAxisYTitle(const Value: string);
    procedure BuildYAxis;
    procedure DrawYAxis(ACanvas: TCanvas);
    procedure DrawXAxis(ACanvas: TCanvas);
    procedure DrawChart(ACanvas: TCanvas);
    procedure BuildXAxis;
    procedure ClearAxis;
    procedure AdjustYAxis;
    procedure SetAxisXOnePerValue(const Value: boolean);
    procedure SetShowXGrid(const Value: boolean);
    procedure SetShowYGrid(const Value: boolean);
    procedure CalculateSeries;
    procedure DrawSeries(ACanvas: TCanvas; Index: Integer);
    procedure AutoColors(ACanvas: TCanvas; Index: Integer; IsBar: boolean);
    procedure SetAxisXScale(const Value: Single);
    procedure SetAxisYScale(const Value: Single);
    procedure SetMonochrome(const Value: boolean);
    function GetLabel(Value: Double): string;
    procedure SetSoftColors(const Value: boolean);
  protected
    procedure Paint; override;
    procedure Changed;
    procedure ChartToClient(const AX, AY: Double; var X, Y: Integer);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    function AddSeries(AKind: TSeriesKind): TNiceSeries;
    function ClientToChart(const X, Y: Integer; var AX, AY: Double): boolean;
    procedure RemoveSeries(ASeries: TNiceSeries);
    procedure Clear;
    property Series[Index: Integer]: TNiceSeries read GetSeries;
    property SeriesCount: Integer read GetSeriesCount;
    function CreateMetafile: TMetafile;
    procedure CopyToClipboard;
  published
    property ShowLegend: boolean read FShowLegend write SetShowLegend;
    property ShowTitle: boolean read FShowTitle write SetShowTitle;
    property ShowXGrid: boolean read FShowXGrid write SetShowXGrid;
    property ShowYGrid: boolean read FShowYGrid write SetShowYGrid;
    property Title: string read FTitle write SetTitle;
    property TitleFont: TFont read FTitleFont write SetTitleFont;
    property AxisXTitle: string read FAxisXTitle write SetAxisXTitle;
    property AxisYTitle: string read FAxisYTitle write SetAxisYTitle;
    property AxisXOnePerValue: boolean read FAxisXOnePerValue
      write SetAxisXOnePerValue;
    property AxisXScale: Single read FAxisXScale write SetAxisXScale;
    property AxisYScale: Single read FAxisYScale write SetAxisYScale;
    property Monochrome: boolean read FMonochrome write SetMonochrome;
    property SoftColors: boolean read FSoftColors write SetSoftColors;
    property BorderStyle;
    property BevelKind;
    property BevelInner;
    property BevelOuter;
    property Align;
    property Anchors;
    property OnMouseMove;
    property OnMouseDown;
    property OnMouseUp;
    property PopupMenu;
  end;

procedure CalculateAxis(AMin, AMax: Double; Count: Integer;
  out Delta, Lowest: Double);

procedure Register;

implementation

{$R NiceChart.res}

uses
  ClipBrd, System.Types, System.UITypes;

procedure Register;
begin
  RegisterComponents('priyatna.org', [TNiceChart]);
end;

const
  OUTER_MARGIN = 20;
  INNER_MARGIN = 10;
  SMALL_MARGIN = 2;
  LEGEND_ITEM = 20;
  AXIS_DEFSIZE = 50;

  Formatter = '0.##';

type


  PAxisInfo = ^TAxisInfo;

  TAxisInfo = record
    Value: Double;
    Px, Py: Integer;
    Caption: string;
  end;

function GetMan10(Value: Double): Double;
var
  Str: string;
begin
  Str := UpperCase(Format('%E', [Value]));
  Result := StrToFloat('1E' + Copy(Str, Pos('E', Str) + 1, Length(Str)));
end;

procedure CalculateAxis(AMin, AMax: Double; Count: Integer;
  out Delta, Lowest: Double);
label
  Retry;
var
  c, n, m10: Double;
begin
  c := Max(2, Count - 1);
  n := (Abs(AMax - AMin) / c);
  m10 := GetMan10(n);
  Delta := 0;
  while (Delta < n) do
    Delta := Delta + (0.5 * m10);
  if (Delta = 0) then
  begin
    Delta := 1;
    Lowest := AMin - (Count div 2);
    Exit;
  end;
Retry:
  Lowest := Trunc(AMin / Delta) * Delta;
  if (Lowest > AMin) then
    Lowest := Lowest - Delta;
  if ((Lowest + (Delta * c)) < AMax) then
  begin
    Delta := Delta + (0.5 * m10);
    goto Retry;
  end;
end;

{ TNiceSeries }

constructor TNiceSeries.Create(AChart: TNiceChart; AKind: TSeriesKind);
begin
  inherited Create;
  Chart := AChart;
  Values := TList.Create;
  FCaption := 'Series';
  Spline := TBSpline.Create;
  FKind := AKind;
  FActive := true;
end;

destructor TNiceSeries.Destroy;
begin
  Spline.Free;
  InternalClear;
  Values.Free;
  inherited Destroy;
end;

procedure TNiceSeries.InternalClear;
var
  X: Integer;
begin
  for X := 0 to Values.Count - 1 do
    Dispose(PXYInfo(Values[X]));
  Values.Clear;
end;

procedure TNiceSeries.Clear;
begin
  InternalClear;
  Chart.Changed;
end;

function TNiceSeries.AddXY(AX, AY: Double; AHint: string): Integer;
var
  Info: PXYInfo;
begin
  Info := New(PXYInfo);
  Info^.X := AX;
  Info^.Y := AY;
  Info^.Px := 0;
  Info^.Py := 0;
  Info^.Rc := Rect(0, 0, 0, 0);
  Info^.Hint := AHint;
  Result := Values.Add(Info);
  Chart.Changed;
end;

procedure TNiceSeries.Remove(Index: Integer);
var
  P: PXYInfo;
begin
  if (Index >= 0) and (Index < Values.Count) then
  begin
    P := Values[Index];
    Values.Remove(P);
    Dispose(P);
    Chart.Changed;
  end;
end;

function TNiceSeries.GetMaxXValue: Double;
var
  X: Integer;
begin
  Result := -MaxDouble;
  for X := 0 to Values.Count - 1 do
    Result := Max(Result, PXYInfo(Values[X])^.X);
end;

function TNiceSeries.GetMinXValue: Double;
var
  X: Integer;
begin
  Result := MaxDouble;
  for X := 0 to Values.Count - 1 do
    Result := Min(Result, PXYInfo(Values[X])^.X);
end;

function TNiceSeries.GetMaxYValue: Double;
var
  X: Integer;
begin
  Result := -MaxDouble;
  for X := 0 to Values.Count - 1 do
    Result := Max(Result, PXYInfo(Values[X])^.Y);
end;

function TNiceSeries.GetMinYValue: Double;
var
  X: Integer;
begin
  Result := MaxDouble;
  for X := 0 to Values.Count - 1 do
    Result := Min(Result, PXYInfo(Values[X])^.Y);
end;

function TNiceSeries.getValue(Index: Integer): PXYInfo;
begin
   Result:=PXYInfo(Values[Index]);
end;

function TNiceSeries.GetValueCount: integer;
begin
   Result:=Values.Count;
end;

procedure TNiceSeries.SetCaption(const Value: string);
begin
  if (FCaption <> Value) then
  begin
    FCaption := Value;
    Chart.Changed;
  end;
end;

procedure TNiceSeries.SetKind(const Value: TSeriesKind);
begin
  if (FKind <> Value) then
  begin
    FKind := Value;
    Chart.Changed;
  end;
end;

{ TNiceChart }

constructor TNiceChart.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ParentColor := False;
  ParentBackground := False;
  ParentFont := False;
  Temp := TStringList.Create;
  Width := 300;
  Height := 200;
  Color := clWhite;
  BevelOuter := bvNone;
  BevelInner := bvNone;
  BorderStyle := bsSingle;
  ControlStyle := ControlStyle + [csNeedsBorderPaint];
  List := TList.Create;
  FShowLegend := true;
  FShowTitle := true;
  FShowXGrid := true;
  FShowYGrid := true;
  FMonochrome := False;
  FTitle := 'Chart Title';
  FTitleFont := TFont.Create;
  FTitleFont.Name := 'Arial';
  FTitleFont.Size := 14;
  FTitleFont.Style := [];
  FTitleFont.OnChange := TitleFontChanged;
  FNormalFont := TFont.Create;
  FNormalFont.Name := 'Arial';
  FAxisXTitle := 'X Axis';
  FAxisYTitle := 'Y Axis';
  FAxisXScale := 1;
  FAxisYScale := 1;
  XAxis := TList.Create;
  YAxis := TList.Create;
  FUpdating := False;
end;

destructor TNiceChart.Destroy;
var
  X: Integer;
begin
  for X := 0 to 15 do
  begin
    if Assigned(Brushes[X]) then
      Brushes[X].Free;
  end;
  InternalClear;
  List.Free;
  FTitleFont.Free;
  FNormalFont.Free;
  XAxis.Free;
  YAxis.Free;
  Temp.Free;
  inherited Destroy;
end;

procedure TNiceChart.InternalClear;
var
  X: Integer;
begin
  for X := 0 to List.Count - 1 do
    TNiceSeries(List[X]).Free;
  ClearAxis;
  List.Clear;
end;

procedure TNiceChart.Paint;
begin
  if HandleAllocated then
    DoPaint;
end;

procedure TNiceChart.DoPaint;
begin
  InternalPaint(Canvas);
end;

procedure TNiceChart.SetMonochrome(const Value: boolean);
begin
  if (FMonochrome <> Value) then
  begin
    FMonochrome := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetSoftColors(const Value: boolean);
begin
  if (FSoftColors <> Value) then
  begin
    FSoftColors := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetShowLegend(const Value: boolean);
begin
  if (FShowLegend <> Value) then
  begin
    FShowLegend := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetAxisXOnePerValue(const Value: boolean);
begin
  if (FAxisXOnePerValue <> Value) then
  begin
    FAxisXOnePerValue := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetShowTitle(const Value: boolean);
begin
  if (FShowTitle <> Value) then
  begin
    FShowTitle := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetTitle(const Value: string);
begin
  if (FTitle <> Value) then
  begin
    FTitle := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetTitleFont(const Value: TFont);
begin
  FTitleFont.Assign(Value);
end;

procedure TNiceChart.TitleFontChanged(Sender: TObject);
begin
  Changed;
end;

procedure TNiceChart.SetAxisXTitle(const Value: string);
begin
  if (FAxisXTitle <> Value) then
  begin
    FAxisXTitle := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetAxisYTitle(const Value: string);
begin
  if (FAxisYTitle <> Value) then
  begin
    FAxisYTitle := Value;
    Changed;
  end;
end;

procedure TNiceChart.SetAxisXScale(const Value: Single);
begin
  if (FAxisXScale <> Value) then
  begin
    FAxisXScale := Value;
    if (FAxisXScale = 0) then
      FAxisXScale := 1;
    Changed;
  end;
end;

procedure TNiceChart.SetAxisYScale(const Value: Single);
begin
  if (FAxisYScale <> Value) then
  begin
    FAxisYScale := Value;
    if (FAxisYScale = 0) then
      FAxisYScale := 1;
    Changed;
  end;
end;

procedure TNiceChart.SetShowXGrid(const Value: boolean);
begin
  if (FShowXGrid <> Value) then
  begin
    FShowXGrid := Value;
    DoPaint;
  end;
end;

procedure TNiceChart.SetShowYGrid(const Value: boolean);
begin
  if (FShowYGrid <> Value) then
  begin
    FShowYGrid := Value;
    DoPaint;
  end;
end;

procedure TNiceChart.BeginUpdate;
begin
  FUpdating := true;
end;

procedure TNiceChart.EndUpdate;
begin
  FUpdating := False;
  Calculate(ClientWidth, ClientHeight);
  DoPaint;
end;

procedure TNiceChart.Changed;
begin
  if not FUpdating then
  begin
    Calculate(ClientWidth, ClientHeight);
    DoPaint;
  end;
end;

procedure TNiceChart.WMSize(var Msg: TWMSize);
begin
  inherited;
  Changed;
end;

procedure TNiceChart.WMEraseBkgnd(var Msg: TWMEraseBkgnd);
begin
  Msg.Result := 1;
end;

function TNiceChart.GetSeries(Index: Integer): TNiceSeries;
begin
  Result := TNiceSeries(List[Index]);
end;

function TNiceChart.AddSeries(AKind: TSeriesKind): TNiceSeries;
begin
  Result := TNiceSeries.Create(Self, AKind);
  List.Add(Result);
end;

procedure TNiceChart.Clear;
begin
  InternalClear;
  Changed;
end;

procedure TNiceChart.RemoveSeries(ASeries: TNiceSeries);
begin
  if Assigned(ASeries) then
  begin
    List.Remove(ASeries);
    ASeries.Free;
    Changed;
  end;
end;

function TNiceChart.GetSeriesCount: Integer;
begin
  Result := List.Count;
end;

procedure TNiceChart.DrawLegend(ACanvas: TCanvas);
var
  X, Y, l, t: Integer;
  th, g: Integer;
begin
  with ACanvas do
  begin
    Pen.Width := 1;
    Pen.Style := psSolid;
    Font.Assign(FNormalFont);
    g := TextHeight('Ag');
    th := (LEGEND_ITEM - g) div 2;
    Brush.Style := bsSolid;
    Brush.Color := clBlack;
    FillRect(Rect(RcLegend.Right, RcLegend.Top + 3, RcLegend.Right + 3,
      RcLegend.Bottom + 3));
    FillRect(Rect(RcLegend.Left + 3, RcLegend.Bottom, RcLegend.Right + 3,
      RcLegend.Bottom + 3));
    Brush.Style := bsClear;
    Rectangle(RcLegend);
    Brush.Style := bsClear;
    l := RcLegend.Left + INNER_MARGIN + LEGEND_ITEM + SMALL_MARGIN;
    for X := 0 to List.Count - 1 do
      if TNiceSeries(List[X]).Active then
      begin
        Temp.Text := Trim(TNiceSeries(List[X]).FCaption);
        t := RcLegend.Top + TNiceSeries(List[X]).Top;
        for Y := 0 to Temp.Count - 1 do
        begin
          TextOut(l, t + th, Trim(Temp[Y]));
          Inc(t, g);
        end;
      end;
  end;
end;

procedure TNiceChart.DrawTitle(ACanvas: TCanvas);
begin
  with ACanvas do
  begin
    Brush.Style := bsClear;
    Font.Assign(FTitleFont);
    DrawText(Handle, PChar(FTitle), Length(FTitle), RcTitle,
      DT_CENTER or DT_VCENTER or DT_WORDBREAK);
  end;
end;

procedure RotTextOut(ACanvas: TCanvas; X, Y, Angle: Integer; Txt: String);
var
  RotFont, OldFont: Integer;
  FBold, FItalic, FUnderline, FStrikeOut: Integer;
begin
  if (Txt = '') then
    Exit;
  SetBkMode(ACanvas.Handle, TRANSPARENT);
  if (fsItalic in ACanvas.Font.Style) then
    FItalic := 1
  else
    FItalic := 0;
  if (fsUnderline in ACanvas.Font.Style) then
    FUnderline := 1
  else
    FUnderline := 0;
  if (fsStrikeOut in ACanvas.Font.Style) then
    FStrikeOut := 1
  else
    FStrikeOut := 0;
  if (fsBold in ACanvas.Font.Style) then
    FBold := FW_BOLD
  else
    FBold := FW_NORMAL;
  RotFont := CreateFont(ACanvas.Font.Height, 0, Angle * 10, 0, FBold, FItalic,
    FUnderline, FStrikeOut, 1, 4, $10, ANTIALIASED_QUALITY, 4,
    PChar(ACanvas.Font.Name));
  OldFont := SelectObject(ACanvas.Handle, RotFont);
  TextOut(ACanvas.Handle, X, Y, PChar(Txt), Length(Txt));
  SelectObject(ACanvas.Handle, OldFont);
  DeleteObject(RotFont);
end;

procedure TNiceChart.InternalPaint(ACanvas: TCanvas);
begin
  with ACanvas do
  begin
    Pen.Color := clBlack;
    Pen.Width := 1;
    Brush.Style := bsSolid;
    Brush.Color := Color;
    FillRect(Rect(0, 0, DestWidth, DestHeight));
  end;
  if FShowLegend and (List.Count > 0) then
    DrawLegend(ACanvas);
  if FShowTitle and (FTitle <> '') then
    DrawTitle(ACanvas);
  DrawXAxis(ACanvas);
  DrawYAxis(ACanvas);
  DrawChart(ACanvas);
end;

procedure TNiceChart.Calculate(AWidth, AHeight: Integer);
var
  X, w, h, Y, g: Integer;
  Titled: boolean;

begin

  ClearAxis;

  DestWidth := AWidth;
  DestHeight := AHeight;
  RcChart := Rect(0, 0, DestWidth, DestHeight);
  MarkSize := Max(1, Round(DestWidth * 0.004));

  InflateRect(RcChart, -OUTER_MARGIN, -OUTER_MARGIN);

  Titled := False;
  if FShowTitle and (FTitle <> '') then
  begin
    Canvas.Font.Assign(TitleFont);
    w := Canvas.TextHeight(FTitle);
    RcTitle := Rect(RcChart.Left, RcChart.Top, RcChart.Right, RcChart.Left + w);
    DrawText(Canvas.Handle, PChar(FTitle), Length(FTitle), RcTitle,
      DT_CENTER or DT_VCENTER or DT_WORDBREAK or DT_CALCRECT);
    RcChart.Top := RcTitle.Bottom + INNER_MARGIN;
    Titled := true;
  end
  else
    SetRectEmpty(RcTitle);

  Canvas.Font.Assign(FNormalFont);
  h := Canvas.TextHeight('Ag');
  RcChart.Bottom := RcChart.Bottom - (2 * h) - INNER_MARGIN -
    (2 * SMALL_MARGIN);

  BuildYAxis;
  w := 0;
  for X := 0 to YAxis.Count - 1 do
    w := Max(w, Canvas.TextWidth(PAxisInfo(YAxis[X])^.Caption));
  RcChart.Left := RcChart.Left + h + INNER_MARGIN + w + (2 * SMALL_MARGIN);
  RcTitle.Left := RcChart.Left;
  RcTitle.Right := RcChart.Right;
  AdjustYAxis;

  if FShowLegend and (List.Count > 0) then
  begin
    Canvas.Font.Assign(FNormalFont);
    w := 0;
    h := INNER_MARGIN;
    g := Canvas.TextHeight('Ag');
    for X := 0 to List.Count - 1 do
      if TNiceSeries(List[X]).Active then
      begin
        TNiceSeries(List[X]).Top := h;
        Temp.Text := Trim(TNiceSeries(List[X]).FCaption);
        for Y := 0 to Temp.Count - 1 do
          w := Max(w, Canvas.TextWidth(Trim(Temp[Y])));
        h := h + Max(LEGEND_ITEM, Temp.Count * g);
        if (X <> List.Count - 1) then
          h := h + SMALL_MARGIN;
      end;
    w := w + (2 * INNER_MARGIN) + LEGEND_ITEM + SMALL_MARGIN;
    h := h + INNER_MARGIN;
    RcLegend := Rect(RcChart.Right - w, RcChart.Top, RcChart.Right,
      RcChart.Top + h);
    RcChart.Right := RcLegend.Left - (2 * INNER_MARGIN);
    if Titled then
      RcTitle.Right := RcChart.Right;
  end
  else
    SetRectEmpty(RcLegend);

  BuildXAxis;

  CalculateSeries;

end;

procedure TNiceChart.ClearAxis;
var
  X: Integer;
begin
  for X := 0 to XAxis.Count - 1 do
    Dispose(PAxisInfo(XAxis[X]));
  XAxis.Clear;
  for X := 0 to YAxis.Count - 1 do
    Dispose(PAxisInfo(YAxis[X]));
  YAxis.Clear;
end;

type
  PDoubleList = ^TDoubleList;
  TDoubleList = array [0 .. 0] of Double;

procedure QuickSortDouble(SortList: PDoubleList; l, R: Integer);
var
  I, J: Integer;
  P, t: Double;
begin
  repeat
    I := l;
    J := R;
    P := SortList^[(l + R) shr 1];
    repeat
      while (SortList^[I] < P) do
        Inc(I);
      while (SortList^[J] > P) do
        Dec(J);
      if I <= J then
      begin
        t := SortList^[I];
        SortList^[I] := SortList^[J];
        SortList^[J] := t;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if l < J then
      QuickSortDouble(SortList, l, J);
    l := I;
  until I >= R;
end;

function TNiceChart.GetLabel(Value: Double): string;
begin
  if (Value = UndefinedValueChart) then
    Result := '~'
  else
    Result := FormatFloat(Formatter, Value);
end;

procedure TNiceChart.BuildXAxis;
var
  X, Y, w: Integer;
  mi, ma: Double;
  Cnt, I, n: Integer;
  Delta, Lowest, l: Double;
  P: PAxisInfo;
  Temp: PDoubleList;
  Vals: TList;
  Last: Double;
  Scale: Double;
  dx: Integer;

begin

  if (List.Count = 0) or ChartEmpty then
    Exit;

  BarCount := 0;
  for X := 0 to List.Count - 1 do
  begin
    if TNiceSeries(List[X]).FActive and (TNiceSeries(List[X]).FKind = skBar)
    then
      Inc(BarCount);
  end;
  if (BarCount > 0) then
    FAxisXOnePerValue := true;

  if FAxisXOnePerValue then
  begin
    w := RcChart.Right - RcChart.Left;
    Cnt := 0;
    for X := 0 to List.Count - 1 do
      Cnt := Cnt + Series[X].Values.Count;
    GetMem(Temp, Cnt * SizeOf(Double));
    I := 0;
    for X := 0 to List.Count - 1 do
      if TNiceSeries(List[X]).FActive then
      begin
        Vals := TNiceSeries(List[X]).Values;
        for Y := 0 to Vals.Count - 1 do
        begin
          Temp^[I] := PXYInfo(Vals[Y])^.X;
          Inc(I);
        end;
      end;
    QuickSortDouble(Temp, 0, Cnt - 1);
    n := 0;
    Last := MaxDouble;
    for X := 0 to Cnt - 1 do
    begin
      l := Temp^[X];
      if (l = Last) then
        Continue;
      Inc(n);
      Last := l;
    end;
    if (BarCount > 0) then
    begin
      Scale := w / n;
      dx := Round(Scale / 2);
      BarWidth := Round(Scale);
    end
    else
    begin
      Scale := w / (n - 1);
      dx := 0;
    end;
    Last := MaxDouble;
    I := 0;
    for X := 0 to Cnt - 1 do
    begin
      l := Temp^[X];
      if (l = Last) then
        Continue;
      P := New(PAxisInfo);
      P^.Value := l;
      P^.Py := RcChart.Bottom;
      P^.Px := RcChart.Left + dx + Round(I * Scale);
      P^.Caption := GetLabel(l / FAxisXScale);
      XAxis.Add(P);
      Last := l;
      Inc(I);
    end;
    FreeMem(Temp);
  end
  else
  begin
    w := RcChart.Right - RcChart.Left;
    Cnt := (w div AXIS_DEFSIZE) + 1;
    mi := MaxDouble;
    ma := -MaxDouble;
    for X := 0 to List.Count - 1 do
      if Series[X].Active then
      begin
        mi := Min(mi, Series[X].GetMinXValue);
        ma := Max(ma, Series[X].GetMaxXValue);
      end;
    CalculateAxis(mi, ma, Cnt, Delta, Lowest);
    Scale := w / (Delta * Max(1, Cnt - 1));
    for X := 0 to Cnt - 1 do
    begin
      l := X * Delta;
      P := New(PAxisInfo);
      P^.Py := RcChart.Bottom;
      P^.Px := RcChart.Left + Round(l * Scale);
      P^.Caption := GetLabel((Lowest + l) / FAxisXScale);
      XAxis.Add(P);
    end;
    FXTranslate.MinValue := Lowest;
    FXTranslate.Scale := Scale;
    FXTranslate.Base := RcChart.Left;
  end;

end;

procedure TNiceChart.BuildYAxis;
var
  X, w: Integer;
  mi, ma: Double;
  Cnt: Integer;
  Delta, Lowest, t: Double;
  P: PAxisInfo;
  Scale: Double;
begin
  if (List.Count = 0) then
    Exit;
  w := RcChart.Bottom - RcChart.Top;
  Cnt := (w div AXIS_DEFSIZE) + 1;
  ChartEmpty := true;
  mi := MaxDouble;
  ma := -MaxDouble;
  for X := 0 to List.Count - 1 do
  begin
    if Series[X].Active and (Series[X].Values.Count > 0) then
    begin
      mi := Min(mi, Series[X].GetMinYValue);
      ma := Max(ma, Series[X].GetMaxYValue);
      ChartEmpty := False;
    end;
  end;
  if ChartEmpty then
    Exit;
  CalculateAxis(mi, ma, Cnt, Delta, Lowest);
  Scale := w / (Delta * Max(1, Cnt - 1));
  for X := 0 to Cnt - 1 do
  begin
    t := X * Delta;
    P := New(PAxisInfo);
    P^.Value := Lowest + t;
    P^.Py := Round(t * Scale);
    P^.Caption := GetLabel((Lowest + t) / FAxisYScale);
    YAxis.Add(P);
  end;
  FYTranslate.MinValue := Lowest;
  FYTranslate.Scale := Scale;
end;

procedure TNiceChart.AdjustYAxis;
var
  X: Integer;
  P: PAxisInfo;
  l: Integer;
begin
  l := RcChart.Left;
  YZero := -1;
  for X := 0 to YAxis.Count - 1 do
  begin
    P := PAxisInfo(YAxis[X]);
    P^.Px := l;
    P^.Py := RcChart.Bottom - P^.Py;
    if (P^.Value = 0) then
      YZero := P^.Py;
  end;
  if (YZero = -1) then
    YZero := RcChart.Bottom;
  FYTranslate.Base := RcChart.Bottom;
end;

procedure TNiceChart.DrawXAxis(ACanvas: TCanvas);
var
  l, t, w, X: Integer;
  P: PAxisInfo;
  Str: string;
  Last: Integer;
begin
  with ACanvas do
  begin
    Pen.Style := psSolid;
    Pen.Width := 3;
    MoveTo(RcChart.Left, RcChart.Bottom);
    LineTo(RcChart.Right, RcChart.Bottom);
    Font.Assign(FNormalFont);
    Font.Style := [fsBold];
    w := RcChart.Right - RcChart.Left;
    t := RcChart.Bottom + INNER_MARGIN + (2 * SMALL_MARGIN) + TextHeight('Ag');
    l := RcChart.Left + ((w - TextWidth(FAxisXTitle)) div 2);
    TextOut(l, t, FAxisXTitle);
    Font.Assign(FNormalFont);
    Pen.Color := clBlack;
    Pen.Width := 1;
    Pen.Style := psSolid;
    t := RcChart.Bottom + (2 * SMALL_MARGIN);
    Last := 0;
    for X := 0 to XAxis.Count - 1 do
    begin
      P := PAxisInfo(XAxis[X]);
      Str := P^.Caption;
      w := TextWidth(Str);
      l := P^.Px - (w div 2);
      if (Last < l) then
      begin
        TextOut(l, t, Str);
        Last := l + w;
      end;
      MoveTo(P^.Px, P^.Py);
      LineTo(P^.Px, P^.Py + SMALL_MARGIN);
    end;
    if FShowXGrid then
    begin
      Pen.Style := psDot;
      Pen.Color := clGray;
      t := RcChart.Top;
      for X := 1 to XAxis.Count - 2 do
      begin
        P := PAxisInfo(XAxis[X]);
        MoveTo(P^.Px, P^.Py);
        LineTo(P^.Px, t);
      end;
      Pen.Color := clBlack;
    end;
  end;
end;

procedure TNiceChart.DrawYAxis(ACanvas: TCanvas);
var
  l, t, h, w: Integer;
  X: Integer;
  Str: string;
  P: PAxisInfo;
begin
  with ACanvas do
  begin
    Pen.Style := psSolid;
    Pen.Width := 3;
    MoveTo(RcChart.Left, RcChart.Top);
    LineTo(RcChart.Left, RcChart.Bottom);
    h := RcChart.Bottom - RcChart.Top;
    l := OUTER_MARGIN;
    Font.Assign(FNormalFont);
    Font.Style := [fsBold];
    t := RcChart.Bottom - ((h - TextWidth(FAxisYTitle)) div 2);
    RotTextOut(ACanvas, l, t, 90, FAxisYTitle);
    Font.Assign(FNormalFont);
    Pen.Color := clBlack;
    Pen.Width := 1;
    Pen.Style := psSolid;
    l := RcChart.Left - (2 * SMALL_MARGIN);
    for X := 0 to YAxis.Count - 1 do
    begin
      P := PAxisInfo(YAxis[X]);
      Str := P^.Caption;
      w := TextWidth(Str);
      h := TextHeight(Str);
      t := P^.Py - (h div 2);
      TextOut(l - w, t, Str);
      MoveTo(P^.Px - SMALL_MARGIN, P^.Py);
      LineTo(P^.Px, P^.Py);
    end;
    if FShowYGrid then
    begin
      l := RcChart.Right;
      for X := 1 to YAxis.Count - 2 do
      begin
        P := PAxisInfo(YAxis[X]);
        if (P^.Value = 0) then
        begin
          Pen.Style := psSolid;
          Pen.Color := clBlack;
        end
        else
        begin
          Pen.Style := psDot;
          Pen.Color := clGray;
        end;
        MoveTo(P^.Px, P^.Py);
        LineTo(l, P^.Py);
      end;
      Pen.Color := clBlack;
    end;
  end;
end;

procedure TNiceChart.DrawChart(ACanvas: TCanvas);
var
  X: Integer;
begin
  with ACanvas do
  begin
    Brush.Style := bsClear;
    Pen.Style := psSolid;
    Pen.Width := 1;
    MoveTo(RcChart.Left, RcChart.Top);
    LineTo(RcChart.Right, RcChart.Top);
    LineTo(RcChart.Right, RcChart.Bottom);
  end;
  for X := 0 to List.Count - 1 do
  begin
    if TNiceSeries(List[X]).Active and (TNiceSeries(List[X]).FKind = skBar) then
      DrawSeries(ACanvas, X);
  end;
  for X := 0 to List.Count - 1 do
  begin
    if TNiceSeries(List[X]).Active and (TNiceSeries(List[X]).FKind <> skBar)
    then
      DrawSeries(ACanvas, X);
  end;
end;


// -----------------------------------------------------------------------------//

procedure MarkerRectangle(ACanvas: TCanvas; X, Y, Size: Integer);
begin
  ACanvas.Rectangle(X - Size, Y - Size, X + Size, Y + Size);
end;

procedure MarkerCircle(ACanvas: TCanvas; X, Y, Size: Integer);
begin
  ACanvas.Ellipse(X - Size, Y - Size, X + Size, Y + Size);
end;

procedure MarkerTriangle1(ACanvas: TCanvas; X, Y, Size: Integer);
begin
  ACanvas.Polygon([Point(X, Y - Size), Point(X + Size, Y + Size),
    Point(X - Size, Y + Size)]);
end;

procedure MarkerTriangle2(ACanvas: TCanvas; X, Y, Size: Integer);
begin
  ACanvas.Polygon([Point(X + Size, Y - Size), Point(X - Size, Y - Size),
    Point(X, Y + Size)]);
end;

procedure MarkerDiamond(ACanvas: TCanvas; X, Y, Size: Integer);
begin
  ACanvas.Polygon([Point(X, Y - Size), Point(X + Size, Y), Point(X, Y + Size),
    Point(X - Size, Y)]);
end;

const
  Colors1: array [0 .. 13] of TColor = (clRed, clBlue, clGreen, clFuchsia,
    clNavy, clMaroon, clBlack, clOlive, clPurple, clTeal, clGray, clLime,
    clYellow, clAqua);
  Colors2: array [0 .. 13] of TColor = ($0066C2FF, $005AFADA, $00F4C84D,
    $00B54DF4, $00669FFF, $00F44D5A, $0066E0FF, $0066FFFF, $00F44DAE, $006863FE,
    $004DF474, $00F4934D, clSilver, clGray);

  Markers: array [0 .. 4] of TMarkerProc = (MarkerRectangle, MarkerCircle,
    MarkerTriangle1, MarkerTriangle2, MarkerDiamond);

procedure TNiceChart.AutoColors(ACanvas: TCanvas; Index: Integer;
  IsBar: boolean);
var
  cl: TColor;
  Idx: Integer;
  Bmp: TBitmap;
begin
  if FMonochrome then
    cl := clBlack
  else if FSoftColors then
    cl := Colors2[Index mod 14]
  else
    cl := Colors1[Index mod 14];
  Marker := Markers[Index mod 5];
  with ACanvas do
  begin
    Pen.Color := cl;
    Brush.Bitmap := nil;
    Brush.Style := bsSolid;
    if IsBar then
      Brush.Color := cl
    else
      Brush.Color := clWhite;
    if IsBar and FMonochrome then
    begin
      Idx := Index mod 16;
      if not Assigned(Brushes[Idx]) then
      begin
        Bmp := TBitmap.Create;
        Bmp.LoadFromResourceName(hInstance, Format('brush%.2d', [Idx + 1]));
        Brushes[Idx] := Bmp;
      end;
      Brush.Bitmap := Brushes[Idx];
    end;
  end;
end;

// -----------------------------------------------------------------------------//

procedure TNiceChart.DrawSeries(ACanvas: TCanvas; Index: Integer);
var
  X: Integer;
  P: PXYInfo;
  l, t, t2: Integer;
  Sr: TNiceSeries;
  Rc: TRect;
begin
  Sr := TNiceSeries(List[Index]);
  if Sr.Active then
  begin
    AutoColors(ACanvas, Index, Sr.FKind = skBar);
    with ACanvas do
    begin
      if (Sr.FKind = skBar) then
      begin
        for X := 0 to Sr.Values.Count - 1 do
        begin
          P := PXYInfo(Sr.Values[X]);
          Rectangle(P^.Rc);
        end;
      end
      else
      begin
        if (Sr.FKind = skLine) then
        begin
          for X := 0 to Sr.Values.Count - 1 do
          begin
            P := PXYInfo(Sr.Values[X]);
            if (X = 0) then
              MoveTo(P^.Px, P^.Py)
            else
              LineTo(P^.Px, P^.Py);
          end;
        end
        else if (Sr.FKind = skSmooth) then
          Sr.Spline.Draw(ACanvas);
        for X := 0 to Sr.Values.Count - 1 do
        begin
          P := PXYInfo(Sr.Values[X]);
          Marker(ACanvas, P^.Px, P^.Py, MarkSize);
        end;
      end;
      if FShowLegend then
      begin
        l := RcLegend.Left + INNER_MARGIN;
        t := RcLegend.Top + Sr.Top;
        if (Sr.FKind = skBar) then
        begin
          Rc := Rect(l, t, l + LEGEND_ITEM, t + LEGEND_ITEM);
          InflateRect(Rc, -2, -2);
          Rectangle(Rc);
        end
        else
        begin
          t2 := t + (LEGEND_ITEM div 2);
          MoveTo(l, t2);
          LineTo(l + LEGEND_ITEM, t2);
          Marker(ACanvas, l + (LEGEND_ITEM div 2), t2, MarkSize);
        end;
      end;
    end;
  end;
end;

procedure TNiceChart.CalculateSeries;
var
  X, Y: Integer;
  Values: TList;
  P: PXYInfo;
  S: TBSpline;
  Vertex: TVertex;
  Sr: TNiceSeries;
  bw, rw, bi, dx, l: Integer;
begin
  if (List.Count = 0) or ChartEmpty then
    Exit;
  bi := 0;
  bw := 0;
  if (BarCount > 0) then
    bw := Round(BarWidth / (BarCount + 1));
  for X := 0 to List.Count - 1 do
  begin
    Sr := TNiceSeries(List[X]);
    if Sr.Active then
    begin
      S := Sr.Spline;
      S.Clear;
      Values := Sr.Values;
      case Sr.FKind of
        skBar:
          begin
            dx := Round(-(BarWidth / 2) + (bw / 2) + (bi * bw) + (bw * 0.1));
            rw := Round(bw * 0.8);
            for Y := 0 to Values.Count - 1 do
            begin
              P := PXYInfo(Values[Y]);
              ChartToClient(P^.X, P^.Y, P^.Px, P^.Py);
              l := P^.Px + dx;
              if (P^.Y < 0) then
                P^.Rc := Rect(l, YZero, l + rw, P^.Py)
              else
                P^.Rc := Rect(l, P^.Py, l + rw, YZero);
            end;
            Inc(bi);
          end;
        skLine:
          begin
            for Y := 0 to Values.Count - 1 do
            begin
              P := PXYInfo(Values[Y]);
              ChartToClient(P^.X, P^.Y, P^.Px, P^.Py);
              P^.Rc := Rect(P^.Px - MarkSize, P.Py - MarkSize, P^.Px + MarkSize,
                P^.Py + MarkSize);
            end;
          end;
        skSmooth:
          begin
            for Y := 0 to Values.Count - 1 do
            begin
              P := PXYInfo(Values[Y]);
              ChartToClient(P^.X, P^.Y, P^.Px, P^.Py);
              P^.Rc := Rect(P^.Px - MarkSize, P.Py - MarkSize, P^.Px + MarkSize,
                P^.Py + MarkSize);
              Vertex.X := P^.Px;
              Vertex.Y := P^.Py;
              S.AddPoint(Vertex);
            end;
            S.Interpolated := true;
            S.Fragments := S.NumberOfPoints * 20;
          end;
      end;
    end;
  end;
end;

procedure TNiceChart.ChartToClient(const AX, AY: Double; var X, Y: Integer);
var
  I: Integer;
begin
  if FAxisXOnePerValue then
  begin
    for I := 0 to XAxis.Count - 1 do
    begin
      if (AX = PAxisInfo(XAxis[I])^.Value) then
      begin
        X := PAxisInfo(XAxis[I])^.Px;
        Break;
      end;
    end;
  end
  else
    X := FXTranslate.Base + Round((AX - FXTranslate.MinValue) *
      FXTranslate.Scale);
  Y := FYTranslate.Base - Round((AY - FYTranslate.MinValue) *
    FYTranslate.Scale);
end;

function TNiceChart.ClientToChart(const X, Y: Integer;
  var AX, AY: Double): boolean;
var
  I: Integer;
  n, d: Integer;
begin
  Result := PtInRect(RcChart, Point(X, Y));
  if Result then
  begin
    if FAxisXOnePerValue then
    begin
      n := MaxInt;
      for I := 0 to XAxis.Count - 1 do
      begin
        d := Abs(X - PAxisInfo(XAxis[I])^.Px);
        if (d < n) then
        begin
          AX := PAxisInfo(XAxis[I])^.Value;
          n := d;
        end;
      end;
    end
    else
      AX := FXTranslate.MinValue + ((X - FXTranslate.Base) / FXTranslate.Scale);
    AY := FYTranslate.MinValue + ((FYTranslate.Base - Y) / FYTranslate.Scale);
  end;
end;

function TNiceChart.CreateMetafile: TMetafile;
const
  InitWidth = 800;
  InitHeight = 600;
var
  mc: TMetafileCanvas;
  AWidth, AHeight: Integer;
begin
  AWidth := InitWidth;
  AHeight := InitHeight;
  Calculate(AWidth, AHeight);
  if (RcLegend.Bottom > (AHeight - OUTER_MARGIN)) then
    AHeight := RcLegend.Bottom + OUTER_MARGIN;
  if ((RcChart.Right - RcChart.Left) < (RcChart.Bottom - RcChart.Top)) then
    AWidth := AWidth + ((RcChart.Bottom - RcChart.Top) -
      (RcChart.Right - RcChart.Left));
  if (AWidth <> InitWidth) or (AHeight <> InitHeight) then
    Calculate(AWidth, AHeight);
  Result := TMetafile.Create;
  Result.Width := AWidth;
  Result.Height := AHeight;
  mc := TMetafileCanvas.Create(Result, 0);
  InternalPaint(mc);
  mc.Free;
  Calculate(ClientWidth, ClientHeight);
end;

procedure TNiceChart.CopyToClipboard;
var
  Wmf: TMetafile;
begin
  Wmf := CreateMetafile;
  Clipboard.Assign(Wmf);
  Wmf.Free;
end;

end.
