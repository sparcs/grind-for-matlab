unit NoInifileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls;

type
  TNoInifileDialog = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    SaveDialog1: TSaveDialog;
    InifileEdit: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NoInifileDialog: TNoInifileDialog;

implementation

uses ModelDialog;

{$R *.DFM}

procedure TNoInifileDialog.Button1Click(Sender: TObject);
begin
  begin
    ModelDlg.SaveDialog1.InitialDir := ModelDlg.ThePath;
    ModelDlg.SaveDialog1.FileName := InifileEdit.text;
    if ModelDlg.SaveDialog1.Execute then
      InifileEdit.text := ModelDlg.SaveDialog1.FileName;
  end;

end;

end.
