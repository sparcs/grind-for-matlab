unit FindFileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids,  DirOutln, Spin, Vcl.FileCtrl;

type
  TFindFileDialog = class(TForm)
    DirectoryListBox: TDirectoryListBox;
    FileListBox: TFileListBox;
    FileEdit: TEdit;
    FoundList: TListBox;
    PreviewMemo: TMemo;
    Label2: TLabel;
    DirLabel: TLabel;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    SearchEdit: TEdit;
    SearchButton: TButton;
    SearchLabel: TLabel;
    ParMemo: TMemo;
    ModelMemo: TMemo;
    SchemeCheck: TCheckBox;
    DriveComboBox1: TDriveComboBox;
    FilterComboBox: TFilterComboBox;
    Label5: TLabel;
    Button1: TButton;
    PreviewLabel: TLabel;
    procedure FileListBoxClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FoundListClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FileListBoxDblClick(Sender: TObject);
    procedure SearchButtonClick(Sender: TObject);
  private
    DirsList: TStringList;
    curfile: string;
    curdir: string;
    function GetFileName: string;
    function GetInitialDir: string;
    procedure SetInitialDir(const Value: string);
    procedure SetFileName(const Value: string);
    { Private declarations }
  public
    function Execute: boolean;
    property InitialDir: string read GetInitialDir write SetInitialDir;
    property Filename: string read GetFileName write SetFileName;
    { Public declarations }
  end;

var
  FindFileDialog: TFindFileDialog;

implementation
uses System.UITypes;
{$R *.DFM}

procedure TFindFileDialog.FileListBoxClick(Sender: TObject);
var
  i, n: integer;
  s: string;
begin
  if FileListBox.ItemIndex >= 0 then
    with FileListBox do
    begin
      if (curfile <> '') and (PreviewMemo.Lines.IndexOf('%scheme') > 0) and
        not SchemeCheck.Checked then
        if (MessageDlg
          (format('Are you sure to delete the scheme definition in "%s"?',
          [curfile]), mtConfirmation, [mbYes, mbNo], 0) = idNo) then
          SchemeCheck.Checked := True
        else
          ModelMemo.Modified := True;
      if (curfile <> '') and (ModelMemo.Modified or ParMemo.Modified) then
        if MessageDlg(format('Save changes in preview to "%s"?', [curfile]),
          mtConfirmation, [mbYes, mbNo], 0) = idYes then
        begin
          s := '%model'#13#10 + ModelMemo.Text + '%commands'#13#10 +
            ParMemo.Text;
          if SchemeCheck.Checked then
          begin
            n := PreviewMemo.Lines.IndexOf('%scheme');
            if n > 0 then
              for i := n to PreviewMemo.Lines.Count - 1 do
                s := s + PreviewMemo.Lines[i] + #13#10;
          end;
          PreviewMemo.Text := s;
          PreviewMemo.Lines.SaveToFile(curdir + '\' + curfile);
        end;
      curdir := DirectoryListBox.Directory;
      curfile := Items[ItemIndex];
      PreviewMemo.Lines.LoadFromFile(curdir + '\' + curfile);
      FileListBox.Hint := curdir + '\' + curfile;
      ModelMemo.Clear;
      ParMemo.Clear;
      if PreviewMemo.Lines[0] = '%model' then
      begin
        i := 1;
        while (i < PreviewMemo.Lines.Count) and
          (PreviewMemo.Lines[i] <> '%commands') do
        begin
          ModelMemo.Lines.Add(PreviewMemo.Lines[i]);
          inc(i);
        end;
        inc(i);
        while (i < PreviewMemo.Lines.Count) and
          (PreviewMemo.Lines[i] <> '%scheme') do
        begin
          ParMemo.Lines.Add(PreviewMemo.Lines[i]);
          inc(i);
        end;
        SchemeCheck.Checked := i < PreviewMemo.Lines.Count;
        ModelMemo.Modified := False;
        ModelMemo.SelStart := 0;
        ModelMemo.SelLength := 0;
        ParMemo.Modified := False;
        ParMemo.SelStart := 0;
        ParMemo.SelLength := 0;
      end;
    end;
end;

procedure TFindFileDialog.Button1Click(Sender: TObject);
var
  rec: TSearchRec;
  extens: string;
  mask, s: string;

  procedure FindFiles(dir: string; var rec: TSearchRec);
  var
    arec: TSearchRec;
  begin
    dir := stringreplace(dir + '\', '\\', '\', [rfReplaceAll]);
    if FindFirst(dir + mask, faArchive, rec) = 0 then
      repeat
        if (rec.Attr = faArchive) and
          ((extens = '') or (ExtractFileExt(rec.name) = extens)) then
        begin
          FoundList.Items.Add(rec.name);
          DirsList.Add(dir);
        end;
      until findNext(rec) <> 0;
    FindClose(rec);
    if FindFirst(dir + '*.', faDirectory, rec) = 0 then
      while findNext(rec) = 0 do
      begin
        if (rec.Attr and faDirectory > 0) and (rec.name <> '..') and
          (rec.name <> '.') then
          FindFiles(dir + rec.name, arec);
      end;
    FindClose(rec);
  end;

begin
  Screen.Cursor := crHourglass;
  try
    extens := ExtractFileExt(FilterComboBox.mask);
    if extens = '.*' then
      extens := '';
    FoundList.Items.Clear;
    DirsList.Clear;
    s := SearchEdit.Text;
    repeat
      if pos(';', s) = 0 then
      begin
        mask := s;
        s := '';
      end
      else
      begin
        mask := copy(s, 1, pos(';', s) - 1);
        s := copy(s, pos(';', s) + 1, length(s));
      end;
      if (pos('*', mask) = 0) and (pos('?', mask) = 0) then
        mask := '*' + mask + '*';
      FindFiles(DirectoryListBox.Directory, rec);
    until s = '';
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFindFileDialog.FormCreate(Sender: TObject);
begin
  DirsList := TStringList.Create;
  DirsList.Sorted := False;
  SearchButtonClick(Sender);
end;

procedure TFindFileDialog.FormDestroy(Sender: TObject);
begin
  DirsList.Free;
end;

procedure TFindFileDialog.FoundListClick(Sender: TObject);
begin
  if FoundList.ItemIndex >= 0 then
  begin
    DirectoryListBox.Directory := DirsList[FoundList.ItemIndex];
    FileEdit.Text := FoundList.Items[FoundList.ItemIndex];
    FileListBox.ItemIndex := FileListBox.Items.IndexOf(FileEdit.Text);
    FileListBox.OnClick(FileListBox);
    FoundList.Hint := curdir + '\' + curfile;
  end;
end;

function TFindFileDialog.Execute: boolean;
begin
  Result := (ShowModal = mrOK);
end;

function TFindFileDialog.GetFileName: string;
begin
  Result := FileEdit.Text;
end;

function TFindFileDialog.GetInitialDir: string;
begin
  Result := DirectoryListBox.Directory;
end;

procedure TFindFileDialog.SetInitialDir(const Value: string);
begin
  DirectoryListBox.Directory := Value;
end;

procedure TFindFileDialog.SetFileName(const Value: string);
begin
  FileEdit.Text := Value;
  FileListBox.ItemIndex := FileListBox.Items.IndexOf(Value);
  FileListBox.OnClick(FileListBox);

end;

procedure TFindFileDialog.FormResize(Sender: TObject);
begin
  ModelMemo.Height := (SchemeCheck.Top - ModelMemo.Top - 20) div 2;
  ParMemo.Height := ModelMemo.Height;
  ParMemo.Top := ModelMemo.Top + ModelMemo.Height + 10;
end;

procedure TFindFileDialog.FileListBoxDblClick(Sender: TObject);
begin
  OKBtn.Click;
end;

procedure TFindFileDialog.SearchButtonClick(Sender: TObject);
begin
  ModelMemo.Visible := SearchButton.Caption <> 'Search';
  ParMemo.Visible := ModelMemo.Visible;
  SchemeCheck.Visible := ModelMemo.Visible;
  PreviewLabel.Visible := ModelMemo.Visible;
  FoundList.Visible := not ModelMemo.Visible;
  SearchEdit.Visible := not ModelMemo.Visible;
  SearchLabel.Visible := not ModelMemo.Visible;
  Button1.Visible := not ModelMemo.Visible;
  if ModelMemo.Visible then
    SearchButton.Caption := 'Search'
  else
    SearchButton.Caption := 'Preview';
end;

end.
