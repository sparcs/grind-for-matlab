object TePropDialog: TTePropDialog
  Left = 150
  Top = 127
  Caption = 'Graph properties'
  ClientHeight = 390
  ClientWidth = 551
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 551
    Height = 332
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Series'
      object SeriesColorShape: TShape
        Left = 237
        Top = 134
        Width = 27
        Height = 20
        Brush.Color = clBlue
      end
      object Label5: TLabel
        Left = 20
        Top = 102
        Width = 20
        Height = 13
        Caption = 'Title'
      end
      object StyleLabel: TLabel
        Left = 24
        Top = 176
        Width = 26
        Height = 13
        Caption = 'Style:'
      end
      object SizeLabel: TLabel
        Left = 224
        Top = 176
        Width = 23
        Height = 13
        Caption = 'Size:'
      end
      object BorderColorShape: TShape
        Left = 85
        Top = 134
        Width = 27
        Height = 20
        Brush.Color = clBlue
      end
      object SeriesCombo: TComboBox
        Left = 20
        Top = 13
        Width = 333
        Height = 21
        Style = csDropDownList
        TabOrder = 0
        OnChange = SeriesComboChange
      end
      object SeriesColorButton: TButton
        Left = 276
        Top = 134
        Width = 76
        Height = 24
        Caption = 'Color'
        TabOrder = 1
        OnClick = SeriesColorButtonClick
      end
      object SeriesTitle: TEdit
        Left = 91
        Top = 95
        Width = 262
        Height = 21
        TabOrder = 2
        OnChange = SeriesTitleChange
      end
      object PointStyleCombo: TComboBox
        Left = 88
        Top = 168
        Width = 89
        Height = 21
        Style = csDropDownList
        TabOrder = 3
        Items.Strings = (
          'Rectangle'
          'Circle'
          'Triangle'
          'Down Triangle'
          'Cross'
          'DiagCross'
          'Star'
          'Diamond'
          'Small Dot')
      end
      object SizeEdit: TEdit
        Left = 272
        Top = 168
        Width = 65
        Height = 21
        TabOrder = 4
        Text = '0'
      end
      object SizeUpDown: TUpDown
        Left = 337
        Top = 168
        Width = 15
        Height = 21
        Associate = SizeEdit
        TabOrder = 5
      end
      object BorderColorBtn: TButton
        Left = 124
        Top = 134
        Width = 76
        Height = 24
        Caption = 'Border color'
        TabOrder = 6
        OnClick = BorderColorBtnClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Legend'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object LegAlignmentRadio: TRadioGroup
        Left = 39
        Top = 78
        Width = 137
        Height = 137
        Caption = 'Position'
        Items.Strings = (
          'Left'
          'Right'
          'Top'
          'Bottom')
        TabOrder = 0
      end
      object LegVisibleCheck: TCheckBox
        Left = 39
        Top = 39
        Width = 79
        Height = 14
        Caption = 'Visible'
        TabOrder = 1
      end
      object LegendFontButton: TButton
        Left = 176
        Top = 33
        Width = 75
        Height = 24
        Caption = 'Font'
        TabOrder = 2
        OnClick = LegendFontButtonClick
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Axes'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label3: TLabel
        Left = 156
        Top = 39
        Width = 20
        Height = 13
        Caption = 'Title'
      end
      object GroupBox1: TGroupBox
        Left = 156
        Top = 72
        Width = 287
        Height = 156
        Caption = 'Scale'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 8
        object Label7: TLabel
          Left = 13
          Top = 33
          Width = 41
          Height = 13
          Caption = 'Minimum'
        end
        object Label1: TLabel
          Left = 13
          Top = 55
          Width = 44
          Height = 13
          Caption = 'Maximum'
        end
        object Label2: TLabel
          Left = 13
          Top = 78
          Width = 47
          Height = 13
          Caption = 'Increment'
        end
        object AutomaticCheck: TCheckBox
          Left = 72
          Top = 136
          Width = 97
          Height = 17
          Caption = 'Automatic scale'
          TabOrder = 0
        end
      end
      object AxisLogCheck: TCheckBox
        Left = 228
        Top = 182
        Width = 130
        Height = 14
        Caption = 'Logarithmic'
        TabOrder = 0
      end
      object AxesRadio: TRadioGroup
        Left = 26
        Top = 20
        Width = 111
        Height = 208
        Items.Strings = (
          'Left axis'
          'Right axis'
          'Top axis'
          'Bottom axis')
        TabOrder = 1
        OnClick = AxesRadioClick
      end
      object AxisMinEdit: TEdit
        Left = 228
        Top = 101
        Width = 98
        Height = 21
        TabOrder = 2
        OnChange = AxisMinEditChange
      end
      object AxisMaxEdit: TEdit
        Left = 228
        Top = 124
        Width = 98
        Height = 21
        TabOrder = 3
        OnChange = AxisMinEditChange
      end
      object TitleFontButton: TButton
        Left = 429
        Top = 7
        Width = 75
        Height = 24
        Caption = 'Font'
        TabOrder = 4
        OnClick = TitleFontButtonClick
      end
      object AxisTitleEdit: TEdit
        Left = 228
        Top = 33
        Width = 273
        Height = 21
        TabOrder = 5
        Text = 'AxisTitleEdit'
      end
      object AxisIncrEdit: TEdit
        Left = 228
        Top = 146
        Width = 98
        Height = 21
        TabOrder = 6
      end
      object LabelsFontButton: TButton
        Left = 345
        Top = 106
        Width = 74
        Height = 24
        Caption = 'Font'
        TabOrder = 7
        OnClick = LabelsFontButtonClick
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Titles'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label6: TLabel
        Left = 59
        Top = 33
        Width = 28
        Height = 13
        Caption = 'Titles:'
      end
      object TitlesMemo: TMemo
        Left = 59
        Top = 59
        Width = 371
        Height = 72
        TabOrder = 0
      end
      object TitlesFontButton: TButton
        Left = 371
        Top = 26
        Width = 60
        Height = 20
        Caption = 'Font'
        TabOrder = 1
        OnClick = TitlesFontButtonClick
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 256
    Top = 344
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 152
    Top = 344
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 448
    Top = 32
  end
  object ColorDialog: TColorDialog
    Left = 548
    Top = 107
  end
end
