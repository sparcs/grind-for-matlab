object SymbolDialog: TSymbolDialog
  Left = 559
  Top = 160
  Caption = 'Symbol?'
  ClientHeight = 163
  ClientWidth = 260
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 24
    Width = 109
    Height = 13
    Caption = 'What is in this column?'
  end
  object SymbolCombo: TComboBox
    Left = 48
    Top = 48
    Width = 185
    Height = 21
    Style = csDropDownList
    TabOrder = 0
  end
  object BitBtn1: TBitBtn
    Left = 144
    Top = 128
    Width = 89
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 32
    Top = 128
    Width = 89
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
end
