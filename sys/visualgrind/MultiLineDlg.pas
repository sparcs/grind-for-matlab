unit MultiLineDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CompFrm;

type
  TMultilineDialog = class(TForm)
    ExpressionMemo: TMemo;
    Btns: TCompFrame;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
  private
    function GetExpression: string;
    procedure SetExpression(Value: string);
    { Private declarations }
  public
    { Public declarations }
    property Expression: string read GetExpression write SetExpression;
  end;

var
  MultilineDialog: TMultilineDialog;

implementation

{$R *.DFM}
{ TForm1 }

function TMultilineDialog.GetExpression: string;
{ var
  i: integer; }
begin
  { Result := '';
    for i := 0 to ExpressionMemo.Lines.Count - 1 do
    if ExpressionMemo.Lines[i] <> '' then
    Result := Result + ExpressionMemo.Lines[i] + '\n';
    if Result <> '' then
    Result := copy(Result, 1, length(Result) - 2); }
  Result := ExpressionMemo.Lines.Text;
end;

procedure TMultilineDialog.SetExpression(Value: string);
begin
  ExpressionMemo.Lines.Clear;
  while pos('\n', Value) > 0 do
  begin
    ExpressionMemo.Lines.Add(copy(Value, 1, pos('\n', Value) - 1));
    Value := copy(Value, pos('\n', Value) + 2, length(Value));
  end;
  ExpressionMemo.Lines.Add(Value);
  ExpressionMemo.SelStart := 0;
  ExpressionMemo.SelLength := 0;
end;

procedure TMultilineDialog.FormCreate(Sender: TObject);
begin
  Btns.CurrentEdit := ExpressionMemo;
end;

end.
