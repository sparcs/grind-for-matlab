object ReorderDialog: TReorderDialog
  Left = 347
  Top = 120
  Caption = 'Reorder state variables'
  ClientHeight = 266
  ClientWidth = 269
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 154
    Height = 13
    Caption = 'Select state variables and move:'
  end
  object UpBtn: TBitBtn
    Left = 184
    Top = 88
    Width = 49
    Height = 25
    Caption = 'Up'
    Enabled = False
    TabOrder = 0
    OnClick = UpBtnClick
  end
  object DownBtn: TBitBtn
    Left = 184
    Top = 136
    Width = 49
    Height = 25
    Caption = 'Down'
    Enabled = False
    TabOrder = 1
    OnClick = DownBtnClick
  end
  object BitBtn3: TBitBtn
    Left = 72
    Top = 232
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object BitBtn4: TBitBtn
    Left = 160
    Top = 232
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 3
  end
  object VarsList: TListBox
    Left = 32
    Top = 48
    Width = 137
    Height = 169
    ItemHeight = 13
    TabOrder = 4
    OnClick = VarsListClick
  end
end
