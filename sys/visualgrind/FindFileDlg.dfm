object FindFileDialog: TFindFileDialog
  Left = 349
  Top = 236
  Caption = 'Load model'
  ClientHeight = 369
  ClientWidth = 689
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 24
    Top = 264
    Width = 48
    Height = 13
    Caption = 'File name:'
  end
  object DirLabel: TLabel
    Left = 24
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Search in:'
  end
  object SearchLabel: TLabel
    Left = 416
    Top = 8
    Width = 153
    Height = 13
    Caption = 'File name mask (or part of name)'
  end
  object Label5: TLabel
    Left = 24
    Top = 304
    Width = 59
    Height = 13
    Caption = 'Files of type:'
  end
  object PreviewLabel: TLabel
    Left = 400
    Top = 8
    Width = 38
    Height = 13
    Caption = 'Preview'
  end
  object DirectoryListBox: TDirectoryListBox
    Left = 24
    Top = 48
    Width = 177
    Height = 177
    FileList = FileListBox
    TabOrder = 0
  end
  object FileListBox: TFileListBox
    Left = 208
    Top = 24
    Width = 177
    Height = 201
    FileEdit = FileEdit
    ItemHeight = 13
    Mask = '*.ini'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = FileListBoxClick
    OnDblClick = FileListBoxDblClick
  end
  object FileEdit: TEdit
    Left = 96
    Top = 256
    Width = 289
    Height = 21
    TabOrder = 2
    Text = '*.ini'
  end
  object FoundList: TListBox
    Left = 416
    Top = 48
    Width = 241
    Height = 169
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = FoundListClick
    OnDblClick = FileListBoxDblClick
  end
  object PreviewMemo: TMemo
    Left = 208
    Top = 432
    Width = 277
    Height = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 4
    Visible = False
    WordWrap = False
  end
  object OKBtn: TBitBtn
    Left = 292
    Top = 336
    Width = 89
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 5
  end
  object CancelBtn: TBitBtn
    Left = 192
    Top = 336
    Width = 89
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 6
  end
  object SearchEdit: TEdit
    Left = 416
    Top = 24
    Width = 153
    Height = 21
    TabOrder = 7
  end
  object SearchButton: TButton
    Left = 92
    Top = 336
    Width = 89
    Height = 25
    Caption = 'Search files'
    TabOrder = 8
    OnClick = SearchButtonClick
  end
  object ParMemo: TMemo
    Left = 400
    Top = 128
    Width = 295
    Height = 103
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    WordWrap = False
  end
  object ModelMemo: TMemo
    Left = 400
    Top = 24
    Width = 295
    Height = 105
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    WordWrap = False
  end
  object SchemeCheck: TCheckBox
    Left = 400
    Top = 256
    Width = 193
    Height = 17
    Caption = 'Has scheme definition'
    TabOrder = 11
  end
  object DriveComboBox1: TDriveComboBox
    Left = 24
    Top = 24
    Width = 177
    Height = 19
    DirList = DirectoryListBox
    TabOrder = 12
  end
  object FilterComboBox: TFilterComboBox
    Left = 96
    Top = 296
    Width = 289
    Height = 21
    FileList = FileListBox
    Filter = 'Grind files (*.ini)|*.ini|All files (*.*)|*.*'
    TabOrder = 13
  end
  object Button1: TButton
    Left = 584
    Top = 24
    Width = 75
    Height = 21
    Hint = 'Search in sub-directories'
    Caption = 'Search'
    TabOrder = 14
    OnClick = Button1Click
  end
end
