object AboutBox: TAboutBox
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'About '
  ClientHeight = 210
  ClientWidth = 247
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object StaticText1: TStaticText
    Left = 40
    Top = 16
    Width = 145
    Height = 37
    Caption = 'Visual Grind'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object StaticText2: TStaticText
    Left = 40
    Top = 67
    Width = 114
    Height = 17
    Caption = 'Part of MATLAB GRIND'
    TabOrder = 1
  end
  object OKBtn: TBitBtn
    Left = 69
    Top = 159
    Width = 89
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 2
  end
  object StaticText3: TStaticText
    Left = 40
    Top = 90
    Width = 118
    Height = 17
    Caption = 'Author: Egbert van Nes'
    TabOrder = 3
  end
  object StaticText4: TStaticText
    Left = 40
    Top = 113
    Width = 62
    Height = 17
    Caption = 'Version: 1.2'
    TabOrder = 4
  end
end
