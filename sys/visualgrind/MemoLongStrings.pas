unit MemoLongStrings;

interface

uses vcl.StdCtrls, System.Classes;

type
  TMemoLongstrings = class(TStringlist)
  private
    FMemo: TMemo;
    FMaxLineLength: Integer;
    procedure MemoChange(sender: TObject);
    procedure UpdateMemo;
    procedure SetMemo(const Value: TMemo);
  protected
    procedure Changed; override;
  public
    constructor Create(aMemo: TMemo);
    property Memo: TMemo read FMemo write SetMemo;
  end;

implementation

uses System.SysUtils, Winapi.Windows, Winapi.Messages;
{ TMemoLongstrings }

procedure TMemoLongstrings.Changed;
begin
  if (UpdateCount = 0) then
    UpdateMemo;
end;

constructor TMemoLongstrings.Create(aMemo: TMemo);
begin
  inherited Create;
  FMemo := aMemo;
  if FMemo <> nil then
  begin
    OnChange := FMemo.OnChange;
    FMemo.OnChange := MemoChange;
  end;
  // MemoChange(nil);
  FMaxLineLength := 500;
end;

procedure TMemoLongstrings.MemoChange(sender: TObject);
var
  Sel1: Integer;
  Pos1: Integer;
  s: string;
begin
  if Assigned(OnChange) then
    OnChange(sender);
  if FMemo <> nil then
  begin
    s := FMemo.Lines.Text;
    s := StringReplace(s, #13#13#10, '', [rfReplaceAll]);
    Sel1 := FMemo.SelStart;
    Pos1 := FMemo.Perform(EM_GETFIRSTVISIBLELINE, 0, 0);
    // CharPos1 := FMemo.Perform(EM_CHARFROMPOS,0,0); //Doesnt work completely correct
    Text := s;
    UpdateMemo;
    // FMemo.Perform(EM_LINESCROLL,hiword(CharPos1),Pos1);
    FMemo.Perform(EM_LINESCROLL, 0, Pos1);
    FMemo.SelStart := Sel1;
    FMemo.Perform(EM_SCROLLCARET, 0, 0);
  end;
end;

procedure TMemoLongstrings.SetMemo(const Value: TMemo);
begin
  FMemo := Value;
  UpdateMemo;
end;

procedure TMemoLongstrings.UpdateMemo;
var
  i: Integer;
  s, s1: string;
begin
  if FMemo <> nil then
  begin
    s := '';
    for i := 0 to Count - 1 do
    begin
      s1 := Strings[i];
      if length(s1) > FMaxLineLength then
      begin
        while length(s1) > FMaxLineLength do
        begin
          s := s + copy(s1, 1, FMaxLineLength) + #13#13#10;
          s1 := copy(s1, FMaxLineLength + 1, length(s1));
        end;
        s := s + s1 + #13#10;
      end
      else
        s := s + s1 + #13#10;
    end;
    FMemo.OnChange := nil;
    if CompareStr(FMemo.Text, s) <> 0 then
      FMemo.Text := s;
    FMemo.OnChange := MemoChange;
  end;
end;

end.
