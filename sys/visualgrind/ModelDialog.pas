unit ModelDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, GrindComps, ComCtrls, DataFrm, MemoLongStrings;

type

  TModelDlg = class(TForm)
    Label1: TLabel;
    inifileEdit: TEdit;
    LoadButton: TButton;
    ModelMemo: TMemo;
    ParamMemo1: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    RunBtn: TBitBtn;
    BitBtn2: TBitBtn;
    dotButton: TButton;
    SchemeMemo1: TMemo;
    SaveDialog1: TSaveDialog;
    BitBtn3: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    XaxCombo: TComboBox;
    MinXaxEdit: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Yaxcombo: TComboBox;
    ZaxCombo: TComboBox;
    MinYaxEdit: TEdit;
    MinZaxEdit: TEdit;
    MaxXaxEdit: TEdit;
    MaxYaxEdit: TEdit;
    MaxZaxEdit: TEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    SolverCombo: TComboBox;
    Label7: TLabel;
    RelTolEdit: TEdit;
    AbsTolEdit: TEdit;
    TEdit: TEdit;
    outEdit: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    RelTolLabel: TLabel;
    AbsTolTable: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    StarttEdit: TEdit;
    tstepEdit: TEdit;
    nDaysEdit: TEdit;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label10: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    DefaultsBtn: TBitBtn;
    BitBtn1: TBitBtn;
    PlotNoEdit: TEdit;
    PlotNoUpDown: TUpDown;
    Label11: TLabel;
    Button5: TButton;
    StepSizeEdit: TEdit;
    procedure FormShow(Sender: TObject);
    procedure dotButtonClick(Sender: TObject);
    procedure RunBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LoadButtonClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure TabSheet2Enter(Sender: TObject);
    procedure StarttEditChange(Sender: TObject);
    procedure tEditChange(Sender: TObject);
    procedure outEditChange(Sender: TObject);
    procedure XaxComboChange(Sender: TObject);
    procedure YaxcomboChange(Sender: TObject);
    procedure ZaxComboChange(Sender: TObject);
    procedure SolverComboChange(Sender: TObject);
    procedure RelTolEditChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure inifileEditChange(Sender: TObject);
    procedure DefaultsBtnClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PlotNoEditChange(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    function readcommand(command: string; no: integer): string;
    procedure SolverComboUpdate(Sender: TObject);
  public
    SchemeLongMemo: TMemoLongstrings;
    ParamLongMemo: TMemoLongstrings;
    ThePath: string;
    OKstr: string;
    TheModel: TGrindModel;
    strings: TStringList;
    fixed_solvers: TStringList;
    procedure GetMemoStrings(MemoLines: TStrings);
    function IndexOfpar(command: string; str: TStrings): integer;
    procedure CompileModel;
    function LoadInifile: boolean;
    procedure saveinifile;
    function inifilename: string;
    { Public declarations }
  end;

var
  ModelDlg: TModelDlg;

implementation

uses visgrind, CompDialog, prgTools, NoInifileDlg, FindFileDlg, System.UITypes;

{$R *.DFM}

procedure TModelDlg.CompileModel;
const
  str_param = '%%Parameters:';
  str_init = '%%Initial values:';
  str_comm = '%%Commands:';
var
  i, i2, j, maxparlen: integer;
  comps: TList;
  pargroups: TStringList;
  s, s1: string;

  procedure AddParam(sym, expr, descr, unt: string; maxparlen: integer);
  var
    s, scolon: string;
  begin
    if expr <> '' then
    begin
      s := sym + ' = ';
      if expr[length(expr)] = ';' then
        scolon := ''
      else
        scolon := ';';
      if (descr <> '') or (unt <> '') then
        s := format('%-' + IntToStr(maxparlen + 16) + 's %%%s,%s',
          [format('%-' + IntToStr(maxparlen) + 's = %s%s', [sym, expr, scolon]),
          descr, unt])
      else
        s := format('%-' + IntToStr(maxparlen) + 's = %s%s',
          [sym, expr, scolon]);
      strings.insert(0, s);
    end;
  end;

  function DeleteSection(s: string): boolean;
  var
    i1, i2, i: integer;
  begin
    i1 := 0;
    while (i1 < strings.count) and (strings[i1] <> s) do
      inc(i1);
    Result := i1 < strings.count;
    if Result then
    begin
      i2 := i1 + 1;
      while (i2 < strings.count) and (strings[i2] <> str_comm) and
        (strings[i2] <> str_init) and (strings[i2] <> str_param) do
        inc(i2);
      for i := i2 - 1 downto i1 do
        strings.delete(i);
    end;
  end;
  function hasfromcomps(comp: TGrindComponent): boolean;
  var
    i: integer;
    fcomp: TGrindComponent;
  begin
    Result := False;
    for i := 0 to TheModel.Components.count - 1 do
      if TGrindComponent(TheModel.Components.Items[i]).From = comp then
      begin
        fcomp := TGrindComponent(TheModel.Components.Items[i]).ato;
        if (fcomp <> nil) and (fcomp.TheType in [ctStatevar, ctPermanentvar,
          ctAuxilvar, ctParameter, ctExternvar, ctFlow]) then
        begin
          Result := True;
          exit;
        end;
      end;
  end;

begin
  if TheModel <> nil then
  begin
    ParamLongMemo.BeginUpdate;
    SchemeLongMemo.BeginUpdate;
    try
      if TheModel.DataFileChanged and (ComponentDlg.ExternDatafile <> '') and
        (MessageDlg
        (format('The external data file (%s) has been edited, do you want to save the changes?',
        [ExtractFileName(ComponentDlg.ExternDatafile)]), mtConfirmation,
        [mbYes, mbNo], 0) <> mrNo) then
        ComponentDlg.SaveDataFile;
      ModelMemo.Lines.Clear;
      strings.Clear;
      for i := 0 to ComponentDlg.ModelMemo.Lines.count do
        if ComponentDlg.ModelMemo.Lines.strings[i] <> '' then
          strings.Add('%' + ComponentDlg.ModelMemo.Lines.strings[i]);
      if ComponentDlg.ModelMemo.Lines.count > 0 then
        strings.Add('%');
      comps := TheModel.CompsOfType(ctModelEquations);
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          if Expression <> '' then
            strings.Add(format('%s', [ExpressionFmt]));
      strings.text := strings.text;
      comps := TheModel.CompsOfType(ctExternvar);
      for i := 0 to comps.count - 1 do
        with TExternvar(comps.Items[i]) do
        begin
          s1 := format('defextern %s ', [Symbol]);
          s := format('defextern %s %s %s;',
            [Symbol, Expression, OutsideOption]);
          j := 0;
          while (j < strings.count) and
            (copy(strings[j], 1, length(s1)) <> s1) do
            inc(j);
          if j < strings.count then
            strings[j] := s
          else
            strings.Add(s);
        end;
      comps := TheModel.CompsOfType(ctPermanentvar);
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          if Expression <> '' then
            if Expression2 = '' then
              strings.Add(format('defpermanent %s;', [Symbol]))
            else
              strings.Add(format('defpermanent %s %s;', [Symbol, Expression2]));
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          if Expression <> '' then
            strings.Add(format('%s = %s;', [Symbol, ExpressionFmt]));

      comps := TheModel.CompsOfType(ctUserFunction);
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          if Expression <> '' then
            strings.Add(ExpressionFmt);
      comps := TheModel.CompsOfType(ctAuxilvar);
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          if Expression <> '' then
            strings.Add(format('%s = %s;', [Symbol, ExpressionFmt]));
      comps := TheModel.CompsOfType(ctFlow);
      for i := 0 to comps.count - 1 do
        with TFlow(comps.Items[i]) do
          if fromtoStatevar or hasfromcomps(TFlow(comps.Items[i])) then
          begin
            s := format('%s = %s', [Symbol, Expression]);
            if s <> '' then
              if s[length(s)] = ';' then
                strings.Add(s)
              else
                strings.Add(s + ';');
          end;
      comps := TheModel.CompsOfType(ctTrain);
      for i := 0 to comps.count - 1 do
        with TFlow(comps.Items[i]) do
          if fromtoStatevar or hasfromcomps(TFlow(comps.Items[i])) then
          begin
            s := format('%s = %s', [Symbol, Expression]);
            if s <> '' then
              if s[length(s)] = ';' then
                strings.Add(s)
              else
                strings.Add(s + ';');
          end;
      comps := TheModel.CompsOfType(ctStatevar);
      for i := 0 to comps.count - 1 do
        with TStatevar(comps.Items[i]) do
        begin
          s := EquationText;
          if s <> '' then
            if s[length(s)] = ';' then
              strings.Add(s)
            else
              strings.Add(s + ';');
        end;

      ModelMemo.Lines.text := strings.text;

      GetMemoStrings(ParamLongMemo);
      if DeleteSection(str_param) then
        DeleteSection(str_init)
      else
      begin
        comps := TheModel.CompsOfType(ctStatevar);
        for i := 0 to comps.count - 1 do
        begin
          i2 := IndexOfpar(TGrindComponent(comps.Items[i]).Symbol, strings);
          if i2 >= 0 then
            strings.delete(i2);
        end;
        comps := TheModel.CompsOfType(ctParameter);
        for i := 0 to comps.count - 1 do
        begin
          i2 := IndexOfpar(TGrindComponent(comps.Items[i]).Symbol, strings);
          if i2 >= 0 then
            strings.delete(i2);
        end;
      end;
      if strings.IndexOf(str_comm) < 0 then
        strings.insert(0, str_comm);
      strings.insert(0, '');
      comps := TheModel.CompsOfType(ctParameter);
      comps.Sort(InvCompTextCompare);
      maxparlen := 0;
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          if length(Symbol) > maxparlen then
            maxparlen := length(Symbol);
      comps := TheModel.CompsOfType(ctStatevar);
      comps.Sort(InvCompTextCompare);
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          AddParam(Symbol, ExpressionFmt, Description, TheUnit, maxparlen);
      strings.insert(0, str_init);
      strings.insert(0, '');
      comps := TheModel.CompsOfType(ctParameter);
      comps.Sort(InvCompTextCompare);
      for i := 0 to comps.count - 1 do
        with TGrindComponent(comps.Items[i]) do
          AddParam(Symbol, ExpressionFmt, Description, TheUnit, maxparlen);
      strings.insert(0, str_param);
      ParamLongMemo.text := strings.text;
      SchemeLongMemo.Clear;
      for i := 0 to ComponentDlg.ModelMemo.Lines.count do
        if ComponentDlg.ModelMemo.Lines.strings[i] <> '' then
          SchemeLongMemo.Add('%com=' + ComponentDlg.ModelMemo.Lines.strings[i]);
      if TheModel.IsMatrix then
        SchemeLongMemo.Add('%vec=1');
      if ComponentDlg.ExternDatafile <> '' then
      begin
        SchemeLongMemo.Add('%dat=#F#' + ComponentDlg.ExternDatafile + '#/F#');
        i := IndexOfpar('loaddata', ParamLongMemo);
        if i >= 0 then
          ParamLongMemo.strings[i] := format('loaddata ''#F#%s#/F#''',
            [ComponentDlg.ExternDatafile])
        else
          ParamLongMemo.Add(format('loaddata ''#F#%s#/F#''',
            [ComponentDlg.ExternDatafile]));
      end
      else
      begin
        i := IndexOfpar('loaddata', ParamLongMemo);
        if ComponentDlg.hasanydata then
        begin
          SchemeLongMemo.Add('%dat=-intern');
          if i >= 0 then
            ParamLongMemo.strings[i] := 'loaddata -ini'
          else
            ParamLongMemo.Add('loaddata -ini');
        end
        else if i >= 0 then
          ParamLongMemo.delete(i);
      end;
      pargroups := TStringList.Create;
      try
        pargroups.Sorted := True;
        pargroups.Duplicates := dupIgnore;
        comps := TheModel.CompsOfType(ctParameter);
        for i := 0 to comps.count - 1 do
          with TGrindComponent(comps.Items[i]) do
            pargroups.Add(text);
        repeat
          i := IndexOfpar('par -group', ParamLongMemo);
          if i > 0 then
            ParamLongMemo.delete(i);
        until i < 0;
        if pargroups.count > 1 then
        begin
          for j := 0 to pargroups.count - 1 do
          begin
            s := 'par -group ''' + pargroups[j] + '''';
            for i := 0 to comps.count - 1 do
              with TGrindComponent(comps.Items[i]) do
                if pargroups[j] = text then
                  s := s + ' ' + Symbol;
            ParamLongMemo.Add(s);
          end;
        end;
      finally
        pargroups.Free;
      end;
      for j := 0 to TheModel.Components.count - 1 do
        if not(TGrindComponent(TheModel.Components.Items[j]).TheType
          in [ctConnector, ctFlow, ctTrain]) then
          TGrindComponent(TheModel.Components.Items[j])
            .SaveToString(SchemeLongMemo);
      for j := 0 to TheModel.Components.count - 1 do
        if (TGrindComponent(TheModel.Components.Items[j]).TheType
          in [ctConnector, ctFlow, ctTrain]) then
          TGrindComponent(TheModel.Components.Items[j])
            .SaveToString(SchemeLongMemo);
      if (ComponentDlg.ExternDatafile = '') then
        ComponentDlg.AddToScheme(SchemeLongMemo);
    finally
      ParamLongMemo.EndUpdate;
      SchemeLongMemo.EndUpdate;
    end;
  end;
  ParamMemo1.SelStart := 0;
  ParamMemo1.SelLength := 0;
  ParamMemo1.modified := False;
  ModelMemo.SelStart := 0;
  ModelMemo.SelLength := 0;
  ModelMemo.modified := False;
end;

procedure SetEdit(aEdit: StdCtrls.TEdit; aval: string);
var
  p: TNotifyEvent;
begin
  p := aEdit.OnChange;
  aEdit.OnChange := nil;
  aEdit.text := aval;
  aEdit.OnChange := p;
end;

procedure TModelDlg.GetMemoStrings(MemoLines: TStrings);
var
  i, p: integer;
  s: string;
begin
  strings.Clear;
  i := 0;
  while i < MemoLines.count do
  begin
    s := trim(MemoLines[i]);
    // Merge lines to get a single line for one statement
    if (pos('[', s) > 0) and (pos(']', s) = 0) then
      repeat
        i := i + 1;
        if i < MemoLines.count then
          s := s + #10 + MemoLines[i];
      until (pos(']', MemoLines[i]) > 0) // or (pos('=', MemoLines[i]) > 0)
    else if (pos('{', s) > 0) and (pos('}', s) = 0) then
      repeat
        i := i + 1;
        if i < MemoLines.count then
          s := s + #10 + MemoLines[i];
      until (pos('}', MemoLines[i]) > 0)
    else if (pos('...', s) > 0) then
      repeat
        i := i + 1;
        if i < MemoLines.count then
          s := s + #10 + MemoLines[i];
      until (pos('...', MemoLines[i]) = 0);
    // For convenience make sure that '=' has spaces around it
    p := pos('=', s);
    if (p < length(s)) and (s[p + 1] <> ' ') then
      s := stringReplace(s, '=', '= ', []);
    if (p > 1) and (s[p - 1] <> ' ') then
      s := stringReplace(s, '=', ' =', []);
    strings.Add(s);
    i := i + 1;
  end;
end;

procedure TModelDlg.FormShow(Sender: TObject);

begin
  Application.HintColor := clInfoBk;
  if TheModel.Components.count > 0 then
    CompileModel;
  if TheModel.ModelKind = mkFlow then
  begin
    { solverlist = 'rk4','euler', 'heun','back_euler','ode45','ode23','ode113','ode15s','ode15i', 'ode23s', 'ode23t', 'ode23tb','ode78','ode87','c.euler','c.rk4','c.ode45','bv78';
      fixedstep =   [1 1 1 1 0 0 0 0 0 0 0 0 0 0 1 1 0 0];
    }
    fixed_solvers.Clear;
    fixed_solvers.Add('euler');
    fixed_solvers.Add('rk4');
    fixed_solvers.Add('heun');
    fixed_solvers.Add('back_euler');
    fixed_solvers.Add('c.euler');
    fixed_solvers.Add('c.rk4');
    SolverCombo.Items.Clear;
    SolverCombo.Items.Add('rk4');
    SolverCombo.Items.Add('euler');
    SolverCombo.Items.Add('heun');
    SolverCombo.Items.Add('back_euler');
    SolverCombo.Items.Add('ode45');
    SolverCombo.Items.Add('ode23');
    SolverCombo.Items.Add('ode113');
    SolverCombo.Items.Add('ode15s');
    SolverCombo.Items.Add('ode15i');
    SolverCombo.Items.Add('ode23s');
    SolverCombo.Items.Add('ode23t');
    SolverCombo.Items.Add('ode23tb');
    SolverCombo.Items.Add('ode78');
    SolverCombo.Items.Add('ode87');
    SolverCombo.Items.Add('c.euler');
    SolverCombo.Items.Add('c.rk4');
    SolverCombo.Items.Add('c.ode45');
    SolverCombo.Items.Add('bv78');
  end
  else
  begin
    fixed_solvers.clear;
    SolverCombo.Items.Clear;
    SolverCombo.Items.Add('differ');
    SolverCombo.Items.Add('c.differ');
  end;
  PageControl1.ActivePageIndex := 0;
  RunBtn.Parent := TabSheet1;
  BitBtn2.Parent := TabSheet1;
  BitBtn1.Parent := TabSheet1;
  BitBtn3.Parent := TabSheet1;
  DefaultsBtnClick(nil);
  // RunBtn.Enabled:=inifileEdit.Text<>'';
end;

procedure TModelDlg.dotButtonClick(Sender: TObject);
begin
  SaveDialog1.InitialDir := ThePath;
  SaveDialog1.FileName := inifileEdit.text;
  if SaveDialog1.Execute then
    inifileEdit.text := SaveDialog1.FileName;
end;

procedure TModelDlg.RunBtnClick(Sender: TObject);
begin
  if (TheModel.ModelKind = mkFlow) and TheModel.IsStochastic and
    (SolverCombo.text <> 'euler') then
  begin
    showmessage('Stochastic model should have euler integration');
  end
  else
  begin
    if (TheModel.Components.count = 0) then
      Application.Terminate;
    if (inifileEdit.text = '') then
    begin
      if NoinifileDialog.ShowModal = mrOK then
        inifileEdit.text := NoinifileDialog.inifileEdit.text;
    end;
    if (inifileEdit.text <> '') then
    begin
      saveinifile;
      Application.Terminate;
    end;
  end;
end;

procedure TModelDlg.saveinifile;
var
  outfile: textfile;
  s: string;
  i: integer;
  function checkfullpath(s: string): string;
  var
    p1, p2: integer;
    relpath, s1: string;
  begin
    Result := s;
    p1 := pos('#F#', Result);
    if p1 > 0 then
    begin
      p2 := pos('#/F#', Result);
      s1 := copy(Result, p1 + 3, p2 - p1 - 3);
      relpath := extractrelativepath(ThePath + '\', extractFilePath(s1) + '\') +
        ExtractFileName(s1);
      Result := copy(Result, 1, p1 - 1) + relpath + copy(Result, p2 + 4,
        length(Result));
    end;

  end;

begin
  if inifileEdit.text = '' then
    dotButtonClick(nil);
  if inifileEdit.text <> '' then
  begin
    if pos('.', inifileEdit.text) = 0 then
      inifileEdit.text := inifileEdit.text + '.ini';
    if pos('\', inifileEdit.text) = 0 then
      inifileEdit.text := ThePath + '\' + inifileEdit.text;
    if ExtractFileExt(inifileEdit.text) <> '.tmp' then
    begin
      SaveBakFile(inifileEdit.text);
      MainForm.FileList.AddFile(inifileEdit.text, 1);
    end;
    inifileEdit.text := expandfilename(inifileEdit.text);
    ThePath := ExtractFileDir(inifileEdit.text);
    assignfile(outfile, inifileEdit.text);
    rewrite(outfile);
    try
      writeln(outfile, '%model');
      for i := 0 to ModelMemo.Lines.count - 1 do
      begin
        writeln(outfile, ModelMemo.Lines[i]);
      end;
      writeln(outfile, '%commands');
      for i := 0 to ParamLongMemo.count - 1 do
      begin
        s := stringReplace(ParamLongMemo.strings[i], '  ', ' ', [rfReplaceAll]);
        s := stringReplace(s, '  ', ' ', [rfReplaceAll]);
        writeln(outfile, stringReplace(checkfullpath(s), '  ', ' ',
          [rfReplaceAll]));
      end;
      writeln(outfile, '%scheme');
      for i := 0 to SchemeLongMemo.count - 1 do
      begin
        writeln(outfile, checkfullpath(SchemeLongMemo[i]));
      end;
    finally
      CloseFile(outfile);
    end;
    OKstr := 'OK';
  end
  else
    OKstr := 'Cancel';
  // MainForm.Configfile(False);
end;

procedure TModelDlg.FormCreate(Sender: TObject);
begin
  TheModel := MainForm.Grindmodel;
  SchemeLongMemo := TMemoLongstrings.Create(SchemeMemo1);
  ParamLongMemo := TMemoLongstrings.Create(ParamMemo1);
  fixed_solvers := TStringList.Create;
  strings := TStringList.Create;
  strings.Sorted := False;
  if paramcount > 0 then
  begin
    if Paramstr(1) = 'vismod' then
    begin
      ThePath := Paramstr(2);
      if pos('#', Paramstr(3)) > 0 then
      begin
        inifileEdit.text := Paramstr(4);
        LoadInifile;
        inifileEdit.text := copy(Paramstr(3), pos('#', Paramstr(3)) + 1,
          length(Paramstr(3)));
        if pos('\', inifileEdit.text) = 0 then
          inifileEdit.text := ThePath + '\' + inifileEdit.text;
      end
      else if Paramstr(3) <> 'new' then
      begin
        inifileEdit.text := ThePath + '\' + Paramstr(3);
        if not LoadInifile then
          ShowModal;
      end;
    end
  end;
  OKstr := 'Cancel';
end;

procedure TModelDlg.FormDestroy(Sender: TObject);
var
  outfile: textfile;
begin
  chdir(extractFilePath(Paramstr(0)));
  if Paramstr(4) <> '' then
  begin
    assignfile(outfile, Paramstr(4));
    rewrite(outfile);
    try
      writeln(outfile, OKstr);
      writeln(outfile, ThePath);
      writeln(outfile, inifileEdit.text);
    finally
      CloseFile(outfile);
    end;
  end;
  fixed_solvers.Free;
  strings.Free;
  SchemeLongMemo.Free;
  ParamLongMemo.Free;
end;

function TModelDlg.LoadInifile: boolean;
var
  inifile: textfile;
  s: string;
  comp: TGrindComponent;

  procedure myreadln(var F: textfile; var s: string);
  var
    ch: char;
  begin
    s := '';
    ch := #0;
    while not eof(F) and not charInSet(ch, [#13, #$A]) do
    begin
      read(F, ch);
      if not charInSet(ch, [#10, #13, #$A]) then
        s := s + ch;
    end;
  end;
  procedure readln_dots(var F: textfile; var s: string);
  var
    s1: string;
  begin
    s := '';
    repeat
      myreadln(F, s1);
      if pos('...', s1) > 0 then
        s := s + trim(copy(s1, 1, length(s1) - 3))
      else
        s := s + trim(s1);
    until (pos('...', s1) = 0) or eof(F);
  end;
  procedure CreateScheme;
  var
    i: integer;
  begin
    comp := TheModel.AddComponent(10, 100, ctModelEquations);
    s := '';
    for i := 0 to ModelMemo.Lines.count - 1 do
    begin
      if ModelMemo.Lines[i] <> '' then
      begin
        if i > 0 then
          s := s + '\n';
        s := s + ModelMemo.Lines[i];
      end;
    end;
    comp.Expression := s;
    comp.ExtractParameters;
  end;

begin
  s := inifileEdit.text;
  TheModel.Clear;
  TheModel.ActiveUndo := False;
  inifileEdit.text := s;
  if not FileExists(inifileEdit.text) then
  begin
    showmessage('File "' + inifileEdit.text + '" does not exist');
    inifileEdit.text := '';
    Result := True;
  end
  else
  begin
    Result := True;
    ParamLongMemo.BeginUpdate;
    SchemeLongMemo.BeginUpdate;
    try
      if ExtractFileExt(inifileEdit.text) <> '.tmp' then
        MainForm.FileList.AddFile(expandfilename(inifileEdit.text), 1);
      setcurrentdir(ExtractFileDir(inifileEdit.text));
      assignfile(inifile, inifileEdit.text);
      reset(inifile);
      try
        s := '';
        while (s <> '%model') and not eof(inifile) do
          myreadln(inifile, s);
        while (s <> '%commands') and not eof(inifile) do
        begin
          myreadln(inifile, s);
          if (s <> '%commands') and (s <> '') then
            ModelMemo.Lines.Add(s);
        end;
        ModelMemo.SelStart := 0;
        while (s <> '%scheme') and not eof(inifile) do
        begin
          myreadln(inifile, s);
          if (s <> '%scheme') and (s <> '') then
            ParamLongMemo.Add(s);
        end;
        ParamMemo1.SelStart := 0;
        while not eof(inifile) do
        begin
          readln_dots(inifile, s);
          if s <> '' then
            SchemeLongMemo.Add(s);
        end;
        SchemeMemo1.SelStart := 0;
        if SchemeLongMemo.count = 0 then
          CreateScheme
        else
        begin
          TheModel.ReadScheme(SchemeLongMemo);
          if TheModel.Components.count = 0 then
            CreateScheme;
        end;
      finally
        CloseFile(inifile);
      end;
    finally
      ParamLongMemo.EndUpdate;
      SchemeLongMemo.EndUpdate;
    end;
  end;
  TheModel.ActiveUndo := True;
  ParamMemo1.modified := False;
  ModelMemo.modified := False;
end;

procedure TModelDlg.LoadButtonClick(Sender: TObject);
var
  s: string;
begin
  try
    FindFileDialog.InitialDir := ThePath;
    s := extractFilePath(inifileEdit.text);
    if (s <> '') and (s <> ThePath) then
      FindFileDialog.InitialDir := s;
  except
    FindFileDialog.InitialDir := '';
  end;
  FindFileDialog.FileName := ExtractFileName(inifileEdit.text);
  if FindFileDialog.Execute then
  begin
    inifileEdit.text := FindFileDialog.FileName;
    ThePath := FindFileDialog.InitialDir;
    LoadInifile;
  end;
end;

procedure TModelDlg.BitBtn3Click(Sender: TObject);
begin
  if ((TheModel.Components.count > 0) or (ModelMemo.Lines.count <= 1)) or
    (Application.MessageBox
    ('Going to the scheme will clear the current model definition, OK to continue?',
    'vismod', MB_YESNO) = IDYES) then
    Close;
end;

function TModelDlg.inifilename: string;
begin
  Result := inifileEdit.text;
end;

function TModelDlg.IndexOfpar(command: string; str: TStrings): integer;

  function StrCmpWholeWord(substr, s: string): boolean;
  var
    n: integer;
  begin
    n := length(substr);
    Result := substr = copy(s, 1, n);
    if Result and (length(s) > n) then
      Result := not charInSet(s[n + 1], ['0' .. '9', '_', 'a' .. 'z',
        'A' .. 'Z']);
  end;

begin
  Result := 0;
  if pos('=', command) > 1 then
    command := trim(copy(command, 1, pos('=', command) - 1));
  while (Result < str.count) and not StrCmpWholeWord(command,
    str.strings[Result]) do
    Result := Result + 1;
  if Result >= str.count then
    Result := -1;
end;

function TModelDlg.readcommand(command: string; no: integer): string;
var
  i, i2, k: integer;
  s: string;
begin
  Result := '';
  i := IndexOfpar(command, ParamLongMemo);
  if i >= 0 then
    s := ParamLongMemo.strings[i];
  s := trim(copy(s, length(command) + 1, length(s)));
  if no < 0 then
    Result := s
  else
  begin
    i := 1;
    k := 1;
    while (i <= length(s)) and (k < no) do
    begin
      if s[i] = ' ' then
        k := k + 1;
      i := i + 1;
    end;
    if i <= length(s) then
    begin
      i2 := i + 1;
      while (i2 <= length(s)) and (s[i2] <> ' ') do
        i2 := i2 + 1;
      Result := copy(s, i, i2 - i);
      if (Result <> '') then
      begin
        if (Result[1] = '[') then
          Result := copy(Result, 2, length(Result));
        if (Result <> '') and (Result[length(Result)] = ';') then
          Result := copy(Result, 1, length(Result) - 1);
        if (Result <> '') and (Result[length(Result)] = ']') then
          Result := copy(Result, 1, length(Result) - 1);
      end;
    end;
  end;
end;

procedure TModelDlg.TabSheet2Enter(Sender: TObject);
var
  comm, s1, s2: string;

begin
  if PageControl1.ActivePageIndex = 1 then
  begin
    comm := readcommand('ax x', 1);
    if comm <> '' then
    begin
      XaxCombo.ItemIndex := XaxCombo.Items.IndexOf(comm);
      s1 := readcommand('ax x', 2);
      s2 := readcommand('ax x', 3);
      MinXaxEdit.text := s1;
      MaxXaxEdit.text := s2;
    end;
    comm := readcommand('ax y', 1);
    if comm <> '' then
    begin
      Yaxcombo.ItemIndex := Yaxcombo.Items.IndexOf(comm);
      s1 := readcommand('ax y', 2);
      s2 := readcommand('ax y', 3);
      MinYaxEdit.text := s1;
      MaxYaxEdit.text := s2;
    end;
    comm := readcommand('ax z', 1);
    if comm <> '' then
    begin
      ZaxCombo.ItemIndex := ZaxCombo.Items.IndexOf(comm);
      s1 := readcommand('ax z', 2);
      s2 := readcommand('ax z', 3);
      MinZaxEdit.text := s1;
      MaxZaxEdit.text := s2;
    end;
    comm := readcommand('solver', 1);
    if comm <> '' then
    begin
      SolverCombo.ItemIndex := SolverCombo.Items.IndexOf(comm);
      s1 := readcommand('solver', 2);
      if TheModel.ModelKind <> mkTrain then
        if fixed_solvers.IndexOf(SolverCombo.text) < 0 then
        begin
          s2 := readcommand('solver', 3);
          RelTolEdit.text := s1;
          AbsTolEdit.text := s2;
        end
        else
          StepSizeEdit.text := s1;
      SolverComboChange(nil)
    end;
    PlotNoUpDown.Position := 1;
    comm := readcommand('out -1', -1);
    if comm <> '' then
      outEdit.text := comm;
    comm := readcommand('out -time -1', -1);
    if comm <> '' then
      TEdit.text := comm;
    comm := readcommand('simtime', 1);
    if comm <> '' then
    begin
      s1 := readcommand('simtime', 2);
      s2 := readcommand('simtime', 3);
      StarttEdit.text := comm;
      tstepEdit.text := s1;
      nDaysEdit.text := s2;
    end;
    RunBtn.Parent := TabSheet2;
    BitBtn2.Parent := TabSheet2;
    BitBtn1.Parent := TabSheet2;
    BitBtn3.Parent := TabSheet2;
  end
  else
  begin
    RunBtn.Parent := TabSheet1;
    BitBtn2.Parent := TabSheet1;
    BitBtn1.Parent := TabSheet1;
    BitBtn3.Parent := TabSheet1;
  end;
end;

procedure TModelDlg.StarttEditChange(Sender: TObject);
var
  i: integer;
  comm: string;
begin
  i := IndexOfpar('simtime', ParamLongMemo);
  comm := format('simtime %s %s %s', [StarttEdit.text, tstepEdit.text,
    nDaysEdit.text]);
  if i < 0 then
    ParamLongMemo.Add(comm)
  else
    ParamLongMemo.strings[i] := comm;
end;

procedure TModelDlg.tEditChange(Sender: TObject);
var
  i: integer;
  comm: string;
begin
  i := IndexOfpar(format('out -time -%d', [PlotNoUpDown.Position]),
    ParamLongMemo);
  comm := format('out -time -%d %s', [PlotNoUpDown.Position, TEdit.text]);
  if i < 0 then
  begin
    if TEdit.text <> 't' then
      ParamLongMemo.Add(comm);
  end
  else
    ParamLongMemo.strings[i] := comm;
end;

procedure TModelDlg.outEditChange(Sender: TObject);
var
  i: integer;
  comm: string;
begin
  i := IndexOfpar(format('out -%d', [PlotNoUpDown.Position]), ParamLongMemo);
  comm := format('out -%d %s', [PlotNoUpDown.Position, outEdit.text]);
  if i < 0 then
  begin
    if outEdit.text <> '' then
      ParamLongMemo.Add(comm);
  end
  else
  begin
    if outEdit.text = '' then
      ParamLongMemo.strings[i] := ''
    else
      ParamLongMemo.strings[i] := comm;
  end;
end;

procedure TModelDlg.XaxComboChange(Sender: TObject);
var
  i: integer;
  comm: string;
begin
  i := IndexOfpar('ax x', ParamLongMemo);
  comm := format('ax x %s [%s %s]', [XaxCombo.Items.strings[XaxCombo.ItemIndex],
    MinXaxEdit.text, MaxXaxEdit.text]);
  if i < 0 then
    ParamLongMemo.Add(comm)
  else
    ParamLongMemo.strings[i] := comm;
end;

procedure TModelDlg.YaxcomboChange(Sender: TObject);
var
  i: integer;
  comm: string;
begin
  i := IndexOfpar('ax y', ParamLongMemo);
  comm := format('ax y %s [%s %s]', [Yaxcombo.Items.strings[Yaxcombo.ItemIndex],
    MinYaxEdit.text, MaxYaxEdit.text]);
  if i < 0 then
    ParamLongMemo.Add(comm)
  else
    ParamLongMemo.strings[i] := comm;
end;

procedure TModelDlg.ZaxComboChange(Sender: TObject);
var
  i: integer;
  comm: string;
begin
  i := IndexOfpar('ax z', ParamLongMemo);
  comm := format('ax z %s [%s %s]', [ZaxCombo.Items.strings[ZaxCombo.ItemIndex],
    MinZaxEdit.text, MaxZaxEdit.text]);
  if i < 0 then
    ParamLongMemo.Add(comm)
  else
    ParamLongMemo.strings[i] := comm;
end;

procedure TModelDlg.SolverComboUpdate(Sender: TObject);
var
  isfixed: boolean;
begin
  if TheModel.ModelKind=mkTrain then
  begin
         AbsTolEdit.visible  :=False;
         AbsTolTable.visible  :=False;
         StepSizeEdit.visible  :=False;
         RelTolEdit.visible   :=False;
         RelTolLabel.visible  :=False;
  end
  else
  begin
  isfixed:=fixed_solvers.IndexOf(SolverCombo.Text)>=0;
  AbsTolEdit.visible := not isfixed;
  AbsTolTable.visible := not isfixed;
  StepSizeEdit.visible := isfixed;
  RelTolEdit.visible := not isfixed;
  RelTolLabel.visible := true;
  if not isfixed then
    RelTolLabel.Caption := 'Relative tolerance'
  else
    RelTolLabel.Caption := 'Step size';
  end;
end;

procedure TModelDlg.SolverComboChange(Sender: TObject);
begin
  self.SolverComboUpdate(Sender);
  self.RelTolEditChange(Sender);
end;

procedure TModelDlg.RelTolEditChange(Sender: TObject);
var
  i: integer;
  comm: string;
begin
  i := IndexOfpar('solver', ParamLongMemo);
  if TheModel.ModelKind=mkTrain then
    comm := format('solver %s',
      [SolverCombo.Items.strings[SolverCombo.ItemIndex]])
  else if fixed_solvers.IndexOf(SolverCombo.Text)<0 then
    comm := format('solver %s %s %s',
      [SolverCombo.Items.strings[SolverCombo.ItemIndex], RelTolEdit.text,
      AbsTolEdit.text])
  else
    comm := format('solver %s %s',
      [SolverCombo.Items.strings[SolverCombo.ItemIndex], StepSizeEdit.text]);
  if i < 0 then
    ParamLongMemo.Add(comm)
  else
    ParamLongMemo.strings[i] := comm;
end;

procedure TModelDlg.BitBtn2Click(Sender: TObject);
begin
  { if ((TheModel.Components.Count > 0) or (ModelMemo.Lines.Count <= 1)) then
    close
    else }
  MainForm.ModalResult := mrCancel;
  MainForm.Close;
  Close;
end;

procedure TModelDlg.Button1Click(Sender: TObject);
begin
  MainForm.HelpTopic('simtime');
end;

procedure TModelDlg.Button2Click(Sender: TObject);
begin
  MainForm.HelpTopic('out');
end;

procedure TModelDlg.Button3Click(Sender: TObject);
begin
  MainForm.HelpTopic('ax');
end;

procedure TModelDlg.Button4Click(Sender: TObject);
begin
  MainForm.HelpTopic('solver');
end;

procedure TModelDlg.inifileEditChange(Sender: TObject);
begin
  MainForm.FormShow(Sender);
  // RunBtn.Enabled:=inifileEdit.Text<>'';
end;

procedure TModelDlg.DefaultsBtnClick(Sender: TObject);
var
  j, i, k: integer;
  list: TList;
  s: string;
begin
  SetEdit(StarttEdit, '0');
  SetEdit(tstepEdit, '1000');
  SetEdit(nDaysEdit, '');
  SetEdit(TEdit, 't');
  SetEdit(MinXaxEdit, '0');
  SetEdit(MaxXaxEdit, '10');
  SetEdit(MinYaxEdit, '0');
  SetEdit(MaxYaxEdit, '10');
  SetEdit(MinZaxEdit, '0');
  SetEdit(MaxZaxEdit, '10');
  XaxCombo.Items.Clear;
  Yaxcombo.Items.Clear;
  ZaxCombo.Items.Clear;
  XaxCombo.Items.Add('');
  Yaxcombo.Items.Add('');
  ZaxCombo.Items.Add('');
  for j := 0 to TheModel.Components.count - 1 do
    with (TGrindComponent(TheModel.Components.Items[j])) do
      if TheType in [ctStatevar, ctParameter, ctExternvar] then
      begin
        if (NRows = 1) and (NCols = 1) then
        begin
          XaxCombo.Items.Add(Symbol);
          Yaxcombo.Items.Add(Symbol);
          ZaxCombo.Items.Add(Symbol);
        end
        else if NCols = 1 then
          for i := 1 to NRows do
          begin
            XaxCombo.Items.Add(format('%s(%d)', [Symbol, i]));
            Yaxcombo.Items.Add(format('%s(%d)', [Symbol, i]));
            ZaxCombo.Items.Add(format('%s(%d)', [Symbol, i]));
          end
        else
          for i := 1 to NRows do
            for k := 1 to NCols do
            begin
              XaxCombo.Items.Add(format('%s(%d,%d)', [Symbol, i, k]));
              Yaxcombo.Items.Add(format('%s(%d,%d)', [Symbol, i, k]));
              ZaxCombo.Items.Add(format('%s(%d,%d)', [Symbol, i, k]));
            end
      end;
  if TheModel.ModelKind = mkTrain then
     SolverCombo.ItemIndex := SolverCombo.Items.indexof('differ')
  else
     SolverCombo.ItemIndex := SolverCombo.Items.indexof('ode45');
  SetEdit(RelTolEdit, '5E-5');
  SetEdit(AbsTolEdit, '1E-7');
  SetEdit(StepSizeEdit, '0.1');
  SolverComboUpdate(Sender);
  list := TheModel.CompsOfType(ctStatevar);
  if list.count > 0 then
    XaxCombo.ItemIndex := XaxCombo.Items.IndexOf
      (TGrindComponent(list.Items[0]).Symbol);
  if list.count = 2 then
    Yaxcombo.ItemIndex := XaxCombo.Items.IndexOf
      (TGrindComponent(list.Items[1]).Symbol);
  SetEdit(outEdit, '');
  s := '';
  for j := 0 to list.count - 1 do
    if TGrindComponent(list.Items[j]).TheModel.IsMatrix then
      s := s + '_mean{' + TGrindComponent(list.Items[j]).Symbol + ') '
    else
      s := s + TGrindComponent(list.Items[j]).Symbol + ' ';
  for j := 0 to list.count - 1 do
    if TGrindComponent(list.Items[j]).HasData then
      s := s + format('observed(''%s'') ',
        [TGrindComponent(list.Items[j]).Symbol]);
  list := TheModel.CompsOfType(ctExternvar);
  for j := 0 to list.count - 1 do
    s := s + TGrindComponent(list.Items[j]).Symbol + ' ';
  SetEdit(outEdit, trim(s));
  //GroupBox3.visible := TheModel.ModelKind <> mkTrain;
  if TheModel.ModelKind = mkTrain then
    SetEdit(tstepEdit, '100')
  else
    SetEdit(tstepEdit, '1000');
end;

procedure TModelDlg.BitBtn1Click(Sender: TObject);
begin
  MainForm.HelpTopic('modeldlg');
end;

procedure TModelDlg.FormClose(Sender: TObject; var Action: TCloseAction);
var
  j, i, k: integer;
  s: string;

  function lpos(ch: char; s: string): integer;
  begin
    Result := length(s);
    while (Result > 0) and (s[Result] <> ch) do
      dec(Result);
  end;

begin
  if ParamMemo1.modified then
  begin
    for j := 0 to TheModel.Components.count - 1 do
      with (TGrindComponent(TheModel.Components.Items[j])) do
        if TheType in [ctStatevar, ctParameter, ctExternvar] then
        begin
          i := IndexOfpar(Symbol, ParamLongMemo);
          if i >= 0 then
          begin
            s := ParamLongMemo.strings[i];
            if pos('=', s) > 0 then
            begin
              Expression := copy(s, pos('=', s) + 1, length(s));
              if (pos(';', Expression) > 0) and (pos('[', Expression) = 0) then
                Expression :=
                  trim(copy(Expression, 1, pos(';', Expression) - 1));
            end;
            if pos('%', s) > 0 then
            begin
              s := copy(s, pos('%', s) + 1, length(s));
              k := lpos(',', s);
              if k > 0 then
              begin
                TheUnit := copy(s, k + 1, length(s));
                Description := copy(s, 1, k - 1);
              end
              else
              begin
                TheUnit := '';
                Description := s;
              end;
            end;
          end;
        end;
  end;
end;

procedure TModelDlg.PlotNoEditChange(Sender: TObject);
var
  comm: string;
  list: TList;
  j: integer;
begin
  comm := readcommand(format('out -%d', [PlotNoUpDown.Position]), -1);
  if (comm = '') and (PlotNoUpDown.Position = 1) then
  begin
    list := TheModel.CompsOfType(ctStatevar);
    for j := 0 to list.count - 1 do
      if TheModel.IsMatrix then
        comm := comm + '_mean(' + TGrindComponent(list.Items[j]).Symbol + ') '
      else
        comm := comm + TGrindComponent(list.Items[j]).Symbol + ' ';
    for j := 0 to list.count - 1 do
      if TGrindComponent(list.Items[j]).HasData then
        comm := comm + format('observed(''%s'') ',
          [TGrindComponent(list.Items[j]).Symbol]);
    list := TheModel.CompsOfType(ctExternvar);
    for j := 0 to list.count - 1 do
      comm := comm + TGrindComponent(list.Items[j]).Symbol + ' ';
  end;
  SetEdit(outEdit, trim(comm));
  comm := readcommand(format('out -time -%d', [PlotNoUpDown.Position]), -1);
  if comm <> '' then
    TEdit.text := comm
  else
    TEdit.text := 't';

end;

procedure TModelDlg.Button5Click(Sender: TObject);
begin
  MainForm.HelpTopic('simtime');
end;

end.
