object SymbolErrorDlg: TSymbolErrorDlg
  Left = 0
  Top = 0
  Caption = 'Error in symbol'
  ClientHeight = 143
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ErrorLabel: TLabel
    Left = 32
    Top = 24
    Width = 121
    Height = 13
    Caption = 'Symbol was already used'
  end
  object Label2: TLabel
    Left = 32
    Top = 67
    Width = 77
    Height = 13
    Caption = 'Change symbol:'
  end
  object SymbolEdit: TEdit
    Left = 192
    Top = 64
    Width = 114
    Height = 21
    TabOrder = 0
    OnChange = SymbolEditClick
    OnClick = SymbolEditClick
  end
  object ChangeBtn: TBitBtn
    Left = 32
    Top = 107
    Width = 89
    Height = 25
    Caption = 'Change'
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object IgnoreBtn: TBitBtn
    Left = 248
    Top = 107
    Width = 89
    Height = 25
    Caption = '&Ignore'
    Kind = bkRetry
    NumGlyphs = 2
    TabOrder = 2
    OnClick = IgnoreBtnClick
  end
  object IgnoreAllBtn: TBitBtn
    Left = 140
    Top = 107
    Width = 89
    Height = 25
    Caption = 'Ignore &All'
    Kind = bkAll
    NumGlyphs = 2
    TabOrder = 3
    OnClick = IgnoreAllBtnClick
  end
end
