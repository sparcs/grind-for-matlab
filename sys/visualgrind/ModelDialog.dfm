object ModelDlg: TModelDlg
  Left = 0
  Top = 192
  Caption = 'Model Visual GRIND'
  ClientHeight = 503
  ClientWidth = 819
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 819
    Height = 503
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChange = TabSheet2Enter
    object TabSheet1: TTabSheet
      Caption = 'Model definition'
      ParentShowHint = False
      ShowHint = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label1: TLabel
        Left = 16
        Top = 24
        Width = 71
        Height = 13
        Caption = 'Name of model'
      end
      object Label2: TLabel
        Left = 16
        Top = 80
        Width = 291
        Height = 13
        Caption = 'Model equations (differential equations/difference equations)  '
      end
      object Label3: TLabel
        Left = 16
        Top = 288
        Width = 202
        Height = 13
        Caption = 'Parameters / Initial conditions / Commands'
      end
      object inifileEdit: TEdit
        Left = 136
        Top = 24
        Width = 450
        Height = 21
        Hint = 'Enter here the name of the inifile'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = inifileEditChange
      end
      object LoadButton: TButton
        Left = 663
        Top = 18
        Width = 145
        Height = 33
        Hint = 'Load existing  model'
        Caption = 'Load model'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = LoadButtonClick
      end
      object ModelMemo: TMemo
        Left = 16
        Top = 96
        Width = 610
        Height = 169
        Hint = 'Generated list of model equations (Read only)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 2
        WordWrap = False
      end
      object ParamMemo1: TMemo
        Left = 16
        Top = 304
        Width = 610
        Height = 165
        Hint = 'Generated list of parameters and commands'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 3
        WordWrap = False
      end
      object RunBtn: TBitBtn
        Left = 657
        Top = 316
        Width = 145
        Height = 33
        Hint = 'Save model and go to MATLAB'
        Caption = 'To MATLAB GRIND'
        Kind = bkOK
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = RunBtnClick
      end
      object BitBtn2: TBitBtn
        Left = 657
        Top = 356
        Width = 145
        Height = 33
        Hint = 'Go back to scheme'
        Cancel = True
        Caption = 'To Scheme'
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333333333000033338833333333333333333F333333333333
          0000333911833333983333333388F333333F3333000033391118333911833333
          38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
          911118111118333338F3338F833338F3000033333911111111833333338F3338
          3333F8330000333333911111183333333338F333333F83330000333333311111
          8333333333338F3333383333000033333339111183333333333338F333833333
          00003333339111118333333333333833338F3333000033333911181118333333
          33338333338F333300003333911183911183333333383338F338F33300003333
          9118333911183333338F33838F338F33000033333913333391113333338FF833
          38F338F300003333333333333919333333388333338FFF830000333333333333
          3333333333333333333888330000333333333333333333333333333333333333
          0000}
        ModalResult = 2
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BitBtn3Click
      end
      object dotButton: TButton
        Left = 593
        Top = 24
        Width = 33
        Height = 25
        Hint = 'Select a directory for the inifile'
        Caption = '...'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = dotButtonClick
      end
      object SchemeMemo1: TMemo
        Left = 640
        Top = 96
        Width = 177
        Height = 177
        ScrollBars = ssBoth
        TabOrder = 7
        Visible = False
        WordWrap = False
      end
      object BitBtn3: TBitBtn
        Left = 657
        Top = 436
        Width = 145
        Height = 33
        Hint = 'Exit without saving'
        Caption = 'Exit VISMOD'
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
          F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
          000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
          338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
          45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
          3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
          F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
          000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
          338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
          4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
          8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
          333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
          0000}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = BitBtn2Click
      end
      object BitBtn1: TBitBtn
        Left = 657
        Top = 396
        Width = 145
        Height = 33
        Hint = 'Help on this window'
        Kind = bkHelp
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnClick = BitBtn1Click
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Settings'
      ImageIndex = 1
      ParentShowHint = False
      ShowHint = False
      OnEnter = TabSheet2Enter
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GroupBox1: TGroupBox
        Left = 48
        Top = 200
        Width = 561
        Height = 145
        Caption = 'Phase plane settings (ax)'
        TabOrder = 0
        object Label4: TLabel
          Left = 24
          Top = 48
          Width = 28
          Height = 13
          Caption = 'X axis'
        end
        object Label5: TLabel
          Left = 24
          Top = 80
          Width = 28
          Height = 13
          Caption = 'Y axis'
        end
        object Label6: TLabel
          Left = 24
          Top = 112
          Width = 28
          Height = 13
          Caption = 'Z axis'
        end
        object Label12: TLabel
          Left = 96
          Top = 32
          Width = 90
          Height = 13
          Caption = 'Variable/parameter'
        end
        object Label13: TLabel
          Left = 240
          Top = 32
          Width = 44
          Height = 13
          Caption = 'Minimum:'
        end
        object Label14: TLabel
          Left = 392
          Top = 32
          Width = 47
          Height = 13
          Caption = 'Maximum:'
        end
        object XaxCombo: TComboBox
          Left = 96
          Top = 48
          Width = 97
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          OnChange = XaxComboChange
        end
        object MinXaxEdit: TEdit
          Left = 240
          Top = 48
          Width = 97
          Height = 21
          TabOrder = 1
          Text = '0'
          OnChange = XaxComboChange
        end
        object Yaxcombo: TComboBox
          Left = 96
          Top = 80
          Width = 97
          Height = 21
          Style = csDropDownList
          TabOrder = 2
          OnChange = YaxcomboChange
        end
        object ZaxCombo: TComboBox
          Left = 96
          Top = 112
          Width = 97
          Height = 21
          Style = csDropDownList
          TabOrder = 3
          OnChange = ZaxComboChange
        end
        object MinYaxEdit: TEdit
          Left = 240
          Top = 80
          Width = 97
          Height = 21
          TabOrder = 4
          Text = '0'
          OnChange = YaxcomboChange
        end
        object MinZaxEdit: TEdit
          Left = 240
          Top = 112
          Width = 97
          Height = 21
          TabOrder = 5
          Text = '0'
        end
        object MaxXaxEdit: TEdit
          Left = 392
          Top = 48
          Width = 97
          Height = 21
          TabOrder = 6
          Text = '10'
          OnChange = XaxComboChange
        end
        object MaxYaxEdit: TEdit
          Left = 392
          Top = 80
          Width = 97
          Height = 21
          TabOrder = 7
          Text = '10'
          OnChange = YaxcomboChange
        end
        object MaxZaxEdit: TEdit
          Left = 392
          Top = 112
          Width = 97
          Height = 21
          TabOrder = 8
          Text = '10'
        end
      end
      object GroupBox2: TGroupBox
        Left = 48
        Top = 8
        Width = 561
        Height = 89
        Caption = 'Simulation settings (simtime)'
        TabOrder = 1
        object Label16: TLabel
          Left = 96
          Top = 32
          Width = 47
          Height = 13
          Caption = 'Start time:'
        end
        object Label17: TLabel
          Left = 240
          Top = 32
          Width = 132
          Height = 13
          Caption = 'Number of time steps to run:'
        end
        object Label18: TLabel
          Left = 392
          Top = 23
          Width = 98
          Height = 26
          Caption = 'Number of steps for output (empty = max)'
          WordWrap = True
        end
        object Label10: TLabel
          Left = 24
          Top = 48
          Width = 40
          Height = 13
          Caption = 'Simulate'
        end
        object StarttEdit: TEdit
          Left = 96
          Top = 48
          Width = 96
          Height = 21
          Hint = 'Start time of a default run'
          TabOrder = 0
          Text = '0'
          OnChange = StarttEditChange
        end
        object tstepEdit: TEdit
          Left = 240
          Top = 48
          Width = 96
          Height = 21
          Hint = 'End time of a default run'
          TabOrder = 1
          Text = '1000'
          OnChange = StarttEditChange
        end
        object nDaysEdit: TEdit
          Left = 392
          Top = 48
          Width = 96
          Height = 21
          Hint = 'Number of steps for output'
          TabOrder = 2
          OnChange = StarttEditChange
        end
        object Button1: TButton
          Left = 568
          Top = 8
          Width = 27
          Height = 25
          Hint = 'Help on "simtime"'
          Caption = '?'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = Button1Click
        end
      end
      object GroupBox3: TGroupBox
        Left = 48
        Top = 352
        Width = 561
        Height = 81
        Caption = 'Solver settings (solver)'
        TabOrder = 2
        object Label7: TLabel
          Left = 96
          Top = 24
          Width = 33
          Height = 13
          Caption = 'Solver:'
        end
        object RelTolLabel: TLabel
          Left = 240
          Top = 24
          Width = 89
          Height = 13
          Caption = 'Relative tolerance:'
        end
        object AbsTolTable: TLabel
          Left = 392
          Top = 24
          Width = 91
          Height = 13
          Caption = 'Absolute tolerance:'
        end
        object SolverCombo: TComboBox
          Left = 96
          Top = 40
          Width = 96
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          OnChange = SolverComboChange
          Items.Strings = (
            'rk4'
            'euler'
            'ode45'
            'ode23'
            'ode113'
            'ode15S'
            'ode23S'
            'ode23T'
            'ode23TB')
        end
        object RelTolEdit: TEdit
          Left = 240
          Top = 40
          Width = 96
          Height = 21
          TabOrder = 1
          Text = '5E-5'
          OnChange = RelTolEditChange
        end
        object AbsTolEdit: TEdit
          Left = 392
          Top = 40
          Width = 96
          Height = 21
          TabOrder = 2
          Text = '1E-7'
          OnChange = RelTolEditChange
        end
      end
      object GroupBox4: TGroupBox
        Left = 48
        Top = 104
        Width = 561
        Height = 89
        Caption = 'Time plot settings (out)'
        TabOrder = 3
        object Label8: TLabel
          Left = 24
          Top = 32
          Width = 31
          Height = 13
          Caption = 'X axis:'
        end
        object Label9: TLabel
          Left = 24
          Top = 64
          Width = 49
          Height = 13
          Caption = 'Y axis (list)'
        end
        object Label11: TLabel
          Left = 328
          Top = 40
          Width = 61
          Height = 13
          Caption = 'Plot Number:'
        end
        object tEdit: TEdit
          Left = 96
          Top = 32
          Width = 96
          Height = 21
          TabOrder = 0
          Text = 't'
          OnChange = tEditChange
        end
        object outEdit: TEdit
          Left = 96
          Top = 64
          Width = 393
          Height = 21
          TabOrder = 1
          OnChange = outEditChange
        end
        object PlotNoEdit: TEdit
          Left = 408
          Top = 32
          Width = 65
          Height = 21
          TabOrder = 2
          Text = '1'
          OnChange = PlotNoEditChange
        end
        object PlotNoUpDown: TUpDown
          Left = 473
          Top = 32
          Width = 15
          Height = 21
          Associate = PlotNoEdit
          Min = 1
          Position = 1
          TabOrder = 3
        end
      end
      object Button2: TButton
        Left = 568
        Top = 112
        Width = 27
        Height = 25
        Hint = 'Help on "out"'
        Caption = '?'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 568
        Top = 208
        Width = 27
        Height = 25
        Hint = 'Help on "ax"'
        Caption = '?'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 568
        Top = 360
        Width = 27
        Height = 25
        Hint = 'Help on "solver"'
        Caption = '?'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = Button4Click
      end
      object DefaultsBtn: TBitBtn
        Left = 520
        Top = 448
        Width = 75
        Height = 25
        Caption = 'Default'
        TabOrder = 7
        OnClick = DefaultsBtnClick
      end
      object Button5: TButton
        Left = 568
        Top = 16
        Width = 27
        Height = 25
        Hint = 'Help on "simtime"'
        Caption = '?'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = Button5Click
      end
      object StepSizeEdit: TEdit
        Left = 289
        Top = 392
        Width = 96
        Height = 21
        TabOrder = 9
        Text = '0.1'
        Visible = False
        OnChange = RelTolEditChange
      end
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'ini'
    Filter = 'Grind files (*.ini)|*.ini|All files (*.*)|*.*'
    Title = 'Name for the file'
    Left = 760
    Top = 24
  end
end
