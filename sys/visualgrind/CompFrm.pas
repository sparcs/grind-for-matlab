unit CompFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, GrindComps, ParseExpr;

type
  TCompFrame = class(TFrame)
    Button11: TButton;
    Button7: TButton;
    Button4: TButton;
    Button1: TButton;
    Label9: TLabel;
    Button2: TButton;
    Button5: TButton;
    Button8: TButton;
    Button12: TButton;
    Button10: TButton;
    Button9: TButton;
    Button6: TButton;
    Button3: TButton;
    Button16: TButton;
    Button15: TButton;
    Button14: TButton;
    Button13: TButton;
    Button20: TButton;
    Button19: TButton;
    Button18: TButton;
    Button17: TButton;
    Button21: TButton;
    Button22: TButton;
    Button23: TButton;
    Button24: TButton;
    Button28: TButton;
    Button27: TButton;
    Button26: TButton;
    Button25: TButton;
    VectorizeButton: TButton;
    ConnectList: TListBox;
    functionlist: TListBox;
    Label7: TLabel;
    Label8: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure functionlistDblClick(Sender: TObject);
    procedure functionlistMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure ConnectListMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
  private
    FCurrentEdit: TCustomEdit;
    procedure AddToEdit(S: string);
    { Private declarations }
  public
    TheComp: TGrindComponent;
    procedure SetComponent(Comp: TGrindComponent);
    property CurrentEdit: TCustomEdit read FCurrentEdit write FCurrentEdit;
    { Public declarations }
  end;

implementation

uses ParseClass, System.Types;
{$R *.DFM}

procedure TCompFrame.SetComponent(Comp: TGrindComponent);
var
  i: Integer;
  aList: TList;
  fcomp: TGrindComponent;
begin
  TheComp := Comp;
  if TheComp <> nil then
  begin
    functionlist.Clear;
    TheComp.TheModel.Parser.GetFunctionNames(functionlist.Items);
    for i := functionlist.Items.Count - 1 downto 0 do
      if TExprWord(functionlist.Items.Objects[i]) is TGeneratedFunction then
        functionlist.Items.Delete(i);
    Button1.Enabled := not(TheComp.TheType in [ctConnector, ctCloud]);
    Button2.Enabled := Button1.Enabled;
    Button3.Enabled := Button1.Enabled;
    Button4.Enabled := Button1.Enabled;
    Button5.Enabled := Button1.Enabled;
    Button6.Enabled := Button1.Enabled;
    Button7.Enabled := Button1.Enabled;
    Button8.Enabled := Button1.Enabled;
    Button9.Enabled := Button1.Enabled;
    Button10.Enabled := Button1.Enabled;
    Button11.Enabled := Button1.Enabled;
    Button12.Enabled := Button1.Enabled;
    Button13.Enabled := Button1.Enabled;
    Button14.Enabled := Button1.Enabled;
    Button15.Enabled := Button1.Enabled;
    Button16.Enabled := Button1.Enabled;
    Button17.Enabled := Button1.Enabled;
    Button18.Enabled := Button1.Enabled;
    Button19.Enabled := Button1.Enabled;
    Button20.Enabled := Button1.Enabled;
    Button21.Enabled := Button1.Enabled;
    Button22.Enabled := Button1.Enabled;
    Button23.Enabled := Button1.Enabled;
    Button24.Enabled := Button1.Enabled;
    Button25.Enabled := Button1.Enabled;
    Button26.Enabled := Button1.Enabled;
    Button27.Enabled := Button1.Enabled;
    Button28.Enabled := Button1.Enabled;
    ConnectList.Enabled := Button1.Enabled;
    functionlist.Enabled := Button1.Enabled;
    Label9.Enabled := Button1.Enabled;
    Label8.Enabled := Button1.Enabled;
    Label7.Enabled := Button1.Enabled;
    VectorizeButton.Visible := Button1.Enabled and TheComp.TheModel.IsMatrix;
  end;
  aList := TheComp.TheModel.Connectors(TheComp, cTo);
  ConnectList.Clear;
  for i := 0 to aList.Count - 1 do
  begin
    fcomp := TGrindComponent(aList.Items[i]).From;
    if (fcomp <> nil) and (fcomp.TheType in [ctStatevar, ctPermanentvar,
    ctAuxilvar, ctParameter,  ctExternvar, ctFlow]) then
      ConnectList.Items.addobject(fcomp.Symbol, fcomp);
    if (fcomp <> nil) and (fcomp.TheType = ctUserFunction) then
      ConnectList.Items.addobject(fcomp.Symbol + '()', fcomp);
  end;
end;

procedure TCompFrame.Button1Click(Sender: TObject);
begin
  AddToEdit(TButton(Sender).Caption);
end;

procedure TCompFrame.AddToEdit(S: string);
var
  Len: Integer;
  IsFunction: Boolean;
begin
  if S = '' then
    S := ' ';
  with CurrentEdit do
    if Visible then
    begin
      IsFunction := (Pos('(', S) > 0) and (length(S) > 1);
      Len := Sellength;
      if IsFunction and (Len > 0) then
      begin
        Sellength := 0;
        SelText := Copy(S, 1, Pos('(', S));
        SelStart := SelStart + Len;
        SelText := Copy(S, Pos('(', S) + 1, Len);
        SelStart := SelStart - 1;
      end
      else
      begin
        SelText := S;
        if IsFunction then
          SelStart := SelStart - 1;
      end;
    end;
end;

procedure TCompFrame.functionlistDblClick(Sender: TObject);
begin
  with TListBox(Sender) do
    AddToEdit(Items[ItemIndex]);
end;

procedure TCompFrame.functionlistMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  i: Integer;
  S: string;
begin
  i := functionlist.ItemAtPos(point(X, Y), True);
  if i >= 0 then
  begin
    S := functionlist.Items.Strings[i];
    functionlist.hint := TheComp.TheModel.Parser.GetFunctionDescription(S);
  end
  else
    functionlist.hint := '';
end;

procedure TCompFrame.ConnectListMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  i: Integer;
begin
  i := ConnectList.ItemAtPos(point(X, Y), True);
  if i >= 0 then
    ConnectList.hint := TGrindComponent(ConnectList.Items.Objects[i]).HintText
  else
    ConnectList.hint := '';
end;

end.
