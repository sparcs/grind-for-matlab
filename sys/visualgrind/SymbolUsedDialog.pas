unit SymbolUsedDialog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons;

type
  TSymbolErrorDlg = class(TForm)
    SymbolEdit: TEdit;
    ChangeBtn: TBitBtn;
    IgnoreBtn: TBitBtn;
    ErrorLabel: TLabel;
    Label2: TLabel;
    IgnoreAllBtn: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure IgnoreBtnClick(Sender: TObject);
    procedure IgnoreAllBtnClick(Sender: TObject);
    procedure SymbolEditClick(Sender: TObject);
  private
    { Private declarations }
    FIgnore: boolean;
    FIgnoreAll: boolean;
    TheSymbol:string;
    function getCriticalError: boolean;
  public
    { Public declarations }
    function Run(asymbol: string; comp: TObject): string;
    function HasError(asymbol: string; comp: TObject): boolean;
    property CriticalError: boolean read getCriticalError;
    property Ignore: boolean read FIgnore write FIgnore;
    property IgnoreAll: boolean read FIgnoreAll write FIgnoreAll;
  end;

var
  SymbolErrorDlg: TSymbolErrorDlg;

implementation

uses
  prgTools, grindcomps, visgrind;
{$R *.dfm}

procedure TSymbolErrorDlg.IgnoreBtnClick(Sender: TObject);
begin
  if (pos('unique', ErrorLabel.Caption) = 0) or
    (Application.MessageBox('Are you sure to ignore this critical error?',
    'Symbol Error', MB_YESNO) = idYes) then
    Ignore := True;
end;

procedure TSymbolErrorDlg.IgnoreAllBtnClick(Sender: TObject);
begin
  if (pos('unique', ErrorLabel.Caption) = 0) or
    (Application.MessageBox('Are you sure to ignore this critical error?',
    'Symbol Error', MB_YESNO) = idYes) then
  begin
    IgnoreAll := True;
    Ignore := True;
  end;
end;

procedure TSymbolErrorDlg.FormCreate(Sender: TObject);
begin
  Ignore := False;
  IgnoreAll := False;
end;

function TSymbolErrorDlg.getCriticalError: boolean;
begin
   Result:=(pos('unique', ErrorLabel.Caption) >0) ;
end;

function TSymbolErrorDlg.HasError(asymbol: string; comp: TObject): boolean;
var
  i: integer;
  // v: double;
begin
  ErrorLabel.Caption := '';
  if asymbol = '' then
    ErrorLabel.Caption := 'Symbol may not be empty';
  if (length(asymbol) > 0) and CharInSet(asymbol[1], ['0' .. '9']) then
    ErrorLabel.Caption := 'The first character may not be a digit';
  if isNumber(asymbol) then
    ErrorLabel.Caption := 'Symbol may not be a number';
  for i := 1 to length(asymbol) do
    if not CharInSet(asymbol[i], ['a' .. 'z', 'A' .. 'Z', '0' .. '9', '_']) then
    begin
      ErrorLabel.Caption := 'The character "' + asymbol[i] +
        '" is not allowed in a symbol';
      break;
    end;
  if asymbol = 't' then
    ErrorLabel.Caption := 'The symbol "t" is not unique, t is reserved for time'
  else if not TGrindComponent(comp).TheModel.IsUnique(asymbol, TGrindComponent(comp))
  then
    ErrorLabel.Caption := 'The symbol "' + asymbol + '" is not unique';
  Result := ErrorLabel.Caption <> '';
end;

function TSymbolErrorDlg.Run(asymbol: string; comp: TObject): string;
begin
  Ignore := IgnoreAll;
  Result := asymbol;
  SymbolEdit.Text := asymbol;
  while HasError(Result, comp) and (not Ignore or CriticalError) do
  begin
    TheSymbol:=Result;
    ChangeBtn.Enabled:=False;
    IgnoreBtn.Enabled:=not CriticalError;
    IgnoreAllBtn.Enabled:=IgnoreBtn.Enabled;
    if IgnoreBtn.Enabled then
        Caption:='Error in Symbol'
    else
        Caption:='Critical Error in Symbol';
    ShowModal;
    Result := SymbolEdit.Text;
  end;
end;

procedure TSymbolErrorDlg.SymbolEditClick(Sender: TObject);
begin
  ChangeBtn.Enabled:=TheSymbol<>SymbolEdit.Text;
end;

end.
