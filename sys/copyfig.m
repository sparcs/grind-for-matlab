%COPYFIG   Make a copy of a figure in MATLAB. 
%   Note that when you copy a figure to a figure with a high number, 
%   it is not erased with the command <a href="matlab:help era">era</a>. Subsequently, the figure can
%   be combined with the command <a href="matlab:help combfig">combfig</a>.
%
%   Usage:
%   COPYFIG TONR - Copy the current figure to a new figure with number TONR.
%   COPYFIG FROMNR TONR - Copy figure FROMNR to TONR.
%   COPYFIG('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'from' [handle] - original figure handle.
%     'to' [number] - number of the new figure.
%
%   See also combfig, era
%
%   Reference page in Help browser:
%      <a href="matlab:commands('copyfig')">commands copyfig</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:36 $
function copyfig(varargin)
    
    % Define default values and field names for argument parsing
    fieldnams = {'from', 'h', 'original figure handle.', get(0, 'CurrentFigure'); ...
        'to', 'n', 'number of the new figure', 1000}';
    
    % Parse input arguments
    args = i_parseargs(fieldnams, 'if nargs==1,deffields=''to'';else,deffields=''from,to'';end;', '', varargin);
    
    % Add sys2 directory to the path if i_checkstr function doesn't exist
    if ~exist('i_checkstr', 'file')
        addpath([grindpath filesep 'sys2']);
    end
    
    % Set default 'from' value if not specified
    if ~isfield(args, 'from')
        args.from = get(0, 'CurrentFigure');
    end
    
    % Set default 'to' value if not specified
    if ~isfield(args, 'to')
        % Prompt user for figure numbers if not provided
        prompt = {'Figure number to copy (gcf=current):', 'Copy to:'};
        answer = inputdlg(prompt, 'Copy figure', 1, {'', ''});
        
        % Set 'from' and 'to' values based on user input
        if isempty(answer{1})
            answer{1} = 'gcf';
        end
        args = i_parseargs(fieldnams, 'if nargs==1,deffields=''to'';else,deffields=''from,to'';end;', '', answer);
    end
    
    % Delete the new figure if it already exists
    if ishandle(args.to)
        delete(args.to);
    end
    
    % Create a new figure with the specified number
    h = i_figure(args.to);
    
    % Copy axes objects from the original figure to the new figure
    if ishandle(args.from)
        if strcmp(get(args.from, 'type'), 'axes')
            hax = args.from;
        else
            hax = get(args.from, 'children');
        end
        
        % Copy axes objects to the new figure
        copyobj(hax, h);
        
        % Adjust the position of the axes in the new figure
        axnew = findobj(h, 'type', 'axes');
        set(axnew, 'position', [0.1300 0.1100 0.7750 0.8150], 'Units', 'normalized');
    else
        % Display an error if the source figure doesn't exist
        error('GRIND:copyfig:UnknownFig', 'Copyfig: source figure doesn''t exist');
    end
end
