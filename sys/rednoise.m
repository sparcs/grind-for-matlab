%REDNOISE   Generate red noise
%   Function to generate white or red noise. To be used in differential
%   equations. Itself is a difference equation (Steele and Henderson):
%   T(t) = (1 -1/lambda) * (T(t-1) - T0) + T0 + beta*randn
%
%   In which:
%   T = variable with red noise
%   T0 = mean of red noise
%   lambda = 'period' of red noise. white: lambda=1 red: lambda>1.
%   beta = extend of noise
%   randn = normally distributed random number
%
%   Note: you can set the standard deviation of the resulting series to a certain value
%   with the following formula:(Ives et al., 2003):
%   beta=SD*sqrt(2/lambda-1/lambda^2) (in which SD is the resulting standard
%   deviation).
%
% 
%   Usage:
%   REDNOISE(t,T0,lambda,beta) for explanation of coefficients, see above (t=time).
%       the arguments T0,lambda and beta can only contain functions of parameters, time t, and
%       auxiliary variables (that depend only on parameters)
%   REDNOISE(t,T0,lambda,beta,iset) - If you use several independent rednoise sets 
%   within one set of equations, number them using a unique integer value (iset).
%   REDNOISE(t,T0,lambda,beta,iset,deltat) - define the period deltat for which the
%   difference equation will be calculated (default=1)
%   REDNOISE -D - (from the command line) deactivates all rednoise sets, so the last generated
%   set will be used even if parameters change.
%   REDNOISE -A - (from the command line) activates rednoise again.
%   REDNOISE -U - (from the command line) resets and updates the rednoise data.
%   
%       
%   See also model, externvar, dwiener
%
%   Reference page in Help browser:
%      <a href="matlab:commands('rednoise')">commands rednoise</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:38 $
function T = rednoise(tnow, T0, lambda, beta, eqnr, deltat)
    %function T = rednoise(tnow, T0, labda, beta, isets, deltat)
    global g_rednoise g_grind; %#ok<GVMIS>
    if isnumeric(tnow)
        if nargin < 5
            eqnr = 1;
        end
        if nargin < 6
            deltat = 1;
        end
        if isempty(g_rednoise)
            %use as function outside of model
            redn = rednoise_model;
            redn.addset(T0, lambda, beta,1,deltat);
            redn.reset;
            T = redn.getvalue(tnow, 1,deltat);
        else
            T = g_rednoise.getfunctvalue(tnow, eqnr);
        end
        return;
    end
    if nargin <= 2
        if strncmpi(tnow, '-a', 2)
            if ~g_rednoise.active
                g_rednoise.active = 1;
                g_grind.checks.lastsettings = [];
                disp('rednoise activated');
            else
                disp('rednoise was already active')
            end
        elseif strncmpi(tnow, '-d', 2)
            if g_rednoise.active
                g_rednoise.active = 0;
                g_grind.checks.lastsettings = [];
                disp('rednoise deactivated');
            else
                disp('rednoise was already deactivated')
            end
        elseif strncmpi(tnow, '-u', 2)
            if ~isempty(g_rednoise)
                g_rednoise.reset;
            end
        end
        return;
    end
end
