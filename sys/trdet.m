%TRDET   Plot of trace and determinant of the Jacobian and Hurwitz analysis
%  For 2D models plots of the trace and determinant are made. For higher dimensional models, an analysis of
%  the Routh-Hurwitz criteria is done. The results of this analysis are saved to a structure. 
%  The 2D trace and determinant of the Jacobian matrix determine the stability of equilibria. 
%  Plotting them help to determine the eigenvalues of <a href="trdet1.gif">difference</a> and 
%  <a href="trdet2.gif">differential equations</a>. This function works only for 2 dimensional equations.
%
%  Usage:
%  TRDET - derive trace and determinant plot.
%  TRDET TRUE - numerical Jacobian only
%  TRDET('jacobian',[1 2 3 4 1; 1 0 5 1 4; 1 2 3 4 5; 4 5 4 1 2 ;1 3 4 5 2]) - do the Hurwitz calculations for the provided Jacobian matrix.
%  res=TRDET - save the results of the analysis to a structure with fields:
%  res.F - the net feedback at level (x), see Levins, 1974 (should all be negative).
%  res.character - arguments of the characteristic equation
%  res.powers - powers of the characteristic equation
%  res.routh_table - the Routh table (number of changes in sign of the first column are the number of positive eigenvalues) 
%  res.hurwitz - Hurwitz determinants (should all be positive, to be stable) 
%  
%  TRDET('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'donumerical' [logical] - numerical or analytical Jacobian
%     'characteristic' [number] - enter values of the characteristic equation (for debugging)
%     'jacobian' [number] - values of the Jacobian
%     'simplify' [logical] - simplify analytical results (can be slow)
%     'solve_equilibria' [logical] - try to solve equilibria analytically (true) or general solution only (false)
%  TRDET('-opt1','-opt2',...) - Valid command line <a href="matlab:commands func_args">options</a>:
%     '-g' - only show the generic Jacobian in a symbolic analysis
%     '-s' - do a symbolic analysis of the stability of the equilibria
%      
%         
%  See also eigen, findeq
%
%   Reference page in Help browser:
%      <a href="matlab:commands('trdet')">commands trdet</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:39 $
function [res1,htmlres] = trdet(varargin)
    %(donumerical)
    global g_grind;
    fieldnams = {'jacobian', 'n', 'values of the Jacobian', []; ...
         'characteristic', 'n', 'enter values of the characteristic equation (for debugging)', []; ...
        'simplify', 'l', 'simplify analytical results (can be slow)', false;...
        'solve_equilibria', 'l', 'try to solve equilibria analytically (true) or general solution only (false)', false;...
        'donumerical', 'l', 'numerical or analytical Jacobian', false}';
    htmlres=[];
    args = i_parseargs(fieldnams, 'donumerical', {'-s', '-g'}, varargin);
    if any(strcmp(args.opts, '-g'))
        args.solve_equilibria=false;
    end
    if ~isfield(args,'solve_equilibria')
         args.solve_equilibria=true;
    end
    if ~isfield(args,'simplify')
        args.simplify=false; %can be slow
    end
    isdiffer = false;
    N0 = [];
    if any(strcmp(args.opts, '-s')) %symbolic
        if ~i_hastoolbox('symbolic')
            error('grind:trdet:symbolic','For this option the Symbolic Math Toobox is needed');
        end
        symmod = symmodel;
        if args.solve_equilibria
           symmod.update_equilibria;
           [res,htmlres] = symmod.analyse_stab('simplify',args.simplify);
        else
           [res,htmlres] = symmod.analyse_stab('general',true,'simplify',args.simplify);
        end
        
        if nargout > 0
            res1 = res;
        end
        return
    end
    if isfield(args, 'characteristic') && ~isempty(args.characteristic)
        %While the matrix J which has a given characteristic polynomial is not unique, 
        %it is often convenient to choose an upper Hessenberg matrix called the 
        %(Frobenius) companion matrix or its (lower Hessenberg) transpose.
        dim = length(args.characteristic) - 1;
        args.jacobian = zeros(length(args.characteristic) - 1);
        args.jacobian(2:end, 1:end - 1) = eye(dim - 1);
        ch = args.characteristic;
        ch = ch(:) / ch(1);
        args.jacobian(:, end) = - flipud(ch(2:end));
    else
        args.characteristic = [];
    end
    if ~isfield(args, 'jacobian') || isempty(args.jacobian)
        i_parcheck;
        if ~isfield(args, 'donumerical')
            args.donumerical = 0;
        end
        niters = g_grind.solver.iters;
        N0 = i_initvar;
        args.jacobian = i_eigen(args.donumerical, niters, N0);
        if g_grind.statevars.dim > 2 && g_grind.solver.isdiffer
            error('grind:trdet:dim', 'trdet cannot deal with difference equations with more than 2 dimensions');
        end
        isdiffer = g_grind.solver.isdiffer;
    end
    J = args.jacobian;
    equil = true;
    if ~isempty(N0)
        Nres1 = i_runsinglestep(1, N0', true)';
        if isdiffer
            Nres1 = Nres1 - N0;
        end
        if sum(abs(Nres1)) > 0.001
            equil = false;
            warning('GRIND:trdet:noequilibrium', 'The model is not in an equilibrium\nUse <a href="matlab:findeq">findeq</a> to find the equilibrium');
        end
    end

    if size(J, 1) == 2
 
        %     ud = get(get(0,'currentfigure'), 'userdata');
        %     if isfield(ud, 'iters')
        %         niters = ud.iters;
        %     end
 
        if any(any(isnan(J)))
            %    warning('grind:trdet:nanvals','Warning: Cannot determine trace and determinant as there are NaN values in the Jacobian');
            return;
        end
        if ~isempty(args.characteristic)
            tr = - args.characteristic(2);
            determ = args.characteristic(3);
        else
            tr = trace(J);
            determ = det(J);
            disp('Jacobian (J) =');
            disp(J);
        end
        fprintf('trace(J) = %g\n\n', tr);
        fprintf('det(J)   = %g\n', determ);
        hfig = i_makefig('trdet');
        hold off;
        plot(tr, determ, 'r+', 'markersize', 10);
        i_plotdefaults(hfig);
 
        % daspect([1 1 1]);
        %  oldhold = ishold;
        hold on;
        set(hfig, 'Name', 'Trace and determinant of Jacobian (J)');
        htitle = title('Trace and determinant of Jacobian (J)');
        set(htitle, 'fontweight', 'normal');
        xlabel('trace(J)');
        ylabel('det(J)');
        H1 = get(hfig, 'CurrentAxes');
        max1 = max(abs([tr * 1.1 determ * 1.1]));
        if max1 < 0.18
            lim = [ -0.2 0.2];
        elseif max1 < 0.35
            lim = [ -0.4 0.4];
        elseif max1 < 0.45
            lim = [ -0.5 0.5];
        elseif max1 < 0.95
            lim = [ -1 1];
        elseif max1 < 1.4
            lim = [ -1.5, 1.5];
        elseif max1 < 10
            lim = [ -(round(max1) + 1), round(max1) + 1];
        else
            lim = [ -max1, max1];
        end
        set(H1, 'Xlim', lim);
        set(H1, 'Ylim', lim);
        x = lim(1):(lim(2) - lim(1)) / 100:lim(2);
        if isdiffer
            plot(x, 0.25 * x.^2, ':k');
            x = [lim(1) lim(2)];
            plot(x, x - 1, ':r');
            plot(x, - x - 1, ':r');
            x = [ - 2 2 0 -2];
            y = [1 1 -1 1];
            plot(x, y, '-k');
        else
            plot([lim(1), lim(2)], [0, 0], 'k');
            plot([0, 0], [lim(1), lim(2)], 'k');
            plot(x, 0.25 * x.^2, ':k');
        end

    end
    if ~isdiffer
        %Routh-Hurwitz analysis
 
        %   Characteristic polynomial easy to check: filling the eigenvalues in the equation should be
        %   almost 0
        %
        if equil
            titl = 'current equilibrium';
        else
            titl = 'current state (no equilibrium!)';
        end
        anal = analyse_stability('J', args.jacobian, 'characteristic', args.characteristic, ...
            'title', titl, 'isdiffer', isdiffer, 'equilibrium', N0, 'statevars', {i_statevars_names});
        res = anal.summary;
        analyse_stability.plotresults(res,g_grind.figopts{:});
        if nargout > 0
            res1 = res;
        end
    else
        %to do
        %implement Jury test (= discrete analog of Routh Hurwitz)
        %P(z)=characteristic equation 
        %P(1)>0
        %-1^n P(-1)>0
        %|a_0|<|a_n|
        %Jury table
        %|b_0|>|b_(n-1)|
        %..
        %|q_0|>|q_1|
    end

end

