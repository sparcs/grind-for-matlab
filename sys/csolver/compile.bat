REM This file is used to compile the csolver project, standard Code::Blocks with gcc is used.
REM This is an open source cross platform C++ IDE and compiler see: http://www.codeblocks.org/
REM You can also install another C++ compiler and adapt this file, grindupdate will not overwrite this file
REM
REM Note that the linker of Code::Blocks should produce completely static executables
REM this means that the following options should be in csolver.cbp:
REM         <Linker>
REM					<Add option="-s" />
REM					<Add option="-static-libstdc++" />
REM					<Add option="-static-libgcc" />
REM			</Linker>
REM
REM
"c:\Program Files\CodeBlocks\codeblocks.exe" csolver.cbp --rebuild --target=Release
