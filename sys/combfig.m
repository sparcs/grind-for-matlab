%COMBFIG   Combine several figures
%   Combine several existing figures in a NxN matrix of figures. Note: 
%   <a href="matlab:help era">era</a> (erase) does not clear this figure.
%
%   Usage:
%   COMBFIG - Combine all currently opened figures in one figure.
%   COMBFIG [FIGNO1 FIGNO2.. FIGNOn] - combine the listed figure numbers.
%   COMBFIG [FIGNO1 FIGNO2.. FIGNOn] [ROW COL] - combine the listed 
%   figures, but override the default number of rows/columns (max 4).
%   COMBFIG [FIGNO1 FIGNO2.. FIGNOn] [ROW COL] START - Define a 
%   starting position (default 1). This is useful for combining several
%   versions of the same figure, for instance to combine 4 versions of
%   Figure 1: COMBFIG 1 [2 2] 1; COMBFIG 1 [2 2] 2;
%   COMBFIG 1 [2 2] 3; COMBFIG 1 [2 2] 4 [..];
%  
%   COMBFIG('argname',argvalue,...) - Valid argument <a href="matlab:commands func_args">name-value pairs</a> [with type]:
%     'axis' [x | y | b | t | tx | ty] - name of axis (x=x axis, y=yaxis, b=both, t=titles only, tx=titles x axis only, ty), see '-r'.
%     'cells' [integer>0 and length(integer)<=2] - [ROW COL] number of rows and columns.
%     'figs' [handle or cell] - list of figure handles or numbers or function handles
%     'figno' [integer>0] - number of the figure that is created.
%     'spacing' [number] - spacing between panels (see '-p')
%     'start' [integer>0] - starting position of the first figure.
%   COMBFIG('-opt1','-opt2',...) - Valid command line <a href="matlab:commands func_args">options</a>:
%     '-p' - repositions the panels made with combfig or subplot such that 
%         there is less whitespace.
%     '-p' SPACING  - keep SPACING between the axes in relative units,
%        SPACING = 0.02 is default, SPACING=0 is axis without spacing.
%     '-p' [0 0.05]  - spacing is 0 vertically and 0.05 horizontally.
%     '-r' - Remove overlapping axes and tick texts (assuming that all 
%        axes are the same)
%     '-r' x - Remove only x-axis (y=y-axis; b = both (default)).
%     '-l' - adjust tick labels for zero space ('-p' 0)
%      
%See also copyfig, era, <a href="matlab:help subplot">subplot</a>, replayall
%
%   Reference page in Help browser:
%      <a href="matlab:commands('combfig')">commands combfig</a>

%   Copyright 2024 WUR
%   Revision: 1.2.1 $ $Date: 17-Apr-2024 10:32:36 $
function ndx = combfig(varargin)
    %(fignrs, Cells, starti)
    if ~exist('i_figno', 'file')
        addpath([grindpath filesep 'sys2'])
    end
    fieldnams = {'figs', 'h#c', 'list of figure handles or numbers or function handles', double(get(0, 'children')); ...
        'cells', 'i>0&length(i)<=2', '[ROW COL] number of rows and columns.', []; ...
        'figno', 'i>0', 'number of the figure that is created.', i_figno('combfig'); ...
        'start', 'i>0', 'starting position of the first figure.', 1; ...
        'axis', 'e[x|y|b|t|tx|ty]', 'name of axis (x=x axis, y=yaxis, b=both, t=titles only, tx=titles x axis only, ty), see ''-r''.', 'b'; ...
        'spacing', 'n', 'spacing between panels (see ''-p'')', []}';
    args = i_parseargs(fieldnams, 'if hasoption(''-p''),deffields=''spacing'';elseif hasoption(''-r'')||hasoption(''-l''),deffields=''axis'';else,deffields=''figs,cells,start'';end;', '-p,-r,-l', varargin);
    if nargout == 1 && isfield(args, 'figs') && isfield(args, 'cell') && isfield(args, 'start')
        ndx = sub2ind(flipud(args.figs(:)), args.start, args.cells); %irritating that we need flipud as the cells are different defined
        return;
    end
    if ~isfield(args, 'figs')
        args.figs = get(0, 'children');
    end
    if any(strcmp(args.opts, '-l')) %adjust tick labels for zero space
        if ~isfield(args, 'axis')
            orientation = 'b';
        else
            orientation = args.axis;
        end
        adjustplot(gcf, '-l', orientation)
        return;
    end
    if any(strcmp(args.opts, '-r')) %remove axis
        if ~isfield(args, 'axis')
            orientation = 'b'; %both
        else
            orientation = args.axis;
        end
        adjustplot(gcf, '-r', orientation)
        return;
    end
    if any(strcmp(args.opts, '-p')) %adjust positions
        if ~isfield(args, 'spacing')
            spacing = 0.02;
        else
            spacing = args.spacing;
        end
        adjustplot(gcf, '-p', spacing)
        return;
    end
    if ~isfield(args, 'start')
        args.start = 1;
    end
    nfig = numel(args.figs);
    if ~isfield(args, 'cells')&&~(size(args.figs,1)==1&&size(args.figs,2)>1)
        args.cells=size(args.figs);
        args.figs=args.figs';
        args.figs=args.figs(:)';
    end
    if ~isfield(args, 'cells')
        if nfig == 0
            i_errordlg('Not enough figures to combine');
            error('GRIND:combfig:TooFewFigs', 'Not enough figures to combine');
        elseif nfig == 1
            args.cells = [1 1];
        elseif nfig == 2
            args.cells = [2 1];
        elseif nfig == 3
            args.cells = [3 1];
        elseif nfig == 4
            args.cells = [2 2];
        elseif nfig <= 6
            args.cells = [3 2];
        elseif nfig <= 8
            args.cells = [4 2];
        else
            args.cells = [3 3];
        end
    end
    scale = zeros(1, 2);
    for i = 1:2
        if i == 1
            j = 2;
        else
            j = 1;
        end
        if args.cells(i) == 1
            scale(j) = 1;
        elseif args.cells(i) == 2
            scale(j) = 0.45;
        elseif args.cells(i) == 3
            scale(j) = 0.27;
        else
            scale(j) = 0.2;
        end
    end
    if isfield(args, 'figno') && ~isempty(args.figno)
        h = figure(args.figno);
    elseif exist('i_figno', 'file')
        h = i_figure(i_figno('combfig'));
        if ~iscell(args.figs)
           cm = get(args.figs(1), 'colormap');
           set(h, 'colormap', cm);
        end
    else
        h = figure;
    end
    set(h, 'Name', 'Combined figure');
    ud.rows = args.cells(1);
    ud.cols = args.cells(2);
    set(h, 'userdata', ud);
    
    for i = 1:nfig
        %   s = subplot(args.cells(1), args.cells(2), i + args.start - 1);
        [i1, i2] = ind2sub(args.cells, i + args.start - 1);
        s = subplot(ud.rows, ud.cols, i + args.start - 1, 'tag', sprintf('subplot(%d,%d)', i1, i2));
        %  s=subplot('position',subplotpos(args.cells(1),args.cells(2),i1,i2),'tag',sprintf('subplot(%d,%d)',i1,i2));
        %hs = zeros(ud.rows, ud.cols) - 1;
        
        pos = get(s, 'Position');
        tag = get(s, 'tag');
       
        if ~iscell(args.figs)&&~ishandle(args.figs(i))
            error('GRIND:combfig:cannotfindfig', 'Error combfig: cannot find figure %g', args.figs(i));
        elseif iscell(args.figs) %should be function handles
             f=args.figs{i};
             hs=f();
             adjustplot(hs,'-adjustpos',pos);
             set(hs,'tag',tag);
        else
             delete(s);
          %  hax = findall(args.figs(i), 'type', 'axes');
            hax = get(args.figs(i),'children');
            for k=length(hax):-1:1
                if strcmp(get(hax(k),'type'),'uicontrol')
                    hax(k)=[];
                end
            end
            if isempty(hax)
                error('grind:combfig:noaxes', 'Cannot combine figures without axes')
            end
            %    arrs=findall(args.figs(i),'tag','Arrow');
            % arrows are not added correctly. Too complex to repair it seems.
           % tag1 = get(hax, 'tag');
            
            %hax = hax(~(strcmp(tag1, 'legend') | strcmp(tag1, 'Colorbar')));
            %pos2 = get(ax, 'position');
            %       l=findobj(ax,'tag','legend');
            %       if ~isempty(l)
            
            hs = copyobj(hax, h);
            adjustplot(hs,'-adjustpos',pos);
            ndx1=strcmpi(get(hs,'type'),'axes');
            set(hs(ndx1),'tag',tag);
          %
        end
    end
    if exist('i_grindlegend', 'file')
       i_grindlegend(13);
    end
    if isfield(args, 'spacing')
        combfig('-p', args.spacing);
    end
end



